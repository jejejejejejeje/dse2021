#ifndef D_NMEA_Parse_H
#define D_NMEA_Parse_H

#include <stdint.h>
#include <stdbool.h>
/**********************************************************
 *
 * NMEA_Parse is responsible for ...
 *
 **********************************************************/

#define GPS_HZ   1         ///< GPS 장비설정 현재는 1Hz주기로 받음

#define NP_MAX_CMD_LEN			8		// maximum command length (NMEA address)
#define NP_MAX_DATA_LEN			256		// maximum data length
#define NP_MAX_CHAN				36		// maximum number of channels
#define NP_WAYPOINT_ID_LEN		32		// waypoint max string len

#define MILI_SEC  1000.0
#define MICRO_SEC 1000000.0
#define NANO_SEC  1000000000.0

/**********************************************************/


#pragma pack(push, 1)
typedef struct CNPSatInfo
{
	int32_t	m_32PRN;                    // the Satelite ID
	int32_t	m_32SignalQuality;          // SNR: Signal to noise ration in dBHz ( 0 ~ 99 )
	bool	m_8UsedInSolution;          // Check the Satelite ID, whether the satelite used in solution
	int32_t	m_32Azimuth;                // Azimuth in degrees ( 0 ~ 359 )
	int32_t	m_32Elevation;              // Elevation i ndegrees ( 0 ~ 90 )
}CNPSatInfo_t;

typedef struct NMEA_material{

	char m_pCommand[NP_MAX_CMD_LEN];	// NMEA command
	char m_pData[NP_MAX_DATA_LEN];		// NMEA data

	int32_t m_32CommandCount;           // number of NMEA commands received (processed or not processed)

//// GPGGA Data //////////////////////////////////////////////////
	int8_t  m_8GGAHour;                 // GPS Hour value
	int8_t  m_8GGAMinute;               // GPS Minute value
	int8_t  m_8GGASecond;               // GPS Second value
	int16_t m_16GGAMilliSecond;         // GPS MilliSecond value

	double  m_dGGALatitude;				// South( Latitude < 0 ), North( Latitude > 0 )
	double  m_dGGALongitude;			// West( Longitude < 0 ), East( Longitude > 0 )
	int8_t  m_8GGAGPSQuality;			// 0 = fix not available, 1 = GPS sps mode, 2 = Differential GPS, SPS mode, fix valid, 3 = GPS PPS mode, fix valid
	int8_t  m_8GGANumOfSatsInUse;		// Satellites being used ( 0 ~ 12 )
	double  m_dGGAHDOP;					// Horizontal dilution of precision
	double  m_dGGAAltitude;				// Altitude: mean-sea-level (geoid) meters, according to WGS-84 ellipsoid

	int32_t m_32GGACount;				// Count of GGA get from the start of the system code
	int32_t m_32GGAOldVSpeedSeconds;	// For calculation of "Vertical Speed", One time before present GGA time in second
	double  m_dGGAOldVSpeedAlt;			// For calculation of "Vertical Speed", One time before present GGA Alititude
	double  m_dGGAVertSpeed;			// "Vertical Speed" from calculation
//////////////////////////////////////////////////////////////////

//// GPGSA Data //////////////////////////////////////////////////
	char    m_cGSAMode;					        // M = manual, A = automatic 2D/3D
	int8_t  m_8GSAFixMode;				        // 1 = fix not available, 2 = 2D, 3 = 3D
	int32_t m_32GSASatsInSolution[NP_MAX_CHAN]; // ID of sats in solution
	double  m_dGSAPDOP;					        // Position dilution of precision
	double  m_dGSAHDOP;					        // Horizontal dilution of precision
	double  m_dGSAVDOP;					        // Vertical dilution of precision
	int32_t m_32GSACount;					    // Count of GSA get from the start of the system code
//////////////////////////////////////////////////////////////////

//// GPGSV Data //////////////////////////////////////////////////
	int8_t       m_8GSVTotalNumOfMsg;		    // Not use
	int32_t      m_32GSVTotalNumSatsInView;		// Number of satelite in view
	CNPSatInfo_t m_GSVSatInfo[NP_MAX_CHAN];	    // Informations of satelites
	int32_t      m_32GSVCount;					// Count of GSV get from the start of the system code
//////////////////////////////////////////////////////////////////

//// GPRMB Data //////////////////////////////////////////////////
	char    m_cRMBDataStatus;				          // A = data valid, V = navigation receiver warning
	double  m_dRMBCrosstrackError;		              // Crosstrack error in nautical miles
	char    m_cRMBDirectionToSteer;		              // Direction to steer (L or R) to correct error
	char    m_cRMBOriginWaypoint[NP_WAYPOINT_ID_LEN]; // Origin Waypoint ID
	char    m_cRMBDestWaypoint[NP_WAYPOINT_ID_LEN];   // Destination waypoint ID
	double  m_dRMBDestLatitude;			              // destination waypoint latitude
	double  m_dRMBDestLongitude;			          // destination waypoint longitude
	double  m_dRMBRangeToDest;			              // Range to destination nautical mi
	double  m_dRMBBearingToDest;			          // Bearing to destination, degrees true
	double  m_dRMBDestClosingVelocity;	              // Destination closing velocity, knots
	char    m_cRMBArrivalStatus;			          // A = arrival circle entered, V = not entered
	int32_t m_32RMBCount;					          // Count of RMB get from the start of the system code
//////////////////////////////////////////////////////////////////

//// GPRMC Data //////////////////////////////////////////////////
	int8_t  m_8RMCHour;			// GPS Hour value
	int8_t  m_8RMCMinute;		// GPS Minute value
	int8_t  m_8RMCSecond;		// GPS Second value
    int16_t m_16RMCMillisecond;	// GPS MilliSecond value

	char    m_cRMCDataValid;	// A = Data valid, V = navigation rx warning
	double  m_dRMCLatitude;		// current latitude
	double  m_dRMCLongitude;	// current longitude
	double  m_dRMCGroundSpeed;	// speed over ground, knots
	double  m_dRMCCourse;		// course over ground, degrees true

	int8_t  m_8RMCDay;			// GPS Day value
	int8_t  m_8RMCMonth;		// GPS Month value
	int32_t m_32RMCYear;		// GPS Year value

	double  m_dRMCMagVar;		// magnetic variation, degrees East(+)/West(-)
	int32_t m_32RMCCount;		// Count of RMC get from the start of the system code
//////////////////////////////////////////////////////////////////

//// GPZDA Data //////////////////////////////////////////////////
	int8_t  m_8ZDAHour;			    // GPS Hour value
	int8_t  m_8ZDAMinute;		    // GPS Minute value
	int8_t  m_8ZDASecond;			// GPS Second value
	int8_t  m_8ZDADay;				// GPS Day value
	int8_t  m_8ZDAMonth;			// GPS Month value
	int32_t m_8ZDAYear;				// GPS Year value

	int8_t  m_8ZDALocalZoneHour;	// Local Time(hour)
	int8_t  m_8ZDALocalZoneMinute;	// Local Time(minute)
	int32_t m_32ZDACount;			// Count of ZDA get from the start of the system code
//////////////////////////////////////////////////////////////////

//// GPVTG Data //////////////////////////////////////////////////
	///
	/// GPVTG
	///	GPVTG,x.x,T,x.x,M,x.x,N,x.x,K
	///	.x,T = Track, degrees True
	///	.x,M = Track, degrees Magnetic
	///	.x,N = Speed, knots
	///	.x,K = Speed, Km/hr
	///
	double  m_dVTGTrueTrack;		// True Heading
	double  m_dVTGMagneticTrack;	// Magnetic heading
	double  m_dVTGSpeedKnots;		// GPS Horizontal Speed (knots)
	double  m_dVTGSpeedKm;			// GPS Horizontal Speed (km/h)
	int32_t m_32VTGCount;		    // Count of VTG get from the start of the system code
//////////////////////////////////////////////////////////////////
//// GPGLL Data //////////////////////////////////////////////////
	///
	/// GPGLL
	/// GPGLL,0000.0000,N,00000.0000,E,235947.000,V*2D
	///	0000.0000,N  = Latitude
	///	00000.0000,E = Longitude
	///	235947.000   = UTC time
	///	V            = A = Valid, V = Invalid
	///
	double  m_dGLLLatitude;		// current latitude
	double  m_dGLLLongitude;	// current longitude

	int8_t  m_8GLLHour;			// GPS Hour value
	int8_t  m_8GLLMinute;		// GPS Minute value
	int8_t  m_8GLLSecond;		// GPS Second value
    int16_t m_16GLLMillisecond;	// GPS MilliSecond value

	char    m_cGLLDataValid;	// A = Data valid, V = navigation rx warning

	int32_t m_32GLLCount;		    // Count of VTG get from the start of the system code
//////////////////////////////////////////////////////////////////
}NMEA_material_t;
#pragma pack(pop)

NMEA_material_t nmea_pars;

extern void NMEA_Parse_Create(void);
extern void NMEA_Parse_Destroy(void);

extern bool NMEA_Parse_MSGin(char* msg, int32_t len);

//////////////////////////////////////////////////////////////////

typedef struct TimeStamp
{
    int32_t year;
    int32_t month;
    int32_t day;
    int32_t hour;
    int32_t min;
    int32_t sec;
    int32_t usec;
} TimeStamp_t;

extern void TimeStamp_Create(TimeStamp_t *time_stamp);
extern int32_t TimeStamp_SetRTCtime(TimeStamp_t set_time);




#endif  /* D_FakeNMEA_Parse_H */
