#ifndef D_SerialBase_H
#define D_SerialBase_H

#include <stdint.h>
#include <termios.h>
/**********************************************************
 *
 * SerialBase is responsible for ...
 *
 **********************************************************/

#define BUFFER_SIZE     1024

#pragma pack(push, 1)
typedef struct SerialBuffer
{
    int32_t _size;
    char    _buffer[BUFFER_SIZE];
} SerialBuffer_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct Serial_Setting
{
    char* SERIAL_FD;
    long BAUDRATE;
    long DATABITS;
    long STOPBITS;
    long PARITYON;
    long PARITY;
    long LOCAL;
    long ICANON_ONOFF;
} Serial_Setting_t;
#pragma pack(pop)

extern int32_t SerialBase_Create(Serial_Setting_t* setting);
extern void SerialBase_Destroy(void);
extern SerialBuffer_t* SerialBase_GPSRead(int32_t* fd);
//extern SerialBuffer_t* SerialBase_SERI2Read(int32_t* fd);
#endif  /* D_FakeSerialBase_H */
