/**
    @file       dseBattCheck.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      Battery Check Interface
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역    
*/
#include <unistd.h>         // open() close() write() read() lseek()
#include <stdint.h>         // int32_t 등 library
#include <fcntl.h>          // O_RDWR
#include <stdlib.h>         //malloc atoi
#include <stdio.h>          //FILE
#include <string.h>

#include <mklogfile_util.h>
#include <BXi2c.h>
#include <BXlog.h>

#include "dseBattCheck.h"

#include <linux/i2c-dev.h>
#include <linux/i2c.h>

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
int32_t batt_battCheckOpen(_battInfo *info,              ///< 배터리 전압 측정기능을 초기화한다.
                          int32_t maxAverNum,
                          char* i2cDevName,
                          int32_t i2cAddr);

int32_t batt_battCheckClose(_battInfo *info);            ///< 배터리 전압 측정을 기능을 정리하는 함수

int32_t        batt_initBattState(_battInfo *_info);     ///< Battery의 초기 상태 측정 함수
int32_t        batt_getBattState(_battInfo *_info);      ///< Battery의 상태를 측정하는 함수

//int32_t        mylogfunc(float BattBorder, int32_t st);
static float   batt_getMonitoringValue( _battInfo *_in); ///< I2C로부터 현재 battery 전압값 측정
static int32_t batt_reconnect_i2c(_battInfo *_in);       ///< I2C 연결을 종료하고 다시 connect한다.
/** 배터리의 percentage로 배터리 상태를 계산한다. */
static int32_t batt_calcBattState(float _per, int32_t* _battBorder, int32_t _st);
///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////

/**
@fn      batt_initBattState(_battInfo *_info)
@author  Bongsung Kim
@date    2017.04.24
@brief   Battery의 초기 상태 측정 함수
@param   *_info Battery 정보 구조체 포인터
@return  성공시, 배터리 상태 값(0~6)을 반환. 실패시 -1.
@warning batt_battCheckOpen() 수행 후 수행하여야 한다.\n
         _info->maxIndex 만큼 Vin을 측정하여 이동평균 값을 바로 구한다.
@bug     none
@todo    none
*/
int32_t batt_initBattState(_battInfo *_info)
{
    _info->averVin = 0;

    for (_info->ind_arrVin = 0; _info->ind_arrVin < _info->maxIndex;
                                _info->ind_arrVin++)
    {
        _info->nowVin = batt_getMonitoringValue( _info );
        if (_info->nowVin <= 0 )
            return -1;

        _info->averVin -= _info->arrVin[_info->ind_arrVin] / (float)_info->maxIndex;
        _info->averVin += _info->nowVin / (float)_info->maxIndex;

        _info->arrVin[_info->ind_arrVin] = _info->nowVin;
    }

    _info->ind_arrVin = 0;

    float batt_percent = (_info->averVin - BATT_VOL_MIN) / BATT_MAX_MIN_GAP * 100.0;
    //float batt_percent = (_info->averVin - 10.5) / 2.0 * 100.0;  //MAX 12.5 - LOW 10.5 = 2.0 Vp2p
    //float batt_percent = (_info->averVin - 8.3) / 4.3 * 100.0;
    if      (batt_percent >100.0)     batt_percent = 100.0;
    else if (batt_percent <  0.0)     batt_percent =   0.0;

    if (batt_percent <= _info->battBorder[0])
        _info->st = 0;
    else if (batt_percent <= _info->battBorder[1])
        _info->st = 1;
    else if (batt_percent <= _info->battBorder[2])
        _info->st = 2;
    else if (batt_percent <= _info->battBorder[3])
        _info->st = 3;
    else if (batt_percent <= _info->battBorder[4])
        _info->st = 4;
    else if (batt_percent <= _info->battBorder[5])
        _info->st = 5;
    else
        _info->st = 6;
    printf("averVin : %f, battPer : %f \n\n", _info->averVin, batt_percent);

    return 0;
}

/**
@fn      batt_getBattState(_battInfo *_info)
@author  Bongsung Kim
@date    2017.04.24
@brief   Battery의 상태를 측정하는 함수
@param   *_info Battery 정보 구조체 포인터
@return  성공시, 배터리 상태 값(0~6)을 반환. 실패시 -1.
@warning batt_battCheckOpen() 수행 후 수행하여야 한다.
@bug     none
@todo    none
*/
int32_t batt_getBattState(_battInfo *_info)
{
    _info->nowVin = batt_getMonitoringValue( _info );
    //printf("%f\n",_info->nowVin);
    if (_info->nowVin < 0 )
        return -1;

    _info->averVin -= _info->arrVin[_info->ind_arrVin] / (float)_info->maxIndex;
    _info->averVin += _info->nowVin / (float)_info->maxIndex;

    _info->arrVin[_info->ind_arrVin] = _info->nowVin;

    if ( _info->ind_arrVin == _info->maxIndex-1 )
        _info->ind_arrVin = 0;
    else
        _info->ind_arrVin++;

    //Battery 퍼센트 계산
    _info->battPer = (_info->averVin-BATT_VOL_MIN)/BATT_MAX_MIN_GAP * 100.0;  //MAX 12.5 - LOW 10.5 = 2.0 Vp2p
    if      (_info->battPer >100.0)     _info->battPer = 100.0;
    else if (_info->battPer <  0.0)     _info->battPer =   0.0;

    //배터리 LOG생성 함수 클라이언트의 /home/root경로에 생성되고, BatteryCheckLOG-날짜.log로 생성됨
    //LOGsetInfo("/home/root/", "BatteryCheckLOG");
    //LOG_INFO("averVin\t%f\tnowVin\t%f\tbattState\t%d\n", _info->averVin, _info->nowVin, _info->st);

    //printf("averVin : %f, battPer : %f \n\n", _info->averVin, _info->battPer);
    return batt_calcBattState(_info->battPer, _info->battBorder, _info->st);
}

/**
@fn      batt_getMonitoringValue( _battInfo *_in )
@author  Jinmo Je
@date    2021.05.22
@brief   I2C로부터 현재 battery 전압값 측정
@param   *_in Battery 정보 구조체 포인터
@return  성공시, 배터리의 현재 측정 전압값. 실패시 -1.
@warning batt_battCheckOpen() 수행 후 수행하여야 한다.
@bug     전압 값이 제대로 맞지 않음.
@todo    계산 수식을 배터리 변경 후 테스트 하고 변경해야함.
*/
static float batt_getMonitoringValue( _battInfo *_in )
{
    float v_offset;
    float t_Vin,t_Vin_result;
    float t_Curr,t_Curr_result;
    float vresult;
    uint16_t temp1,temp2;

    union i2c_smbus_data smbus_Bdata;
    struct i2c_smbus_ioctl_data ioctl_Bdata;
   
    smbus_Bdata.block[0] = 6;

    ioctl_Bdata.read_write = I2C_SMBUS_READ;
    ioctl_Bdata.command = 0x00;
    ioctl_Bdata.size = I2C_SMBUS_I2C_BLOCK_DATA;
    ioctl_Bdata.data = &smbus_Bdata;

    if (ioctl(_in->i2c_t->i2cID, I2C_SMBUS, (void *)&ioctl_Bdata) < 0)
    {
        BXlogPutLogMsg("[dseBattCheck] ioctl I2C_SMBUS Failed!!");
        return -1;
    }

    temp1=smbus_Bdata.block[1];
    temp2=smbus_Bdata.block[2];

    t_Curr = (float)((temp1<<4) + (temp2>>4));
    
    temp1=smbus_Bdata.block[5];
    temp2=smbus_Bdata.block[6];

    t_Vin = (float)((temp1<<4) + (temp2>>4));

    //t_Vin_result = t_Vin * 11 / 2 / 1000.0;// + 0.58; // 0.58 is MBR360 Voltage Drop if 1A
    //                                        // * 25 / 1000 reference by datasheet
    t_Vin_result = (t_Vin * 0.0055)+0.2;
    
    //t_Curr_result = (t_Curr * 20.0 * 1000.0 / 20000 );// A! *1000.0;  //  mA      mV / mR
    
///////////////////////////////////////////////////////////////////////////////////////
// 전류에 의해 전압강하되는 만큼 보상해주는 공식
    t_Curr_result = ( 0.126*t_Curr + 3.3 + t_Curr ) / 1000;

    //v_offset = 0.0118*Curr*Curr + 0.005*Curr - 0.0011;
    v_offset = ((0.05*t_Curr_result*t_Curr_result) + (0.005*t_Curr_result)) - 0.0011;

    vresult = t_Vin_result+(v_offset*2);
///////////////////////////////////////////////////////////////////////////////////////
    return vresult;
}

/**
@fn      batt_reconnect_i2c(_battInfo *_in)
@author  Bongsung Kim
@date    2017.04.24
@brief   I2C 연결을 종료하고 다시 connect한다.
@param   *_in Battery 정보 구조체 포인터
@return  성공시, 0. 실패시 -1.
@warning 현재 사용하지 않음.
@bug     none
@todo    none
*/
static int32_t batt_reconnect_i2c(_battInfo *_in)
{
    BXi2c_device_close(_in->i2c_t);

    _in->i2c_t = BXi2c_device_open(_in->i2cDevDir, _in->i2cAddr);
    if (_in->i2c_t == NULL)
    {
        BXlogPutLogMsg("[dseBattCheck] I2C reopen Failed...");
        return -1;
    }
    return 0;
}

/**
@fn      batt_calcBattState(float _per, int32_t* _battBorder, int32_t _st)
@author  Bongsung Kim
@date    2017.04.24
@brief   배터리의 percentage로 배터리 상태를 계산한다.
@param   _per         현재 배터리의 percentage
@param   *_battBorder 배터리 상태의 경계 값 배열
@param   _st          현재 배터리 상태값
@return  성공시, 배터리 상태값(0~6). 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t batt_calcBattState(float _per, int32_t* _battBorder, int32_t _st)
{
    int hysterysys_var = 3;   //갑작스런 배터리량 증감을 방지하기 위한 불감대 영역 range 값

    //printf("battPercent : %f, state : %d V : %f\n", _per, _st, _V);
    switch(_st)
    {
    case BATT_ST0  :
        if      (_per >= _battBorder[_st] + hysterysys_var)
            return BATT_ST1;
        break;

    case BATT_ST6  :
        if      (_per <= _battBorder[_st-1] - hysterysys_var)
            return BATT_ST5;

        break;

    default        :
        if      (_per >= _battBorder[_st] + hysterysys_var)
            return ++_st;
        else if (_per <= _battBorder[_st-1] - hysterysys_var)
            return --_st;
        break;
    }

    return _st;
}

/**
@fn      batt_battCheckOpen(_battInfo *info, int32_t maxAverNum, char* i2cDevName, int32_t i2cAddr)
@author  Bongsung Kim
@date    2017.04.24
@brief   배터리 전압 측정기능을 초기화한다.
@param   *info      배터리 정보 구조체 포인터
@param   maxAverNum 이동 평균으로 사용할 전압값 개수
@param   i2cDevName i2c device 이름
@param   i2cAddr    배터리 체크 ic의 i2c ID
@return  성공시, 0. 실패시 -1.
@warning none
@bug     none
@todo    none
*/
int32_t batt_battCheckOpen(_battInfo *info, int32_t maxAverNum, char* i2cDevName, int32_t i2cAddr)
{   // = {4, 10, 20, 40, 65, 80}
    info->st = BATT_ST6;
    info->battBorder[0] = 4;
    info->battBorder[1] = 10;
    info->battBorder[2] = 20;
    info->battBorder[3] = 40;
    info->battBorder[4] = 65;
    info->battBorder[5] = 80;
    info->battPer       = 100.0;

    info->arrVin = (float *)malloc(maxAverNum*sizeof(float));
    memset(info->arrVin, 0, maxAverNum*sizeof(float));

    info->nowVin = 0;
    info->averVin = 0;
    info->ind_arrVin = 0;
    info->maxIndex = maxAverNum;

    info->i2cAddr = i2cAddr;
    strcpy(info->i2cDevDir, i2cDevName);

    info->i2c_t = BXi2c_device_open(info->i2cDevDir, info->i2cAddr);
    if (info->i2c_t == NULL)
        return -1;

    return 0;
}

/**
@fn      batt_battCheckClose(_battInfo *info)
@author  Bongsung Kim
@date    2017.04.24
@brief   배터리 전압 측정을 기능을 정리하는 함수
@param   *info      배터리 정보 구조체 포인터
@return  성공시, 0.
@warning none
@bug     none
@todo    실패에 대한 정의 필요.
*/
int32_t batt_battCheckClose(_battInfo *info)
{
    BXi2c_device_close(info->i2c_t);
    free(info->arrVin);
    return 0;
}



/*
// 설명: log 파일의 데이터를 생성한다
int32_t mylogfunc(float BattBorder, int32_t st)
{
  LOG_INFO("Batt: %f, st: %d \n", BattBorder, st);
}*/
  
