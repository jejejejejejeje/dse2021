#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/select.h>
#include <pthread.h>

//#include "main.h"

#include "SerialOperator.h"
#include "NMEA_Parse.h"


pthread_mutex_t  mute = PTHREAD_MUTEX_INITIALIZER; // 쓰레드 초기화

Serial_Setting_t    gps_sets; //, seri2_sets;

SerialBuffer_t *recvGPSBuffer; //, *recvSERI2Buffer;

TimeStamp_t gps_time;

static void SerialOperator_initGPS(Serial_Setting_t* sets)
{
    sets->SERIAL_FD = DEV_GPS2LINUX;
    sets->BAUDRATE  = B9600;
    sets->DATABITS  = CS8;
    sets->PARITYON  = 0;
    sets->PARITY    = 0;
    sets->LOCAL     = CLOCAL;
    sets->ICANON_ONOFF = ICANON;
}

/*static void SerialOperator_initSERI2(Serial_Setting_t* sets)
{
    sets->SERIAL_FD = DEV_SERI2LINUX;
    sets->BAUDRATE  = B115200;
    sets->DATABITS  = CS8;
    sets->PARITYON  = 0;
    sets->PARITY    = 0;
    sets->LOCAL     = CLOCAL;
    sets->ICANON_ONOFF = ICANON;
}*/

void SerialOperator_Create(void)
{
    SerialOperator_initGPS(&gps_sets);
    Serial_Ports_t.gps_fd = SerialBase_Create(&gps_sets);

    //SerialOperator_initGPS(&seri2_sets);
    //Serial_Ports_t.seri2_fd = SerialBase_Create(&seri2_sets);
}

void SerialOperator_Destroy(void)
{
    close(Serial_Ports_t.gps_fd);
    //close(Serial_Ports_t.seri2_fd);
    pthread_mutex_destroy(&mute);
}

int32_t SerialOperator_readGPS(void)
{
    int32_t msg_len, ret;

    fd_set  serialFD;
    int maxFD;

    maxFD = Serial_Ports_t.gps_fd + 1;//motion_serial_fd + 1;

    NMEA_Parse_Create();

    while(1)
    {
        FD_ZERO(&serialFD);

        FD_SET(Serial_Ports_t.gps_fd, &serialFD);
        select(maxFD, &serialFD, NULL, NULL, NULL);

        if ( FD_ISSET(Serial_Ports_t.gps_fd, &serialFD) )
        {
            // msg_len = read(Serial_Ports_t.gps_fd, recvGPSBuffer, 1024);
            recvGPSBuffer = SerialBase_GPSRead(&Serial_Ports_t.gps_fd);
            //TimeStamp_Create(&gps_time);
            msg_len = recvGPSBuffer->_size;
            // printf("[SerialOperator_readGPS] recvGPSBuffer->_size: %d, %s\n", recvGPSBuffer->_size, recvGPSBuffer->_buffer);

                // printf("GPS read: %ld(%d)// %s\n", strlen(recvGPSBuffer), msg_len, recvGPSBuffer);
            if(msg_len > 0)
            {
                ret = NMEA_Parse_MSGin(recvGPSBuffer->_buffer, recvGPSBuffer->_size); // Parse_대체 예정
                // printf("GPS read: %ld(%d)// %s\n", strlen(recvGPSBuffer), msg_len, recvGPSBuffer);
                // if(!ret)
                // {
                //     printf("[SerialOperator_readGPS] GPS Parse WRONG\n");
                // }
                // else
                {
                    // ETHERNET SEND AREA
                    // printf("GPS cnt%d, %d, %d, %d\n", nmea_pars.m_32GGACount, nmea_pars.m_32RMCCount, nmea_pars.m_32VTGCount,nmea_pars.m_32ZDACount);

                    //pthread_mutex_lock(&mute); // 잠금을 생성한다.
                    //eth_sendSENSORdata_process(&Sockets_t.clientSock_gps, eth_InitGPSData(recvGPSBuffer));
                    //pthread_mutex_unlock(&mute); // 잠금을 해제한다.
                }
            }
        }
    }

    return -1;
}

/*int32_t SerialOperator_readSERI2(void)
{
    int32_t msg_len, ret;

    fd_set  serialFD;
    int maxFD;

    maxFD = Serial_Ports_t.seri2_fd + 1;

    NMEA_Parse_Create(); //수정

    while(1)
    {
        FD_ZERO(&serialFD);

        FD_SET(Serial_Ports_t.seri2_fd, &serialFD);
        select(maxFD, &serialFD, NULL, NULL, NULL);

        if ( FD_ISSET(Serial_Ports_t.seri2_fd, &serialFD) )
        {
            // msg_len = read(Serial_Ports_t.seri2_fd, recvGPSBuffer, 1024);
            recvGPSBuffer = SerialBase_SERI2Read(&Serial_Ports_t.seri2_fd);
            
            msg_len = recvGPSBuffer->_size;
            // printf("[SerialOperator_readSERI2] recvSERi2Buffer->_size: %d, %s\n", recvSERI2Buffer->_size, recvSERI2Buffer->_buffer);

                // printf("SERI2 read: %ld(%d)// %s\n", strlen(recvSERI2Buffer), msg_len, recvSERI2Buffer);
            if(msg_len > 0)
            {
                //ret = NMEA_Parse_MSGin(recvSERI2Buffer->_buffer, recvSERI2Buffer->_size); // Parse_대체 예정
                // printf("SERI2 read: %ld(%d)// %s\n", strlen(recvSERI2Buffer), msg_len, recvSERi2Buffer);
                // if(!ret)
                // {
                //     printf("[SerialOperator_readSERI2] SERI2 Parse WRONG\n");
                // }
                // else
                {
                    // ETHERNET SEND AREA
                    // printf("GPS cnt%d, %d, %d, %d\n", nmea_pars.m_32GGACount, nmea_pars.m_32RMCCount, nmea_pars.m_32VTGCount,nmea_pars.m_32ZDACount);

                    //pthread_mutex_lock(&mute); // 잠금을 생성한다.
                    //eth_sendSENSORdata_process(&Sockets_t.clientSock_gps, eth_InitGPSData(recvGPSBuffer));
                    //pthread_mutex_unlock(&mute); // 잠금을 해제한다.
                }
            }
        }
    }

    return -1;
}*/