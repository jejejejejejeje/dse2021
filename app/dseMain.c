/**
    @file       dseMain.c
    @date       2021.06.14
    @author     Jinmo Je
    @brief      다이버 소나 메인 소스.\n
                GPIO 및 key driver를 초기화 \n
                저장, FPGA 통신, TCP 통신 스레드를 초기화 \n
                배터리 및 수압, 수온 센서 및 모션 센서를 I2C로 주기적 통신
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

/**
    @mainpage   Diver Sonar Eye 2021 메인 페이지
    @section intro 소개
    - 소개    : Diver Sonar Eye 2021 리눅스 어플리케이션
    @section Program 프로그램 명
    - 프로그램명 : dse2021 어플리케이션
    - 프로그램 내용 : FPGA로부터 받은 소나데이터를 display에 가시화한다.
    @section CREATEINFO 작성정보
    - 작성자 : Jinmo Je
    - 작성일 : 2021.06.14
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
    - Jinmo Je/2021.06.14 : 1.0.0 - First.\n
                            1. 모션데이터 파싱 코드 추가.\n
                            2. 배터리 확인하는 i2c 코드 수정.\n
                            3. RTC시간 main에서 1초 주기로 읽어들이게 수정.\n
                            4. 망고보드 관련 코드 제거.\n
                            5. 스크린샷 기능 제거.\n
    */

#include <string.h>
#include <stdio.h>          //FILE
#include <stdlib.h>         //malloc atoi
#include <unistd.h>         // open() close() write() read() lseek()
#include <stdint.h>         // int32_t 등 library
#include <fcntl.h>          // O_RDWR
#include <sys/ioctl.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>         // Signal Process
#include <sys/mman.h>
#include <gx.h>             //gxLib 영상 Draw 관련
#include <gxbdf.h>          //gxLib 폰트 관련
//#include <CL/cl.h>          //openCL
#include <sys/socket.h>     //Ethernet
#include <netinet/in.h>     //Ethernet
#include <arpa/inet.h>      //Ethernet

#include <linux/rtc.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <getopt.h>


#include <BXlog.h>
#include <BXtcpudp.h>
#include <BXi2c.h>
#include <BXctrlLED.h>

#include "dseMain.h"       //
#include "dseBattCheck.h"  //
#include "dsePressCheck.h" //
#include "dseMotion.h"
#include "SerialOperator.h"

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
                                        //Argument Control
static void    main_argumentCtrl( int32_t argc, char* argv[] ); ///< 프로그램 시작 argument를 관리하고 초기화한다.
static void    main_printUsage();           ///< 어플리케이션 실행 시 인자들과 사용법을 출력한다.
                                    //Initialize Function
static int32_t main_userDriverInit();       ///< User Driver를 open하고 초기화한다.
static void    main_userDriverClose();      ///< User Driver를 close한다.
static int32_t main_threadInit();           ///< 각종 thread들을 초기화한다.
static void    main_batteryCheckInit(_battInfo *_info);  ///< Battery 정보를 초기화한다.
static void    main_pressCheckInit(_pressInfo *_info);   ///< 수압, 수온 정보를 초기화한다.
static void    main_motionCheckInit(_motionInfo *_info); ///< 모션 정보를 초기화한다.

int32_t        main_getBattState();         ///< Battery의 State(0~6)을 반환
float          main_getBattPercent();       ///< Battery의 percentage를 반환
float          main_getWaterDepth();        ///< 측정 수심을 반환
float          main_getTemp();              ///< 측정 수온을  반환
_Bool          main_getIsDinving();         ///< Diving 유무를 반환
int32_t        main_getDTSec();             ///< Diving 시간의 초를 반환
int32_t        main_getDTMin();             ///< Diving 시간의 분을 반환
float          main_getHeading();           ///< 측정 헤딩 값을  반환
float          main_getPitch();             ///< 측정 피치 값을  반환
float          main_getRoll();              ///< 측정 롤 값을  반환
int32_t        main_checkFTPOperation();    ///< FTP 접속자가 있는지 확인하여 결과를 반환
int32_t        main_checkTCPOperation();    ///< TCP 접속자가 있는지 확인하여 결과를 반환
int32_t        main_killSystem();           ///< System을 완전히 종료

//static void    main_waitADCConditionVar();
//void           main_sendSignalADCConditionVar();

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////

////////////////global Variable////////////////////////////
int32_t methodFPGAComm = T_VER_REALTIME;    ///< FPGA 통신 방법 정의. 기본값 REALTIME
int32_t cpuVer = IMX6Q_TINYREX;             ///< CPU 보드 버전. 기본값 tinyrex
_Bool en_RTC = 1;                           ///< RTC 사용 유무
char diskName[17] = "/media/mmcblk2p3";     ///< 저장 파일 저장 경로
char diskDeviceName[15] = "/dev/mmcblk2p3"; ///< 저장 디스크의 디바이스 드라이버 경로
float glob_gain = GAIN_MIDDLE;
int logo = 0; ///< 로고 유무 조절
int full_flag=0;
// pthread_cond_t  cond_adc   = PTHREAD_COND_INITIALIZER;
// pthread_mutex_t mut_adc    = PTHREAD_MUTEX_INITIALIZER;

uint16_t *RAWdata1;               ///< RAW Data Buffer 1 (Double Buffering)
uint16_t *RAWdata2;               ///< RAW Data Buffer 2 (Double Buffering)
uint8_t   mode;                   ///< 2ch인지 3ch인지 mode설정. 2ch-0 3ch-1
int32_t   drawYZ;                 ///< 레이더의 Y축 Zero Point Y좌표.(Display 좌상단이 (0,0))
int32_t   radius;                 ///< 레이더의 반지름. 0M 지점부터 최외각까지의 거리. (단위 픽셀 개수)
int32_t   fpgaSampleCount;        ///< RawData의 channel Count(Width, 한 라인의 샘플 개수)
uint8_t   t_range    = 20;        ///< 버튼 입력으로 변경된 range의 값
uint8_t   maxRange   = 75;        ///< range 최대값
uint8_t   gainMode   = GAIN_MIDDLE; ///< 현재 gain Mode
uint8_t   f_gainOrRange = 0;      ///< Gain이나 range change flag. 0-No Change 1-gain Change 2-Range Change
uint8_t   f_change = 0;
//float     rangeMeter = 20;
float     gainOffset = 0.0;       ///< Gain Offset. 현재 0
int8_t    logTableEn = 1;         ///< 로그 변환 곡선 RomTable 사용 유무. 1-RomTable 사용. 0-직접 계산
uint32_t  threshold = 0;          ///< Threashold 이하의 raw data 값은 0으로 만듬.
float     VA_LIMIT_LOG = 200;     ///< 로그 변환 곡선 수식 인자1. 로그 곡선이 적용되는 최대 data 값.
float     VA_UP = 100;            ///< 로그 변환 곡선 수식 인자2. 로그 곡선의 기울기에 관여.
float     VA_DOWN = 30;           ///< 로그 변환 곡선 수식 인자3. 로그 곡선의 기울기에 관여.
float     VA_MAX_X = 5000;        ///< 로그 변환 곡선 수식 인자4. 로그 곡선 이후로 일차 변환 곡선이 적용되는 최대 data 값.
float     DATA_MAX = 767;         ///< 0~65536의 Raw Data를 Mapping할 최대 값. Color Table의 개수

uint8_t   en_i2cBattCheck = 1;    ///< Battery Check 모드 사용 유무.
uint8_t   en_i2cPressCheck = 1;   ///< 수압,수온 Check 모드 사용 유무.
uint8_t   en_i2cMotionCheck = 1;  ///< 모션 Check 모드 사용 유무.
uint8_t   en_timeCheck = 1;       ///< RTC 시간 Check 모드 사용 유무.
int8_t    freq_correction = 1;

static _battInfo  battInfo;       ///< Battery 정보 구조체 인스턴스
static _pressInfo pressInfo;      ///< 수심, 수압, 센서 정보, 다이빙 유무를 가진 구조체 인스턴스
static _motionInfo motionInfo;    ///< 모션센서 정보 구조체 인스턴스
struct rtc_time t_rtc;            ///< RTC 시간 정보 구조체 인스턴스

////////////////global Variable////////////////////////////

/**
@fn      main(int32_t argc, char* argv[])
@author  Jinmo Je
@date    2021.06.14
@brief   main 함수.\n
         argument 처리 및 각종 초기화.\n
         0.5초 간격으로 battery 및 수심, 수온, 다이빙 유무, 모션 데이터를 확인한다.\n
         1초 간격으로 RTC 시간 값을 읽어들인다.\n

@param   argc   argument 개수
@param   argv[] argument 내용이 있는 string 배열.
@return  0
@warning none
@bug     none
@todo    none
*/

void sig_handler(int sig)
{
   if(sig==SIGPIPE)
   {
       printf("SIG_PIPE\n");
   }
    else
    {
        BXlogPrintf("signal %d\n", sig);
        printf("signal %d\n",sig);
    }
}

int32_t main(int32_t argc, char* argv[])
{
    int xx;
    int32_t ret;
    int press_count = 4;
    int motion_count = 3;
    int time_count = 2;
    int batt_count = 1;
    // Argument Control

    for(xx=18;xx<32;xx++)
    {
       if(xx!=SIGTERM) signal(xx,sig_handler);
    }
    main_argumentCtrl(argc, argv);

    // Log File Create
    if (BXlogCreateLogFile(diskName, en_RTC) < 0)
        printf("[dseMain] ERR: Log File Created Failed...\n");

    // BXlogPrintLogo();
    BXlogWriteVersionLog(diskName, DSE2021_VERSION);
    BXlogPutLogMsg("---------------------------------------------------------------------------");
    BXlogPrintf("-------------------    Diver Sonar Eye 2021 V%s    ---------------------", DSE2021_VERSION);
    BXlogPutLogMsg("--------------------------------------------------------------------------- \n\n\n");

                                    //-- Initialize User Driver Module
    if ( main_userDriverInit() < 0 )
        BXlogErrHandling("[dseMain] ERR: UserDriverInit Failed...");

                                    //-- Threads Initialize
    if ( main_threadInit() < 0 )
        BXlogErrHandling("[dseMain] ERR: threadOpen Failed...");

                                    //-- LED State Change (Timer -> always on)
    if (BXledBrightness_0(LED2_DIR)<0)
        BXlogPutLogMsg("[dseMain] ERR: BXledBrightness_255 LED is missing!");

                                    //-- HW Monitoring (V & I) ADC Initialize I2C
    main_batteryCheckInit(&battInfo);
                                    //-- Pressure & Temperature Check Initialize I2C
    main_pressCheckInit(&pressInfo);
                                    //-- Motion Check Initialize I2C
    main_motionCheckInit(&motionInfo);



    BXlogPutLogMsg("[dseMain] Batt. & Press. & Motion Measure Start!!");


    while(1)
    {
        usleep(25000);
        //sleep(1);

        if ((press_count == 5) && en_i2cPressCheck)
        {
            ret = press_updatePressTemp(&pressInfo);
            if (ret == -1)
            {
                BXlogPutLogMsg("[dseMain] ERR: Init Pressure & Temp. i2c Failed");
            }
            else if (ret == -2)
            {
                BXlogPutLogMsg("[dseMain] ERR: Pressure & Temp. Sensor Reserved");
            }
            else if (ret == -3)
            {
                BXlogPutLogMsg("[dseMain] ERR: Pressure & Temp. Sensor Err");
            }

            press_divingCheck(&pressInfo);
            if (pressInfo.enLog)
            {
                if (press_putLog(&pressInfo) == -2)
                    pressInfo.enLog = 0;
            }
            press_count = 0;
        }

        if ((motion_count == 5) && en_i2cMotionCheck)
        {
            motion_updateMotion(&motionInfo);
            motion_count = 0;
        }

        if ((time_count == 20) && en_timeCheck)
        {
            time_rtcGetTime(&t_rtc);
            time_count = 0;
        }

        if ((batt_count == 20) && en_i2cBattCheck)
        {
            battInfo.st = batt_getBattState(&battInfo);
            batt_count = 0;
        }

        press_count++;
        motion_count++;
        time_count++;
        batt_count++;
    }

//cleanup_app :
    pthread_cancel(thr_draw);
    pthread_cancel(thr_fpga);
    pthread_mutex_destroy(&mut_draw);
    pthread_mutex_destroy(&mut_fpga);
    pthread_cond_destroy(&cond_draw);
    pthread_cond_destroy(&cond_fpga);
    pthread_cancel(thr_tcp);
    pthread_mutex_destroy(&mut_tcp);
    pthread_mutex_destroy(&mut_tcp);
    main_userDriverClose();
    if (en_i2cBattCheck == 1)
        batt_battCheckClose(&battInfo);
    if (en_i2cPressCheck)
    {
        press_pressCheckClose(&pressInfo);
        //Log File 생성 취소 170210
        //press_logFileClose(&pressInfo);
    }
    if (en_i2cMotionCheck)
        motionCheckClose(&motionInfo);

    return 0;
}

/** Argument Option Parmeter */
static struct option ap_options[] = {
    {"help"     , 0, NULL, 'h'},  ///< help
    {"log"      , 1, NULL, 'l'},  ///< log
    {"threshold", 1, NULL, 't'},  ///< threshold
    {"version"  , 0, NULL, 'v'},  ///< version
    {"gain"     , 1, NULL, 'g'},  ///< gain offset
    {"fpga"     , 1, NULL, 'f'},  ///< fpga Communication Method
    {"savefile" , 1, NULL, 's'},  ///< save file extension mode
    {NULL       , 0, NULL, '\0'}
};

/**
@fn      main_argumentCtrl(int32_t argc, char* argv[])
@author  Jinmo Je
@date    2021.06.14
@brief   입력 받은 argument의 예외처리 및 파라미터 초기화를 실행한다.

@param   argc   argument 개수
@param   argv[] argument 내용이 있는 string 배열.
@return  void
@warning 실행 인자는 -h, -l, -t, -v, -g, -f, -s이고
         반드시 채널 수가 app 실행 이름 다음 인자로 와야한다.
@bug     none
@todo    none
*/
static void main_argumentCtrl(int32_t argc, char* argv[])
{
    if (argc == 1)
    {
        main_printUsage();
        exit(0);
    }

    switch (atoi(argv[1]))
    {
    case 2  : //2CH
        fpgaSampleCount = ONEPAGE_SAMPLES * 2;
        drawYZ   = (int)(DRAWHEIGHT*0.9765);
        radius   = 630;
        mode     = MODE_2CH;
        maxRange = 75;
        break;
    case 3  : //3CH
        fpgaSampleCount = ONEPAGE_SAMPLES * 6;
        drawYZ = 620;
        radius = 510;
        mode     = MODE_3CH;
        maxRange = 75;
        break;
    case 4  :  //no-logo, 2CH // 디버깅용 동작 유저는 모르게 유지
        logo = 0;
        fpgaSampleCount = ONEPAGE_SAMPLES * 2;
        drawYZ   = (int)(DRAWHEIGHT*0.9765);
        radius   = 630;
        mode     = MODE_2CH;
        maxRange = 75;
        break;
    case 5  :  //no-logo, 3CH // 디버깅용 동작 유저는 모르게 유지
        logo = 0;
        fpgaSampleCount = ONEPAGE_SAMPLES * 6;
        drawYZ = 620;
        radius = 510;
        mode     = MODE_3CH;
        maxRange = 75;
        break;
    default :
        main_printUsage();
        exit(0);
        break;
    }

    int32_t opt;
    while( (opt = getopt_long(argc, argv, "hl:t:vg:f:s:", ap_options, NULL)) != -1 )
    {
        switch (opt)
        {
        case 'h':
            main_printUsage();
            exit(0);
            break;
        case 'l':
            logTableEn = 0;
            break;
        case 't':
            threshold    = strtoul(optarg, NULL, 10);
            printf("[dseMain] threshold Set : %d\n", threshold);
            logTableEn   = 0;
            break;
        case 'v':
            printf("[dseMain] dse   : Diver Sonar Eye Ver %s", DSE2021_VERSION);
            exit(0);
            break;
        case 'g':
            gainOffset  = strtoul(optarg, NULL, 10);
            printf("[dseMain] gain offset: %f\n", gainOffset);
            break;
        case 'f':
            methodFPGAComm = strtoul(optarg, NULL, 10);
            cpuVer = IMX6Q_TINYREX;
            en_RTC = 1;
            strcpy(diskName, "/media/mmcblk2p3");
            strcpy(diskDeviceName, "/dev/mmcblk2p3");
            printf("[dseMain] FPGA Communication Method : %d\n", methodFPGAComm);
            break;
        case 's':
                if (strcmp(optarg, "dse") == 0)
                {
                    fileExtentMode = FILE_EXTENT_MODE_DSE;
                    fileExtentByte = sizeof(uint8_t);
                }
                else if (strcmp(optarg, "ac1") == 0)
                {
                    fileExtentMode = FILE_EXTENT_MODE_AC1;
                    fileExtentByte = sizeof(uint16_t);
                }
                else
                {
                    main_printUsage();
                    exit(0);
                }
            break;
        case '?':
            main_printUsage();
            exit(0);
            break;
        default:
            main_printUsage();
            exit(0);
            break;
        }
    }
}

/**
@fn      main_printUsage()
@author  Bongsung Kim
@date    2017.04.24
@brief   argument로 입력가능한 목록과 실행 방법을 출력한다.
@return  void
@warning none
@bug     none
@todo    none
*/
static void main_printUsage()
{
    printf("dse   : Diver Sonar Eye Ver %s\n", DSE2021_VERSION);
    printf("Usage : \n"
           "\tdse <LaderNum> <Options>\n");
    printf("Options:\n");
    printf("\tLaderNum: 2 or 3\t\t\t: Channel Number\n");
    printf("\t-g <number>\t\t\t\t: Gain Offset\n");
    printf("\t-l <LIMITLOG> <UP> <DOWN> <MAXX>\t: Log Parameter\n");
    printf("\t-t <number>\t\t\t\t: Threshold\n");
    printf("\t-f <number>\t\t\t\t: FPGA Comm. Method 1(FULL FIFO) or 2(Real TIME). default 2\n");
    printf("\t-s <mode>\t\t\t\t: Save Extention Mode Select\n");
    printf("\t\t\t dse, ac1(RAW) default- dse\n");
    printf("\t\t\t\t 1 - FPGA Full Frame Comm.\n");
    printf("\t\t\t\t 2 - FPGA Linux Real Time Comm.\n");
    printf("\t-h\t\t\t\t\t: Help Command\n");
}

//--------------------------------------------------------------------
// 설명:  ADC Condition Variable 신호를 기다린다.
// 반환:
//--------------------------------------------------------------------
/*static void main_waitADCConditionVar()
{
    pthread_mutex_lock(&mut_adc);
    pthread_cond_wait(&cond_adc, &mut_adc);
    pthread_mutex_unlock(&mut_adc);
}*/

//--------------------------------------------------------------------
// 설명:  ADC Condition Variable 신호를 준다.
// 반환:
//--------------------------------------------------------------------
// void main_sendSignalADCConditionVar()
// {
//     pthread_mutex_lock(&mut_adc);
//     pthread_cond_signal(&cond_adc);
//     pthread_mutex_unlock(&mut_adc);
// }

/**
@fn      main_userDriverInit()
@author  Bongsung Kim
@date    2017.04.24
@brief   user driver(keys, gpio_to_FPGA)를 초기화한다.\n
         keys 드라이버는 해당 app의 pid를 전송하고,
         gpio 드라이버는 channel mode를 select한다.
@return  성공시, 0. 실패시, -1
@warning KEYDEVNAME와 GPIODEVNAME 매크로가 가르키는 경로에
         해당하는 드라이버가 반드시 있어야한다.
@bug     none
@todo    none
*/
static int32_t main_userDriverInit()
{
    if ( (keysfd = open(KEYDEVNAME, O_RDWR)) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : /dev/keys Driver Open Failed...!");
        return -1;
    }

    if ( sig_sigFuncInit(getpid()) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : initSignalFunction() Failed...!");
        return -1;
    }

    if ( (gpiofd = open(GPIODEVNAME, O_RDWR)) <0 )                   //GPIO 컨트롤 드라이버 오픈
    {
        BXlogPutLogMsg("[dseMain] ERR : /dev/GPIO_to_FPGA GPIO Device Open failed");
        return -1;
    }
    BXlogPutLogMsg("[dseMain] GPIO Driver Open Success!");

    // Linux to FPGA Mode Select!!
    if( ioctl(gpiofd, IOCTL_MODE_SELECT, &mode) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : IOCTL_MODE_SELECT");
        return -1;
    }

    return 0;
}

/**
@fn      main_userDriverClose()
@author  Bongsung Kim
@date    2017.04.24
@brief   user driver(keys, gpio_to_FPGA)를 close한다.
@return  void
@warning none
@bug     none
@todo    none
*/
static void main_userDriverClose()
{
    close( keysfd );
    close( gpiofd );
}

/**
@fn      main_threadInit()
@author  Jinmo Je
@date    2021.06.14
@brief   사용할 thread 들을 초기화한다. (draw, fpga, tcp, serial)
@return  성공시, 0. 실패시, -1
@warning none
@bug     none
@todo    none
*/
static int32_t main_threadInit()
{
    //------Thread Open---------------------------

    SerialOperator_Create();

    // Thread 1 - Draw Thread Init.

    if ( pthread_create(&thr_draw, NULL, draw_drawThread, NULL) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : Thread Draw Create Failed!!");
        return -1;
    }

    pthread_detach(thr_draw);
    // Thread 1 - Draw Thread Init. END

    // Thread 2 - fpga Thread Init.

    if ( pthread_create(&thr_fpga, NULL, fpga_fpgaThread, NULL) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : Thread FPGA Create Failed!!");
        return -1;
    }

    pthread_detach(thr_fpga);
    // Thread 2 - fpga Thread Init. END

    // Thread 3 - TCP Thread Init.
    pthread_t thr_tcp;

    if ( pthread_create(&thr_tcp, NULL, tcp_tcpThread, NULL) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : Thread TCP Create Failed!!");
        return -1;
    }

    pthread_detach(thr_tcp);
    // Thread 3 - TCP Thread Init. END

    pthread_t thr_seri_gps;

    if ( pthread_create(&thr_seri_gps, NULL, SerialOperator_readGPS, NULL) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : Thread Serial readGPS Create Failed!!");
        return -1;
    }

    pthread_detach(thr_seri_gps);

    /*pthread_t thr_seri2;

    if ( pthread_create(&thr_seri2, NULL, SerialOperator_readSERI2, NULL) < 0 )
    {
        BXlogPutLogMsg("[dseMain] ERR : Thread TCP Create Failed!!");
        return -1;
    }

    pthread_detach(thr_seri2);*/

    return 0;
}

/**
@fn      main_batteryCheckInit(_battInfo *_info)
@author  Bongsung Kim
@date    2017.04.24
@brief   battery Check 기능을 초기화한다. \n
         I2C 연결 오류 시, 배터리 측정 기능은 disable 된다.
@param   *_info 배터리 정보 구조체 포인터
@return  none
@warning I2C_DEV_NAME 매크로가 가르키는 i2c 버스에
         LTC_I2C_ADDR 매크로가 가르키는 i2c id를 가진
         LTC4151 소자가 있어야 한다.
@bug     none
@todo    none
*/
static void main_batteryCheckInit(_battInfo *_info)
{
    if (batt_battCheckOpen(_info, MAX_BATT_AVER_NUM, I2C_DEV_NAME, LTC_I2C_ADDR) < 0)
    {
        BXlogPutLogMsg("[dseMain] ERR: battCheckOpen Failed!");
        BXlogPutLogMsg("[dseMain] Battary Check Mode Disabled");
        en_i2cBattCheck = 0;
    }
    else
    {
        BXlogPutLogMsg("[dseMain] Battary Check Mode enabled");
        en_i2cBattCheck = 1;
    }

label_battCheckInit:
    if (en_i2cBattCheck == 1)
    {
        if (batt_initBattState(_info) < 0)
        {
            BXlogPutLogMsg("[dseMain] ERR: initBattState Get V Value Failed..");
            BXlogPutLogMsg("[dseMain] Battary Check Mode Disabled");
            en_i2cBattCheck = 0;
            goto label_battCheckInit;
        }
    }
    else
    {
        _info->st = 6;
    }
}

/**
@fn      main_getBattState()
@author  Bongsung Kim
@date    2017.04.24
@brief   Battery 상태를 반환한다.
@param   none
@return  배터리 상태값 반환(int 0~6)
@warning none
@bug     none
@todo    none
*/
int32_t main_getBattState()
{
    return battInfo.st;
}

/**
@fn      main_getBattPercent()
@author  Bongsung Kim
@date    2017.04.24
@brief   _BATTINFO 의 battPer 반환한다.
@param   none
@return  배터리 전압 값 Percentage(0~100)
@warning none
@bug     none
@todo    none
*/
float main_getBattPercent()
{
    return battInfo.battPer;
}

/**
@fn      main_getBattVoltage()
@author  JunMyeung Kim
@date    2017.06.29
@brief   _BATTINFO 의 averVin 반환한다.
@param   none
@return  배터리 전압 값
@warning none
@bug     none
@todo    none
*/
float main_getBattVoltage()
{
    return battInfo.averVin;
}

/**
@fn      main_pressCheckInit(_pressInfo *_info)
@author  Bongsung Kim
@date    2017.04.24
@brief   Pressure & Temperature Check 기능을 초기화한다.\n
         수심, 수온 센서와 i2c 통신 초기화 실패시 수심,수온 측정 모드를 disable한다.
@param   *_info 수심,수온,다이빙 정보 구조체 포인터
@return  none
@warning I2C_DEV_NAME 매크로가 가르키는 i2c 버스에
         PRESS_I2C_ADDR 매크로가 가르키는 i2c id를 가진
         86bsd 소자가 있어야 한다.
@bug     none
@todo    none
*/
static void main_pressCheckInit(_pressInfo *_info)
{
    int32_t ret;

    if (press_pressCheckOpen(_info, I2C_DEV_NAME, PRESS_I2C_ADDR) < 0)
    {
        BXlogPutLogMsg("[dseMain] ERR: pressCheckOpen Failed!");
        BXlogPutLogMsg("[dseMain] Pressure & Temperature Check Mode Disabled");
        en_i2cPressCheck = 0;
    }
    else
    {
        BXlogPutLogMsg("[dseMain] Pressure & Temperature Check Mode enabled");
        en_i2cPressCheck = 1;
    }


label_PressCheckInit :
    if (en_i2cPressCheck == 1)
    {
        ret = press_updatePressTemp(_info);
        if ( ret == -1)
        {
            BXlogPutLogMsg("[dseMain] ERR: Init Pressure & Temp. i2c Failed");
            BXlogPutLogMsg("[dseMain] Pressure & Temperature Check Mode Disabled");
            en_i2cPressCheck = 0;
            goto label_PressCheckInit;
        }
        else if (ret == -2)
            BXlogErrHandling("[dseMain] ERR: Pressure & Temp. Sensor Reserved");
        else if (ret == -3)
            BXlogErrHandling("[dseMain] ERR: Pressure & Temp. Sensor Err");
        else if (ret == -4)
            BXlogErrHandling("[dseMain] ERR: Pressure & Temp. Value is strange.");

        //Log 파일 생성 취소 170210
        // ret = press_logFileOpen(_info);
        // if (ret < 0)
        // {
        //     BXlogPutLogMsg("[dseMain] ERR: LogFile Create Failed....");
        //     BXlogPutLogMsg("[dseMain] LogFile Not Sync...");
        // }
    }
    else
    {
        _info->press = 999;
        _info->temp  = 999;
        _info->depth = 999;
    }


    _info->divingMin = 0;
    _info->divingSec = 0;
    _info->isDiving  = 0;
    _info->divingCnt = 0;
}

/**
@fn      main_getWaterDepth()
@author  Bongsung Kim
@date    2017.04.24
@brief   계산된 수심값을 반환한다.
@param   none
@return  수심 값.(Meter)
@warning none
@bug     none
@todo    none
*/
float main_getWaterDepth()
{
    return pressInfo.depth;
}

/**
@fn      main_getTemp()
@author  Bongsung Kim
@date    2017.04.24
@brief   측정된 수온을 반환한다.
@param   none
@return  수온 값.(화씨)
@warning none
@bug     none
@todo    none
*/
float main_getTemp()
{
    return pressInfo.temp;
}

/**
@fn      main_getIsDinving()
@author  Bongsung Kim
@date    2017.04.24
@brief   Diving 여부 값을 반환한다.
@param   none
@return  Diving 여부 값 (true-다이빙 중. false-다이빙중이 아님.)
@warning none
@bug     none
@todo    none
*/
_Bool main_getIsDinving()
{
    return pressInfo.isDiving;
}

/**
@fn      main_getDTSec()
@author  Bongsung Kim
@date    2017.04.24
@brief   Diving 시간의 초를 반환한다.
@param   none
@return  초. 0~59
@warning none
@bug     none
@todo    none
*/
int32_t main_getDTSec()
{
    return pressInfo.divingSec;
}

/**
@fn      main_getDTMin()
@author  Bongsung Kim
@date    2017.04.24
@brief   Diving 시간의 분를 반환한다.
@param   none
@return  분. 0~
@warning none
@bug     none
@todo    none
*/
int32_t main_getDTMin()
{
    return pressInfo.divingMin;
}

/**
@fn      main_motionCheckInit(_motionInfo *_info)
@author  Jinmo Je
@date    2021.05.22
@brief   Motion Check 기능을 초기화한다.\n
         모션센서와 i2c 통신 초기화 실패시 모션 측정 모드를 disable한다.
@param   *_info 모션센서 정보 구조체 포인터
@return  none
@warning I2C_MOTION_DEV_NAME 매크로가 가르키는 i2c 버스에
         MOTION_I2C_ADDR 매크로가 가르키는 i2c id를 가진
         SENtrall_MM_BLUE 소자가 있어야 한다.
@bug     none
@todo    none
*/
static void main_motionCheckInit(_motionInfo *_info)
{
    int32_t ret;

    if (motionCheckOpen(_info, I2C_MOTION_DEV_NAME, MOTION_I2C_ADDR) < 0)
    {
        BXlogPutLogMsg("[dseMain] ERR: motionCheckOpen Failed!");
        BXlogPutLogMsg("[dseMain] motion Check Mode Disabled");
        en_i2cMotionCheck = 0;
    }
    else
    {
        BXlogPutLogMsg("[dseMain] motion Check Mode enabled");
        en_i2cMotionCheck = 1;
    }


label_MotionCheckInit :
    if (en_i2cMotionCheck == 1)
    {
        ret = motion_updateMotion(_info);
        if ( ret == -1)
        {
            BXlogPutLogMsg("[dseMain] ERR: Init Motion. i2c Failed");
            BXlogPutLogMsg("[dseMain] Motion Check Mode Disabled");
            en_i2cMotionCheck = 0;
            goto label_MotionCheckInit;
        }
    }
    else
    {
        _info->heading = 9;
        _info->pitch  = 9;
        _info->roll = 9;
    }
}

/**
@fn      main_getHeading()
@author  Jinmo Je
@date    2021.05.22
@brief   측정된 헤딩 값을 반환한다.
@param   none
@return  도. 
@warning none
@bug     none
@todo    none
*/
float main_getHeading()
{
    return motionInfo.heading;
}

/**
@fn      main_getPitch()
@author  Jinmo Je
@date    2021.05.22
@brief   측정된 피치 값을 반환한다.
@param   none
@return  도. 
@warning none
@bug     none
@todo    none
*/
float main_getPitch()
{
    return motionInfo.pitch;
}

/**
@fn      main_getRoll()
@author  Jinmo Je
@date    2021.05.22
@brief   측정된 롤 값을 반환한다.
@param   none
@return  도. 
@warning none
@bug     none
@todo    none
*/
float main_getRoll()
{
    return motionInfo.roll;
}

/**
@fn      main_checkFTPOperation()
@author  Bongsung Kim
@date    2017.04.24
@brief   FTP 접속 중인지 ps -ef로 확인한다.
@param   none
@return  FTP 접속자가 있다면 0, 없으면 -1
@warning none
@bug     none
@todo    none
*/
int32_t main_checkFTPOperation()
{
    char t_string[256];
    char t_buf[32];
    FILE *fp;

    snprintf(t_string, 256, "ps -ef | grep 'pure-ftpd (IDLE)' | grep -v grep | awk '{print $1}'");

    fp = popen(t_string, "r");
    fseek(fp, 0, SEEK_SET);
    //fscanf(fp, "%d", &pid);
    while(fgets(t_buf, 32, fp) != NULL);

    pclose(fp);

    if (strcmp(t_buf,"")==0)
        return -1;

    return 0;
}


/**
@fn      main_checkTCPOperation()
@author  JM Kim
@date    2017.05.15
@brief   TCP 접속 중인지 netstat로 확인한다.
@param   none
@return  TCP 접속자가 있다면 0, 없으면 -1
@warning none
@bug     none
@todo    none
*/
int32_t main_checkTCPOperation()
{
    char t_string[256];
    char t_buf[32];
    FILE *fp;

    snprintf(t_string, 256, "netstat | grep tcp | awk '{print $1}'");

    fp = popen(t_string, "r");
    fseek(fp, 0, SEEK_SET);
    //fscanf(fp, "%d", &pid);
    while(fgets(t_buf, 32, fp) != NULL);

    pclose(fp);

    if (strcmp(t_buf,"")==0)
        return -1;

        return 0;

}

/**
@fn      main_killSystem()
@author  Bongsung Kim
@date    2017.04.24
@brief   강제 종료를 위해 driver로 kill 명령을 전송하여 system을 강제 종료한다.
@param   none
@return  ioctl 실패 시, -1 반환, 성공시 0 반환.
@warning none
@bug     none
@todo    none
*/
int32_t main_killSystem()
{
    if( ioctl(gpiofd, IOCTL_KILL, NULL) < 0 )
    {
        BXlogPutLogMsg("[main] ERR : IOCTL_KILL");
        return -1;
    }

    return 0;
}
