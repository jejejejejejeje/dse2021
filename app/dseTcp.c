/**
    @file       dseTcp.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 TCP 컨트롤러
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#include <stdio.h>          //FILE
#include <unistd.h>
#include <stdlib.h>         //malloc atoi
#include <sys/socket.h>     //Ethernet
#include <netinet/in.h>     //Ethernet
#include <arpa/inet.h>      //Ethernet
#include <pthread.h>
#include <signal.h>         // Signal Process
#include <sys/epoll.h>
#include <string.h>
#include <math.h>
#include <linux/rtc.h>
#include <time.h>
#include <sys/time.h>

#include <BXlog.h>
#include <BXtcpudp.h>
#include <BXepoll.h>

#include "dseMain.h"
#include "dseTcp.h"

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
void *tcp_tcpThread();

static int32_t tcp_Serverinit();
static void    tcp_SendHeaderInit();
static void    tcp_epollInit();
static int32_t tcp_processEpollSignal(int32_t _count);
static int32_t tcp_addEpollServer();
static int32_t tcp_addEpollClient();
static int32_t tcp_delEpollClient();

static void    tcp_clientInit();
static int32_t tcp_accept();
static int32_t tcp_rcvOperation();
static int32_t tcp_arrangeCMD();
static int32_t tcp_sendOper(int8_t _ind);
static int32_t tcp_sendRTCTime();
static int32_t tcp_format();
static int32_t tcp_sendFormatFailed();
                                                    //
static void tcp_waitTCPConditionVar();
void        tcp_sendSignalTCPConditionVar();
///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////

////////////////global Variable////////////////////////////
                                //thread
pthread_t thr_tcp;                                      ///< TCP 통신 Thread
pthread_cond_t  cond_tcp = PTHREAD_COND_INITIALIZER;    ///< TCP Thread와 다른 thread간 동기화를 위한 조건 변수
pthread_mutex_t mut_tcp  = PTHREAD_MUTEX_INITIALIZER;   ///< TCP Thread와 다른 thread간 동기화를 위한 mutex

int8_t   f_startTcp = 0;  ///< TCP 실시간 주사 모드 Start Flag

//BX TCP socket
BXsocket_t *serv_sock;      ///< server socket 정보 구조체. (user-made)
BXsocket_t clnt_sock[10];   ///< client socket 정보 구조체. (user-made)
//--- TCP Recv ...
uint32_t recv_buf[RECV_BUFFER_SIZE];  ///< tcp 수신 버퍼
uint32_t recvNumBytes;                ///< tcp 수신 Bytes 카운터

SEND_CMD  *recv_cmd            ;
SEND_DATA send_data            ;

BXepoll_t *epollObj;                  ///< tcp epoll 관리자.

uint32_t tcpFrameCnt;

long tv_formatS;   ///< format 시작 시간
long tv_formatE;   ///< format 종료 시간
////////////////global Variable////////////////////////////

/**
@fn      tcp_tcpThread()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP 처리 thread 함수.
@param   none
@return  (void *)0
@warning none
@bug     none
@todo    none
*/
void *tcp_tcpThread()
{
    int32_t evCnt = 0;
    int8_t  t_saveIndexOffset = 0;
                                        //tcp Server Init.
    tcp_Serverinit();

    tcp_SendHeaderInit();

    tcp_epollInit();

    while(1)
    {
                                            //epoll Waiting
        evCnt = BXepollWait(epollObj, 10, 10);
                                            //epoll signal Processing
        tcp_processEpollSignal(evCnt);

        if(f_startTcp)
        {
            tcp_waitTCPConditionVar();

            t_saveIndexOffset = f_bufSwitching;

            tcp_sendOper(t_saveIndexOffset);

            pthread_mutex_unlock(&mut_tcp);
        }
        usleep(50);
    }

    BXepollClose(epollObj);
    return (void *)0;
}

/**
@fn      tcp_Serverinit()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP Sever Init 함수
@param   none
@return  성공시, 0. 실패시, 프로그램 종료.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_Serverinit()
{
    //   1. Socket Open!!!!
    serv_sock = BXnewSocket(BX_TCP_SOCKET, BX_NET_SERVER, htonl(INADDR_ANY), 4668);
    if(serv_sock==NULL)
        BXlogErrHandling("[dseTcp]ERR: Critical newSocket ERR!");

    BXlogPutLogMsg("[dseTcp] Server Socket Created!");

    //   2. Socket Bind!!!!
    if (serv_sock->socketBind(serv_sock) < 0)
        BXlogErrHandling("[dseTcp]ERR : BIND ERR!");

    BXlogPutLogMsg("[dseTcp] Server Bind() Success!");

    //   3. Socket Listen!!!!
    if (serv_sock->socketListen(serv_sock) < 0)
        BXlogErrHandling("[dseTcp]ERR: Listen() ERR!\n");

    BXlogPutLogMsg("[dseTcp] Server Listen() Success!");

    // SIGPIPE 오류 무시 위해서 선언.
    signal(SIGPIPE, SIG_IGN);

    return 0;
}

/**
@fn      tcp_SendHeaderInit()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP로 보내는 실시간 프레임 데이터 헤더 초기화
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void tcp_SendHeaderInit()
{
    send_data.StartFlag    = 0xFFFFFFFE;
    send_data.PacketType   = CMD_RAW_DATA;
    send_data.PacketLength = sizeof(send_data);
    send_data.Width        = fpgaSampleCount;
    send_data.FrameNum     = 0;
    send_data.voltage      = main_getBattState();
}

/**
@fn      tcp_epollInit()
@author  Bongsung Kim
@date    2017.04.25
@brief   Epoll 초기화. 관리는 최대 10개. 최대 10ms timeout.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void tcp_epollInit()
{
    epollObj = BXepollOpen(10, 10);
    if (epollObj == NULL)
        BXlogErrHandling("[dseTcp] ERR: epoll Create Failed!!");
                                    //--TCP 서버를 epoll에 등록
    if ( tcp_addEpollServer() < 0 )
        BXlogErrHandling("[dseTcp] ERR: epoll Add Failed!!");
}

/**
@fn      tcp_addEpollServer()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP 서버 소켓을 epoll에 등록.
@param   none
@return  성공시, 0. 실패시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_addEpollServer()
{
    return BXepollAdd(epollObj, serv_sock->socketID, EPOLLIN | EPOLLERR | EPOLLRDHUP);
}

/**
@fn      tcp_addEpollClient()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP 클라이언트 소켓을 epoll에 등록.
@param   none
@return  성공시, 0. 실패시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_addEpollClient()
{
    return BXepollAdd(epollObj, clnt_sock[0].socketID, EPOLLIN | EPOLLERR | EPOLLRDHUP);
}

/**
@fn      tcp_delEpollClient()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP 클라이언트 소켓을 epoll에서 삭제.
@param   none
@return  성공시, 0. 실패시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_delEpollClient()
{
    return BXepollDel(epollObj, clnt_sock[0].socketID);
}

/**
@fn      tcp_processEpollSignal(int32_t _count)
@author  Bongsung Kim
@date    2017.04.25
@brief   Epoll Process
@param   _count : 발생한 epoll event 개수.
@return  성공시, 0. 실패시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_processEpollSignal(int32_t _count)
{
    int32_t i; //int32_t ret = 0;
    int32_t _fd = -1;
    uint32_t _ev = 0;


    for (i=0; i<_count; i++)
    {
        _fd = epollObj->events[i].data.fd;
        _ev = epollObj->events[i].events;
                                        //서버이벤트(클라이언트 접속)
        if (_fd == serv_sock->socketID)
        {
            tcp_clientInit();
        }
        else
        {
            if (_ev & EPOLLIN)     //Data Read
            {
                tcp_rcvOperation();
            }
            if (_ev & EPOLLRDHUP)  //Disconnet
            {
                BXlogPutLogMsg("[dseTcp]Disconnet!!");
                if (tcp_delEpollClient() < 0)
                    BXlogErrHandling("[dseTcp]ERR: epoll Del Client Failed!!");
                f_startTcp = 0;
            }
            if (_ev & EPOLLERR)    //Error
            {
                BXlogPutLogMsg("[dseTcp]POLL TCP ERR:");
                f_startTcp = 0;
            }
        }
    }
    return 0;
}

/**
@fn      tcp_clientInit()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP로 Client가 접속 요청시 accept 및 client를 epoll에 등록.
@param   _count : 발생한 epoll event 개수.
@return  none
@warning none
@bug     none
@todo    none
*/
static void tcp_clientInit()
{
    if (tcp_accept() < 0)
        BXlogErrHandling("[dseTcp] ERR: TCP ACCEPT ERR!!");

    if ( tcp_addEpollClient() < 0 )
        BXlogErrHandling("[dseTcp] ERR: epoll Add Client Failed!!");
}

/**
@fn      tcp_accept()
@author  Bongsung Kim
@date    2017.04.25
@brief   tcp Client Accept 및 접속 정보 출력.
@param   none
@return  성공시, 0. 실패시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_accept()
{
    clnt_sock[0].socketID = serv_sock->socketAccept(serv_sock, &clnt_sock[0]);
    if (clnt_sock[0].socketID < 0)
    {
        return -1;
    }

    BXlogPrintf("[dseTcp]Connected from %s:%d\n",
                  inet_ntoa(clnt_sock[0].addr.sin_addr),
                  ntohs(clnt_sock[0].addr.sin_port) );

    recvNumBytes = 0;

    return 0;
}

/**
@fn      tcp_rcvOperation()
@author  Bongsung Kim
@date    2017.04.25
@brief   tcp cmd 입력 받았을 때 동작 함수.
@param   none
@return  tcp_arrangeCMD()의 반환값.
@warning none
@bug     none
@todo    none
@see     tcp_arrangeCMD()
*/
static int32_t tcp_rcvOperation()
{
    ssize_t  numByteRcv = 0;
   //

    numByteRcv = serv_sock->socketRecv(&clnt_sock[0], (char *)recv_buf + recvNumBytes, RECV_BUFFER_SIZE*sizeof(uint32_t) - recvNumBytes, 0);
    if (numByteRcv<0)
    {
        BXlogPutLogMsg("[dseTcp] Connetion Broken!![RECV]");
        return -1;
    }

    recvNumBytes += numByteRcv;

    return tcp_arrangeCMD();
}

/**
@fn      tcp_arrangeCMD()
@author  Bongsung Kim
@date    2017.04.25
@brief   command에 따른 동작을 처리한다.
@param   none
@return  성공시, 0. 실패시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_arrangeCMD()
{
    int32_t ret = 0;
    recv_cmd = (SEND_CMD *)recv_buf;

    while(recvNumBytes >= sizeof(SEND_CMD))
    {
        switch (recv_cmd->PacketType)
        {
            case CMD_START :
                BXlogPutLogMsg("[dseTcp] TCP Data Transfer Start!!!");
                f_startTcp = 1;
                tcpFrameCnt = 0;
            break;
            
            case CMD_STOP  :
                BXlogPutLogMsg("[dseTcp] TCP Data Transfer End!!!");
                f_startTcp = 0;
            break;

            case CMD_SET_TIME :
                BXlogPrintf("[dseTcp] DSE Time Set %02d/%02d/%02d %02d:%02d:%02d",
                                recv_cmd->Value[0],
                                recv_cmd->Value[1],
                                recv_cmd->Value[2],
                                recv_cmd->Value[3],
                                recv_cmd->Value[4],
                                recv_cmd->Value[5]);
                BXlogPrintf("[dseTcp] DSE Time Zone UTC+%d",recv_cmd->Value[6]);

                ret = time_setTime((uint32_t *)recv_cmd->Value);
                if (ret < 0)
                    BXlogErrHandling("[dseTcp] ERR: RTC Set Failed...\n Prog. Shutdown.");
            break;

            case CMD_GET_TIME :
                ret = tcp_sendRTCTime();
                if (ret == -1)
                    BXlogErrHandling("[dseTcp] ERR: RTC Transfer Failed...\n TCP Send Err.");
                else if (ret == -2)
                    BXlogErrHandling("[dseTcp] ERR: RTC Transfer Failed...\n RTC Read Err.");
            break;

            case CMD_FORMAT :
                tv_formatS = time_getProcessClock();
                ret = tcp_format();
                tv_formatE = time_getProcessClock();
                if ((tv_formatE - tv_formatS) < 5000)
                    tcp_sendFormatFailed();
            break;

            case CMD_DSE :
            break;

            case CMD_AC1 :
            break;
        }

        //recv_buf 정렬
        recvNumBytes -= sizeof(SEND_CMD);
        memcpy(recv_buf, recv_buf + (sizeof(SEND_CMD)/4),
               RECV_BUFFER_SIZE*sizeof(uint32_t) - sizeof(SEND_CMD));
    }


    return ret;
}

/**
@fn      tcp_sendOper(int8_t _ind)
@author  Bongsung Kim
@date    2017.04.25
@brief   tcp 한 프레임 전송 처리
@param   _ind : double Buffering에 따라 어느 버퍼를 사용할 것인지 인덱스.
@return  성공시, 0. 실패시, -1.
@warning none
@bug     PC와 실시간 동작이 제대로 되지않는 듯.
@todo    none
*/
static int32_t tcp_sendOper(int8_t _ind)
{
    //int32_t sndHeight = floor((t_range+1) * 39.37 + 0.5);
    int32_t sndHeight = fpga_calcDataTotalHeight((int32_t)t_range);
    ssize_t numByte;

    send_data.Height     = sndHeight;
    send_data.FrameSize  = send_data.Height * fpgaSampleCount;
    send_data.RangeMeter = t_range;
    send_data.gainMode   = gainMode;
    send_data.FrameNum   = tcpFrameCnt;
    send_data.voltage    = main_getBattState();

    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&send_data, sizeof(send_data), 0);
    if (numByte <= 0)
        return -1;
    if (!_ind)
    {
        numByte = serv_sock->socketSend(&clnt_sock[0],
                                        (char *)RAWdata1,
                                        fpgaSampleCount * sndHeight*sizeof(uint16_t),
                                        0);
        if (numByte <= 0)
                return -1;
    }
    else
    {
        numByte = serv_sock->socketSend(&clnt_sock[0],
                                        (char *)RAWdata2,
                                        fpgaSampleCount * sndHeight*sizeof(uint16_t),
                                        0);
        if (numByte <= 0)
                return -1;
    }
    return 0;
}

/**
@fn      tcp_sendRTCTime()
@author  Jinmo Je
@date    2021.06.14
@brief   RTC 시간을 PC로 전송.
@param   none
@return  성공시, 0. rtc read 실패시, -2. tcp 전송 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_sendRTCTime()
{
    SEND_CMD _sndCmd;
    ssize_t numByte;

    _sndCmd.StartFlag    = 0xFFFFFFFE;
    _sndCmd.PacketLength = 40;
    _sndCmd.PacketType   = CMD_GET_TIME;

    _sndCmd.Value[0] = t_rtc.tm_year;
    _sndCmd.Value[1] = t_rtc.tm_mon;
    _sndCmd.Value[2] = t_rtc.tm_mday;
    _sndCmd.Value[3] = t_rtc.tm_hour;
    _sndCmd.Value[4] = t_rtc.tm_min;
    _sndCmd.Value[5] = t_rtc.tm_sec;

    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&_sndCmd, sizeof(SEND_CMD), 0);
    if(numByte <= 0)
        return -1;

    BXlogPutLogMsg("[dseTcp] CMD_GET_RTC_TIME : Get RTC time : ");
    BXlogPrintf("[dseTcp] %02d/%02d/%02d %02d:%02d:%02d",
                            _sndCmd.Value[0],
                            _sndCmd.Value[1],
                            _sndCmd.Value[2],
                            _sndCmd.Value[3],
                            _sndCmd.Value[4],
                            _sndCmd.Value[5]);
    return 0;
}

/**
@fn      tcp_format()
@author  Bongsung Kim
@date    2017.04.25
@brief   data 저장 공간을 Format한다.
@param   none
@return  성공시, 0. 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_format()
{
    SEND_CMD _sndCmd;
    ssize_t  numByte;

    char t_str[100];

    _sndCmd.StartFlag    = 0xFFFFFFFE;
    _sndCmd.PacketLength = 40;
    _sndCmd.PacketType   = CMD_FORMAT;

            //Save 중이면 Save 정지를 기다림.
    while(f_save == SAVE_START)
    {
        f_save = SAVE_STOP;
        usleep(500000);
    }

                                    //Format Start
    BXlogPrintf("[dseTcp] %s Format Operation...", diskName);

    _sndCmd.Value[0] = 1;
    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&_sndCmd, sizeof(SEND_CMD), 0);
    if(numByte <= 0)
        return -1;

                                    //1. unmount
    BXlogPrintf("[dseTcp] unmounting device %s", diskDeviceName);
    sprintf(t_str, "umount %s", diskName);
    system(t_str);

    _sndCmd.Value[0] = 2;
    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&_sndCmd, sizeof(SEND_CMD), 0);
    if(numByte <= 0)
        return -1;

                                    //2. Format
    printf("[dseTcp] Format Device %s\n", diskDeviceName);
    sprintf(t_str, "mkfs.ext3 -j %s -L data > /dev/null", diskDeviceName);
    system(t_str);
    system("");

    _sndCmd.Value[0] = 3;
    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&_sndCmd, sizeof(SEND_CMD), 0);
    if(numByte <= 0)
        return -1;

                                    //3. Mount
    printf("[dseTcp] Mounting Device %s\n", diskName);
    sprintf(t_str, "mount %s %s", diskDeviceName, diskName);
    system(t_str);
    system("");

    _sndCmd.Value[0] = 4;
    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&_sndCmd, sizeof(SEND_CMD), 0);
    if(numByte <= 0)
        return -1;

                                    //4. Reboot
    BXlogPutLogMsg("[dseTcp] Format Success! Reboot");
    _sndCmd.Value[0] = 5;
    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&_sndCmd, sizeof(SEND_CMD), 0);
    if(numByte <= 0)
        return -1;
    system("reboot");

    return 0;
}

/**
@fn      tcp_sendFormatFailed()
@author  Bongsung Kim
@date    2017.04.25
@brief   Format 실패시, PC로 실패를 알림.
@param   none
@return  성공시, 0. tcp 전송 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t tcp_sendFormatFailed()
{
    SEND_CMD _sndCmd;
    ssize_t  numByte;

    _sndCmd.StartFlag    = 0xFFFFFFFE;
    _sndCmd.PacketLength = 40;
    _sndCmd.PacketType   = CMD_FORMAT;

    _sndCmd.Value[0] = 10;
    numByte = serv_sock->socketSend(&clnt_sock[0], (char *)&_sndCmd, sizeof(SEND_CMD), 0);
    if(numByte <= 0)
        return -1;

    return 0;

}

/**
@fn      tcp_waitTCPConditionVar()
@author  Bongsung Kim
@date    2017.04.25
@brief   CP Condition Variable 을 wait한다.
@param   none
@return  none
@warning mut_tcp에 대한 unlock을 수행하지 않음. 그러므로 이 함수 호출 후에는 반드시 unlock 수행해야함.
@bug     none
@todo    none
*/
static void tcp_waitTCPConditionVar()
{
    pthread_mutex_lock(&mut_tcp);     //lock
    pthread_cond_wait(&cond_tcp, &mut_tcp);
    //pthread_mutex_unlock(&mut_draw);     //unlock
}

/**
@fn      tcp_sendSignalTCPConditionVar()
@author  Bongsung Kim
@date    2017.04.25
@brief   TCP Condition Variable 신호를 준다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
void tcp_sendSignalTCPConditionVar()
{
    pthread_mutex_lock(&mut_tcp);     //lock
    pthread_cond_signal(&cond_tcp);
    pthread_mutex_unlock(&mut_tcp);     //unlock
}
