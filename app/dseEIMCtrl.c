/**
    @file       dseEIMCtrl.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 Fpga 통신(EIM) 컨트롤러
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#include <stdio.h>
#include <stdint.h>         // int32_t 등 library
#include <signal.h>         // Signal Process
#include <unistd.h>         // open() close() write() read() lseek()
#include <fcntl.h>          // O_RDWR
#include <stdlib.h>         //malloc
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <math.h>
#include <string.h>
#include <pthread.h>

#include <BXlog.h>

#include "dseMain.h"
#include "dseEIMCtrl.h"

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
void          *fpga_fpgaThread();

int32_t        fpga_calcDataTotalHeight(int32_t _range);

void           fpga_sendSignalFPGAConditionVar();
//static void    fpga_waitFPGAConditionVar();

static void   *fpga_FPGAmmap(int32_t byteSize);
static int32_t fpga_FPGAmunmap(void *mm, int32_t byteSize);
static int32_t fpga_fpgaInfoinit(fpga_t *_info, float _range, int32_t _width);
static int32_t fpga_getSonarData(fpga_t *_info,
                                uint16_t *_buf,
                                uint16_t *_fpga,
                                uint8_t _swBeam);
static int32_t fpga_pinChangeToStart(int32_t *_waitTime);
static int32_t fpga_eimFPGA2MemCpy(fpga_t *_info,
                                  uint16_t *_dst, uint16_t *_src,
                                  uint8_t _sw);
static void    fpga_makeInputExam(fpga_t *_info, uint16_t* _dst, uint8_t _sw);
static int32_t fpga_vacateFifo(uint16_t *_fpga);
///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////

////////////////global Variable////////////////////////////

pthread_t thr_fpga;                                     ///< FPGA 통신 Thread
pthread_cond_t  cond_fpga = PTHREAD_COND_INITIALIZER;   ///< FPGA Thread와 다른 thread간 동기화를 위한 조건 변수
pthread_mutex_t mut_fpga  = PTHREAD_MUTEX_INITIALIZER;  ///< FPGA Thread와 다른 thread간 동기화를 위한 mutex

int8_t f_bufSwitching = 0;                              ///< Double Buffering의 버퍼 인덱스
////////////////global Variable////////////////////////////

/**
@fn      fpga_fpgaThread()
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA Communication Thread Function
@param   none
@return  (void *)0
@warning none
@bug     none
@todo    none
*/
void *fpga_fpgaThread()
{
    fpga_t  fpgaInfo;
    uint16_t *fpga = (uint16_t *)fpga_FPGAmmap(MAXHEIGHT*fpgaSampleCount*sizeof(uint16_t));
    if (fpga == NULL)
        BXlogErrHandling("[dseEIMCtrl] ERR: FPGA memory mapping Failed!!\n");

    RAWdata1 = (uint16_t *)malloc(fpgaSampleCount*MAXHEIGHT*sizeof(uint16_t));
    RAWdata2 = (uint16_t *)malloc(fpgaSampleCount*MAXHEIGHT*sizeof(uint16_t));

    //Wait Start Signal
#if START_KEY_INT
    draw_waitSignalStartConditionVar();
#endif

    BXlogPutLogMsg("[dseDraw] FPGA Thread : Thread loop Start!");

    while(1)
    {
                                            //Init FPGA info.
                                            //(range, wait Time, height, width)
        fpga_fpgaInfoinit(&fpgaInfo, t_range, fpgaSampleCount);

                                            //Get Sonar Data
        if (!f_bufSwitching)
        {
            if (fpga_getSonarData(&fpgaInfo, RAWdata1, fpga, 0) < 0)
                BXlogErrHandling("[dseEIMCtrl] Err: fpga_getSonarData Err.");

            if (methodFPGAComm == T_VER_REALTIME)
            {
                if (mode == MODE_3CH)
                {
                    usleep(50);
                    if (fpga_getSonarData(&fpgaInfo, RAWdata1, fpga, 1) < 0)
                        BXlogErrHandling("[dseEIMCtrl] Err: fpga_getSonarData Err.");
                }
            }
        }
        else
        {
            if (fpga_getSonarData(&fpgaInfo, RAWdata2, fpga, 0) < 0)
                BXlogErrHandling("[dseEIMCtrl] Err: fpga_getSonarData Err.");

            if (methodFPGAComm == T_VER_REALTIME)
            {
                if (mode == MODE_3CH)
                {
                    usleep(50);
                    if (fpga_getSonarData(&fpgaInfo, RAWdata2, fpga, 1) < 0)
                        BXlogErrHandling("[dseEIMCtrl] Err: fpga_getSonarData Err.");
                }
            }
        }
                                            //Send signal to Draw Thread
        draw_sendSignalDrawConditionVar();
        save_sendSignalSaveConditionVar();
        tcp_sendSignalTCPConditionVar();

        usleep(20000);

        if (methodFPGAComm == T_VER_FULLFIFO)
        {
            fpga_vacateFifo(fpga);
        }
                                            //RAW data Buffer Index Switching
        f_bufSwitching ^= 1;
    }

//cleanup_fpgaThread :
    free(RAWdata1);
    free(RAWdata2);
    fpga_FPGAmunmap(fpga, MAXHEIGHT*fpgaSampleCount*sizeof(uint16_t));

    return (void *)0;
}

/**
@fn      fpga_FPGAmmap(int32_t byteSize)
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA EIM memory mapping. \n
         FPGA 통신을 위해 FPGA의 가상 메모리를 할당한다.
@param   byteSize : FPGA로부터 한번에 읽어올 데이터의 Byte Size
@return  성공 시 해당 FPGA 메모리의 시작 주소 포인터\n
         실패 시 NULL
@warning none
@bug     none
@todo    none
*/
static void *fpga_FPGAmmap(int32_t byteSize)
{
    int memfd = open("/dev/mem",O_RDWR | O_SYNC);
    if (memfd < 0)
    {
        BXlogPutLogMsg("[dseEIMCtrl] ERR : memfd Open Failed...");
        return NULL;
    }

    void *ret = (void *)mmap(0, byteSize, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, 0x08000000);

    close(memfd);

    return ret;
}

/**
@fn      fpga_FPGAmunmap(void *mm, int32_t byteSize)
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA 통신을 위해 할당한 가상 메모리를 헤제한다.
@param   *mm      : 할당 해제할 가상 메모리의 주소.(포인터)
@param   byteSize : FPGA로부터 한번에 읽어올 데이터의 Byte Size
@return  munmap()함수의 return 값.
@warning byteSize는 반드시 *mm을 할당할 때의 byte 크기여야 한다.
@bug     none
@todo    none
*/
static int32_t fpga_FPGAmunmap(void *mm, int32_t byteSize)
{
    return munmap(mm, byteSize);
}

/**
@fn      fpga_fpgaInfoinit(fpga_t *_info, float _range, int32_t _width)
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA 통신에 필요한 Data 들 Initialze 한다.
@param   *_info : fpga 정보 구조체(fpga_t).
@param   _range : 거리 정보(Meter)
@param   _width : channel count. raw data의 가로 데이터 개수.
@return  성공시 0. 실패 시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t fpga_fpgaInfoinit(fpga_t *_info, float _range, int32_t _width)
{
    _info->rangeMeter = _range;
    _info->waitTimeforSona = (int)ceil(TIMEUNIT * (_range+1) + 1);
    //_info->h = (int)floor((_range+1) * 39.37 + 0.5);
    _info->h = fpga_calcDataTotalHeight((int32_t)_range);

    if (methodFPGAComm == T_VER_FULLFIFO)
    {
        _info->w = _width;
    }
    else if (methodFPGAComm == T_VER_REALTIME)
    {
        if (mode == MODE_3CH)
            _info->w = _width / 2;
        else
            _info->w = _width;
    }

    return 0;
}

/**
@fn      fpga_getSonarData(fpga_t *_info,
                           uint16_t *_buf,
                           uint16_t *_fpga,
                           uint8_t _swBeam)
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA로부터 한 프레임 RawData를 읽어온다.
@param   *_info   : fpga 정보 구조체(fpga_t).
@param   *_buf    : Raw Data가 저장될 버퍼의 포인터(주소)
@param   *_fpga   : FPGA의 가상 메모리 포인터.
@param   *_swBeam : 3ch 교차빔 모드에서 교차 빔 인덱스
@return  성공시 0. 실패 시, -1.
@warning DEBUG_INPUT가 1이면 FPGA와 통신을 하지 않고 가상데이터를 생성한다.
@bug     none
@todo    none
*/
static int32_t fpga_getSonarData(fpga_t *_info,
                                uint16_t *_buf,
                                uint16_t *_fpga,
                                uint8_t _swBeam)
{
    if (methodFPGAComm == T_VER_REALTIME)
    {
        if (ioctl(gpiofd, IOCTL_CHANNELSWITCH, &_swBeam) < 0) {
            BXlogPutLogMsg("[dseEIMCtrl] ERR : IOCTL_CHANNELSWITCH");
            return -1;
        }
    }

    if (fpga_pinChangeToStart(&_info->waitTimeforSona) < 0) {
        BXlogPutLogMsg("[dseEIMCtrl] ERR: fpga_pinChangeToStart err.");
        return -1;
    }

#if DEBUG_INPUT
    fpga_makeInputExam(_info, _buf, _swBeam);
#else
    if (fpga_eimFPGA2MemCpy(_info, _buf, _fpga, _swBeam) < 0)
        return -1;
#endif

    return 0;
}

/**
@fn      fpga_pinChangeToStart(int32_t *_waitTime)
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA에게 데이터 취득 시작 신호를 보낸다.
@param   *_waitTime   : 데이터 취득 후 wait Time(현재 미사용).
@return  성공시 0. 실패 시, -1.
@warning _waitTime은 REALTIME 통신 모드(교차빔 모드)에서는 사용하지 않는다.
@bug     none
@todo    none
*/
static int32_t fpga_pinChangeToStart(int32_t *_waitTime)
{
    //--------DONE_OUT Low
    if( ioctl(gpiofd, IOCTL_DONE_SINAL_OFF, NULL) < 0 )
    {
        BXlogPutLogMsg("[dseEIMCtrl] ERR : IOCTL_DONE_SINAL_OFF");
        return -1;
    }

    //--------TRIGOUT High
    if( ioctl(gpiofd, IOCTL_TRIGER_TOGGLE, _waitTime) < 0 )
    {
        BXlogPutLogMsg("[dseEIMCtrl] ERR : IOCTL_TRIGER_TOGGLE");
        return -1;
    }

    return 0;
}

/**
@fn      fpga_eimFPGA2MemCpy(fpga_t *_info,
                             uint16_t *_dst, uint16_t *_src,
                             uint8_t _sw)
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA의 한 프레임 데이터를 read 버퍼에 copy한다.\n
         f_fifoRdy 신호가 들어올 때마다 정해진 개수 만큼 읽어서 한프레임을 저장을 완료한다.
@param   *_info : fpga 정보 구조체(fpga_t).
@param   *_dst  : read 버퍼 포인터.
@param   *_src  : fpga 가상 메모리 포인터.
@param   *_sw   : 교차빔 모드시, 교차빔 인덱스.
@return  성공시 0. 실패 시, -1.
@warning _dst에는 FPGA 가상 메모리 포인터. \n
         기본적인 memcpy 사용.
         한 프레임의 가장 첫 데이터를 읽기 전에 데이터를 1개 읽어서 쓰레기 처리해주어야 한다.
@bug     none
@todo    none
*/
static int32_t fpga_eimFPGA2MemCpy(fpga_t *_info,
                                  uint16_t *_dst, uint16_t *_src,
                                  uint8_t _sw)
{
    uint16_t tmp_trash[1];
    _Bool f_first = 1;

    if (methodFPGAComm == T_VER_REALTIME)
    {
        int32_t f_fifoRdy = 0;
        int32_t x = 0, y = 0;

        int32_t xIter = _info->w / FPGA_RDDATA_SIZE; //주의할 점은 FPGA_RDDATA_SIZE는 128의 배수여야.

        //memcpy(tmp_trash, _src , 1*sizeof(uint16_t));
        for (y = 0; y<_info->h; y++)
        {
            for(x=0; x<xIter; x++)
            {
                // 1개 읽어줘야함 지금은
                do{
                    if( ioctl(gpiofd, IOCTL_GET_FIFOCHECK, &f_fifoRdy) < 0 )
                    {
                        BXlogPutLogMsg("[dseEIMCtrl] ERR: fifo Rdy Read Failed...");
                        return -1;
                    }
                }while(f_fifoRdy==0);

                if (f_first)
                {
                    memcpy(tmp_trash, _src , 1*sizeof(uint16_t));
                    f_first = 0;
                }

                if (mode == MODE_3CH)
                {
                    if (_sw == 0)
                        memcpy( _dst + x*FPGA_RDDATA_SIZE + y*_info->w*2,
                                _src ,
                                FPGA_RDDATA_SIZE*sizeof(uint16_t) );
                    else
                        memcpy( _dst + (x+3)*FPGA_RDDATA_SIZE + y*_info->w*2,
                                _src ,
                                FPGA_RDDATA_SIZE*sizeof(uint16_t) );
                }
                else
                    memcpy( _dst + x*FPGA_RDDATA_SIZE + y*_info->w,
                            _src ,
                            FPGA_RDDATA_SIZE*sizeof(uint16_t) );
            }
        }
        //--------DONE_OUT High
        if( ioctl(gpiofd, IOCTL_DONE_SINAL_ON, NULL) <0 )
        {
            BXlogPutLogMsg("[dseEIMCtrl] Err : IOCTL_DONE_SINAL_ON");
            return -1;
        }
    }
    else if(methodFPGAComm == T_VER_FULLFIFO)
    {
        //--------wait kernel Timer
        //int8_t f_timer = 0;
        //do {
        //    if( ioctl(gpiofd, IOCTL_TIMER_INTERRUPT, &f_timer) < 0 )
        //    {
        //        BXlogPutLogMsg("[dseEIMCtrl] ERR : IOCTL_TIMER_INTERRUPT");
        //        return -1;
        //    }
        //}while(f_timer==0);//while(ret);

        //--------DONE_OUT High
        if( ioctl(gpiofd, IOCTL_DONE_SINAL_ON, NULL) <0 )
        {
            BXlogPutLogMsg("[dseEIMCtrl] Err : IOCTL_DONE_SINAL_ON");
            return -1;
        }

        memcpy(tmp_trash, _src , 1*sizeof(uint16_t));
        memcpy( _dst, _src , _info->h * _info->w *sizeof(uint16_t) );
    }
    return 0;
}

/**
@fn      fpga_makeInputExam(fpga_t *_info, uint16_t* _dst, uint8_t _sw)
@author  Bongsung Kim
@date    2017.04.24
@brief   테스트용 Input Raw Data를 생성한다.
@param   *_info : fpga 정보 구조체(fpga_t).
@param   *_dst  : read 버퍼 포인터.
@param   *_sw   : 교차빔 모드시, 교차빔 인덱스.
@return  none
@warning none
@bug     none
@todo    none
*/
static void fpga_makeInputExam(fpga_t *_info, uint16_t* _dst, uint8_t _sw)
{
    int32_t x,y;
    uint16_t grad = 0;

    switch (mode)
    {
    case MODE_2CH :
        for (y = 0; y<_info->h; y++)
        {
            for (x= 0; x<_info->w; x++)
            {
                _dst[y*_info->w + x] = grad;
            }
            if (grad == 3000)
                grad = 0;
            else
                grad += 20;
        }
        break;

    case MODE_3CH :
        if (_sw == 0)
        {
            for (y = 0; y<_info->h; y++)
            {
                for (x= 0; x<_info->w; x++)
                {
                    _dst[y*_info->w*2 + x] = grad;
                }
                if (grad == 3000)
                    grad = 0;
                else
                    grad += 20;
            }
        }
        else
        {
            for (y = 0; y<_info->h; y++)
            {
                for (x= 384; x<_info->w*2; x++)
                {
                    _dst[y*_info->w*2 + x] = grad;
                }
                if (grad == 3000)
                    grad = 0;
                else
                    grad += 20;
            }
        }
        break;
    }

}

/**
@fn      fpga_vacateFifo(uint16_t *_fpga)
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA Fifo full 신호를 체크하고 Done 신호를 떨어뜨린다.
@param   *_fpga : fpga 가상 메모리 포인터.
@return  성공시 0. 실패시 -1.
@warning 교차빔 모드(REAL_TIME)에서는 사용하지 않는다.
@bug     none
@todo    none
*/
static int32_t fpga_vacateFifo(uint16_t *_fpga)
{
    int32_t cntFifoCheck = 0;
    int32_t f_fifoCheck = 0;
    uint16_t t_trash[512];

    do
    {
        memcpy(t_trash, _fpga , 512*sizeof(uint16_t));

        if ( ioctl(gpiofd, IOCTL_GET_FIFOCHECK, &f_fifoCheck) < 0 )
        {
            BXlogPutLogMsg("[dseEIMCtrl] ERR : IOCTL_GET_FIFOCHECK");
            return -1;
        }
        //printf("%d\n", f_fifoCheck);
        if (cntFifoCheck==100)
        {
            BXlogPutLogMsg("[dseEIMCtrl] cnt_fifocheck 100!!!!!");
            break;
        }
        cntFifoCheck++;

    }while(f_fifoCheck);

    return 0;
}

/**
@fn      fpga_calcDataTotalHeight(int32_t _range)
@author  Bongsung Kim
@date    2017.04.24
@brief   _range를 이용하여 입력받는 전체 데이터의 height를 구한다.
@param   _range : 계산할 range(Meter)
@return  성공시 거리에 따른 raw data Height(data count)
@warning none
@bug     none
@todo    none
*/
int32_t fpga_calcDataTotalHeight(int32_t _range)
{
    return (int32_t)floor((_range+1.0) * METER_PER_PIXEL + 0.5);
}

/**
@fn      fpga_waitFPGAConditionVar()
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA Condition Variable 을 wait한다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
//static void fpga_waitFPGAConditionVar()
//{
//    pthread_mutex_lock(&mut_fpga);     //lock
//    pthread_cond_wait(&cond_fpga, &mut_fpga);
//    pthread_mutex_unlock(&mut_fpga);     //lock
//}

/**
@fn      fpga_sendSignalFPGAConditionVar()
@author  Bongsung Kim
@date    2017.04.24
@brief   FPGA Condition Variable 신호를 준다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
void fpga_sendSignalFPGAConditionVar()
{
    pthread_mutex_lock(&mut_fpga);     //lock
    pthread_cond_signal(&cond_fpga);
    pthread_mutex_unlock(&mut_fpga);     //unlock
}



