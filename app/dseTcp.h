/**
    @file       dseTcp.h
    @date       2017.04.25
    @author     Bongsung Kim
    @brief      다이버 소나 TCP 관련 인터페이스 헤더.
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#ifndef _DSETCP_H_
#define _DSETCP_H_

#define RECV_BUFFER_SIZE            100         ///> Receive Temp Buffer Size

/**
    @struct  SEND_CMD
    @date    2017.04.25
    @author  Bongsung Kim
    @brief   TCP Command 패킷 구조체
    @warning none
*/
typedef struct{
    uint32_t StartFlag;     ///< startFlag = 0xFFFFFFFE
    int32_t  PacketType;    ///< CMD Type
    int32_t  PacketLength;  ///< 4+4+4+4*7
    int32_t  Value[7];
} SEND_CMD;

/**
    @struct  SEND_DATA
    @date    2017.04.25
    @author  Bongsung Kim
    @brief   실시간 데이터 전송 시 프레임의 헤더 구조체
    @warning none
*/
typedef struct{
    uint32_t StartFlag     ;  ///< startFlag = 0xFFFFFFFE
    int32_t  PacketType    ;  ///< CMD_RAW_DATA
    int32_t  PacketLength  ;  ///< sizeof(SEND_DATA)
    int32_t  Width         ;  ///< channel count.
    int32_t  Height        ;  ///< data count.
    int32_t  FrameNum      ;
    int32_t  FrameSize     ;
    int32_t  RangeMeter    ;
    int32_t  gainMode      ;
    int32_t  voltage       ;
} SEND_DATA;

/** @brief Command 정의 */
enum
{
    CMD_START=0x00, ///< 실시간 데이터 전송 시작
    CMD_STOP,       ///< 실시간 데이터 전송 종료
    CMD_RAW_DATA,   ///< Raw Data 패킷.
    CMD_SET_TIME,   ///< RTC 시간 설정
    CMD_GET_TIME,   ///< RTC 시간 획득
    CMD_FORMAT,     ///< 저장 디스크 포맷 명령
    CMD_MAX,
    CMD_DSE,
    CMD_AC1
};

#endif
