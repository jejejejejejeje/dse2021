/**
    @file       dseRomTable.h
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      각종 Rom Table 정의 파일 헤더
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#ifndef _DSEROMTABLE_H_
#define _DSEROMTABLE_H_

/** [1000] 레이더의 addr map 생성시 레이더의 위치에 따른 x축 좌표 table. 0~127 */
extern unsigned char freq_consts[];
/** [128] 주파수 차이에 따라 y축으로 데이터를 땡기기 위한 테이블. */
extern unsigned char freq_offset[];
extern unsigned char freq_offset_under10m[];

extern unsigned short log_table[];      ///< 안씀.
/** 현재는 [5000] 로그 변환 곡선 Rom Table. 로그 변환 곡선 수식 변경시 배열 개수 달라질수 있음. */
extern unsigned short temp_log_table[];
/** [768] Color Table(R,G,B) */
extern unsigned char color_table[][3];
/** 한 레이더 페이지의 외곽을 강조하기 위한 게인. */
extern unsigned short u_curve[];

extern float table_100m[];

extern float power_table[];

extern float log_table_new[];

extern const uint8_t mantissa_table[4096];

#endif
