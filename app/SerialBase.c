#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "SerialBase.h"

static int32_t SerialBase_initPort(Serial_Setting_t* setting);

int32_t SerialBase_Create(Serial_Setting_t* setting)
{
    int32_t serial_fd;

    serial_fd = SerialBase_initPort(setting);

    return serial_fd;
}

void SerialBase_Destroy(void)
{
}

static int32_t SerialBase_initPort(Serial_Setting_t* setting)
{
    struct termios newtio;
    int32_t serial_fd;

    serial_fd = open( (const char*)(setting->SERIAL_FD), O_RDWR | O_NOCTTY);
    // printf("%d\n", errno);
    if ( serial_fd < 0 ) {
        printf( "[SerialBase_initPort] Serial port [%s] fail. !!\r\n", setting->SERIAL_FD);
        return -1;
    }
    printf("[SerialBase_initPort] %s serial = %d baud = %ld\n", setting->SERIAL_FD, serial_fd, setting->BAUDRATE);

    memset( &newtio, 0, sizeof(newtio) );

    newtio.c_cflag     = setting->BAUDRATE | setting->DATABITS | setting->PARITYON | setting->PARITY | setting->LOCAL;
    newtio.c_iflag     = IGNPAR; //| ICRNL;
    newtio.c_oflag     = 0;
    newtio.c_lflag     = setting->ICANON_ONOFF;// 캐노니컬 모드로 받는다(캐노니컬 모드: 얼마나 받던 한줄씩(\0) 처리)
    newtio.c_cc[VMIN]  = 255;     // MIN은 read가 리턴되기 위한 최소한의 문자 개수
    newtio.c_cc[VTIME] = 1;     // time-out 값으로 사용. time-out 값은 TIME*0.1초.
    tcflush(serial_fd, TCIFLUSH);
    tcsetattr(serial_fd, TCSANOW, &newtio);

    return serial_fd;
}

SerialBuffer_t seri_GPSdata;
SerialBuffer_t* SerialBase_GPSRead(int32_t* fd)
{
    memset(seri_GPSdata._buffer, 0, BUFFER_SIZE);

    seri_GPSdata._size = read(*fd, seri_GPSdata._buffer, BUFFER_SIZE);
    // printf("[Base] read: %d// %s\n", seri_GPSdata._size, seri_GPSdata._buffer);
            // printf("%ld\n", strlen(seri_GPSdata._buffer));

    return &seri_GPSdata;
}

/*SerialBuffer_t seri_SERI2data;
SerialBuffer_t* SerialBase_SERI2Read(int32_t* fd)
{
    memset(seri_SERI2data._buffer, 0, BUFFER_SIZE);

    seri_SERI2data._size = read(*fd, seri_SERI2data._buffer, BUFFER_SIZE);
    // printf("[Base] read: %d// %s\n", seri_SERI2data._size, seri_SERI2data._buffer);
            // printf("%ld\n", strlen(seri_SERI2data._buffer));

    return &seri_SERI2data;
}*/