/**
    @file       dseMain.h
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 메인 헤더파일
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#ifndef _DSEMAIN_H_
#define _DSEMAIN_H_

#include <linux/rtc.h>
//---------------------------------
//#define 전처리기
//---------------------------------

#define DSE2021_VERSION          "1.0.0" /**< DSE2021 버전 */
//--------------------------------------------------// #if Definition
#define IMX6Q_TINYREX        1  /**< Tinyrex 버전 식별자 */

#define AUTO_SHUTDOWNEN      1  /**< 입력 없을시에 자동종료 기능 on/off 처리기 */
#define AUTO_SHUTDOWNTIME    5*60 /**< 자동종료 동작 대기 시간 */
#define DEBUG_INPUT          0  /**< Debuging ONOFF 1이면 input을 임의로 만든다 */
//#define LOGO_DISPLAY         1  /**< LOGO Display 유무 */ 20170724부터 로고유무는 실행시 옵션값 4로 설정
#define STORAGE2MAIN         0   /**<메인화면에 저장용량표기 유무*/
#define DISPLAY_FPS          0  /**< FPS 디스플레이 유무 */
#define START_KEY_INT        1  /**< 첫화면 사용 유무. 미사용시 첫화면 없이 넘어감. */
#define EN_BATT_CHECK_I2C    1  /**< i2c를 이용한 배터리 체크 사용 유무 */
#define EN_PRESS_CHECK_I2C   1  /**< i2c를 이용한 압력 및 수온 체크 사용 유무 */
#define TVG_EN               0  /**< TVG 사용 유무. 현재 사용안함. */
#define T_VER_FULLFIFO       1  /**< FPGA 통신 방식. FPGA에서 한프레임을 모두 FIFO에 저장하는 방식 */
#define T_VER_REALTIME       2  /**< FPGA 통신 방식. FPGA에서 prog_full을 알려주면 일정 개수 읽어가는 방식 */

//--------------------------------------------------// SonarView 관련
#define DRAWWIDTH            1280   /**< Display의 가로 해상도 */
#define DRAWHEIGHT           720    /**< Display의 세로 해상도 */

//--------------------------------------------------// 입력 버퍼의 최대 해상도 정의
#define MAXHEIGHT            3000   /**< FPGA에게 받는 RAWData 저장 버퍼 할당을 위한 임의의 최대 라인수 */

//--------------------------------------------------// DSE Channel Mode
#define MODE_2CH        0           /**< 초음파 센서 2ch의 define */
#define MODE_3CH        1           /**< 초음파 센서 3ch의 define */
#define ONEPAGE_SAMPLES 128         /**< 한 레이더 페이지의 가로 데이터 개수 */

//--------------------------------------------------// Range 값 범위 정의
#define MIN_RANGE            5      /**< 최소 range 값 */
#define RANGE_UNIT_UNDER30M  5      /**< 버튼 입력시 증가하는 range 단위 */
#define RANGE_UNIT_OVER30M   10     /**< range 30M 이상부터 증가하는 range 단위 */
#define RANGE_UNIT_OVER50M   25     /**< range 50M 이상부터 증가하는 range 단위 */

//--------------------------------------------------// Range 값 범위 정의
#define GAIN_LOW    2             /**< Gain Low    1.0 */
#define GAIN_LM     3             /**< Gain Low    2.0 */
#define GAIN_MIDDLE 5             /**< Gain Middle 3.0 */
#define GAIN_HM     7             /**< Gain Low    5.0 */
#define GAIN_HIGH   9             /**< Gain Hight  7.0 */
#define GAIN_AUTO   GAIN_HIGH + 1 /**< Auto Gain */

//--------------------------------------------------// Save 관련 Define
//#define FILE_NAME_EXTENTION  ".ac1"
//#define FILE_NAME_EXTENTION  ".dse"
#define FILE_EXTENTION_DSE   ".dse" /**< 저장 파일 확장자 .dse */
#define FILE_EXTENTION_AC1   ".ac1" /**< 저장 파일 확장자 .ac1 */
#define FILE_EXTENT_MODE_DSE 0      /**< .dse 확장자 저장 모드 */
#define FILE_EXTENT_MODE_AC1 1      /**< .ac1 확장자 저장 모드 */
#define SAVE_WAIT            0      /**< save State. 대기상태 */
#define SAVE_START           1      /**< save State. 저장상태 */
#define SAVE_STOP            2      /**< save State. 저장 종료 입력 상태 */
#define ERR_SAVEERR         -1      /**< save Err.  */
#define ERR_DISKSIZE_FULL   -2      /**< save Err. disk가 꽉 찼음을 알림 */

//--------------------------------------------------// 배터리 측정관련 정의
#define MAX_BATT_AVER_NUM     5             /**< 배터리 전압값의 이동평균값 계산을 위한 이동평균 데이터 개수 */
#define I2C_DEV_NAME         "/dev/i2c-2"   /**< LTC4151이 연결된 i2c의 디바이스 드라이버 경로 */
#define LTC_I2C_ADDR         0x6f           /**< LTC4151의 i2c ID */
#define PRESS_I2C_ADDR       0x28           /**< 수압수온센서의 i2c ID */
#define I2C_MOTION_DEV_NAME  "/dev/i2c-1"   /**< 모션센서에 연결된 i2c의 디바이스 드라이버 경로 */
#define MOTION_I2C_ADDR      0x28           /**< 모션센서의 i2c ID */

#define RTC0_DEV             "/dev/rtc0"    /**< RTC의 디바이스 드라이버 경로 */

//--------------------------------------------------// User Driver 관련
                                                    // 사용하는 driver name 정의
#define KEYDEVNAME      "/dev/keys"         /**< Key 관련 디바이스 드라이버 경로 */
#define GPIODEVNAME     "/dev/GPIO_to_FPGA" /**< GPIO 관련 디바이스 드라이버 경로 */
#define FRAMEBUFFER_DEV "/dev/fb0"          /**< FrameBuffer 관련 디바이스 드라이버 경로 */

                                                    //keys ioctl 메세지
#define IOCTL_MAGIC_GPIO        'g' /**< GPIO 드라이버의 Magic Number */
#define IOCTL_MAGIC_KEYS        'h' /**< Keys 드라이버의 Magic Number */
#define IOCTL_SW_CMD_GET        _IOR(IOCTL_MAGIC_KEYS, 0, int)          /**< 어떤 스위치가 push됬는지 확인 */
#define IOCTL_WAIT_FIRST        _IO(IOCTL_MAGIC_KEYS, 1)                /**< 첫화면에서 스위치 입력시까지 드라이버단에서 block. (현재 미구현) */
                                                    //GPIO ioctl 메세지
#define IOCTL_TRIGER_TOGGLE     _IOW(IOCTL_MAGIC_GPIO, 0, int)          /**< 한 프레임 Data 취득 트리거 토글 */
#define IOCTL_DONE_SINAL_ON     _IO (IOCTL_MAGIC_GPIO, 1)               /**< 한 프레임 Data 취득 끝(Done) 알림*/
#define IOCTL_TIMER_INTERRUPT   _IOR(IOCTL_MAGIC_GPIO, 2, unsigned char)/**< 미사용*/
#define IOCTL_GET_FIFOCHECK     _IOR(IOCTL_MAGIC_GPIO, 3, unsigned int) /**< Fifo Full 신호 polling*/
#define IOCTL_DONE_SINAL_OFF    _IO (IOCTL_MAGIC_GPIO, 4)               /**< 한 프레임 Data 취득 끝(Done) 초기화(0)*/
#define IOCTL_MODE_SELECT       _IOW(IOCTL_MAGIC_GPIO, 5, unsigned char)/**< 2ch, 3ch 모드를 FPGA에 알림(FPGA 미구현)*/
#define IOCTL_CHANNELSWITCH     _IOW(IOCTL_MAGIC_GPIO, 6, unsigned char)/**< 3ch 모드에서 교차빔 방법 사용시, 빔 교차시키는 명령.*/
#define IOCTL_TXPWRON           _IO (IOCTL_MAGIC_GPIO, 7)               /**< TX Power On. */
#define IOCTL_TXPWROFF          _IO (IOCTL_MAGIC_GPIO, 8)               /**< TX Power Off. */
#define IOCTL_KILL              _IO (IOCTL_MAGIC_GPIO, 9)               /**< 시스템의 전원을 완전히 내림. */
#define IOCTL_GAIN_SEL          _IO (IOCTL_MAGIC_GPIO, 10)				///< HW GAIN SELECT
#define IOCTL_PWD_SEL           _IO (IOCTL_MAGIC_GPIO, 11)				///< HW PWD SELECT
#define IOCTL_ONOFF_ON          _IO (IOCTL_MAGIC_GPIO, 12)
#define IOCTL_ONOFF_OFF         _IO (IOCTL_MAGIC_GPIO, 13)

                                                    //keys 에서 signal 받았을때 처리할 sw CMD
#define SW_CMD_RANGE    1 /**< Range 조절 스위치 입력 상태 */
#define SW_CMD_GAIN     2 /**< Gain 조절 스위치 입력 상태 */
#define SW_CMD_SAVE     3 /**< save 시작 및 종료 버튼 입력 상태 */
#define SW_CMD_FIRST    4 /**< 첫화면 넘어가기 위한 스위치 입력 상태 */
#define SW_CMD_CHANGE   5

//--------------------------------------------------// LED Directory
#define LED1_DIR "/sys/class/leds/LED1" /**< gpio-led 1 파일 경로 */
#define LED2_DIR "/sys/class/leds/LED2" /**< gpio-led 2 파일 경로 */

#define SHUTDOWN_BATT_PERCENT 1.0       /**< 강제 종료시의 배터리 퍼센트. */
#define SHUTDOWN_BATT_VOLTAGE 9.5       /**< 강제 종료시의 배터리 전압. */
//---------------------------------
// 전역 변수들
//---------------------------------
extern int32_t methodFPGAComm;        ///< FPGA 통신 방법 정의. 기본값 REALTIME
extern int32_t cpuVer;                ///< CPU 보드 버전. 기본값 tinyrex
extern _Bool en_RTC;                  ///< RTC 사용 유무
extern char diskName[17];             ///< 저장 파일 저장 경로
extern char diskDeviceName[15];       ///< 저장 디스크의 디바이스 드라이버 경로
extern int logo;
extern int full_flag;
extern float glob_gain;
extern _Bool           firstStartSig; ///< 첫화면에서 넘어가는 flag

int32_t                keysfd;        ///< keys 드라이버 file descriptor
int32_t                gpiofd;        ///< gpio 드라이버 file descriptor

                                //Screen Buffer
extern uint8_t        *buf_screen;    ///< framebuffer 실제 픽셀 데이터 버퍼

                                //thread
extern pthread_cond_t  cond_start;    ///< 첫화면을 벗어났다는 알림을 위한 조건 변수
extern pthread_mutex_t mut_start;     ///< 첫화면을 벗어났다는 알림을 위한 mutex
// extern pthread_cond_t  cond_adc;
// extern pthread_mutex_t mut_adc;
extern pthread_t       thr_draw;      ///< draw Thread
extern pthread_t       thr_fpga;      ///< FPGA 통신 Thread
extern pthread_t       thr_tcp ;      ///< TCP 통신 Thread
extern pthread_t       thr_save;      ///< File Save Thread
extern pthread_t       thr_seri_gps;
//extern pthread_t       thr_seri2;
extern pthread_cond_t  cond_draw;     ///< draw Thread와 다른 thread간 동기화를 위한 조건 변수
extern pthread_mutex_t mut_draw ;     ///< draw Thread와 다른 thread간 동기화를 위한 mutex
extern pthread_cond_t  cond_fpga;     ///< FPGA Thread와 다른 thread간 동기화를 위한 조건 변수
extern pthread_mutex_t mut_fpga ;     ///< FPGA Thread와 다른 thread간 동기화를 위한 mutex
extern pthread_cond_t  cond_save;     ///< Save Thread와 다른 thread간 동기화를 위한 조건 변수
extern pthread_mutex_t mut_save ;     ///< Save Thread와 다른 thread간 동기화를 위한 mutex
extern pthread_cond_t  cond_tcp ;     ///< TCP Thread와 다른 thread간 동기화를 위한 조건 변수
extern pthread_mutex_t mut_tcp  ;     ///< TCP Thread와 다른 thread간 동기화를 위한 mutex

                                //sonar View
extern uint16_t       *RAWdata1;        ///< RAW Data Buffer 1 (Double Buffering)
extern uint16_t       *RAWdata2;        ///< RAW Data Buffer 2 (Double Buffering)
extern int8_t          f_bufSwitching;  ///< Double Buffering의 버퍼 인덱스
extern uint8_t         mode;            ///< 2ch인지 3ch인지 mode설정. 2ch-0 3ch-1
extern int32_t         drawYZ;          ///< 레이더의 Y축 Zero Point Y좌표.(Display 좌상단이 (0,0))
extern int32_t         radius;          ///< 레이더의 반지름. 0M 지점부터 최외각까지의 거리. (단위 픽셀 개수)
extern int32_t         fpgaSampleCount; ///< RawData의 channel Count(Width, 한 라인의 샘플 개수)
extern uint8_t         t_range ;        ///< 버튼 입력으로 변경된 range의 값
extern uint8_t         maxRange;        ///< range 최대값
extern uint8_t         gainMode;        ///< 현재 gain Mode
extern uint8_t         f_gainOrRange;   ///< Gain이나 range change flag. 0-No Change 1-gain Change 2-Range Change
extern uint8_t         f_change;

extern float           gainOffset  ;    ///< Gain Offset. 현재 0
extern int8_t          logTableEn  ;    ///< 로그 변환 곡선 RomTable 사용 유무. 1-RomTable 사용. 0-직접 계산
extern uint32_t        threshold   ;    ///< Threashold 이하의 raw data 값은 0으로 만듬.
extern float           VA_LIMIT_LOG;    ///< 로그 변환 곡선 수식 인자1. 로그 곡선이 적용되는 최대 data 값.
extern float           VA_UP       ;    ///< 로그 변환 곡선 수식 인자2. 로그 곡선의 기울기에 관여.
extern float           VA_DOWN     ;    ///< 로그 변환 곡선 수식 인자3. 로그 곡선의 기울기에 관여.
extern float           VA_MAX_X    ;    ///< 로그 변환 곡선 수식 인자4. 로그 곡선 이후로 일차 변환 곡선이 적용되는 최대 data 값.
extern float           DATA_MAX    ;    ///< 0~65536의 Raw Data를 Mapping할 최대 값. Color Table의 개수

extern int8_t          f_save      ;      ///< Save State
extern float           diskSize    ;      ///< 파일 저장 디스크의 크기
extern float           diskAvailableSize; ///< 파일 저장 디스크의 가용 크기
extern int32_t         fileExtentMode;    ///< 파일 저장 모드 state. .dse: 0  .ac1: 1
extern int32_t         fileExtentByte;    ///< 저장 파일의 데이터 하나의 Bytes 개수.

extern uint8_t         en_i2cBattCheck;   ///< Battery Check 모드 사용 유무.
extern uint8_t         en_i2cPressCheck;  ///< 수압,수온 Check 모드 사용 유무.
extern uint8_t         en_i2cMotionCheck; ///< 모션 Check 모드 사용 유무.

                                //time 관련
extern float           fps      ;         ///< Display 영상 Frame Per Second
extern float           frameRate;         ///< Display 영상의 한 프레임 주사 속도

extern uint8_t         f_callAvailableSaveTime; ///< 저장 가능 시간 측정 Enable Flag
extern uint8_t         f_drawSaveFileName;      ///< Save File Name Display Enable Flag(현재 미사용)

                                //tcp 관련
extern int8_t          f_startTcp;              ///< TCP 실시간 주사 모드 Start Flag
extern int8_t          freq_correction;

//---------------------------------
//전역 함수들
//---------------------------------
                                                //-- dseMain.c
extern int32_t main_getBattState();         ///< Battery의 State(0~6)을 반환
extern float   main_getBattPercent();       ///< Battery의 percentage를 반환
extern float   main_getBattVoltage();       ///< Battery의 average Voltage를 반환
extern float   main_getWaterDepth();        ///< 측정 수심을 반환
extern float   main_getTemp();              ///< 측정 수온을  반환
extern _Bool   main_getIsDinving();         ///< Diving 유무를 반환
extern int32_t main_getDTSec();             ///< Diving 시간의 초를 반환
extern int32_t main_getDTMin();             ///< Diving 시간의 분을 반환
extern float   main_getHeading();           ///< 측정 헤딩 값을  반환
extern float   main_getPitch();             ///< 측정 피치 값을  반환
extern float   main_getRoll();              ///< 측정 롤 값을  반환
extern int32_t main_checkFTPOperation();    ///< FTP 접속자가 있는지 확인하여 결과를 반환
extern int32_t main_checkTCPOperation();    ///< TCP 접속자가 있는지 확인하여 결과를 반환
extern int32_t main_killSystem();           ///< System을 완전히 종료
//extern void    main_sendSignalADCConditionVar();
                                                //-- dseSignal.c
extern int32_t sig_sigFuncInit(pid_t userPid);      ///< Signal Control Function Initialize
                                                //-- dseDraw.c
extern void   *draw_drawThread();                   ///< Display Thread 함수.
extern void    draw_sendSignalDrawConditionVar();   ///< Draw Thread에 pthread_cond_signal 신호를 인가한다.
extern void    draw_waitSignalStartConditionVar();  ///< 첫 화면을 넘어기기 위해 cond_start의 신호를 기다린다.
                                                //-- dseEIMCtrl.c
extern void   *fpga_fpgaThread();                           ///< FPGA 통신 Thread
extern int32_t fpga_calcDataTotalHeight(int32_t _range);    ///< range에 따른 raw data height 계산 함수.
extern void    fpga_sendSignalFPGAConditionVar();           ///< FPGA 조건변수 신호 전달 함수
                                                //-- dseSave.c
extern void    *save_saveThread();                  ///< Save Thread 함수.
extern void     save_sendSignalSaveConditionVar();  ///< Draw Condition Variable 신호를 준다.
extern int32_t  save_saveFileInit();                ///< dse Save 용량 초과 시 파일 디스크립터 종료 후 다시 파일 저장을 시작
extern char    *save_getSaveFileName();             ///< save FileName을 반환
extern uint32_t save_getSaveMin();                  ///< save Time Minute을 반환
extern uint32_t save_getSaveSec();                  ///< save Time Minute을 반환
extern uint32_t save_getAvailableSaveMin();         ///< 저장 가능한 minute을 반환
extern uint32_t save_getAvailableSaveSec();         ///< 저장 가능한 second을 반환
extern uint32_t save_calcAvailableSaveTime();       ///< 현재 상태에서 녹화 가능한 시간을 구한다.
extern uint32_t save_getSaveDiskStorage();          ///< save disk의 남은 용량을 구한다. 전역변수인 diskAvailableSize와 diskSize에 저장.
                                                //-- dseTime.c
extern int32_t  time_getLinuxSecond();              ///< 시스템 시간을 로딩해서 초만 반환.
extern int32_t  time_setTime(int32_t *_timeInfo);   ///< 전체 시간 세팅 및 표준 시간대 변경
extern int32_t  time_rtcGetTime(struct rtc_time *rtc_time_info); ///> 현재 rtc 시간을 불러옴.
extern int32_t  time_getProcessClock();             ///< 프로세스 시간을 ms 단위로 반환
                                                //-- dseTcp.c
extern void    *tcp_tcpThread();                    ///< TCP 처리 thread 함수
extern void     tcp_sendSignalTCPConditionVar();   ///< TCP Condition Variable 신호를 준다.

extern struct rtc_time t_rtc;
#endif
