/**
    @file       dseSave.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 저장 관련 인터페이스
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#include <unistd.h>         // open() close() write() read() lseek()
#include <stdint.h>         // int32_t 등 library
#include <fcntl.h>          // O_RDWR
#include <stdlib.h>         //malloc atoi
#include <stdio.h>          //FILE
#include <pthread.h>
#include <linux/rtc.h>
#include <math.h>
#include <string.h>

#include <BXlog.h>

#include <sys/time.h>
#include <time.h>
#include "dseMain.h"
#include "dseSave.h"
#include "dseRomTable.h"
#include "dseSonarView2D.h"

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
void   *save_saveThread();
int32_t save_saveFileInit();
int32_t save_saveFileClose(_saveFileInfo *info);

char    *save_getSaveFileName();
uint32_t save_getSaveMin();
uint32_t save_getSaveSec();
uint32_t save_getAvailableSaveMin();
uint32_t save_getAvailableSaveSec();
uint32_t save_calcAvailableSaveTime();
uint32_t save_getSaveDiskStorage();
static uint32_t save_calcOneFrameSize();

static int32_t  save_frameSaveOper(void *_dst, uint16_t *_src);
static void     save_arrangeLineAC1Format(uint16_t *_dst, uint16_t *_src, int32_t _indY, int32_t _ch);
static void     save_arrangeLineDSEFormat(uint8_t *_dst, uint16_t *_src, int32_t _indY, int32_t _ch, int32_t _realH, float gain, int16_t pffrange);
static int32_t  save_confirmSaveState();
static int32_t  save_refreshSaveTime(int32_t _prev);
static int32_t  save_reInitSave();

static void save_makeFileName(char* _str);

static void     save_waitSaveConditionVar();
void            save_sendSignalSaveConditionVar();
///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////

////////////////global Variable////////////////////////////

struct timespec curtime;
struct timespec starttime;

pthread_t       thr_save;                               ///< File Save Thread
pthread_cond_t  cond_save = PTHREAD_COND_INITIALIZER;   ///< Save Thread와 다른 thread간 동기화를 위한 조건 변수
pthread_mutex_t mut_save  = PTHREAD_MUTEX_INITIALIZER;  ///< Save Thread와 다른 thread간 동기화를 위한 mutex

int8_t        f_save = SAVE_WAIT;                       ///< Save State
_saveFileInfo saveInfo;                                 ///< save File 정보 구조체
dseSaveHeader saveHeader;                               ///< save File header 구조체

uint8_t      *save_buf_dse = NULL;  ///< 파일을 저장하기 위한 임시 버퍼(.dse용)
uint16_t     *save_buf_ac1 = NULL;  ///< 파일을 저장하기 위한 임시 버퍼(.ac1용)
uint32_t      fileFrameCnt = 0;     ///< file Frame Count
uint8_t      *saveBMP_buf;          ///< BMP 파일 버퍼.

float         diskSize         = 0;     ///< 파일 저장 디스크의 크기
float         diskAvailableSize= 0;     ///< 파일 저장 디스크의 가용 크기

sv2D_t *svin;             ///< Sonar View 관련 구조체

float expectFps[] = {15.5,18.2,12.8,12.6,12,11.1,0,10.2,0,8.55,0,0,0,0,9.57,0,0,0,0,9.6}; ///< range 별 예상 FPS(5,10,15,20,25,30,35,40,45,50)
//float expectFps[10] = {14,13.8,13.7,13.7,13.5,13.4,13.4,13.2,13.2,11.5}; ///< range별 측정된 FPS
//float expectFps[10] = {20,19,18,16,15,14,13,12,11,10}; ///< range 별 예상 FPS(5,10,15,20,25,30,35,40,45,50) 백업용
int32_t fileExtentMode = FILE_EXTENT_MODE_AC1;      ///< 파일 저장 모드 state. .dse: 0  .ac1: 1
int32_t fileExtentByte = sizeof(uint16_t);           ///< 저장 파일의 데이터 하나의 Bytes 개수.

////////////압축기법 추가/////////////////////
// int size_frameData = 256*LIMIT_SAVEHEIGHT;
// typedef unsigned char byte;
// typedef uint32_t _int;
// typedef struct
// {
//     _int next[256];
// } lzw_enc_t;

// void write_bits(_int x, _int *tmp, int *bits, int *o_bits, int *outlen, byte *out, int *en_outlen);
// void lzw_encode(byte *in, int max_bits, int len, int *en_outlen);
// lzw_enc_t de[90000];
// byte encoding_out[256 * (LIMIT_SAVEHEIGHT+1)];

// void write_bits(_int x, _int *tmp, int *bits, int *o_bits, int *outlen, byte *out, int *en_outlen)
// {
//     *tmp = (*tmp << *bits) | x;
//     *o_bits += *bits;
//     while (*o_bits >= 8)
//     {
//         *o_bits -= 8;
//         out[(*outlen)++] = *tmp >> *o_bits;
//         *tmp &= (1 << *o_bits) - 1;
//         (*en_outlen)++;
//     }
// }
// void lzw_encode(byte *in, int max_bits, int len, int *en_outlen)
// {
//     int i;
//     int bits = 9, next_shift = 512;
//     _int code, c, nc, next_code = M_CLR;
//     int n_item = M_CLR + len;
//     //de = (lzw_enc_t *)malloc();


//     n_item =len;
//     int outlen = 0, o_bits = 0;
//     _int tmp = 0;
//     len += 2;

//     for (code = *(in++); --len; )
//     {
//         c = *(in++);
//         if (nc = de[code].next[c])
//             code = nc;
//         else
//         {
//             write_bits(code, &tmp, &bits, &o_bits, &outlen, encoding_out, en_outlen);
//             de[code].next[c] = next_code++;
//             code = c;
//         }
//         if (next_code == next_shift)
//             ++bits, next_shift *= 2;
//     }
// }
////////////////global Variable////////////////////////////

/**
@fn      save_saveThread()
@author  Bongsung Kim
@date    2017.04.24
@brief   Save Thread 함수.
@param   none
@return  0
@warning 저장 버튼 입력시 생성.
@bug     none
@todo    none
*/
void *save_saveThread()
{
    int32_t prevSec = 0; //저장 시간을 알기 위한 이전 초
    int32_t t_saveIndexOffset = 0;

                                                //save buffer 할당
    if (fileExtentMode == FILE_EXTENT_MODE_DSE)
        save_buf_dse = (uint8_t *)malloc(MAXHEIGHT*fpgaSampleCount*fileExtentByte);
    else if (fileExtentMode == FILE_EXTENT_MODE_AC1)
        save_buf_ac1 = (uint16_t *)malloc(MAXHEIGHT*fpgaSampleCount*fileExtentByte);

    while(1)
    {
                                                //save close & stop
        if (save_confirmSaveState())
            break;

    //Wait Save Signal
    save_waitSaveConditionVar();
                                                //저장할 버퍼 선택 flag 받고 한프레임 save
        t_saveIndexOffset = f_bufSwitching;
        if (!t_saveIndexOffset)
        {
            if     (fileExtentMode == FILE_EXTENT_MODE_DSE)
                save_frameSaveOper((void *)save_buf_dse, RAWdata1);
            else if(fileExtentMode == FILE_EXTENT_MODE_AC1)
                save_frameSaveOper((void *)save_buf_ac1, RAWdata1);
        }
        else
        {
            if     (fileExtentMode == FILE_EXTENT_MODE_DSE)
                save_frameSaveOper((void *)save_buf_dse, RAWdata2);
            else if(fileExtentMode == FILE_EXTENT_MODE_AC1)
                save_frameSaveOper((void *)save_buf_ac1, RAWdata2);
        }
        //printf("[dseSave] offset = %d\n", t_saveIndexOffset);

                                                //reflash Save Time
        prevSec = save_refreshSaveTime(prevSec);

    pthread_mutex_unlock(&mut_save);     //unlock
        // memset(de, 0, sizeof(de));
    }
    if (save_buf_dse != NULL)
        free(save_buf_dse);
    if (save_buf_ac1 != NULL)
        free(save_buf_ac1);

    return (void *)0;
}

/**
@fn      save_frameSaveOper(void *_dst, uint16_t *_src)
@author  Jinmo Je
@date    2021.06.14
@brief   한 프레임 save 동작 함수. 데이터 정렬 후, write 한다.
@param   *_dst : save 버퍼.
@param   *_src : save 할 프레임이 저장된 버퍼.
@return  성공 시, 0. 실패 시, -1.
@warning fileExtentMode에 따라 파일 포맷 다르게 저장.
@bug     none
@todo    none
*/
static int32_t save_frameSaveOper(void *_dst, uint16_t *_src)
{
    float   save_gain  = gainMode;

    //int32_t saveHeight = (int)floor((t_range+1.0) * 39.37 + 0.5);
    int32_t saveHeight;
    int32_t realHeight = fpga_calcDataTotalHeight((int32_t)t_range);
    // if ((realHeight > LIMIT_SAVEHEIGHT) && (fileExtentMode == FILE_EXTENT_MODE_DSE))
    //     saveHeight = LIMIT_SAVEHEIGHT;
    // else
        saveHeight = realHeight;

    saveHeader.start_packet  = 0xFFFFFFFE;
    saveHeader.packet_length = 0;
    saveHeader.version       = 242;                 ///< 143: Gain,PWD 조정전 버전, 242: Gain, PWD 조정 후 버전
    saveHeader.id            = 0;
    saveHeader.interval      = (int)frameRate*1000;
    saveHeader.burst_no      = fileFrameCnt;
    saveHeader.range         = t_range;
    saveHeader.gain          = save_gain;
    saveHeader.data_count    = saveHeight;
    saveHeader.ch_count      = fpgaSampleCount;
    saveHeader.year          = t_rtc.tm_year-2000;
    saveHeader.month         = t_rtc.tm_mon;
    saveHeader.day           = t_rtc.tm_mday;
    saveHeader.hour          = t_rtc.tm_hour;
    saveHeader.min           = t_rtc.tm_min;
    saveHeader.sec           = t_rtc.tm_sec;
    saveHeader.depth         = main_getWaterDepth();
    saveHeader.temp          = main_getTemp();
    saveHeader.dtMin         = main_getDTMin();
    saveHeader.dtSec         = main_getDTSec();
    saveHeader.isDiving      = (int32_t)main_getIsDinving();
    saveHeader.sensor_id     = 170710;
    fileFrameCnt++;

    //fwrite((char*)&header, sizeof(header), 1, saveFileInfo.fd);
    //fwrite((char*)input, inputHeight*fpgaSampleCount*sizeof(uint16_t), 1, saveFileInfo.fd);

    int y;
    int x;
    // int en_outlen = 0, dec_outlen = 0;

    if (fileExtentMode == FILE_EXTENT_MODE_DSE)
    {
        for ( y = 0; y<saveHeight; y++)
        {
            //256이냐 768이냐에 따라 저장 다르게 함
            save_arrangeLineDSEFormat((uint8_t *)_dst, _src, y, fpgaSampleCount, realHeight, glob_gain, t_range);
        }
        // en_outlen = 0;
        // if(saveHeight != realHeight)
        // {
        //     lzw_encode((byte *)_dst, 9, size_frameData, &en_outlen);
        // }
        // else
        // {
        //     lzw_encode((byte *)_dst, 9, saveHeight, &en_outlen);
        // }
        // memcpy(_dst, encoding_out, en_outlen);
        // saveHeader.en_outlen = en_outlen;
    }
    else if (fileExtentMode == FILE_EXTENT_MODE_AC1)
    {
        for ( y = 0; y<saveHeight; y++)
        {
            //256이냐 768이냐에 따라 저장 다르게 함
            save_arrangeLineAC1Format((uint16_t *)_dst, _src, y, fpgaSampleCount);
        }
    }
    if (write(saveInfo.fd, (char*)&saveHeader, sizeof(saveHeader)) < 0)
    {
        BXlogPutLogMsg("[dseSave] filesave Failed1. Maybe disk full...");
        save_reInitSave();
        return -1;
    }
    if (write(saveInfo.fd, (char*)_dst, fpgaSampleCount*saveHeight*fileExtentByte) < 0)
    {
        BXlogPutLogMsg("[dseSave] filesave Failed2. Maybe disk full...");
        save_reInitSave();
        return -1;
    }

    uint32_t fileEndChar = 0xA5A5A5A5;
    //fwrite((char*)&fileEndChar, 4, 1, saveFileInfo.fd);
    if (write(saveInfo.fd, (char*)&fileEndChar, 4) < 0)
    {
        BXlogPutLogMsg("[dseSave] filesave Failed3. Maybe disk full...");
        save_reInitSave();
        return -1;
    }
            //printf("%d \n",saveHeader.en_outlen);
    fsync(saveInfo.fd);
    return 0;
}

/**
@fn      save_arrangeLineAC1Format(uint16_t *_dst, uint16_t *_src, int32_t _indY, int32_t _ch)
@author  Bongsung Kim
@date    2017.04.24
@brief   한 라인 데이터를 저장 AC2포맷에 맞춰서 정렬함.
@param   *_dst : save 버퍼.
@param   *_src : save 할 프레임이 저장된 버퍼.
@param   _indY : 저장 할 라인 넘버.(height, data count)
@param   _ch  : 한 라인의 채널(width) 개수. 256, 768
@return  성공 시, 0. 실패 시, -1.
@warning fileExtentMode에 따라 파일 포맷 다르게 저장.
@bug     none
@todo    none
*/
static void save_arrangeLineAC1Format(uint16_t *_dst, uint16_t *_src, int32_t _indY, int32_t _ch)
{
    int32_t x = 0;

    if (_ch == 256)
    {
        memcpy(_dst + _indY*_ch, _src + _indY*_ch,
               ONEPAGE_SAMPLES*sizeof(uint16_t));
        for ( x = ONEPAGE_SAMPLES; x < ONEPAGE_SAMPLES*2; x++)
        {
            _dst[_indY*_ch + x] =
                _src[_indY*_ch + ((ONEPAGE_SAMPLES*2-1) - (x - ONEPAGE_SAMPLES))];
        }
    }
    else if (_ch == 768)
    {
        //4번 레이더 - not 2번 데이터
        for (x = 0; x<128; x++)
        {
            _dst[_indY*_ch + x] = _src[_indY*_ch + (383-(x-  0))];
        }
        //2번 레이더 - not 1번 데이터
        for (x = 128; x<256; x++)
        {
            _dst[_indY*_ch + x] = _src[_indY*_ch + (255-(x-128))];
        }
        //0번 레이더 - 3번 데이터
        memcpy((_dst + _indY*_ch) + 256, (_src + _indY*_ch) + 384, 128*sizeof(uint16_t));
        //1번 레이더 - not 5번 데이터
        for (x = 384; x<512; x++)
        {
            _dst[_indY*_ch + x] = _src[_indY*_ch + (767-(x-384))];
        }
        //3번 레이더 - not 4번 데이터
        for (x = 512; x<640; x++)
        {
            _dst[_indY*_ch + x] = _src[_indY*_ch + (639-(x-512))];
        }
        //5번 레이더 - 0번 데이터
        memcpy((_dst + _indY*_ch) + 640, (_src + _indY*_ch) +   0, 128*sizeof(uint16_t));
    }
}

/**
@fn      save_arrangeLineDSEFormat(uint8_t *_dst, uint16_t *_src, int32_t _indY, int32_t _ch, int32_t _realH)
@author  Bongsung Kim
@date    2017.04.24
@brief   한 라인 데이터를 저장 AC2포맷에 맞춰서 정렬함.
@param   *_dst  : save 버퍼.
@param   *_src  : save 할 프레임이 저장된 버퍼.
@param   _indY  : 저장 할 라인 넘버.
@param   _ch    : 한 라인의 채널(width) 개수. 256, 768
@param   _realH : 저장할 프레임의 height(data_count)
@return  성공 시, 0. 실패 시, -1.
@warning .dse 포맷은 height가 아무리 커도 LIMIT_SAVEHEIGHT로 줄임.
@bug     none
@todo    none
*/
static void save_arrangeLineDSEFormat(uint8_t *_dst, uint16_t *_src, int32_t _indY, int32_t _ch, int32_t _realH, float gain, int16_t pffrange)
{
    int32_t x = 0;
    int32_t y = 0;
    int32_t t = 0;
    // float t = 0, tt = 0, ttt=0;
    int32_t z;
    // if (_realH > LIMIT_SAVEHEIGHT)
    //     y = floor((_indY*1.0)/(LIMIT_SAVEHEIGHT*1.0) * _realH);
    // else
        y = _indY;

	if (_ch == 256)
	{
        // clock_gettime(CLOCK_MONOTONIC,&starttime);
		for (x = 0; x < ONEPAGE_SAMPLES; x++)                  //   0 ~ 127
		{
			t = _src[y*_ch + x];
            if(t > 4095) t = 4095;
            _dst[_indY*_ch + x] = mantissa_table[t];
            /* if(t > 2999) t = 2999;
			//tt = power_table[(int)t];
            tt = pow(t,1.2);
            if (pffrange <= 50)
                  ttt = floor(tt* log_table_new[x] *0.025 * gain + 0.5);
            else
                  ttt = floor(tt* log_table_new[x] * 0.025 * gain*table_100m[x] + 0.5);

			if (ttt > 255)
            {
                _dst[_indY*_ch + x] = 255;
            }
			else
            {
                _dst[_indY*_ch + x] = (uint8_t)ttt;
            } */

		}
        // clock_gettime(CLOCK_MONOTONIC,&curtime);
        // printf("%d.%d\n", curtime.tv_sec-starttime.tv_sec, curtime.tv_nsec-starttime.tv_nsec);
		for (x = ONEPAGE_SAMPLES, z = 127; x < ONEPAGE_SAMPLES * 2; x++, z--)  // 128 ~ 255
		{
			t = _src[y*_ch + ((ONEPAGE_SAMPLES * 2 - 1) - (x - ONEPAGE_SAMPLES))];
            if(t > 4095) t = 4095;
            _dst[_indY*_ch + x] = mantissa_table[t];
            /* if(t > 2999) t = 2999;
			tt = pow(t,1.2);
            if (pffrange <= 50)
                  ttt = floor(tt* log_table_new[z] *0.025 * gain + 0.5);
            else
                  ttt = floor(tt* log_table_new[z] * 0.025 * gain*table_100m[z] + 0.5);
			if (ttt > 255)
            {
                _dst[_indY*_ch + x] = 255;
            }
			else
            {
                _dst[_indY*_ch + x] = (uint8_t)ttt;
            } */
		}
	}

    // if (_ch == 256)
    // {
    //     for ( x = 0; x < ONEPAGE_SAMPLES; x++)                  //   0 ~ 127
    //     {
    //         t = _src[y*_ch + x];
    //
    //         if ( t >= 3000 )
    //             _dst[_indY*_ch + x] = 255;
    //         else
    //             _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
    //     }
    //     for ( x = ONEPAGE_SAMPLES; x < ONEPAGE_SAMPLES*2; x++)  // 128 ~ 255
    //     {
    //         t = _src[y*_ch + ((ONEPAGE_SAMPLES*2-1) - (x - ONEPAGE_SAMPLES))];
    //
    //         if ( t >= 3000 )
    //             _dst[_indY*_ch + x] = 255;
    //         else
    //             _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
    //     }
    // }
    else if (_ch == 768) //2 5 1 3 4 0 -> 2 1 3 5 4 0
    {
        //4번 레이더 - not 2번 데이터
        for (x = 0; x<128; x++)
        {
            t = _src[_indY*_ch + (383-(x-  0))];

            if ( t >= 3000 )
                _dst[_indY*_ch + x] = 255;
            else
                _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
        }
        //2번 레이더 - not 1번 데이터
        for (x = 128; x<256; x++)
        {
            t = _src[_indY*_ch + (255-(x-128))];

            if ( t >= 3000 )
                _dst[_indY*_ch + x] = 255;
            else
                _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
        }
        //0번 레이더 - 3번 데이터
        for (x = 256; x<384; x++)
        {
            t = _src[_indY*_ch + (384+(x-256))];

            if ( t >= 3000 )
                _dst[_indY*_ch + x] = 255;
            else
                _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
        }
        //1번 레이더 - not 5번 데이터
        for (x = 384; x<512; x++)
        {
            t = _src[_indY*_ch + (767-(x-384))];

            if ( t >= 3000 )
                _dst[_indY*_ch + x] = 255;
            else
                _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
        }
        //3번 레이더 - not 4번 데이터
        for (x = 512; x<640; x++)
        {
            t = _src[_indY*_ch + (639-(x-512))];

            if ( t >= 3000 )
                _dst[_indY*_ch + x] = 255;
            else
                _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
        }
        //5번 레이더 - 0번 데이터
        for (x = 640; x<768; x++)
        {
            t = _src[_indY*_ch + (0+(x-640))];

            if ( t >= 3000 )
                _dst[_indY*_ch + x] = 255;
            else
                _dst[_indY*_ch + x] = (uint8_t)floor(temp_log_table[(int)t]*1.0/767.0 * 255 + 0.5);
        }
    }
}

/**
@fn      save_reInitSave()
@author  JunMyeung Kim
@date    2017.08.30
@brief   dse Save 용량 초과 시 파일 디스크립터 종료 후 다시 파일 저장을 시작함. 전체용량Full 이되면 저장기능 종료
@return  성공 시, 0. 실패 시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t save_reInitSave()
{
    if(full_flag == 1)
    {
        f_save=SAVE_ERROR;
        BXledBrightness_0(LED2_DIR);
    }
    else
    {
        if ( 0 > save_saveFileClose(&saveInfo) )
        {
            BXlogErrHandling("[dseSave] ERR: reInitSave saveFileClose Failed...!!");
            return 0;
        }
        f_save = save_saveFileInit();
            if ( ERR_SAVEERR == f_save )
                return -1;
            else if ( ERR_DISKSIZE_FULL == f_save )
            {
                return 0;
            }
    }

    return 0;
}

/**
@fn      save_confirmSaveState()
@author  Bongsung Kim
@date    2017.04.24
@brief   save state를 확인하고 stop이면 파일 저장을 종료하고 상태를 wait로 바꾼다.
@return  save State가 stop이 아니면 0. save state가 stop이면 1.
@warning none
@bug     none
@todo    none
*/
static int32_t save_confirmSaveState()
{
    if(f_save==SAVE_STOP)
    {
        if ( 0 > save_saveFileClose(&saveInfo) )
        {
            BXlogErrHandling("[dseSave] ERR: threadSaveCtrl saveFileClose Failed...!!");
            full_flag = 2;
            f_save=SAVE_ERROR;
            if ( BXledBrightness_0(LED2_DIR)<0 )
            BXlogPutLogMsg("[dseSignal] ERR: BXledTriggerSelect LED is missing!");
            BXlogPutLogMsg("[dseSignal] Save End!");

            return 0;
        }
        printf("save_saveFileClose(): %d", save_saveFileClose(&saveInfo));
        f_save = SAVE_WAIT;
        fileFrameCnt = 0;
        return 1;
    }
    else if(f_save==SAVE_ERROR)
    {
        return 1;
    }

    return 0;
}

/**
@fn      save_refreshSaveTime(int32_t _prev)
@author  Bongsung Kim
@date    2017.04.24
@brief   이전 초 값과 지금 시스템 시간의 초값을 비교하여 1초 차이가 나면 save 시간을 갱신한다.
@param   _prev : 이전 초 값
@return  성공 시 현재 시스템 시간의 초 값을 반환한다.
@warning none
@bug     none
@todo    none
*/
static int32_t save_refreshSaveTime(int32_t _prev)
{
    int nowSec = time_getLinuxSecond();
    if (_prev != nowSec)
    {
        if (saveInfo.saveSec == 59)
        {
            saveInfo.saveMin++;
            saveInfo.saveSec = 0;
        }
        else
            saveInfo.saveSec++;

        if (saveInfo.availableSaveMin==0 &&
            saveInfo.availableSaveSec==0   )
        {
            saveInfo.availableSaveSec = 00;
            saveInfo.availableSaveMin = 00;
        }
        else if (saveInfo.availableSaveSec == 0)
        {
            saveInfo.availableSaveMin--;
            saveInfo.availableSaveSec = 59;
        }
        else
            saveInfo.availableSaveSec--;
    }

    return nowSec;
}

/**
@fn      save_saveFileInit()
@author  Jinmo Je
@date    2021.06.14
@brief   File Save를 위해서 저장 경로의 용량 확인 및 파일 디스크립터를 Open 등 초기화를 수행한다.
@param   none
@return  성공 시 f_save Flag(SAVE_START) 를 반환한다.\n
         할당 실패시 -1, 용량 꽉 찼을 시 -2.
@warning 파일이름 생성은 망고보드(No RTC)에서는 번호순, TinyRex(RTC)에서는 현재 시간이 이름이 된다.
@bug     none
@todo    none
*/
int32_t save_saveFileInit()
{
    save_getSaveDiskStorage();

    //용량이 3GB/100MB 가 안된다면 FULL 상태로 만듬
    if ( diskAvailableSize < 3000000/*102400.0*/ )
    {
        BXlogPrintf("[dseSave] %s available size is too small...", diskName);
        BXlogPrintf("[dseSave]You need clean %s disk...", diskName);

        return ERR_DISKSIZE_FULL;
    }

    //파일 이름 정의
    char temp_date[100];
    save_makeFileName(temp_date);

    saveInfo.fd = open(temp_date, O_WRONLY | O_CREAT, 0777);
    if ( 0 > saveInfo.fd )
    {
        BXlogPutLogMsg("[dseSave]Save file open failed...!");
        return ERR_SAVEERR;
    }

    BXlogPrintf("[dseSave] Save Start! file name: %s", temp_date);

                            //save 시간 초기화
    saveInfo.saveMin = 0;
    saveInfo.saveSec = 0;
                            //save Frame Count 초기화
    fileFrameCnt     = 0;

    return SAVE_START;
}

/**
@fn      save_saveFileClose(_saveFileInfo *info)
@author  Bongsung Kim
@date    2017.04.24
@brief   저장 중인 save File Descriptor를 close한다.
@param   *info : save File 정보 구조체(_saveFileInfo)
@return  close() 함수의 return.
@warning none
@bug     none
@todo    none
*/
int32_t save_saveFileClose(_saveFileInfo *info)
{
    f_callAvailableSaveTime = 1;
    return close(info->fd);
}

/**
@fn      save_calcAvailableSaveTime()
@author  Bongsung Kim
@date    2017.04.24
@brief   현재 상태에서 녹화 가능한 시간을 구한다.
@param   none
@return  성공시 0. 실패시, -1.
@warning none
@bug     none
@todo    none
*/
uint32_t save_calcAvailableSaveTime()
{
    uint32_t t_sec;
                                        //저장 디스크의 전체 크기 구함.
                                        //diskAvailableSize
                                        //diskSize
    save_getSaveDiskStorage();
    if ( full_flag == 1 )
    {
        saveInfo.availableSaveMin = 0;
        saveInfo.availableSaveSec = 0;
        return 0;
    }
                                        //한프레임 사이즈 저장.
    saveInfo.oneFrameSize = save_calcOneFrameSize();
    if (fileExtentMode == FILE_EXTENT_MODE_DSE)
        t_sec = (uint32_t)((diskAvailableSize-3000000.0) / ((float)saveInfo.oneFrameSize/1024.0/2) / expectFps[t_range/5-1]);  ///< ((가용 용량)-3GB)/(한페이지 사이즈)/예상FPS => 한페이지 사이즈는 데이터형에 맞춰서 KByte 단위로 사용
    else if (fileExtentMode == FILE_EXTENT_MODE_AC1)
        t_sec = (uint32_t)((diskAvailableSize+diskSize*0.05) / ((float)saveInfo.oneFrameSize/1024.0) / expectFps[t_range/5-1]);

     printf("%f %f %f\n", diskAvailableSize, (float)saveInfo.oneFrameSize/1024.0, expectFps[t_range/5-1]);
     printf("%u\n",t_sec/60);
    saveInfo.availableSaveMin = (uint32_t)(t_sec / 60.0);
    saveInfo.availableSaveSec = (uint32_t)(t_sec % 60);

    return 0;
}

/**
@fn      save_getSaveDiskStorage()
@author  Bongsung Kim
@date    2017.04.24
@brief   save disk의 남은 용량을 구한다. 전역변수인 diskAvailableSize와 diskSize에 저장한다.
@param   none
@return  성공시 남은 용량. 실패 시 -1
@warning none
@bug     none
@todo    none
*/
uint32_t save_getSaveDiskStorage()
{
    //SAVEDISK의 디스크 남은 용량 체크
    FILE *fp;
    char buff[10];
    char t_str[100];
    float fivepersent,buftemp;
    sprintf(t_str, "df | grep %s | awk '{print $2}'", diskName);
    fp = popen(t_str, "r");
    if (fp == NULL)
        BXlogErrHandling("[dseMain] popen failed!!!");

    while(fgets(buff, 10, fp) != NULL);
    pclose(fp);
    buftemp = (float)atoi(buff);
    fivepersent = diskSize*0.05;
    diskSize = buftemp-fivepersent;
    if(diskSize<0) diskSize = 0;
    //printf("diskSize: %f\n", diskSize);
    sprintf(t_str, "df | grep %s | awk '{print $4}'", diskName);
    fp = popen(t_str, "r");
    if (fp == NULL)
        BXlogErrHandling("[dseSave] ERR: popen failed!!!");

    while(fgets(buff, 10, fp) != NULL);
//    {
//        printf(SAVEDISKNAME " %sKB remined!\n",buff);
//    }
    pclose(fp);
    diskAvailableSize = (float)atoi(buff);
    if(diskAvailableSize<=3000000.0)
    {
        full_flag = 1;
    }
    //printf("a_diskSize: %f\n", diskAvailableSize);

    return 0;
}

/**
@fn      save_getSaveFileName()
@author  Bongsung Kim
@date    2017.04.24
@brief   save FileName을 반환한다.
@param   none
@return  file Name String.
@warning none
@bug     none
@todo    none
*/
char *save_getSaveFileName()
{
    return saveInfo.fileName;
}

/**
@fn      save_getSaveMin()
@author  Bongsung Kim
@date    2017.04.24
@brief   save Time Minute을 반환한다.
@param   none
@return  save Time Minute
@warning none
@bug     none
@todo    none
*/
uint32_t save_getSaveMin()
{
    return saveInfo.saveMin;
}

/**
@fn      save_getSaveSec()
@author  Bongsung Kim
@date    2017.04.24
@brief   save Time Minute을 반환한다.
@param   none
@return  save Time Second
@warning none
@bug     none
@todo    none
*/
uint32_t save_getSaveSec()
{
    return saveInfo.saveSec;
}

/**
@fn      save_getAvailableSaveMin()
@author  Bongsung Kim
@date    2017.04.24
@brief   저장 가능한 minute을 반환한다.
@param   none
@return  저장 가능한 minute
@warning none
@bug     none
@todo    none
*/
uint32_t save_getAvailableSaveMin()
{
    return saveInfo.availableSaveMin;
}

/**
@fn      save_getAvailableSaveSec()
@author  Bongsung Kim
@date    2017.04.24
@brief   저장 가능한 second을 반환한다.
@param   none
@return  저장 가능한 second
@warning none
@bug     none
@todo    none
*/
uint32_t save_getAvailableSaveSec()
{
    return saveInfo.availableSaveSec;
}

/**
@fn      save_calcOneFrameSize()
@author  Bongsung Kim
@date    2017.04.24
@brief   현재 range에서 한 프레임의 크기(Byte)를 구한다.
@param   none
@return  한 프레임의 크기(Byte)
@warning none
@bug     none
@todo    none
*/
static uint32_t save_calcOneFrameSize()
{
    uint32_t frameSize;

    frameSize = sizeof(_saveFileInfo) +
            fpgaSampleCount * fpga_calcDataTotalHeight((int32_t)t_range) * sizeof(uint16_t);
    return frameSize;
}

/**
@fn      save_makeFileName(char* _str)
@author  Jinmo Je
@date    2021.06.14
@brief   현재 시간으로 저장 파일 이름을 만든다.
@param   *_str : 파일 이름이 이 버퍼에 저장된다.
@return  저장 가능한 minute
@warning none
@bug     none
@todo    none
*/
static void save_makeFileName(char* _str)
{
    char t_str[256];
    int32_t i=0;

    sprintf(_str, "%s/data", diskName);
    if (access(_str, F_OK))
    {
        BXlogPrintf("[dseSave] %s Dir. is invalid.. data Folder Create!", _str);
        sprintf(t_str, "mkdir %s", _str);
        system(t_str);
    }

    if (fileExtentMode==FILE_EXTENT_MODE_DSE)
        sprintf(t_str, "%s/%02d%02d%02d_%02d%02d%s" ,_str,
                                t_rtc.tm_year-2000,
                                t_rtc.tm_mon,
                                t_rtc.tm_mday,
                                t_rtc.tm_hour,
                                t_rtc.tm_min,
                                FILE_EXTENTION_DSE);
    else if (fileExtentMode==FILE_EXTENT_MODE_AC1)
        sprintf(t_str, "%s/%02d%02d%02d_%02d%02d%s" ,_str,
                                t_rtc.tm_year-2000,
                                t_rtc.tm_mon,
                                t_rtc.tm_mday,
                                t_rtc.tm_hour,
                                t_rtc.tm_min,
                                FILE_EXTENTION_AC1);

    do{
        if (access(t_str, F_OK)) //해당 파일의 이름이 같은것이 없으면
        {
            strcpy(saveInfo.fileName, strrchr(t_str, '/')+1);
            break;
        }

        if (fileExtentMode==FILE_EXTENT_MODE_DSE)
            sprintf(t_str, "%s/%02d%02d%02d_%02d%02d_%d%s" ,_str,
                                    t_rtc.tm_year-2000,
                                    t_rtc.tm_mon,
                                    t_rtc.tm_mday,
                                    t_rtc.tm_hour,
                                    t_rtc.tm_min,
                                    ++i,
                                    FILE_EXTENTION_DSE);
        else if (fileExtentMode==FILE_EXTENT_MODE_AC1)
            sprintf(t_str, "%s/%02d%02d%02d_%02d%02d_%d%s" ,_str,
                                    t_rtc.tm_year-2000,
                                    t_rtc.tm_mon,
                                    t_rtc.tm_mday,
                                    t_rtc.tm_hour,
                                    t_rtc.tm_min,
                                    ++i,
                                    FILE_EXTENTION_AC1);

    }while(1);

    strcpy(_str, t_str);
}

/**
@fn      save_waitSaveConditionVar()
@author  Bongsung Kim
@date    2017.04.24
@brief   Draw Condition Variable 을 wait한다.
@param   none
@return  none
@warning mut_save를 unlock하지 않는다. 함수 호출 후 반드시 mutex_unlock을 수행해야 한다.
@bug     none
@todo    none
*/
static void save_waitSaveConditionVar()
{
    pthread_mutex_lock(&mut_save);     //lock
    pthread_cond_wait(&cond_save, &mut_save);
    //pthread_mutex_unlock(&mut_draw);     //unlock
}

/**
@fn      save_sendSignalSaveConditionVar()
@author  Bongsung Kim
@date    2017.04.24
@brief   Draw Condition Variable 신호를 준다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
void save_sendSignalSaveConditionVar()
{
    pthread_mutex_lock(&mut_save);     //lock
    pthread_cond_signal(&cond_save);
    pthread_mutex_unlock(&mut_save);     //unlock
}
