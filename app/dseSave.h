/**
    @file       dseSave.h
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 메인 헤더파일
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#ifndef _DSESAVE_H_
#define _DSESAVE_H_

//--------------------------------------------------// Save 관련 Define
#define SAVE_WAIT            0   ///< Save State Wait
#define SAVE_START           1   ///< Save State Start
#define SAVE_STOP            2   ///< Save State Stop
#define SAVE_FULL            3
#define SAVE_ERROR           4

#define ERR_SAVEERR         -1   ///< Save State Err
#define ERR_DISKSIZE_FULL   -2   ///< Save State Err Disk Full

#define LIMIT_SAVEHEIGHT    730  ///< .dse 포맷 한 프레임 최대 height
/* ----------- LZW stuff -------------- */
#define M_CLR 256 /* clear table marker */
#define M_EOD 257 /* end-of-data marker */
#define M_NEW 258 /* new code index */
/**
    @struct  SAVEFILE_T
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   저장 파일 관련 구조체.
    @warning none
*/
typedef struct SAVEFILE_T
{
    int32_t  fd;                ///< 저장 파일 file Discriptor
    char     fileName[50];      ///< 저장 파일 이름 string.
    uint32_t saveMin;           ///< 저장 시간 (분)
    uint32_t saveSec;           ///< 저장 시간 (초)
    uint32_t availableSaveMin;  ///< 용량 대비 저장 가능 시간(분)
    uint32_t availableSaveSec;  ///< 용량 대비 저장 가능 시간(초)
    uint32_t oneFrameSize;      ///< 한 프레임의 Byte Size
}_saveFileInfo;

/**
    @struct  DSE_SAVEHEADER
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   저장 파일 프레임 헤더 구조체.
    @warning none
*/
typedef struct DSE_SAVEHEADER
{
    int32_t  start_packet  ; ///< Start Packet
    int32_t  packet_length ; ///< 헤더 Byte Size
    int32_t  version       ;
    int32_t  id            ;
    int32_t  interval      ; ///< 한 프레임 도시 속도
    int32_t  burst_no      ; ///< 프레임 넘버
    int32_t  range         ;
    float    gain          ;
    uint8_t  year          ;
    uint8_t  month         ;
    uint8_t  day           ;
    uint8_t  hour          ;
    uint8_t  min           ;
    uint8_t  sec           ;
    uint16_t reserved      ;
    float    depth         ;
    float    temp          ;
    uint32_t dtMin         ; ///< 다이빙 지속 시간(분)
    uint32_t dtSec         ; ///< 다이빙 지속 시간(초)
    uint32_t isDiving      ; ///< 다이빙 모드 유무
    int32_t  sensor_id     ; ///< 20170710 센서 구별 ID : 현재 170710 으로 초기화 차후 적용될 파라미터임.
    int32_t  en_outlen     ; ///< 가변 길이 압축에 따른 DATA 길이 수치
    int32_t  dont_care3[22];
    int32_t  data_count    ; ///< = height
    int32_t  ch_count      ; ///< = width
}dseSaveHeader;

#endif
