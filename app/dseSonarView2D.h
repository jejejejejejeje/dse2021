/**
    @file       dseSonarView2D.h
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 레이더 변환 정보 관련 헤더 파일
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#ifndef _DSESONARVIEW2D_H_
#define _DSESONARVIEW2D_H_

#define PI                   3.141592   /**< 원주율 값 */

#define ONEPAGE_SAMPLES      128        /**< 하나의 레이더 페이지의 가로 샘플 데이터 개수 */

#define AUTO_GAIN_THRESHOLD  1800       /**< Auto Gain 관련 파라미터. raw 데이터의 값 threshold */
#define AUTO_GAIN_CNT_LIMIT  1500       /**< threshold 이상의 데이터 개수 제한 */
#define AUTO_GAIN_CNT_HYS    200        /**< 경계점에서의 hysterisys */

#define HIST_TOTAL_NUM       768        /**< 히스토그램의 x축 최대값 */

/**
    @struct  drawVector_
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   가이드 선의 좌표 구조체.
    @warning none
*/
typedef struct drawVector_
{
    uint16_t x0;
    uint16_t y0;
    uint16_t x1;
    uint16_t y1;
} vector_t;

/**
    @struct  fontVector_
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   거리(Meter)관련 텍스트 및 좌표 구조체.
    @warning none
*/
typedef struct fontVector_
{
    uint16_t leftX;
    uint16_t leftY;
    uint16_t rightX;
    uint16_t rightY;
    char     txt[8];
} fontvector_t;

/**
    @struct  sv2D_t_
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   Sonar View 2D Object
    @warning none
*/
typedef struct sv2D_t_ sv2D_t;
struct sv2D_t_
{
    vector_t    *drawLineVector;    ///< 가이드 선의 좌표 구조체.
    fontvector_t drawFontVector[5]; ///< 거리(Meter)관련 텍스트 및 좌표 구조체.

    int32_t  drawwidth;             ///< 실제 display 영역의 가로 해상도.
    int32_t  drawheight;            ///< 실제 display 영역의 세로 해상도.
    uint32_t lader_radius;          ///< lader 반지름
    uint32_t lader_Xz;              ///< lader의 중심(원점) x좌표
    uint32_t lader_Yz;              ///< lader의 중심(원점) y좌표

    float    trunc_degree;          ///< 각 페이지당 truncate할 각도. 각 레이더는 y축에 가까운 부분이 잘린다.
    float    onePageAngle ;         ///< 하나의 레이더 페이지의 각도(degree)
    uint32_t lineUnder0P6M;         ///< 0.6M에 해당하는 라인 넘버. 0.6M 이하는 도시하지 않음.

    int      drawSampleCount;       ///< Raw Data의 한 라인 width. 샘플 데이터 개수.

    float    maxDeg;                ///< 모든 레이더 페이지의 각도(Degree) 합.
    uint32_t realDrawWidth;         ///< Display 버퍼 내에서 레이더가 표현되는 ROI의 width.
    uint32_t x_draw_Sta;            ///< Display 버퍼 내 Draw ROI 영역의 Start X 좌표
    uint32_t x_draw_End;            ///< Display 버퍼 내 Draw ROI 영역의 End   X 좌표
    uint32_t y_draw_Sta;            ///< Display 버퍼 내 Draw ROI 영역의 Start Y 좌표

                                // Moving Average 관련 변수
    int32_t  x_ratio;               ///< 평균 마스크의 x축 사이즈/2. 3*3이면 1.
    int32_t  y_ratio;               ///< 평균 마스크의 y축 사이즈/2. 3*3이면 1.

    float   p0_rightDeg ;  ///< 1사분면 x축부터 0번 페이지 오른쪽면까지의 각도
    float   p0_leftDeg  ;  ///< 1사분면 x축부터 0번 페이지 왼쪽면까지의 각도
    float   p1_rightDeg ;  ///< 1사분면 x축부터 1번 페이지 오른쪽면까지의 각도
    float   p1_leftDeg  ;  ///< 1사분면 x축부터 1번 페이지 왼쪽면까지의 각도
    float   p2_rightDeg ;  ///< 1사분면 x축부터 2번 페이지 오른쪽면까지의 각도
    float   p2_leftDeg  ;  ///< 1사분면 x축부터 2번 페이지 왼쪽면까지의 각도
    float   p3_rightDeg ;  ///< 1사분면 x축부터 3번 페이지 오른쪽면까지의 각도
    float   p3_leftDeg  ;  ///< 1사분면 x축부터 3번 페이지 왼쪽면까지의 각도
    float   p4_rightDeg ;  ///< 1사분면 x축부터 4번 페이지 오른쪽면까지의 각도
    float   p4_leftDeg  ;  ///< 1사분면 x축부터 4번 페이지 왼쪽면까지의 각도
    float   p5_rightDeg ;  ///< 1사분면 x축부터 5번 페이지 오른쪽면까지의 각도
    float   p5_leftDeg  ;  ///< 1사분면 x축부터 5번 페이지 왼쪽면까지의 각도

    float   range_meter ;             ///< 현재 프레임의 range(Meter)
    float   gain_value  ;             ///< global Gain 값.
    uint8_t gainMode    ;             ///< Gain Mode
    int32_t inputheight ;             ///< Raw Data의 height(data_count)
    int32_t cnt_autogain_th_overflow; ///< autogain 변수. th를 넘는 데이터 counter

    char    gainStr[12]  ;      ///< Gain Status Text
    char    rangeStr[6]  ;      ///< Range Status Text

    uint32_t *addrMap, *addrMapX, *addrMapY;         ///< Raw data->Lder의 Address Map.

    uint16_t *RAWdata;          ///< Raw Data 버퍼.
    uint32_t *dataHist;         ///< 히스토그램 버퍼.
    uint32_t *accHist;          ///< 누적 분포 히스토그램 버퍼.

    int       (*getDrawLineNum)(sv2D_t *); ///< 가이드 라인의 개수 반환
    int       (*reInit)(sv2D_t *, float , uint8_t); ///< 정보 초기화 및 갱신
    int32_t   (*setGain)(sv2D_t *_sv2D, uint8_t _newGainMode); ///< gain 모드 적용
    int32_t   (*initHistogram)(sv2D_t *);  ///< 히스토그램 초기화 함수
    float     (*setAutoGain)(sv2D_t *, float, float); ///< 현재 프레임에 대한 Auto gain 생성.
};

/** Sonar View object 초기화 */
extern sv2D_t* sonarview2D_init(/*uint16_t*,*/
                                int , int , int , int , int , float , float ,
                                int , int , int , float, uint8_t);
/** Sonar View object 삭제 및 할당 해제 */
extern void    sonarview2D_close(sv2D_t *);


#endif
