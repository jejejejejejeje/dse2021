/**
    @file       dseMotion.h
    @date       2021.05.14
    @author     Jinmo Je
    @brief      다이버 소나 모션 측정 관련 헤더
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/


#ifndef DSEMOTION_H_
#define DSEMOTION_H_


/**
    @struct  _MOTIONINFO
    @date    2021.05.14
    @author  Jinmo Je
    @brief   모션값 관련 정보 구조체
    @warning none
*/
typedef struct _MOTIONINFO
{
    float   heading;        ///< 헤딩 값
    float   pitch;          ///< 피치 값
    float   roll;           ///< 롤 값


    char    i2cDevDir[20];  ///< 센서가 연결된 i2c 버스의 디바이스 경로
    int32_t i2cAddr;        ///< 센서의 i2c ID
    BXi2c_t *i2c_t;         ///< i2c 통신 인터페이스 구조체 포인터

}_motionInfo;

extern int32_t motion_updateMotion(_motionInfo *info);  ///< 모션센서 헤딩, 피치, 롤 값을 업데이트한다.

/** 모션 측정 기능을 활성화한다. */
extern int32_t motionCheckOpen(_motionInfo *info, char* i2cDevName, int32_t i2cAddr);
extern int32_t motionCheckClose(_motionInfo *info);  ///< 모션 측정 기능을 종료한다.

uint8_t motionbuf[12];

#endif /* DSEMOTION_H_ */
