#!/bin/bash


echo "------------------------------------------------"
echo "->make DSE..."

make clean -C "${0%/*}"
if [ $? -ne 0 ]; then
    exit 1
fi

make -C "${0%/*}"
if [ $? -ne 0 ]; then
    exit 1
fi

echo "------------------------------------------------"
echo "Make End..."
echo "------------------------------------------------"
echo "copy linux RFS..."
echo " to ../../0_BSP/1_image/rootfs/usr/sbin/"

cp  "${0%/*}"/dse2021 ../../0_BSP/1_image/rootfs/usr/sbin/dse2021
if [ $? -ne 0 ]; then
    exit 1
fi

echo "Copy End"
