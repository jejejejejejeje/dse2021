/**
    @file       dseMotion.c
    @date       2021.05.14
    @author     Jinmo Je
    @brief      다이버 소나 모션값 모니터링 인터페이스
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/
#include <unistd.h>         // open() close() write() read() lseek()
#include <stdint.h>         // int32_t 등 library
#include <fcntl.h>          // O_RDWR
#include <stdlib.h>         //malloc atoi
#include <stdio.h>          //FILE
#include <string.h>
#include <time.h>

#include <BXi2c.h>
#include <BXlog.h>

#include "dseMotion.h"
#include "dseMain.h"

#include <linux/i2c-dev.h>
#include <linux/i2c.h>

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
/** 모션 측정 기능을 활성화한다. */
int32_t motionCheckOpen(_motionInfo *info, char* i2cDevName, int32_t i2cAddr);

int32_t motionCheckClose(_motionInfo *info);     ///< 모션 측정 기능을 종료한다.

int32_t motion_updateMotion(_motionInfo *info);  ///< 모션 값을 업데이트한다.

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////



/**
@fn      motion_updateMotion(_motionInfo *info)
@author  Jinmo Je
@date    2021.05.22
@brief   모션센서 헤딩, 피치, 롤 값을 업데이트한다.
@param   *info 센서 정보 구조체 포인터
@return  성공시 0, 실패시 -1.
@warning motionCheckOpen() 수행 후 수행하여야 한다.
@bug     none
@todo    none
*/
int init = 1;

int32_t motion_updateMotion(_motionInfo *info)
{
    union i2c_smbus_data smbus_Mdata;
    struct i2c_smbus_ioctl_data ioctl_Mdata;
    
    union motiondata_type {
        float ftype;
        uint8_t btype[4];   
    };

    union motiondata_type motion_heading;
    union motiondata_type motion_pitch;
    union motiondata_type motion_roll;

    if(init==1)
    {
        smbus_Mdata.block[0] = 3;
        smbus_Mdata.block[1] = 0x1E;
        smbus_Mdata.block[2] = 0x0A;
        smbus_Mdata.block[3] = 0x0F;
    
        ioctl_Mdata.read_write = I2C_SMBUS_WRITE;
        ioctl_Mdata.command = 0x55;
        ioctl_Mdata.size = I2C_SMBUS_I2C_BLOCK_DATA;
        ioctl_Mdata.data = &smbus_Mdata;

        if (ioctl(info->i2c_t->i2cID, I2C_SMBUS, (void *)&ioctl_Mdata) < 0)
        {
            printf("[BXi2c.c]i2c_smbus_readWrite I2C_SMBUS ioctl Error..!!\n");
            return -1;
        }

        smbus_Mdata.byte = 0x02;
    
        ioctl_Mdata.read_write = I2C_SMBUS_WRITE;
        ioctl_Mdata.command = 0x32;
        ioctl_Mdata.size = BX_I2C_SMBUS_BYTE_DATA;
        ioctl_Mdata.data = &smbus_Mdata;

        if (ioctl(info->i2c_t->i2cID, I2C_SMBUS, (void *)&ioctl_Mdata) < 0)
        {
            printf("[BXi2c.c]i2c_smbus_readWrite I2C_SMBUS ioctl Error..!!\n");
            return -1;
        }

        smbus_Mdata.byte = 0x06;
    
        ioctl_Mdata.read_write = I2C_SMBUS_WRITE;
        ioctl_Mdata.command = 0x54;
        ioctl_Mdata.size = BX_I2C_SMBUS_BYTE_DATA;
        ioctl_Mdata.data = &smbus_Mdata;

        if (ioctl(info->i2c_t->i2cID, I2C_SMBUS, (void *)&ioctl_Mdata) < 0)
        {
            printf("[BXi2c.c]i2c_smbus_readWrite I2C_SMBUS ioctl Error..!!\n");
            return -1;
        }

        smbus_Mdata.byte = 0x07;
    
        ioctl_Mdata.read_write = I2C_SMBUS_WRITE;
        ioctl_Mdata.command = 0x33;
        ioctl_Mdata.size = BX_I2C_SMBUS_BYTE_DATA;
        ioctl_Mdata.data = &smbus_Mdata;

        if (ioctl(info->i2c_t->i2cID, I2C_SMBUS, (void *)&ioctl_Mdata) < 0)
        {
            printf("[BXi2c.c]i2c_smbus_readWrite I2C_SMBUS ioctl Error..!!\n");
            return -1;
        }

        init = 2;
    }
    else if(init==2)
    {
        smbus_Mdata.byte = 0x01;
    
        ioctl_Mdata.read_write = I2C_SMBUS_WRITE;
        ioctl_Mdata.command = 0x34;
        ioctl_Mdata.size = BX_I2C_SMBUS_BYTE_DATA;
        ioctl_Mdata.data = &smbus_Mdata;

        if (ioctl(info->i2c_t->i2cID, I2C_SMBUS, (void *)&ioctl_Mdata) < 0)
        {
            printf("[BXi2c.c]i2c_smbus_readWrite I2C_SMBUS ioctl Error..!!\n");
            return -1;
        }

        init=0;
    }
    else
    {
        smbus_Mdata.block[0] = 12;

        ioctl_Mdata.read_write = I2C_SMBUS_READ;
        ioctl_Mdata.command = 0x00;
        ioctl_Mdata.size = I2C_SMBUS_I2C_BLOCK_DATA;
        ioctl_Mdata.data = &smbus_Mdata;

        if (ioctl(info->i2c_t->i2cID, I2C_SMBUS, (void *)&ioctl_Mdata) < 0)
        {
            printf("[BXi2c.c]i2c_smbus_readWrite I2C_SMBUS ioctl Error..!!\n");
            return -1;
        }
        motion_heading.btype[0] = smbus_Mdata.block[1];
        motion_heading.btype[1] = smbus_Mdata.block[2];
        motion_heading.btype[2] = smbus_Mdata.block[3];
        motion_heading.btype[3] = smbus_Mdata.block[4];

        motion_pitch.btype[0] = smbus_Mdata.block[5];
        motion_pitch.btype[1] = smbus_Mdata.block[6];
        motion_pitch.btype[2] = smbus_Mdata.block[7];
        motion_pitch.btype[3] = smbus_Mdata.block[8];

        motion_roll.btype[0] = smbus_Mdata.block[9];
        motion_roll.btype[1] = smbus_Mdata.block[10];
        motion_roll.btype[2] = smbus_Mdata.block[11];
        motion_roll.btype[3] = smbus_Mdata.block[12];

        //printf("%d %d %d %d %d %d\n", smbus_Mdata.block[1], smbus_Mdata.block[2], smbus_Mdata.block[3],smbus_Mdata.block[4],smbus_Mdata.block[5],smbus_Mdata.block[6]);
        //printf("heading = %f , pitch = %f, roll = %f\n", motion_heading.ftype*180/3.14, motion_pitch.ftype*180/3.14, motion_roll.ftype*180/3.14);

        info->heading = motion_heading.ftype*180/3.14;
        info->pitch = motion_pitch.ftype*180/3.14;
        info->roll = motion_roll.ftype*180/3.14;
    }

    return 0;
}

/**
@fn      motionCheckOpen(_motionInfo *info, char* i2cDevName, int32_t i2cAddr)
@author  Jinmo Je
@date    2021.05.14
@brief   모션 측정 기능을 활성화한다.
@param   *info 센서 정보 구조체 포인터
@param   i2cDevName i2c 버스의 디바이스 경로
@param   i2cAddr 센서의 i2c ID
@return  성공시 0. 실패시 -1.
@warning none.
@bug     none
@todo    none
*/
int32_t motionCheckOpen(_motionInfo *info, char* i2cDevName, int32_t i2cAddr)
{
    info->i2cAddr = i2cAddr;
    strcpy(info->i2cDevDir, i2cDevName);

    info->heading = -1.0;
    info->pitch = -1.0;
    info->roll  = -1.0;

    info->i2c_t = BXi2c_device_open(info->i2cDevDir, info->i2cAddr);
    if (info->i2c_t == NULL)
        return -1;

    return 0;
}

/**
@fn      motionCheckClose(_motionInfo *info)
@author  Jinmo Je
@date    2021.05.14
@brief   모션 측정 기능을 종료한다.
@param   *info 센서 정보 구조체 포인터
@return  성공시 0.
@warning none.
@bug     none
@todo    none
*/
int32_t motionCheckClose(_motionInfo *info)
{
    BXi2c_device_close(info->i2c_t);
    return 0;
}