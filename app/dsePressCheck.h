/**
    @file       dsePressCheck.h
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 수심수온 측정 관련 헤더
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/


#ifndef DSEPRESSCHECK_H_
#define DSEPRESSCHECK_H_

#define BARGAGE_SENSOR20         20.0      //86BSD-020BG-3AIC MODEL 게이지압으로 최대 20bar 표기 센서
//#define PSIABSOL_SENSOR150       150.0     //86BSD-150PA-3AIC MODEL 절대압으로 최대 150psi 표기 센서

#define ST_NORMAL    0      /**< 센서 측정 결과 센서값 정상 */
#define ST_RESERV    1      /**< 센서 측정 결과 센서값 이상 */
#define ST_NOTYET    2      /**< 센서 측정 결과 센서 준비가 안됨 */
#define ST_SENSERBUG 3      /**< 센서 측정 결과 센서 버그 */

#define PSI          14.7   /**< PSI 단위 */
#define BAR          1      /**< Bar 단위 */
#define PRESS_UNIT   PSI    /**< 현재 사용 압력 단위 */

#define DIVING_ON    1        /**< Diving 중 */
#define DIVING_OFF   0        /**< Diving 중이 아님 */
#define DIVING_ON_DEPTH  1.0  /**< Diving 모드 시작 수심 조건 */
#define DIVING_OFF_DEPTH 0.5  /**< Diving 모드 종료 수심 조건 */
#define DIVING_ON_CNT    4    /**< Diving 모드 시작 최소 유지 시간(초) */

#define PRESS_TEMP_INIT_VALUE -1000.0   /**< 수압 초기 값.*/

#define PRESS_THRESHOLD_PSI   14.7      /**< 데이터 튀는 걸 방지하기 위한 PSI Thr. */
#define PRESS_THRESHOLD_BAR    1.0      /**< 데이터 튀는 걸 방지하기 위한 Bar Thr. */
#define TEMP_THRESHOLD         5.0      /**< 온도 튀는 걸 방지하기 위한 temp. thr. */

/**
    @struct  _PRESSINFO
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   수심, 수압, 수온, 다이빙 모드 관련 정보 구조체
    @warning none
*/
typedef struct _PRESSINFO
{
    int32_t devSt;          ///< 센서의 상태 정보
    float   press;          ///< 수압 값(Bar or PSI)
    float   temp;           ///< 온도 값(화씨)
    float   depth;          ///< 수심 (Meter)

    int32_t divingCnt;
    _Bool   isDiving;       ///< Diving Mode 상태
    int32_t divingMin;      ///< 최근 Diving Mode 유지 시간(분)
    int32_t divingSec;      ///< 최근 Diving Mode 유지 시간(초)
    time_t  divingStartTime;///< Diving Mode 시작 시간
    time_t  divingEndTime;  ///< Diving Mode 종료 시간

    char    i2cDevDir[20];  ///< 센서가 연결된 i2c 버스의 디바이스 경로
    int32_t i2cAddr;        ///< 센서의 i2c ID
    BXi2c_t *i2c_t;         ///< i2c 통신 인터페이스 구조체 포인터

    int32_t logFd;          ///< Log 파일 File Descriptor
    _Bool   enLog;          ///< Log 파일 생성 및 출력 Enable Flag
}_pressInfo;

extern int32_t press_updatePressTemp(_pressInfo *info);  ///< 수압, 수심, 수온을 업데이트한다.
extern int32_t press_divingCheck    (_pressInfo *info);  ///< Diving 상태를 확인한다.
extern int32_t press_putLog         (_pressInfo *_info); ///< Log File에 현재 상태를 출력한다.

/** 수압,수심,수온 측정 기능을 활성화한다. */
extern int32_t press_pressCheckOpen(_pressInfo *info, 
                                    char* i2cDevName,
                                    int32_t i2cAddr);
extern int32_t press_pressCheckClose(_pressInfo *info);  ///< 수압, 수심, 수온 측정 기능을 종료한다.
extern int32_t press_logFileOpen    (_pressInfo *_info); ///< Log File을 생성한다.
extern int32_t press_logFileClose   (_pressInfo *_info); ///< Log File을 Close한다.

#endif /* DSEPRESSCHECK_H_ */
