/**
    @file       dseDraw.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 Draw 관련 인터페이스
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#include <stdio.h>          //FILE
#include <sys/ioctl.h>
#include <stdint.h>
#include <unistd.h>         // open() close() write() read() lseek()
#include <gx.h>             //gxLib 영상 Draw 관련
#include <gxbdf.h>          //gxLib 폰트 관련
#include <pthread.h>
#include <BXlog.h>
#include <math.h>
#include <linux/rtc.h>

#include <sched.h>

#include <BXctrlLED.h>

#include "dseMain.h"
#include "dseSonarView2D.h"
#include "dseRomTable.h"
#include "NMEA_Parse.h"

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
void          *draw_drawThread();       ///< Display Thread 함수.
                                                //
static int32_t draw_gxlibInit();
static int32_t draw_loadPngIcon();
static int32_t draw_loadFont();
                                                //
static void     draw_printFirstScreen();
static void     draw_printShutdownScreen();
static void     draw_drawLader(sv2D_t* _sv, uint16_t* _src , uint8_t *_buf);
static uint32_t draw_averageFiltering(sv2D_t* _sv,
                                     uint16_t* _src,
                                     uint32_t _x,
                                     uint32_t _y);
static uint32_t draw_applylogGain(uint32_t _val);
static void     draw_colorMapping(uint32_t _val, uint8_t *_dst, uint32_t _x, uint32_t _y);

static void     draw_drawLineArc(sv2D_t* _sv);
static void     draw_drawLine(int32_t _numLine, vector_t *_vector);
static void     draw_drawArc(uint32_t _xz, uint32_t _yz, uint32_t _r, float _deg, float _range);

static void     draw_drawTxt(sv2D_t *_sv);
static void     draw_drawSaveInfo();
static void     draw_drawDiskSize();
static void     draw_drawDiskSize2();
static void     draw_drawSaveFileName();
static void     draw_drawSaveTime();
static void     draw_drawNoticeChange(char* gainStr, char* rangeStr);
static void     draw_drawDistance(fontvector_t *_txtInfo);
static void     draw_drawGain(uint8_t _gain);
static void     draw_drawFPS();
static void     draw_drawTime();
static void     draw_drawTimeforFirstScreen();
static void     draw_drawDepth();
static void     draw_drawTemperature();
static void     draw_divingMode();
static void     draw_drawgps();
static void     draw_heading();
static void     draw_getDateChar(char* temp_date);
static void     draw_drawTxtVer();

static void     draw_drawImage();
static void     draw_drawLOGOimg();
static void     draw_drawRECimg();
static void     draw_drawBattImg();

static int32_t  draw_compareAndRefresh(sv2D_t *_sv);
                                                //
static void     draw_calFPS();
                                                //
static void draw_waitDrawConditionVar();        ///< cond_draw 조건 변수의 신호를 기다린다.
void        draw_sendSignalDrawConditionVar();  ///< Draw Thread에 pthread_cond_signal 신호를 인가한다.

static void draw_sendSignalStartConditionVartoAll(); ///< cond_start 신호를 기다리는 모든 스레드에 알림을 준다.
void        draw_waitSignalStartConditionVar();      ///< 첫 화면을 넘어기기 위해 cond_start의 신호를 기다린다.
///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////

////////////////global Variable////////////////////////////
                                //Draw Buffers
dc_t     *dc_screen;    ///< framebuffer 구조체
dc_t     *dc_temp  ;    ///< framebuffer 구조체 2
uint8_t  *buf_screen;   ///< framebuffer 실제 픽셀 데이터 버퍼
                                //icon PNG
png_t    *png_batt0  ;  ///< 배터리 상태 0의 이미지 구조체
png_t    *png_batt1  ;  ///< 배터리 상태 1의 이미지 구조체
png_t    *png_batt2  ;  ///< 배터리 상태 2의 이미지 구조체
png_t    *png_batt3  ;  ///< 배터리 상태 3의 이미지 구조체
png_t    *png_batt4  ;  ///< 배터리 상태 4의 이미지 구조체
png_t    *png_batt5  ;  ///< 배터리 상태 5의 이미지 구조체
png_t    *png_batt6  ;  ///< 배터리 상태 6의 이미지 구조체
png_t    *png_recicon;  ///< 녹화 아이콘의 이미지 구조체
png_t    *png_logo   ;  ///< 로고 이미지 구조체
                                //font file(bdf)
static font_t   *fontNanum25; ///< 25 Size Font 구조체
static font_t   *fontNanum40; ///< 40 Size Font 구조체
static font_t   *fontNanum15; ///< 15 Size Font 구조체
static font_t   *fontNanum20; ///< 20 Size Font 구조체
                                //sonar view
sv2D_t *svInfo;             ///< Sonar View 관련 구조체
float   fps       = 18.0;   ///< Display 영상 Frame Per Second
float   frameRate = 0.0;    ///< Display 영상의 한 프레임 주사 속도
                                //thread
/** 첫화면을 벗어났다는 알림을 위한 조건 변수*/
pthread_cond_t  cond_start = PTHREAD_COND_INITIALIZER;
/** 첫화면을 벗어났다는 알림을 위한 mutex*/
pthread_mutex_t mut_start  = PTHREAD_MUTEX_INITIALIZER;

pthread_t thr_draw;                                    ///< draw Thread
pthread_cond_t  cond_draw = PTHREAD_COND_INITIALIZER;  ///< draw Thread와 다른 thread간 동기화를 위한 조건 변수
pthread_mutex_t mut_draw  = PTHREAD_MUTEX_INITIALIZER; ///< draw Thread와 다른 thread간 동기화를 위한 mutex
                                //Time 측정 관련 변수
const long long     NANOS = 1000000000LL;
struct timespec     startTS, endTS;
uint64_t            retDiff   = 0;

uint8_t f_callAvailableSaveTime = 1;  ///< 저장 가능 시간 측정 Enable Flag
uint8_t f_drawSaveFileName = 0;       ///< Save File Name Display Enable Flag(현재 미사용)
//-------------global Variable----------------//



/**
@fn      draw_drawThread()
@author  Jinmo Je
@date    2021.06.14
@brief   Display Thread 함수.
@param   none
@return  (void *)0
@warning none
@bug     none
@todo    none
*/
void *draw_drawThread()
{
    uint32_t sec_cnt  = 0;

    // schedule 조정. 실시간으로
    struct sched_param p;
    p.__sched_priority = 99;
    if (sched_setscheduler(0, SCHED_FIFO, &p) < 0 )
        BXlogErrHandling("[dseDraw]Sched_setscheduler Failed!");

                                    //framebuffer 관련 init. - gxlib관련
    if (draw_gxlibInit() < 0)
        BXlogErrHandling("[dseDraw] ERR: gxlib Initialize Failed...!");

                                    //각종 이미지(png) file Load
    if (draw_loadPngIcon() < 0)
        BXlogErrHandling("[dseDraw] ERR: PNG File Load Failed...!");

                                    //Font file(bdf) Load
    if (draw_loadFont()< 0)
        BXlogErrHandling("[dseDraw] ERR: BDF File Load Failed...!");

                                    //SonarView Info Initialize
    svInfo = sonarview2D_init(/*RAWdata,*/
                              DRAWWIDTH, DRAWHEIGHT,
                              radius, DRAWWIDTH/2, drawYZ,
                              1.0, 23.0, fpgaSampleCount, 1, 1,
                              t_range, gainMode);
    if (svInfo == NULL)
        BXlogErrHandling("[dseDraw] ERR: BXsonarview2D object create Failed!!");
    BXlogPutLogMsg("[dseDraw] SonarView Initialize End!!");

                                    //Draw First Screen And Wait cond_var
#if START_KEY_INT
    BXlogPutLogMsg("[dseDraw] Wait Any key input...");
    BXlogPutLogMsg("[dseDraw] Print Screen First Page.");

    // First Page Loop
    while(1)
    {
        //if (main_getBattPercent() < SHUTDOWN_BATT_PERCENT)
        if( main_getBattVoltage() < SHUTDOWN_BATT_VOLTAGE)  //9.5V 이하시 SHUTDOWN
        {
            draw_printShutdownScreen();
        }

        draw_printFirstScreen();

//        if (main_checkFTPOperation())
        if (main_checkTCPOperation())
        {
            sec_cnt++;
        }
        else
        {
            sec_cnt = 0;
        }

        // 5분 이상 FTP 접속이 없을 시에 KILL
        if (sec_cnt > AUTO_SHUTDOWNTIME)
           {
#if AUTO_SHUTDOWNEN == 1
                main_killSystem();
#endif
           }

        if (firstStartSig)
        {
            draw_sendSignalStartConditionVartoAll();
            ioctl(gpiofd, IOCTL_TXPWRON, NULL);
            //ioctl(gpiofd, IOCTL_ONOFF_OFF, NULL);
            break;
        }
        usleep(20000);
        //sleep(1);
        //main_waitSignalStartConditionVar();
    }

#endif
    //-- LED State Change (always on -> heartbeat)
    if (BXledTriggerSelect(LED1_DIR, "heartbeat")<0)
        BXlogPutLogMsg("[dseMain] ERR: BXledTriggerSelect LED is missing!");

    BXlogPutLogMsg("[dseDraw] Draw Thread : Wait First Screen End!");
    BXlogPutLogMsg("[dseDraw] Draw Thread : Thread loop Start!");

    int8_t f_bufSW = 0;

    // Draw Main Loop
    while(1)
    {
        if(!f_change)
        {
            draw_calFPS();
            draw_waitDrawConditionVar();

            gx_clear( dc_temp, gx_color( 0, 0, 0, 255));
            if (!f_bufSW)
            {
                draw_drawLader(svInfo, RAWdata1, (uint8_t *) dc_temp->mapped);
            }
            else
            {
                draw_drawLader(svInfo, RAWdata2, (uint8_t *) dc_temp->mapped);
            }
            f_bufSW ^= 1;

            draw_drawLineArc(svInfo);

            draw_drawTxt(svInfo);

            draw_drawImage(svInfo);

            gx_bitblt(dc_screen,0,0, dc_temp, 0, 0, dc_temp->width-1, dc_temp->height-1);
            usleep(50000);

            if (draw_compareAndRefresh(svInfo) < 0)
                BXlogErrHandling("[dseDraw] Err: Program Shutdown...");

            pthread_mutex_unlock(&mut_draw);     //unlock

            if (main_getBattVoltage() < SHUTDOWN_BATT_VOLTAGE)
            {
                draw_printShutdownScreen();
            }

            if(f_change)
            {
                ioctl(gpiofd, IOCTL_ONOFF_OFF, NULL);
            }
        }
        else
        {
            gx_clear( dc_temp, gx_color( 0, 0, 0, 255));
            draw_drawBattImg();
            draw_drawDiskSize();

            if(en_RTC)
                draw_drawTime();

            if (en_i2cPressCheck)
            {
                draw_drawDepth();
                draw_drawTemperature();
                draw_divingMode();
            }

            if (en_i2cMotionCheck)
            {
                draw_heading();
            }

            draw_drawgps();

            gx_bitblt(dc_screen,0,0, dc_temp, 0, 0, dc_temp->width-1, dc_temp->height-1);
            usleep(50000);

            if (main_getBattVoltage() < SHUTDOWN_BATT_VOLTAGE)
            {
                draw_printShutdownScreen();
            }

            if(!f_change)
            {
                ioctl(gpiofd, IOCTL_ONOFF_ON, NULL);
            }
        }
    }

    return (void *)0;
}

/**
@fn      draw_drawImage()
@author  Bongsung Kim
@date    2017.04.24
@brief   icon, logo 등의 이미지를 출력한다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawImage()
{
    //REC File Save Display
    draw_drawRECimg();

    draw_drawBattImg();
}

/**
@fn      draw_drawImage()
@author  Bongsung Kim
@date    2017.04.24
@brief   Logo Image를 그린다.
@param   none
@return  none
@warning none
@bug     none
@todo    절대 좌표를 상대 좌표로 치환 필요.
*/
static void draw_drawLOGOimg()
{
    //Logo Draw
//#if LOGO_DISPLAY
    if(logo == 1)
    {
        //gx_bitblt( dc_temp, 80, 30, ( dc_t *)png_logo, 0, 0, png_logo->width, png_logo->height);    //logo position BDSNET
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.078125), (int)(DRAWHEIGHT*0.10416), ( dc_t *)png_logo, 0, 0, png_logo->width, png_logo->height);    //logo position cwi
    }
//#endif
}

/**
@fn      draw_drawRECimg()
@author  Bongsung Kim
@date    2017.04.24
@brief   REC 그림을 그린다.
@param   none
@return  none
@warning none
@bug     none
@todo    절대 좌표를 상대 좌표로 치환 필요.
*/
static void draw_drawRECimg()
{
    if (f_save == SAVE_START)
    {
        if (time_getLinuxSecond()%2)
        {
            gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.068359375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_recicon, 0, 0, png_recicon->width, png_recicon->height);
        }
        //gx_text_out( dc_temp, 110 , 62 , "REC");
    }
}

/**
@fn      draw_drawBattImg()
@author  Bongsung Kim
@date    2017.04.24
@brief   배터리 상태에 따라 그림을 출력한다.
@param   none
@return  none
@warning none
@bug     none
@todo    절대 좌표를 상대 좌표로 치환 필요.
*/
static void draw_drawBattImg()
{
    int32_t st = main_getBattState();

    //Display Battery limit
    switch(st)
    {
    case 0 :
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.849609375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_batt0, 0, 0, png_batt0->width, png_batt0->height);
        break;
    case 1 :
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.849609375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_batt1, 0, 0, png_batt1->width, png_batt1->height);
        break;
    case 2 :
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.849609375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_batt2, 0, 0, png_batt2->width, png_batt2->height);
        break;
    case 3 :
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.849609375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_batt3, 0, 0, png_batt3->width, png_batt3->height);
        break;
    case 4 :
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.849609375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_batt4, 0, 0, png_batt4->width, png_batt4->height);
        break;
    case 5 :
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.849609375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_batt5, 0, 0, png_batt5->width, png_batt5->height);
        break;
    case 6 :
        gx_bitblt( dc_temp, (int)(DRAWWIDTH*0.849609375), (int)(DRAWHEIGHT*0.078125), ( dc_t *)png_batt6, 0, 0, png_batt6->width, png_batt6->height);
        break;
    default :
        break;
    }
}

/**
@fn      draw_calFPS()
@author  Bongsung Kim
@date    2017.04.24
@brief   FPS 를 측정한다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_calFPS()
{
    //영상 전체 한프레임 시간 체크
    clock_gettime(CLOCK_MONOTONIC, &endTS);
    retDiff = NANOS * (endTS.tv_sec - startTS.tv_sec) + (endTS.tv_nsec - startTS.tv_nsec);
    frameRate = retDiff*1.0 /1000000.0;
    fps = 1000.0 / frameRate;  //fps로

    clock_gettime(CLOCK_MONOTONIC, &startTS);
}

/**
@fn      draw_gxlibInit()
@author  Bongsung Kim
@date    2017.04.24
@brief   gxlib library Initialze
@param   none
@return  성공시 0, 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t draw_gxlibInit()
{
                                                    //gxLib 초기화
    if (gx_open(FRAMEBUFFER_DEV) != GX_SUCCESS)
    {
        BXlogPutLogMsg("[dseDraw] ERR :gx_open() Failed...!");
        return -1;
    }
                                                    //화면 출력용 screen DC 획득
    if ( (dc_screen = gx_get_screen_dc()) == NULL )
    {
        BXlogPutLogMsg("[dseDraw] ERR :Get dc_screen Failed...!");
        return -1;
    }
                                                    //임시 저장용 화면 buffer DC 획득
    if ( (dc_temp = gx_get_compatible_dc(dc_screen)) == NULL )
    {
        BXlogPutLogMsg("[dseDraw] ERR :Get dc_buffer Failed...!");
        return -1;
    }
                                                    //DC 검은색으로 클리어
    gx_clear( dc_screen, gx_color( 0, 0, 0, 255));
    gx_clear( dc_temp  , gx_color( 0, 0, 0, 255));

    buf_screen = (uint8_t *)dc_screen->mapped;
    return 0;
}

/**
@fn      draw_loadPngIcon()
@author  Bongsung Kim
@date    2017.04.24
@brief   icon Image (png) load
@param   none
@return  성공시 0, 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t draw_loadPngIcon()
{
    png_batt0 = (png_t *)gx_png_open("/home/root/img/batt0.png");
    if (png_batt0==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: batt0.png File Open Failed...!");
        return -1;
    }
    png_batt1 = (png_t *)gx_png_open("/home/root/img/batt1.png");
    if (png_batt1==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: batt1.png File Open Failed...!");
        return -1;
    }
    png_batt2 = (png_t *)gx_png_open("/home/root/img/batt2.png");
    if (png_batt2==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: batt2.png File Open Failed...!");
        return -1;
    }
    png_batt3 = (png_t *)gx_png_open("/home/root/img/batt3.png");
    if (png_batt3==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: batt3.png File Open Failed...!");
        return -1;
    }
    png_batt4 = (png_t *)gx_png_open("/home/root/img/batt4.png");
    if (png_batt4==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: batt4.png File Open Failed...!");
        return -1;
    }
    png_batt5 = (png_t *)gx_png_open("/home/root/img/batt5.png");
    if (png_batt5==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: batt5.png File Open Failed...!");
        return -1;
    }
    png_batt6 = (png_t *)gx_png_open("/home/root/img/batt6.png");
    if (png_batt6==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: batt6.png File Open Failed...!");
        return -1;
    }

    png_recicon = (png_t *)gx_png_open("/home/root/img/icon_rec.png");
    if (png_recicon==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: icon_rec.png File Open Failed...!");
        return -1;
    }
    //png_logo    = (png_t *)gx_png_open("/home/root/img/BDSNET_logo.png");
    png_logo    = (png_t *)gx_png_open("/home/root/img/cwi_logo.png");
    if (png_logo==NULL)
    {
        BXlogPutLogMsg("[dseDraw] ERR: logo.png File Open Failed...!");
        return -1;
    }

    return 0;
}

/**
@fn      draw_loadFont()
@author  Bongsung Kim
@date    2017.04.24
@brief   Font file (bdf) load
@param   none
@return  성공시 0, 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t draw_loadFont()
{
    if ( ( fontNanum25 = gx_open_font( "/home/root/font/nanum25.bdf")) == NULL )
    {
        BXlogPutLogMsg("[dseDraw] ERR: Open font fontNanum25 Failed...!");
        return -1;
    }
    if ( ( fontNanum40 = gx_open_font( "/home/root/font/nanum40.bdf")) == NULL )
    {
        BXlogPutLogMsg("[dseDraw] ERR: Open font fontNanum40 Failed...!");
        return -1;
    }
    if ( ( fontNanum15 = gx_open_font( "/home/root/font/nanum15.bdf")) == NULL )
    {
        BXlogPutLogMsg("[dseDraw] ERR: Open font fontNanum15 Failed...!");
        return -1;
    }
    if ( ( fontNanum20 = gx_open_font( "/home/root/font/nanum20.bdf")) == NULL )
        {
            BXlogPutLogMsg("[dseDraw] ERR: Open font fontNanum20 Failed...!");
            return -1;
        }

    return 0;

}

/**
@fn      draw_printFirstScreen()
@author  Bongsung Kim
@date    2017.04.24
@brief   프로그램 시작 전 Press Any Key... 화면을 띄운다.\n
         첫 화면에는 시간, 디스크 사이즈, 로고, 배터리 등이 출력된다.(dc_temp에 그림)
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_printFirstScreen ()
{
    gx_clear( dc_temp, gx_color( 0, 0, 0, 255));

    dc_temp->font         = fontNanum40;
    dc_temp->font_color   = gx_color( 255, 255, 255, 255);
    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.3125), (int)(DRAWHEIGHT*0.45572916),
                            "Press Any key ...");
    if (en_RTC)
        draw_drawTimeforFirstScreen();
    draw_drawTxtVer();
    save_getSaveDiskStorage();
    draw_drawDiskSize();
    draw_drawLOGOimg();
    draw_drawBattImg();

    gx_bitblt(dc_screen,0,0, dc_temp, 0, 0, dc_temp->width-1, dc_temp->height-1);
}

/**
@fn      draw_printShutdownScreen()
@author  Bongsung Kim
@date    2017.04.24
@brief   배터리 low 시에 shutdown 경고 화면을 띄운다.(dc_temp에 그림.)
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_printShutdownScreen()
{
    gx_clear( dc_temp, gx_color( 0, 0, 0, 255));

    dc_temp->font         = fontNanum40;
    dc_temp->font_color   = gx_color( 255, 255, 255, 255);
    gx_text_out( dc_temp,(int)(DRAWWIDTH*0.1953125), (int)(DRAWHEIGHT*0.44270833),
                 "Power is turned off soon.");
    dc_temp->font         = fontNanum25;
    gx_text_out( dc_temp,(int)(DRAWWIDTH*0.205078125), (int)(DRAWHEIGHT*0.546875),
                 "Please charged the battery.");


    draw_drawLOGOimg();
    draw_drawBattImg();

    gx_bitblt(dc_screen,0,0, dc_temp, 0, 0, dc_temp->width-1, dc_temp->height-1);

    sleep(8);

    main_killSystem();
}

/**
@fn      draw_drawTxtVer()
@author  Bongsung Kim
@date    2017.04.24
@brief   버전 정보를 화면에 띄운다. (dc_temp에 그림.)
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawTxtVer()
{
    char tempChar[100];

    dc_temp->font         = fontNanum20;
    dc_temp->font_color = gx_color(0,255,0,255);
    gx_text_out( dc_temp,(int)(DRAWWIDTH*0.634765625), (int)(DRAWHEIGHT*0.716145833), "Ver.");

    dc_temp->font         = fontNanum25;
    dc_temp->font_color = gx_color(255,255,255,255);
    sprintf(tempChar, "%s", DSE2021_VERSION);
    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.6640625), (int)(DRAWHEIGHT*0.768229166), tempChar);
}

/**
@fn      draw_waitDrawConditionVar()
@author  Bongsung Kim
@date    2017.04.24
@brief   Draw Condition Variable 을 wait한다.
@param   none
@return  none
@warning mut_draw 뮤텍스를 unlock 하지 않는다.\n
         함수 호출 후 반드시 pthread_mutex_unlock(&mut_draw);을 별도로 수행해야 한다.
@bug     none
@todo    none
*/
static void draw_waitDrawConditionVar()
{
    pthread_mutex_lock(&mut_draw);     //lock
    pthread_cond_wait(&cond_draw, &mut_draw);
    //pthread_mutex_unlock(&mut_draw);     //unlock
}

/**
@fn      draw_sendSignalDrawConditionVar()
@author  Bongsung Kim
@date    2017.04.24
@brief   Draw Condition Variable 신호를 준다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
void draw_sendSignalDrawConditionVar()
{
    pthread_mutex_lock(&mut_draw);     //lock
    pthread_cond_signal(&cond_draw);
    pthread_mutex_unlock(&mut_draw);     //unlock
}

/**
@fn      draw_sendSignalStartConditionVartoAll()
@author  Bongsung Kim
@date    2017.04.24
@brief   First Start Condition Variable 신호를 준다.\n
         broadcast로 신호를 준다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_sendSignalStartConditionVartoAll()
{
    pthread_cond_broadcast(&cond_start);
}

/**
@fn      draw_sendSignalStartConditionVartoAll()
@author  Bongsung Kim
@date    2017.04.24
@brief   First Start Condition Variable 신호를 기다린다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
void draw_waitSignalStartConditionVar()
{
    pthread_cond_wait(&cond_start, &mut_start);
    pthread_mutex_unlock(&mut_start);
}

/**
@fn      draw_drawLader()
@author  Bongsung Kim
@date    2017.04.24
@brief   레이더 영상을 그린다.
@param   _sv  : sonar View 구조체
@param   _src : RAW데이터
@param   _buf : 그릴 영상의 디스플레이 버퍼
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawLader(sv2D_t* _sv, uint16_t* _src , uint8_t *_buf)
{
    uint32_t x = 0, y = 0;
    uint32_t temp = 0;
    uint32_t t_val = 0;
    int lader_num = 0;
    int ind_Ucurve = 0;
    float    val_ucurve = 0.0;
    uint32_t newX, newY;
                                            // 히스토그램 생성
    if ( _sv->initHistogram(_sv) < 0)
        BXlogErrHandling("[dseMain.c] ERR: Histogram Initialize Failed!!");

                                            // Draw Loop
//     for (y = _sv->y_draw_Sta; y < _sv->lader_Yz; y++)
//         {
//             for (x = _sv->x_draw_Sta; x < _sv->x_draw_End; x++)
//             {
//                 temp = _sv->addrMap[y*_sv->drawwidth + x];
//                 if (!temp)
//                     continue;
//                 //newX = (temp >> 11) & 0x000003FF;
//                 //newY = temp         & 0x000007FF;
//                 //UCurve 값 구하기

//                 newX = _sv->addrMapX[y*_sv->drawwidth + x];
//                 newY = _sv->addrMapY[y*_sv->drawwidth + x];

//                 lader_num  = ((temp >> 11) & 0x000003FF) / 128;
// 			    if(fpgaSampleCount == 256)
// 			    {
// 			    	if (lader_num==0)
// 			    		ind_Ucurve = ((temp >> 11) & 0x000003FF) % 128;
//                     else
//                         ind_Ucurve = ((temp >> 11) & 0x000003FF) % 128;
//                         //ind_Ucurve = ((temp >> 11) & 0x000003FF) % 128;
//                         //ind_Ucurve = (128 - ((temp >> 11) & 0x000003FF) % 128);
//                     val_ucurve = ((float)u_curve[ind_Ucurve]-140) * 0.02;
// 			    }
//                 else
//                     val_ucurve = 1;
//                 //UCurve 값 구하기

//                 t_val = draw_averageFiltering(_sv, _src, x, y);

// #if TVG_EN
//                 t_val = floor(t_val * tvgTable[newY] + 0.5);
// #else
//                 //t_val = floor(t_val*1.0 * (_sv->gain_value + gainOffset) + 0.5);
//                 t_val = floor((float)t_val *
//                               (_sv->gain_value + gainOffset) *
//                               val_ucurve + 0.5);
// #endif
//                 if (t_val > AUTO_GAIN_THRESHOLD)
//                     _sv->cnt_autogain_th_overflow++;

//                 t_val = draw_applylogGain(t_val);

//                 _sv->dataHist[t_val]++;

//                 draw_colorMapping(t_val, _buf, x, y);

//             }
//         }

        for (y = _sv->y_draw_Sta; y < _sv->lader_Yz; y++)
        {
            for (x = _sv->x_draw_Sta; x < _sv->x_draw_End; x++)
            {
                // temp = _sv->addrMap[y*_sv->drawwidth + x];
                // if (!temp)
                //     continue;
                //newX = (temp >> 11) & 0x000003FF;
                //newY = temp         & 0x000007FF;
                //UCurve 값 구하기

                newX = _sv->addrMapX[y*_sv->drawwidth + x];
                newY = _sv->addrMapY[y*_sv->drawwidth + x];

                lader_num  = newX / 128;
			    if(fpgaSampleCount == 256)
			    {
			    	if (lader_num==0)
                    {
			    	    ind_Ucurve = newX % 128;
                        //val_ucurve = ((float)u_curve[ind_Ucurve]-140) * 0.02;
                    }
                    else
                    {
                        ind_Ucurve = newX % 128;
                        //val_ucurve = ((float)u_curve[ind_Ucurve]-140) * 0.02;
                    }
                        //ind_Ucurve = ((temp >> 11) & 0x000003FF) % 128;
                        //ind_Ucurve = (128 - ((temp >> 11) & 0x000003FF) % 128);
                    // val_ucurve = ((float)u_curve[ind_Ucurve]-140) * 0.02; //기존 50m 용
                    // val_ucurve = ((float)u_curve[ind_Ucurve]) * 0.02; //Blueview Sensor 용
                    if(t_range>0&&t_range<=50)
                    val_ucurve = ((float)u_curve[ind_Ucurve]-140) * 0.02; //기존 50m 용
                    else if(t_range>50 && t_range<=100)
                    val_ucurve = ((float)u_curve[ind_Ucurve]-140) * (0.0001161 * ind_Ucurve * ind_Ucurve - 0.02206 * ind_Ucurve + 1.422) * 0.02;  //100m용
                    else
                    val_ucurve = ((float)u_curve[ind_Ucurve]-140) * (0.0001161 * ind_Ucurve * ind_Ucurve - 0.02206 * ind_Ucurve + 1.422) * 0.02;  //100m용
                    //val_ucurve = ((float)u_curve[ind_Ucurve]) * (0.0002042*ind_Ucurve*ind_Ucurve - 0.0232*ind_Ucurve + 1.623) * 0.02; // testing
			    }
                else
                    val_ucurve = 1;
                //UCurve 값 구하기

                //t_val = draw_averageFiltering(_sv, _src, x, y);
                //t_val = floor()
                t_val = _src[newY * fpgaSampleCount + newX];

                t_val = (int)floor((float)t_val / 9.0 + 0.5);

#if TVG_EN
                t_val = floor(t_val * tvgTable[newY] + 0.5);
#else
                //t_val = floor(t_val*1.0 * (_sv->gain_value + gainOffset) + 0.5);
                t_val = floor((float)t_val *
                              (_sv->gain_value + gainOffset) *
                              val_ucurve + 0.5);
#endif
                if (t_val > AUTO_GAIN_THRESHOLD)
                    _sv->cnt_autogain_th_overflow++;

                t_val = draw_applylogGain(t_val);

                _sv->dataHist[t_val]++;

                draw_colorMapping(t_val, _buf, x, y);

            }
        }




}

/**
@fn      draw_averageFiltering(sv2D_t* _sv, uint16_t* _src, uint32_t _x, uint32_t _y)
@author  Bongsung Kim
@date    2017.04.24
@brief   3x3 평균 필터링
@param _sv  : sonar View 구조체
@param _src : RAW데이터
@param _x   : 평균 마스크가 적용될 관심영역의 중앙 x좌표
@param _y   : 평균 마스크가 적용될 관심영역의 중앙 y좌표
@return 평균 마스크가 적용된 결과 값 반환.
@warning _x, _y 좌표는 display의 가로 세로 해상도를 벗어나서는 안된다.
@bug     none
@todo    none
*/
static uint32_t draw_averageFiltering(sv2D_t* _sv, uint16_t* _src, uint32_t _x, uint32_t _y)
{
    uint32_t val = 0;
    uint32_t newX = 0, newY = 0;

    // newX = (_sv->addrMap[(_y+0)*_sv->drawwidth + (_x+0)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y+0)*_sv->drawwidth + (_x+0)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    // newX = (_sv->addrMap[(_y-1)*_sv->drawwidth + (_x+0)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y-1)*_sv->drawwidth + (_x+0)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    // newX = (_sv->addrMap[(_y+0)*_sv->drawwidth + (_x-1)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y+0)*_sv->drawwidth + (_x-1)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    // newX = (_sv->addrMap[(_y+1)*_sv->drawwidth + (_x+0)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y+1)*_sv->drawwidth + (_x+0)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    // newX = (_sv->addrMap[(_y+0)*_sv->drawwidth + (_x+1)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y+0)*_sv->drawwidth + (_x+1)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    // newX = (_sv->addrMap[(_y-1)*_sv->drawwidth + (_x-1)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y-1)*_sv->drawwidth + (_x-1)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    // newX = (_sv->addrMap[(_y-1)*_sv->drawwidth + (_x+1)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y-1)*_sv->drawwidth + (_x+1)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    // newX = (_sv->addrMap[(_y+1)*_sv->drawwidth + (_x-1)] >> 11) & 0x000003FF;
    // newY = (_sv->addrMap[(_y+1)*_sv->drawwidth + (_x-1)] >>  0) & 0x000007FF;
    // val += _src[newY * fpgaSampleCount + newX];
    newX = (_sv->addrMap[(_y+1)*_sv->drawwidth + (_x+1)] >> 11) & 0x000003FF;
    newY = (_sv->addrMap[(_y+1)*_sv->drawwidth + (_x+1)] >>  0) & 0x000007FF;
    val += _src[newY * fpgaSampleCount + newX];

    val = (int)floor((float)val / 9.0 + 0.5);

    return val;
}

/**
@fn      draw_applylogGain(uint32_t _val)
@author  Bongsung Kim
@date    2017.04.24
@brief   Log 변환 곡선을 적용한다.
@param _val : Log 변환 곡선을 적용할 raw 데이터 값(0~65535)
@return Log 변환 곡선이 적용 된 후의 결과값 ( 0~ DATA_MAX )
@warning logTableEn이 low일 때, 수식이 적용된다. 수식의 파라미터는 입력 argument로 조절 가능하다.
@bug     none
@todo    none
*/
static uint32_t draw_applylogGain(uint32_t _val)
{
    uint32_t ret = 0;

    if (logTableEn)
    {
        if (_val < VA_MAX_X)
            ret = temp_log_table[_val];
        else
            ret = DATA_MAX;
    }
    else
    {
        if (_val < threshold)
            ret = 0;
        else if((_val-threshold) < VA_LIMIT_LOG)
            ret = floor(VA_UP*log10((_val-threshold)*1.0 / VA_DOWN + 1.0) + 0.5);
        else if((_val-threshold) > VA_MAX_X)
            ret = DATA_MAX;
        else
        {
            float tempLog = floor(VA_UP*log10(VA_LIMIT_LOG / VA_DOWN + 1.0) + 0.5);
            ret = floor((DATA_MAX - tempLog) / (VA_MAX_X - VA_LIMIT_LOG)
                                * ((_val-threshold)*1.0 - VA_LIMIT_LOG) +
                                tempLog + 0.5);
        }
    }

    return ret;
}

/**
@fn      draw_colorMapping(uint32_t _val, uint8_t *_dst, uint32_t _x, uint32_t _y)
@author  Bongsung Kim
@date    2017.04.24
@brief   Color Mapping
@param   _val  : 변환할 픽셀의 값
@param   *_dst : 변환한 픽셀 값이 저장될 display 버퍼
@param   _x    : 저장될 display의 x 좌표
@param   _x    : 저장될 display의 y 좌표
@return  none
@warning _val은 raw data의 값이 아니라, color Table의 최대 개수 만큼 normalizing 된 값이다.
         여기서는 최대 767이다.
@bug     none
@todo    none
*/
static void draw_colorMapping(uint32_t _val, uint8_t *_dst, uint32_t _x, uint32_t _y)
{
    // if (_val < 512)
    // {
    //     _dst[_y*DRAWWIDTH*4 + _x*4 + 2] = _val / 2;  //R
    //     _dst[_y*DRAWWIDTH*4 + _x*4 + 1] = _val / 3;  //G
    //     _dst[_y*DRAWWIDTH*4 + _x*4 + 0] = 0;  //B
    // }
    // else
    // {
    //     _dst[_y*DRAWWIDTH*4 + _x*4 + 2] = 255;  //R
    //     int green = floor((255.0 - 170.0) / (767.0 - 512.0) * ((float)_val - 512.0) + 170.0);
    //     int blue = floor((255.0) / (767.0 - 512.0)*((float)_val - 512.0));
    //     _dst[_y*DRAWWIDTH*4 + _x*4 + 1] = green;
    //     _dst[_y*DRAWWIDTH*4 + _x*4 + 0] = blue;  //B
    // }
    // _dst[_y*DRAWWIDTH*4 + _x*4 + 2] = color_table[_val][0];  //R
    // _dst[_y*DRAWWIDTH*4 + _x*4 + 1] = color_table[_val][1];  //G
    // _dst[_y*DRAWWIDTH*4 + _x*4 + 0] = color_table[_val][2];  //B
    _dst[_y*DRAWWIDTH*4 + _x*4 + 2] = color_table[_val][0];  //R
    _dst[_y*DRAWWIDTH*4 + _x*4 + 1] = color_table[_val][1];  //G
    _dst[_y*DRAWWIDTH*4 + _x*4 + 0] = color_table[_val][2];  //B
}

/**
@fn      draw_drawLineArc(sv2D_t* _sv)
@author  Bongsung Kim
@date    2017.04.24
@brief   각 레이더의 구분선 및 거리에 따른 가이드 호를 dc_temp에 그린다.
@param   *_sv : Sonar View 구조체.
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawLineArc(sv2D_t* _sv)
{
    //256, 512, 768
    dc_temp->pen_color = gx_color(180,180,180,255);

    uint8_t numLine = _sv->getDrawLineNum(_sv);

    draw_drawLine(numLine, _sv->drawLineVector);

    draw_drawArc(_sv->lader_Xz, _sv->lader_Yz, _sv->lader_radius, _sv->maxDeg, _sv->range_meter);
}

/**
@fn      draw_drawLine(int32_t _numLine, vector_t *_vector)
@author  Bongsung Kim
@date    2017.04.24
@brief   각 레이더의 구분선을 dc_temp에 그린다.
@param   _numLine : 구분선 및 가이드 선의 최대 개수.
@param   *_vector : 각 레이더에 따른 구분선 및 가이드 선의 정보 구조체.
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawLine(int32_t _numLine, vector_t *_vector)
{
    int32_t i;
    for (i=0; i<_numLine; i++)
    {
        if (i==0 || i==(_numLine-1))
            gx_line(dc_temp, _vector[i].x0,
                             _vector[i].y0,
                             _vector[i].x1,
                             _vector[i].y1);
        else
        {
            gx_line_some(dc_temp, _vector[i].x0,
                                  _vector[i].y0,
                                  _vector[i].x1,
                                  _vector[i].y1, 10);
            gx_line_some(dc_temp, _vector[i].x1,
                                  _vector[i].y1,
                                  _vector[i].x0,
                                  _vector[i].y0, 10);
        }
    }
}

/**
@fn      draw_drawArc(uint32_t _xz, uint32_t _yz, uint32_t _r, float _deg, float _range)
@author  Bongsung Kim
@date    2017.04.24
@brief   거리에 따른 가이드 호를 dc_temp에 그린다.
@param   _xz    : 레이더를 포함하는 원의 zero point x축 좌표.
@param   _yz    : 레이더를 포함하는 원의 zero point y축 좌표.
@param   _r     : 레이더를 포함하는 원의 반지름. zero point에서 최외각 호 까지의 픽셀 수.
@param   _deg   : 가이드 호의 각도
@param   _range : 현재 영상의 최대 range
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawArc(uint32_t _xz, uint32_t _yz, uint32_t _r, float _deg, float _range)
{
    //----------Arc Draw
    gx_arc( dc_temp, _xz, _yz, _r, _deg, 0);
    gx_arc( dc_temp, _xz, _yz, (_r*(0.6/_range)), _deg, 0);

    gx_arc( dc_temp, _xz, _yz, (_r*0.75), _deg, 10);
    gx_arc( dc_temp, _xz, _yz, (_r*0.5 ), _deg, 10);
    gx_arc( dc_temp, _xz, _yz, (_r*0.3 ), _deg, 10);
}

/**
@fn      draw_drawTxt(sv2D_t *_sv)
@author  Jinmo Je
@date    2021.06.14
@brief   화면에 출력되는 글씨를 모두 dc_temp에 출력한다.
@param   *_sv : Sonar View 구조체.
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawTxt(sv2D_t *_sv)
{
    dc_temp->font_color   = gx_color( 255, 255, 255, 255);

    draw_drawNoticeChange(_sv->gainStr, _sv->rangeStr);
                                            //Distance Display
    draw_drawDistance(_sv->drawFontVector);
                                            //Gain Display
    draw_drawGain(_sv->gainMode);
                                            //save File name Display
    draw_drawSaveInfo();
                                            //FPS Display
#if DISPLAY_FPS
    draw_drawFPS();
#endif
                                            //Time Display
    if(en_RTC)
        draw_drawTime();
#if STORAGE2MAIN                           //Disk size
    save_getSaveDiskStorage();
    draw_drawDiskSize2();
#endif                                          //Water Depth & Temp. Disp.
    if (en_i2cPressCheck)
    {
        draw_drawDepth();
        draw_drawTemperature();
        draw_divingMode();
    }
    if (en_i2cMotionCheck)
    {
        draw_heading();
    }

    draw_drawgps();
}

/**
@fn      draw_drawSaveInfo()
@author  Bongsung Kim
@date    2017.04.24
@brief   f_callAvailableSaveTime의 요청이 있을 시에,
         Save File의 이름, 저장 가능 시간 정보 등을 출력한다.
@param   *_sv : Sonar View 구조체.
@return  none
@warning 저장 파일 이름은 현재 출력하지 않는다.(주석)
@bug     none
@todo    none
*/
static void draw_drawSaveInfo()
{
    if (f_callAvailableSaveTime)
    {
        //160822 저장 시 용량 확인 및 표시
        save_calcAvailableSaveTime();
        //save_getSaveDiskStorage();
        f_callAvailableSaveTime = 0;
    }

    //draw_drawDiskSize();
    draw_drawSaveTime();
    /*if (f_drawSaveFileName)
        draw_drawSaveFileName();*/
}

/**
@fn      draw_drawSaveFileName()
@author  Bongsung Kim
@date    2017.04.24
@brief   save File의 이름 dc_temp에 출력하는 함수.
@param   none
@return  none
@warning 현재 사용하지 않음.
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawSaveFileName()
{
    char temp[100];

    sprintf(temp, "File   : %s", save_getSaveFileName());
    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.556640625), (int)(DRAWHEIGHT*0.91145833), temp);
}

/**
@fn      draw_drawSaveTime()
@author  Bongsung Kim
@date    2017.04.24
@brief   save File의 이름을 출력하는 함수. dc_temp에 그림.
@param   none
@return  none
@warning 현재 사용하지 않음.
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawSaveTime()
{
    char temp[100];
    int nextInd_X;
    dc_temp->font         = fontNanum25;

    if (f_save == SAVE_START)
        dc_temp->font_color = gx_color(255,54,54,255);
    else
        dc_temp->font_color = gx_color(150,150,150,255);
    if(full_flag==0)
    {
        sprintf(temp, "%2d", save_getSaveMin());
        nextInd_X = gx_text_out( dc_temp, (int)(DRAWWIDTH*0.09765625), (int)(DRAWHEIGHT*0.11979166), temp);
        dc_temp->font         = fontNanum20;
        sprintf(temp, " : %02d", save_getSaveSec());
        nextInd_X = gx_text_out( dc_temp, nextInd_X , (int)(DRAWHEIGHT*0.11979166) , temp);
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,255,255,255);
        sprintf(temp, " | %3d", save_getAvailableSaveMin());
        nextInd_X = gx_text_out( dc_temp, nextInd_X , (int)(DRAWHEIGHT*0.11979166) , temp);
        dc_temp->font         = fontNanum20;
        sprintf(temp, " : %02d", save_getAvailableSaveSec());
        nextInd_X = gx_text_out( dc_temp, nextInd_X , (int)(DRAWHEIGHT*0.11979166) , temp);
    }
    else if(full_flag==1)
    {
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,54,54,255);
        sprintf(temp, "  STORAGE FULL");
        nextInd_X = gx_text_out( dc_temp, (int)(DRAWWIDTH*0.09765625), (int)(DRAWHEIGHT*0.11979166), temp);
    }
    else
    {
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,54,54,255);
        sprintf(temp, "  STORAGE ERROR");
        nextInd_X = gx_text_out( dc_temp, (int)(DRAWWIDTH*0.09765625), (int)(DRAWHEIGHT*0.11979166), temp);
    }
//    sprintf(temp, "  %02d:%02d | %03d:%02d", save_getSaveMin(), save_getSaveSec(),
//                                           save_getAvailableSaveMin(),
//                                           save_getAvailableSaveSec());
//    gx_text_out( dc_temp, 110 , 92 , temp);
}

/**
@fn      draw_drawSaveTime()
@author  Bongsung Kim
@date    2017.04.24
@brief   disk의 크기, 사용 가능한 크기를 화면(dc_temp)에 출력한다.
@param   none
@return  none
@warning none
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawDiskSize()
{
    char t_total[15], t_avail[15];
    char temp[20];

    if(full_flag==0)
    {
        if (diskSize > 1048576.0)
            sprintf(t_total, "%.1f G", diskSize / 1024.0 / 1024.0 );
        else if (diskSize > 1024.0)
            sprintf(t_total, "%.1f M", diskSize / 1024.0 );
        else
            sprintf(t_total, "%.1f K", diskSize );

        if (diskAvailableSize > 1048576.0)
            sprintf(t_avail, "%.1f G", diskAvailableSize / 1024.0 / 1024.0 );
        else if (diskAvailableSize > 1024.0)
            sprintf(t_avail, "%.1f M", diskAvailableSize / 1024.0 );
        else
            sprintf(t_avail, "%.1f K", diskAvailableSize );

        if(!f_change)
        {
            dc_temp->font         = fontNanum20;
            dc_temp->font_color = gx_color(0,255,0,255);
            gx_text_out( dc_temp, (int)(DRAWWIDTH*0.634765625), (int)(DRAWHEIGHT*0.625), "Disk");

            dc_temp->font         = fontNanum25;
            dc_temp->font_color = gx_color(255,255,255,255);

            sprintf(temp, "%s / %s", t_avail, t_total);
            gx_text_out( dc_temp, (int)(DRAWWIDTH*0.6640625), (int)(DRAWHEIGHT*0.6770833), temp);
        }
        else
        {
            dc_temp->font         = fontNanum20;
            dc_temp->font_color = gx_color(0,255,0,255);
            gx_text_out( dc_temp, 800, 700, "Disk");

            dc_temp->font         = fontNanum25;
            dc_temp->font_color = gx_color(255,255,255,255);

            sprintf(temp, "%s / %s", t_avail, t_total);
            gx_text_out( dc_temp, 870, 700, temp);
        }
    }
    else if(full_flag==1)
    {
        if(!f_change)
        {
            dc_temp->font         = fontNanum25;
            dc_temp->font_color = gx_color(255,54,54,255);
            sprintf(temp, "STORAGE FULL");
            gx_text_out( dc_temp, (int)(DRAWWIDTH*0.6640625), (int)(DRAWHEIGHT*0.6770833), temp);
        }
        else
        {
            dc_temp->font         = fontNanum25;
            dc_temp->font_color = gx_color(255,54,54,255);
            sprintf(temp, "STORAGE FULL");
            gx_text_out( dc_temp, 870, 700, temp);
        }
    }
    else
    {
        if(!f_change)
        {
            dc_temp->font         = fontNanum25;
            dc_temp->font_color = gx_color(255,54,54,255);
            sprintf(temp, "STORAGE ERROR");
            gx_text_out( dc_temp, (int)(DRAWWIDTH*0.6640625), (int)(DRAWHEIGHT*0.6770833), temp);
        }
        else
        {
            dc_temp->font         = fontNanum25;
            dc_temp->font_color = gx_color(255,54,54,255);
            sprintf(temp, "STORAGE ERROR");
            gx_text_out( dc_temp, 870, 700, temp);
        }
    }
}
static void draw_drawDiskSize2()
{
    char t_total[15], t_avail[15];
    char temp[20];

    if (diskSize > 1048576.0)
        sprintf(t_total, "%.1f G", diskSize / 1024.0 / 1024.0 );
    else if (diskSize > 1024.0)
        sprintf(t_total, "%.1f M", diskSize / 1024.0 );
    else
        sprintf(t_total, "%.1f K", diskSize );

    if (diskAvailableSize > 1048576.0)
        sprintf(t_avail, "%.1f G", diskAvailableSize / 1024.0 / 1024.0 );
    else if (diskAvailableSize > 1024.0)
        sprintf(t_avail, "%.1f M", diskAvailableSize / 1024.0 );
    else
        sprintf(t_avail, "%.1f K", diskAvailableSize );


    dc_temp->font         = fontNanum20;
    dc_temp->font_color = gx_color(0,255,0,255);
    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.078125), (int)(DRAWHEIGHT*0.625), "Disk");

    dc_temp->font         = fontNanum25;
    dc_temp->font_color = gx_color(255,255,255,255);

    sprintf(temp, "%s / %s", t_avail, t_total);
    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.107421875), (int)(DRAWHEIGHT*0.67708333), temp);
}

/**
@fn      draw_drawNoticeChange(char* gainStr, char* rangeStr)
@author  Bongsung Kim
@date    2017.04.24
@brief   버튼 입력 시 변경되는 gain, range 등을 화면(dc_temp)중앙에 잠시 출력한다.
@param   *gainStr  : 출력한 gain 관련 string.
@param   *rangeStr : 출력한 range 관련 string.
@return  none
@warning none
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawNoticeChange(char* gainStr, char* rangeStr)
{
    dc_temp->font         = fontNanum40;

    if(f_gainOrRange == 1)
        gx_text_out( dc_temp, (int)(DRAWWIDTH*0.41015625), (int)(DRAWHEIGHT*0.46875), gainStr);
    else if (f_gainOrRange == 2)
        gx_text_out( dc_temp, (int)(DRAWWIDTH*0.44921875), (int)(DRAWHEIGHT*0.46875), rangeStr);
    else if (f_gainOrRange == 3)
        gx_text_out( dc_temp, (int)(DRAWWIDTH*0.44921875), (int)(DRAWHEIGHT*0.46875), "Shot!!");
}

/**
@fn      draw_drawDistance(fontvector_t *_txtInfo)
@author  Bongsung Kim
@date    2017.04.24
@brief   거리 정보를 화면(dc_temp)에 출력한다.
@param   *_txtInfo : range에 따른 출력할 거리 텍스트 정보 구조체
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawDistance(fontvector_t *_txtInfo)
{
    int i;
    int meterPrintX;
    dc_temp->font         = fontNanum15;

    for (i=1;i<5;i++)
    {
        gx_text_out( dc_temp, _txtInfo[i].leftX ,
                              _txtInfo[i].leftY ,
                              _txtInfo[i].txt);
        gx_text_out( dc_temp, _txtInfo[i].rightX,
                              _txtInfo[i].rightY,
                              _txtInfo[i].txt);
    }

    dc_temp->font         = fontNanum25;

    meterPrintX = gx_text_out( dc_temp, _txtInfo[0].leftX ,
                          _txtInfo[0].leftY ,
                          _txtInfo[0].txt);
    dc_temp->font         = fontNanum20;
    gx_text_out( dc_temp, meterPrintX, _txtInfo[0].leftY, ".0M");
    dc_temp->font         = fontNanum25;
    meterPrintX = gx_text_out( dc_temp, _txtInfo[0].rightX,
                          _txtInfo[0].rightY,
                          _txtInfo[0].txt);
    dc_temp->font         = fontNanum20;
    gx_text_out( dc_temp, meterPrintX, _txtInfo[0].rightY, ".0M");
}

/**
@fn      draw_drawGain(uint8_t _gain)
@author  Bongsung Kim
@date    2017.04.24
@brief   현재 Gain 정보를 화면(dc_temp)에 출력한다.
@param   _gain : 현재 gain 모드. (1-Low 2-Middle 3-High 4-Auto)
@return  none
@warning none
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawGain(uint8_t _gain)
{
    char tempChar[100];

    int32_t labelX = 900;       // gain label 좌표
    int32_t labelY = (int)(DRAWHEIGHT*0.807291666);       // gain label 좌표
    int32_t firstSpellX = 920;  // 첫 스펠링의 좌표
    int32_t firstSpellY = (int)(DRAWHEIGHT*0.872395833);  // 첫 스펠링의 좌표
    int32_t SpellX = 920;       // 두번째 스펠링부터의 좌표
    int32_t SpellY = (int)(DRAWHEIGHT*0.872395833);       // 두번째 스펠링부터의 좌표
/*
    int32_t labelX = 930;       // gain label 좌표
    int32_t labelY = (int)(DRAWHEIGHT*0.807291666);       // gain label 좌표
    int32_t firstSpellX = 960;  // 첫 스펠링의 좌표
    int32_t firstSpellY = (int)(DRAWHEIGHT*0.872395833);  // 첫 스펠링의 좌표
    int32_t SpellX = 950;       // 두번째 스펠링부터의 좌표
    int32_t SpellY = (int)(DRAWHEIGHT*0.872395833);       // 두번째 스펠링부터의 좌표


    int32_t labelX = (int)(DRAWWIDTH*0.673828125);       // gain label 좌표
    int32_t labelY = (int)(DRAWHEIGHT*0.807291666);       // gain label 좌표
    int32_t firstSpellX = (int)(DRAWWIDTH*0.703125);  // 첫 스펠링의 좌표
    int32_t firstSpellY = (int)(DRAWHEIGHT*0.872395833);  // 첫 스펠링의 좌표
    int32_t SpellX = (int)(DRAWWIDTH*0.7421875);       // 두번째 스펠링부터의 좌표
    int32_t SpellY = (int)(DRAWHEIGHT*0.872395833);       // 두번째 스펠링부터의 좌표
*/
    dc_temp->font         = fontNanum20;
    dc_temp->font_color = gx_color(0,255,0,255);
    gx_text_out( dc_temp, labelX , labelY , "Gain");

    dc_temp->font         = fontNanum40;
    dc_temp->font_color    = gx_color(255,255,255,255);
    //Gain value Display
    switch(_gain)
    {
    case GAIN_LOW :
        sprintf(tempChar, "L");
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        dc_temp->font = fontNanum25;
        sprintf(tempChar, "ow");
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
        break;
    case GAIN_LM :
        sprintf(tempChar, "L.M.");
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        break;
    case GAIN_MIDDLE :
        sprintf(tempChar, "M");
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        dc_temp->font = fontNanum25;
        sprintf(tempChar, "iddle");
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
        break;
    case GAIN_HM :
        sprintf(tempChar, "H.M.");
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        break;
    case GAIN_HIGH :
        sprintf(tempChar, "H");
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        dc_temp->font = fontNanum25;
        sprintf(tempChar, "igh");
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
        break;
    case GAIN_AUTO :
        sprintf(tempChar, "A");
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        dc_temp->font = fontNanum25;
        sprintf(tempChar, "uto");
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
        break;
    }

}

/**
@fn      draw_drawFPS()
@author  Bongsung Kim
@date    2017.04.24
@brief   화면(dc_temp)에 현재 fps를 출력하는 함수.
@param   none
@return  none
@warning none
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawFPS()
{
    char tempChar[100];

    dc_temp->font         = fontNanum25;

    sprintf(tempChar, "F - %0.2f(%0.1fms)", fps, frameRate);
    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.673828125), (int)(DRAWHEIGHT*0.9375), tempChar);
}

/**
@fn      draw_drawTime()
@author  Bongsung Kim
@date    2017.04.24
@brief   화면(dc_temp)에 현재 RTC 시간을 출력하는 함수.
@param   none
@return  none
@warning none
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawTime()
{
    char tempChar[100];
    
    if(!f_change)
    {
        dc_temp->font         = fontNanum25;

        draw_getDateChar(tempChar);
        gx_text_out( dc_temp, (int)(DRAWWIDTH*0.72265625), (int)(DRAWHEIGHT*0.1145833), tempChar);
    }
    if (f_change)
    {
        dc_temp->font         = fontNanum40;
        dc_temp->font_color = gx_color(255,255,255,255);

        if(t_rtc.tm_hour < 12)
            sprintf(tempChar, "%04d/%02d/%02d AM %02d:%02d:%02d",
                            t_rtc.tm_year, t_rtc.tm_mon,
                            t_rtc.tm_mday, t_rtc.tm_hour,
                            t_rtc.tm_min,  t_rtc.tm_sec);
        else if (t_rtc.tm_hour == 12)
            sprintf(tempChar, "%04d/%02d/%02d PM %02d:%02d:%02d",
                                t_rtc.tm_year, t_rtc.tm_mon,
                                t_rtc.tm_mday, t_rtc.tm_hour,
                                t_rtc.tm_min,  t_rtc.tm_sec);
        else
            sprintf(tempChar, "%04d/%02d/%02d PM %02d:%02d:%02d",
                                t_rtc.tm_year, t_rtc.tm_mon,
                                t_rtc.tm_mday, t_rtc.tm_hour-12,
                                t_rtc.tm_min,  t_rtc.tm_sec);

        gx_text_out( dc_temp, (int)(200), (int)(180), tempChar);
    }
}

/**
@fn      draw_drawTimeforFirstScreen()
@author  Jinmo Je
@date    2021.06.14
@brief   첫 화면에서 화면(dc_temp)에 시간을 년월일 시분초 모두 출력하는 함수
@param   none
@return  none
@warning none
@bug     none
@todo    dc_temp의 절대 좌표를 상대 좌표로 바꿀 필요가 있음.
*/
static void draw_drawTimeforFirstScreen()
{
    char tempChar[100];

    dc_temp->font         = fontNanum20;
    dc_temp->font_color = gx_color(0,255,0,255);
    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.078125), (int)(DRAWHEIGHT*0.716145833), "Time");

    dc_temp->font         = fontNanum25;
    dc_temp->font_color = gx_color(255,255,255,255);

    if(t_rtc.tm_hour < 12)
        sprintf(tempChar, "%04d/%02d/%02d AM %02d:%02d:%02d",
                           t_rtc.tm_year, t_rtc.tm_mon,
                           t_rtc.tm_mday, t_rtc.tm_hour,
                           t_rtc.tm_min,  t_rtc.tm_sec);
    else if (t_rtc.tm_hour == 12)
        sprintf(tempChar, "%04d/%02d/%02d PM %02d:%02d:%02d",
                            t_rtc.tm_year, t_rtc.tm_mon,
                            t_rtc.tm_mday, t_rtc.tm_hour,
                            t_rtc.tm_min,  t_rtc.tm_sec);
    else
        sprintf(tempChar, "%04d/%02d/%02d PM %02d:%02d:%02d",
                            t_rtc.tm_year, t_rtc.tm_mon,
                            t_rtc.tm_mday, t_rtc.tm_hour-12,
                            t_rtc.tm_min,  t_rtc.tm_sec);

    gx_text_out( dc_temp, (int)(DRAWWIDTH*0.107421875), (int)(DRAWHEIGHT*0.7682291666), tempChar);
}

/**
@fn      draw_getDateChar(char* temp_date)
@author  Jinmo Je
@date    2021.06.14
@brief   rtc의 시간을 temp_date 버퍼에 string으로 출력.\n
         시간 형식은 AM/PM 형식.
@param   *temp_date : rtc의 시간이 이 버퍼에 출력됨.
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_getDateChar(char* temp_date)
{
    int ret;

    //sprintf(temp_date, "%02d/%02d/%02d %02d:%02d:%02d",
    //              t_rtc.tm_year-100, t_rtc.tm_mon+1, t_rtc.tm_mday,
    //              t_rtc.tm_hour,     t_rtc.tm_min,   t_rtc.tm_sec);
    if(t_rtc.tm_hour < 12)
        sprintf(temp_date, "AM %02d:%02d", t_rtc.tm_hour, t_rtc.tm_min);
    else if (t_rtc.tm_hour == 12)
        sprintf(temp_date, "PM %02d:%02d", t_rtc.tm_hour, t_rtc.tm_min);
    else
        sprintf(temp_date, "PM %02d:%02d", t_rtc.tm_hour-12, t_rtc.tm_min);
}

/**
@fn      draw_drawDepth()
@author  Jinmo Je
@date    2021.06.14
@brief   수심을 화면 (dc_temp)에 출력.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawDepth()
{
    char tempChar[100];
    int32_t labelX = 190;        // label 좌표
    int32_t labelY = (int)(DRAWHEIGHT*0.80729166);       // label 좌표
    int32_t firstSpellX = 200;  // 첫 스펠링의 좌표
    int32_t firstSpellY = (int)(DRAWHEIGHT*0.872395833);  // 첫 스펠링의 좌표
    int32_t SpellX = 200;       // 두번째 스펠링부터의 좌표
    //int32_t SpellX = 950;       // 두번째 스펠링부터의 좌표
    int32_t SpellY = (int)(DRAWHEIGHT*0.872395833);       // 두번째 스펠링부터의 좌표

/*    int32_t labelX = (int)(DRAWWIDTH*0.078125);        // label 좌표
    int32_t labelY = (int)(DRAWHEIGHT*0.80729166);       // label 좌표
    int32_t firstSpellX = (int)(DRAWWIDTH*0.107421875);  // 첫 스펠링의 좌표
    int32_t firstSpellY = (int)(DRAWHEIGHT*0.872395833);  // 첫 스펠링의 좌표
    int32_t SpellX = (int)(DRAWWIDTH*0.7421875);       // 두번째 스펠링부터의 좌표
    int32_t SpellY = (int)(DRAWHEIGHT*0.872395833);       // 두번째 스펠링부터의 좌표
*/
    float wd = main_getWaterDepth();

    if(!f_change)
    {
        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(255,0,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Depth");

        //First Spelling Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);
        sprintf(tempChar, "%2d", (int32_t)floor(wd));
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        //Remind Spelling Print
        dc_temp->font         = fontNanum25;
        sprintf(tempChar, ".%02dM", (int)floor((wd-floor(wd))*100));
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
    }
    else
    {
        labelX = 500;
        labelY = 570;
        firstSpellX;
        firstSpellY = 570;
        SpellX;
        SpellY = 570;

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        firstSpellX = gx_text_out( dc_temp, labelX , labelY , " Depth  ");

        //First Spelling Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);
        sprintf(tempChar, "%2d", (int32_t)floor(wd));
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        //Remind Spelling Print
        dc_temp->font         = fontNanum25;
        sprintf(tempChar, ".%02dM", (int)floor((wd-floor(wd))*100));
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
    }
}

/**
@fn      draw_drawTemperature()
@author  Jinmo Je
@date    2021.06.14
@brief   온도를 화면 (dc_temp)에 출력.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawTemperature()
{
    char tempChar[100];
    int32_t labelX = 360;        // label 좌표
    int32_t labelY = (int)(DRAWHEIGHT*0.80729166);       // label 좌표
    int32_t firstSpellX = 370;  // 첫 스펠링의 좌표
    int32_t firstSpellY = (int)(DRAWHEIGHT*0.872395833);  // 첫 스펠링의 좌표
    int32_t SpellX = 370;       // 두번째 스펠링부터의 좌표
    //int32_t SpellX = 950;       // 두번째 스펠링부터의 좌표
    int32_t SpellY = (int)(DRAWHEIGHT*0.872395833);       // 두번째 스펠링부터의 좌표

/*    int32_t labelX = (int)(DRAWWIDTH*0.234375);        // label 좌표
    int32_t labelY = (int)(DRAWHEIGHT*0.80729166);       // label 좌표
    int32_t firstSpellX = (int)(DRAWWIDTH*0.263671875);  // 첫 스펠링의 좌표
    int32_t firstSpellY = (int)(DRAWHEIGHT*0.872395833);  // 첫 스펠링의 좌표
    int32_t SpellX = (int)(DRAWWIDTH*0.7421875);       // 두번째 스펠링부터의 좌표
    int32_t SpellY = (int)(DRAWHEIGHT*0.872395833);       // 두번째 스펠링부터의 좌표
*/
    float tem = main_getTemp();

    if(!f_change)
    {
        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(0,255,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Temp");

        //First Spelling Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%2d", (int32_t)floor(tem));
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        //Remind Spelling Print
        dc_temp->font         = fontNanum25;
        sprintf(tempChar, ".%02d℃", (int)floor((tem-floor(tem))*100));
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
    }
    else
    {
        labelX = 200;
        labelY = 570;
        firstSpellX;
        firstSpellY = 570;
        SpellX;
        SpellY = 570;

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        firstSpellX = gx_text_out( dc_temp, labelX , labelY , " Temp  ");

        //First Spelling Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%2d", (int32_t)floor(tem));
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        //Remind Spelling Print
        dc_temp->font         = fontNanum25;
        sprintf(tempChar, ".%02d℃", (int)floor((tem-floor(tem))*100));
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);

    }
}

/**
@fn      draw_drawgps()
@author  Jinmo Je
@date    2021.07.26
@brief   GPS 값을 화면 (dc_temp)에 출력.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_drawgps()
{
    char tempChar[100];

    if(!f_change)
    {
        int32_t labelX = 190;      // label 좌표
        int32_t labelY = 280;      // label 좌표
        int32_t firstSpellX = 200; // 첫 스펠링의 좌표
        int32_t firstSpellY = 330; // 첫 스펠링의 좌표
        int32_t SpellX = 200;      // 두번째 스펠링부터의 좌표
        int32_t SpellY = 330;      // 두번째 스펠링부터의 좌표

    /*    int32_t labelX = 80;       // label 좌표
        int32_t labelY = 280;      // label 좌표
        int32_t firstSpellX = 110; // 첫 스펠링의 좌표
        int32_t firstSpellY = 330; // 첫 스펠링의 좌표
        int32_t SpellX = 100;      // 두번째 스펠링부터의 좌표
        int32_t SpellY = 330;      // 두번째 스펠링부터의 좌표
    */
        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(255,0,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Lati");

        //Min Print
        dc_temp->font         = fontNanum25;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%f", nmea_pars.m_dRMCLatitude);
        gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);

        labelX = 190;      // label 좌표
        labelY = 180;      // label 좌표
        firstSpellX = 200; // 첫 스펠링의 좌표
        firstSpellY = 230; // 첫 스펠링의 좌표
        SpellX = 200;      // 두번째 스펠링부터의 좌표
        SpellY = 230;      // 두번째 스펠링부터의 좌표

        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(255,0,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Longi");

        //Min Print
        dc_temp->font         = fontNanum25;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%f", nmea_pars.m_dRMCLongitude);
        gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
    }
    else
    {
        int32_t label1X = 200;   // label 좌표
        int32_t label1Y = 330;   // label 좌표
        int32_t Spell1X;         // 첫 스펠링의 좌표
        int32_t Spell1Y = 330;   // 첫 스펠링의 좌표
        int32_t label2X = 650;
        int32_t label2Y = 330;
        int32_t Spell2X;         // 두번째 스펠링부터의 좌표
        int32_t Spell2Y = 330;   // 두번째 스펠링부터의 좌표

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        Spell1X = gx_text_out( dc_temp, label1X , label1Y , " Longi  ");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%f", nmea_pars.m_dRMCLongitude);
        gx_text_out( dc_temp, Spell1X , Spell1Y , tempChar);

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        Spell2X = gx_text_out( dc_temp, label2X , label2Y , " Lati  ");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%f", nmea_pars.m_dRMCLatitude);
        gx_text_out( dc_temp, Spell2X , Spell2Y , tempChar);
    }
}

/**
@fn      draw_heading()
@author  Jinmo Je
@date    2021.06.14
@brief   모션 값을 화면 (dc_temp)에 출력.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_heading()
{
    char tempChar[100];

    float heading = main_getHeading();
    float pitch = main_getPitch();
    float roll = main_getRoll();

    if(!f_change)
    {
        int32_t labelX = 900;      // label 좌표
        int32_t labelY = 280;      // label 좌표
        int32_t firstSpellX = 920; // 첫 스펠링의 좌표
        int32_t firstSpellY = 330; // 첫 스펠링의 좌표
        int32_t SpellX = 920;      // 두번째 스펠링부터의 좌표
        int32_t SpellY = 330;      // 두번째 스펠링부터의 좌표

        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(255,0,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Pitch");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%.1f˚", pitch);
        gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);

        labelX = 900;      // label 좌표
        labelY = 380;      // label 좌표
        firstSpellX = 920; // 첫 스펠링의 좌표
        firstSpellY = 430; // 첫 스펠링의 좌표
        SpellX = 920;      // 두번째 스펠링부터의 좌표
        SpellY = 430;      // 두번째 스펠링부터의 좌표

        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(255,0,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Roll");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%.1f˚", roll);
        gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);

        labelX = 900;      // label 좌표
        labelY = 480;      // label 좌표
        firstSpellX = 920; // 첫 스펠링의 좌표
        firstSpellY = 530; // 첫 스펠링의 좌표
        SpellX = 920;      // 두번째 스펠링부터의 좌표
        SpellY = 530;      // 두번째 스펠링부터의 좌표

        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(255,0,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Heading");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%.1f˚", heading);
        gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
    }
    else
    {
        int32_t label1X = 200;           // label 좌표
        int32_t label1Y = 450;           // label 좌표
        int32_t Spell1X;                 // 첫 스펠링의 좌표
        int32_t Spell1Y = 450;           // 첫 스펠링의 좌표
        int32_t label2X = 500;
        int32_t label2Y = 450;
        int32_t Spell2X;                 // 두번째 스펠링부터의 좌표
        int32_t Spell2Y = 450;           // 두번째 스펠링부터의 좌표
        int32_t label3X = 815;
        int32_t label3Y = 450;
        int32_t Spell3X;
        int32_t Spell3Y = 450;

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        Spell1X = gx_text_out( dc_temp, label1X , label1Y , " Pitch  ");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%.1f˚", pitch);
        gx_text_out( dc_temp, Spell1X , Spell1Y , tempChar);

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        Spell2X = gx_text_out( dc_temp, label2X , label2Y , " Roll  ");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%.1f˚", roll);
        gx_text_out( dc_temp, Spell2X , Spell2Y , tempChar);

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        Spell3X = gx_text_out( dc_temp, label3X , label3Y , "Heading  ");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);

        sprintf(tempChar, "%.1f˚", heading);
        gx_text_out( dc_temp, Spell3X , Spell3Y , tempChar);
    }
}


/**
@fn      draw_divingMode()
@author  Bongsung Kim
@date    2017.04.24
@brief   압력 센서를 이용해 diving 중인지 여부와 diving 시간을 화면에(dc_temp) 표현한다.
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void draw_divingMode()
{
    char tempChar[100];
    int32_t labelX = 190;      // label 좌표
    int32_t labelY = 480;      // label 좌표
    int32_t firstSpellX = 200; // 첫 스펠링의 좌표
    int32_t firstSpellY = 530; // 첫 스펠링의 좌표
    int32_t SpellX = 200;      // 두번째 스펠링부터의 좌표
    int32_t SpellY = 530;      // 두번째 스펠링부터의 좌표
/*    int32_t labelX = 80;       // label 좌표
    int32_t labelY = 480;      // label 좌표
    int32_t firstSpellX = 110; // 첫 스펠링의 좌표
    int32_t firstSpellY = 530; // 첫 스펠링의 좌표
    int32_t SpellX = 100;      // 두번째 스펠링부터의 좌표
    int32_t SpellY = 530;      // 두번째 스펠링부터의 좌표 */
    int32_t dtMin = main_getDTMin();
    int32_t dtSec = main_getDTSec();

    if(!f_change)
    {
        //label Print
        dc_temp->font         = fontNanum20;
        dc_temp->font_color = gx_color(255,0,0,255);
        gx_text_out( dc_temp, labelX , labelY , "Dive Time");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);
        sprintf(tempChar, "%2d'", dtMin);
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        //Sec Print
        dc_temp->font         = fontNanum25;
        sprintf(tempChar, "%02d", dtSec);
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
    }
    else
    {
        labelX = 815;
        labelY = 570;
        firstSpellX;
        firstSpellY = 570;
        SpellX;
        SpellY = 570;

        //label Print
        dc_temp->font         = fontNanum25;
        dc_temp->font_color = gx_color(255,0,0,255);
        firstSpellX = gx_text_out( dc_temp, labelX , labelY , "Dive Time");

        //Min Print
        dc_temp->font         = fontNanum40;
        if (main_getIsDinving())
            dc_temp->font_color = gx_color(100,100,255,255);
        else
            dc_temp->font_color = gx_color(255,255,255,255);
        sprintf(tempChar, "%2d'", dtMin);
        SpellX = gx_text_out( dc_temp, firstSpellX , firstSpellY , tempChar);
        //Sec Print
        dc_temp->font         = fontNanum25;
        sprintf(tempChar, "%02d", dtSec);
        gx_text_out( dc_temp, SpellX , SpellY , tempChar);
    }
}

/**
@fn      draw_compareAndRefresh(sv2D_t *_sv)
@author  Bongsung Kim
@date    2017.04.24
@brief   거리 정보와 게인 정보가 변경 되었을때 비교하여 영상을 갱신한다. 또한 auto gain을 계산한다.
@param   *_sv : sonar View 구조체
@return  none
@warning none
@bug     none
@todo    none
*/
static int32_t draw_compareAndRefresh(sv2D_t *_sv)
{
    if (_sv->range_meter != t_range)
    {
        //f_gainOrRange = 2;
        //gainTxtKeepCnt = 0;
        if (_sv->reInit(_sv, t_range, gainMode) < 0)
        {
            BXlogPutLogMsg("[dseMain.c]Err : reInit Failed!!");
            return -1;
        }
    }
    else if (_sv->gainMode != gainMode)
    {
        //f_gainOrRange = 1;
        //gainTxtKeepCnt = 0;
        _sv->setGain(_sv, gainMode);
    }

    if (_sv->gainMode == GAIN_AUTO)
    {
        if ( _sv->setAutoGain(_sv, 0.1, 0.9) < 0)
        {
            BXlogPutLogMsg("[dseMain.c] ERR: makeAccHistogram Failed!!");
            return -1;
        }
        //printf("gain : %f\n\n" , sonarview_t->gain_value);
    }
    _sv->cnt_autogain_th_overflow = 0;

    return 0;
}
