/**
    @file       dseSonarView2D.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 레이더 변환 정보 관련 Sonar View Object 인터페이스
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
    BongsungKim/2017.04.24 : sv2D_t 구조체로 init과 address Map 생성, reInit,
                             Line Vector Init 등 설계.
    BongsungKim/2017.04.24 : sv2D_t 구조체에 gainStr, rangeStr 추가.\n
                             화면에 뿌려주기 위한 gain String과 Range String.
    BongsungKim/2017.04.24 : setGain 함수 추가. sv2D_t 에 가상함수 구현.\n
                             sv2D_t.gainMode 추가.\n
                             sonarview2D_init() 함수의 float gain을 uint8_t의 gainMode로 변경\n
                             gainMode 적용으로 하나의 키로 AUTO와 MANUAL 변경을 위해서....\n
                             gainMode 값에 따라 gain_value를 정의하도록 변경.\n
                             gainStr에 AUTO 추가.\n
                             reInit() _newGain 인수를 gainMode로 변경.
    BongsungKim/2017.04.24 : 768 3채널에 맞게 수정. 레이더 맵핑에서 continue 되는거 삭제.\n
                             M 글자 위치 변경
    BongsungKim/2017.04.25 : cpyToBufferFromAddrMap() 미사용으로 삭제.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>

#include "dseMain.h"
#include "dseSonarView2D.h"
#include "dseRomTable.h"    //로그 테이블, freq. offset, 비율에 따른 x좌표 값 롬테이블

///////////////////////////////////////////////////////////
//           Function Definition                         //
///////////////////////////////////////////////////////////
static  int32_t   getDrawLineNum        (sv2D_t *_sv2D);
static  int32_t   VariablePar_init      (sv2D_t* _sv2D,
                                         float _range, uint8_t _gainMode);
static  int32_t   setGain               (sv2D_t *_sv2D, uint8_t _newGainMode);
static  int32_t   lineVector_init       (sv2D_t* _sv2D);
static  int32_t   addrMap_init          (sv2D_t* _sv2D);
static  int32_t   reInit                (sv2D_t *_sv2D, float _newRange,
                                         uint8_t _newGainMode);
static  int32_t initHistogram           (sv2D_t* _sv2D);
static  int32_t makeAccHistogram        (sv2D_t* _sv2D);
static  float   setAutoGain             (sv2D_t* _sv2D,
                                    float _coeffLowPer, float _coeffHighPer);

sv2D_t* sonarview2D_init(/*uint16_t *RAWdata,*/
                         int32_t _drawwidth, int32_t _drawheight,
                         int32_t _lader_radius, int32_t _xz, int32_t _yz,
                         float _trunc_degree, float _onePageAngle,
                         int32_t _drawSampleCount,
                         int32_t _aver_x_ratio, int32_t _aver_y_ratio,
                         float _range_meter, uint8_t _gainMode);
void    sonarview2D_close               (sv2D_t* _sv2D);
///////////////////////////////////////////////////////////
//           Function Definition                         //
///////////////////////////////////////////////////////////

/**
@fn      getDrawLineNum(sv2D_t *_sv2D)
@author  Bongsung Kim
@date    2017.04.24
@brief   Lader영상의 Page에 따른 가이드 라인의 개수를 반환한다.
@param   _sv2D : sonar View object
@return  Lader영상의 Page에 따른 가이드 라인의 개수
@warning none
@bug     none
@todo    none
*/
static int32_t getDrawLineNum(sv2D_t *_sv2D)
{
    return _sv2D->drawSampleCount/ONEPAGE_SAMPLES+1;
}

/**
@fn      VariablePar_init(sv2D_t* _sv2D, float _range, uint8_t _gainMode)
@author  Bongsung Kim
@date    2017.04.24
@brief   range에 따른 유동 변수를 Init한다.
@param  _sv2D  : 유동 변수를 갱신할 object
@param  _range : 갱신할 range 값
@param  _gain  : 갱신할 gain 모드.
@return  성공시 0.
@warning none
@bug     none
@todo    none
*/
static int32_t VariablePar_init(sv2D_t* _sv2D, float _range, uint8_t _gainMode)
{
    _sv2D->range_meter    = _range;
    _sv2D->inputheight    = (int32_t)floor(_sv2D->range_meter * 39.37 + 0.5);
    _sv2D->lineUnder0P6M  = floor(((0.6)/_sv2D->range_meter) * _sv2D->lader_radius);
    _sv2D->gainMode       = _gainMode;
    setGain ( _sv2D, _gainMode );

    if      ( GAIN_LOW == _gainMode )
        sprintf(_sv2D->gainStr, "    LOW");
    else if ( GAIN_LM  == _gainMode )
        sprintf(_sv2D->gainStr, "     L.M.");
    else if ( GAIN_MIDDLE == _gainMode )
        sprintf(_sv2D->gainStr, "MIDDLE");
    else if ( GAIN_HM == _gainMode )
        sprintf(_sv2D->gainStr, "     H.M.");
    else if ( GAIN_HIGH == _gainMode )
        sprintf(_sv2D->gainStr, "    HIGH");
    else if ( GAIN_AUTO == _gainMode )
        sprintf(_sv2D->gainStr, "    AUTO");

    sprintf(_sv2D->rangeStr, "%4.1fM", _range);

    return 0;
}

/**
@fn      lineVector_init(sv2D_t* _sv2D)
@author  Bongsung Kim
@date    2017.04.24
@brief   Lader의 가이드 라인과 range 텍스트의 좌표, 텍스트 값을 초기화 한다.
@param  _sv2D  : 갱신할 object
@return  성공시 0. 실패시, -1.
@warning range 텍스트의 좌표 값은 절대 좌표로 calibration 되어 있음.
@bug     none
@todo    none
*/
static int32_t lineVector_init(sv2D_t* _sv2D)
{
    _sv2D->drawLineVector = (vector_t *)malloc((_sv2D->drawSampleCount/ONEPAGE_SAMPLES+1) * sizeof(vector_t));
    if (_sv2D->drawLineVector == NULL)
    {
        printf("[BXsonarview2D.c] ERR : drawLineVector Init Failed!!\n");
        return -1;
    }

    float angle = 90.0 + ((_sv2D->onePageAngle - _sv2D->trunc_degree) *
                            (_sv2D->drawSampleCount*1.0 / (ONEPAGE_SAMPLES*1.0)))/2.0;

    int32_t iter;
    for (iter = 0; iter < ( _sv2D->drawSampleCount / ONEPAGE_SAMPLES + 1); iter++ )
    {
        _sv2D->drawLineVector[iter].x0 = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter) * cos(angle*PI/180)+0.5);
        _sv2D->drawLineVector[iter].y0 = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter) * sin(angle*PI/180)+0.5);
        _sv2D->drawLineVector[iter].x1 = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*cos(angle*PI/180)+0.5);
        _sv2D->drawLineVector[iter].y1 = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*sin(angle*PI/180)+0.5);

        angle -= (_sv2D->onePageAngle - _sv2D->trunc_degree);
    }

    switch(_sv2D->drawSampleCount)
    {
        case 256 :
            _sv2D->drawFontVector[0].leftX = _sv2D->drawLineVector[0].x1-100;
            _sv2D->drawFontVector[0].leftY = _sv2D->drawLineVector[0].y1+5;
            _sv2D->drawFontVector[0].rightX = _sv2D->drawLineVector[_sv2D->drawSampleCount/ONEPAGE_SAMPLES].x1+3;
            _sv2D->drawFontVector[0].rightY = _sv2D->drawLineVector[_sv2D->drawSampleCount/ONEPAGE_SAMPLES].y1+5;
            sprintf(_sv2D->drawFontVector[0].txt,"%2d", (int)_sv2D->range_meter);
            _sv2D->drawFontVector[1].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.75*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-65;
            _sv2D->drawFontVector[1].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.75*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            _sv2D->drawFontVector[1].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.75*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+5;
            _sv2D->drawFontVector[1].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.75*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            sprintf(_sv2D->drawFontVector[1].txt,"%4.1fM",(_sv2D->range_meter*0.75));
            _sv2D->drawFontVector[2].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.5*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-65;
            _sv2D->drawFontVector[2].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.5*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            _sv2D->drawFontVector[2].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.5*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+5;
            _sv2D->drawFontVector[2].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.5*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            sprintf(_sv2D->drawFontVector[2].txt,"%4.1fM",(_sv2D->range_meter*0.5));
            _sv2D->drawFontVector[3].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.3*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-65;
            _sv2D->drawFontVector[3].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.3*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            _sv2D->drawFontVector[3].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.3*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+5;
            _sv2D->drawFontVector[3].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.3*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            sprintf(_sv2D->drawFontVector[3].txt,"%4.1fM",(_sv2D->range_meter*0.3));
            _sv2D->drawFontVector[4].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-65;
            _sv2D->drawFontVector[4].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+7;
            _sv2D->drawFontVector[4].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+9;
            _sv2D->drawFontVector[4].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+7;
            sprintf(_sv2D->drawFontVector[4].txt,"%4.1fM",(_sv2D->range_meter*(0.6/_sv2D->range_meter)));
            break;
        case 512 :
            _sv2D->drawFontVector[0].leftX = _sv2D->drawLineVector[0].x1-5;
            _sv2D->drawFontVector[0].leftY = _sv2D->drawLineVector[0].y1+35;
            _sv2D->drawFontVector[0].rightX = _sv2D->drawLineVector[_sv2D->drawSampleCount/ONEPAGE_SAMPLES].x1-30;
            _sv2D->drawFontVector[0].rightY = _sv2D->drawLineVector[_sv2D->drawSampleCount/ONEPAGE_SAMPLES].y1+35;
            sprintf(_sv2D->drawFontVector[0].txt,"%2d", (int)_sv2D->range_meter);
            _sv2D->drawFontVector[1].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.75*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-35;
            _sv2D->drawFontVector[1].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.75*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+10;
            _sv2D->drawFontVector[1].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.75*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+1;
            _sv2D->drawFontVector[1].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.75*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+10;
            sprintf(_sv2D->drawFontVector[1].txt,"%4.1fM",(_sv2D->range_meter*0.75));
            _sv2D->drawFontVector[2].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.5*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-35;
            _sv2D->drawFontVector[2].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.5*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+10;
            _sv2D->drawFontVector[2].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.5*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+1;
            _sv2D->drawFontVector[2].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.5*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+10;
            sprintf(_sv2D->drawFontVector[2].txt,"%4.1fM",(_sv2D->range_meter*0.5));
            _sv2D->drawFontVector[3].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.3*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-35;
            _sv2D->drawFontVector[3].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.3*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+10;
            _sv2D->drawFontVector[3].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.3*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+1;
            _sv2D->drawFontVector[3].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.3*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+10;
            sprintf(_sv2D->drawFontVector[3].txt,"%4.1fM",(_sv2D->range_meter*0.3));
            _sv2D->drawFontVector[4].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-37;
            _sv2D->drawFontVector[4].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+7;
            _sv2D->drawFontVector[4].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+5;
            _sv2D->drawFontVector[4].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+7;
            sprintf(_sv2D->drawFontVector[4].txt,"%4.1fM",(_sv2D->range_meter*(0.6/_sv2D->range_meter)));
            break;
        case 768 :
            _sv2D->drawFontVector[0].leftX = _sv2D->drawLineVector[0].x1;
            _sv2D->drawFontVector[0].leftY = _sv2D->drawLineVector[0].y1+30;
            _sv2D->drawFontVector[0].rightX = _sv2D->drawLineVector[_sv2D->drawSampleCount/ONEPAGE_SAMPLES].x1-90;
            _sv2D->drawFontVector[0].rightY = _sv2D->drawLineVector[_sv2D->drawSampleCount/ONEPAGE_SAMPLES].y1+30;
            sprintf(_sv2D->drawFontVector[0].txt,"%2d", (int)_sv2D->range_meter);
            _sv2D->drawFontVector[1].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.75*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-25;
            _sv2D->drawFontVector[1].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.75*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+18;
            _sv2D->drawFontVector[1].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.75*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-20;
            _sv2D->drawFontVector[1].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.75*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+18;
            sprintf(_sv2D->drawFontVector[1].txt,"%4.1fM",(_sv2D->range_meter*0.75));
            _sv2D->drawFontVector[2].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.5*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-25;
            _sv2D->drawFontVector[2].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.5*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+18;
            _sv2D->drawFontVector[2].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.5*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-20;
            _sv2D->drawFontVector[2].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.5*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+18;
            sprintf(_sv2D->drawFontVector[2].txt,"%4.1fM",(_sv2D->range_meter*0.5));
            _sv2D->drawFontVector[3].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.3*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-25;
            _sv2D->drawFontVector[3].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.3*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+18;
            _sv2D->drawFontVector[3].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*0.3*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-25;
            _sv2D->drawFontVector[3].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*0.3*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+18;
            sprintf(_sv2D->drawFontVector[3].txt,"%4.1fM",(_sv2D->range_meter*0.3));
            _sv2D->drawFontVector[4].leftX  = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*cos((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-35;
            _sv2D->drawFontVector[4].leftY  = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*sin((90.0+_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            _sv2D->drawFontVector[4].rightX = _sv2D->lader_Xz + floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*cos((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)-10;
            _sv2D->drawFontVector[4].rightY = _sv2D->lader_Yz - floor((float)_sv2D->lader_radius*(0.6/_sv2D->range_meter)*sin((90.0-_sv2D->maxDeg*1.0/2.0)*PI/180)+0.5)+15;
            sprintf(_sv2D->drawFontVector[4].txt,"%4.1fM",(_sv2D->range_meter*(0.6/_sv2D->range_meter)));
            break;
    }

    return 0;
}

/**
@fn      addrMap_init(sv2D_t* _sv2D)
@author  Bongsung Kim
@date    2017.04.24
@brief   Raw Data->Lader를 위해, Display 버퍼 영역에서 raw data방향 address map을 생성한다.
@param  _sv2D  : sonar view object
@return  성공시 0. 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t addrMap_init(sv2D_t* _sv2D)
{
    int32_t laderNum = _sv2D->drawSampleCount / ONEPAGE_SAMPLES;//Number of 128 Data
    int32_t arrangeOrder[6];

    if (_sv2D->drawSampleCount == 256)
    {
        arrangeOrder[0] = 0; arrangeOrder[3] = 4;
        arrangeOrder[1] = 1; arrangeOrder[4] = 2;
        arrangeOrder[2] = 5; arrangeOrder[5] = 0;//Allocate 128Data in the Page(Page order is 4 2 0 1 3 5)
    }
    else
    {
        arrangeOrder[0] = 3; arrangeOrder[3] = 4;
        arrangeOrder[1] = 5; arrangeOrder[4] = 2;
        arrangeOrder[2] = 1; arrangeOrder[5] = 0;//Allocate 128Data in the Page(Page order is 4 2 0 1 3 5)
    }


    int32_t x_pos_offset ;                      //x position offset
    float seta_26 = _sv2D->onePageAngle * PI /180;
    float page_from0_seta0 = (90.0 + _sv2D->onePageAngle)*PI/180.0;
    float page_from0_seta1 = (90.0)*PI/180.0;
    float page_from0_seta2 = (90.0 + 2*_sv2D->onePageAngle)*PI/180.0;
    float page_from0_seta3 = (90.0 - 1*_sv2D->onePageAngle)*PI/180.0;
    float page_from0_seta4 = (90.0 + 3*_sv2D->onePageAngle)*PI/180.0;
    float page_from0_seta5 = (90.0 - 2*_sv2D->onePageAngle)*PI/180.0;
    int32_t page = 0; float page_seta = 0.0;
    float ratio_XinArc; int32_t index_ratioX;
    //////kbs Temp Ordering

    uint32_t x, y;
    uint32_t newX, newY;
    float  maxSetaDeg = 90 + _sv2D->maxDeg / 2;
    float  lowSetaDeg = 90 - _sv2D->maxDeg / 2;
    int32_t    lineUnder0P6M = (int32_t)floor(((0.6)/_sv2D->range_meter) * _sv2D->lader_radius);
    float    onePageSeta;
    float    line_r;
    float    seta, deg, l_arc;

    onePageSeta = ( (_sv2D->onePageAngle * (_sv2D->drawSampleCount/ONEPAGE_SAMPLES))
                    /(laderNum*1.0) ) * PI /180.0;

    memset(_sv2D->addrMap, 0 , _sv2D->drawwidth*_sv2D->drawheight*sizeof(uint32_t));
    memset(_sv2D->addrMapX, 0 , _sv2D->drawwidth*_sv2D->drawheight*sizeof(uint32_t));
    memset(_sv2D->addrMapY, 0 , _sv2D->drawwidth*_sv2D->drawheight*sizeof(uint32_t));

    for ( y = _sv2D->y_draw_Sta; y <= _sv2D->lader_Yz; y++)
        for ( x = _sv2D->x_draw_Sta; x <= _sv2D->x_draw_End; x++)
        {
            line_r = sqrt( (_sv2D->lader_Xz-x)*(_sv2D->lader_Xz-x)*1.0 +
                           (_sv2D->lader_Yz-y)*(_sv2D->lader_Yz-y)*1.0 );
            seta   = acos( ((int32_t)x-(int32_t)_sv2D->lader_Xz)*1.0/line_r );
            deg    = seta / PI * 180.0;


            if ( line_r >= _sv2D->lader_radius || line_r < lineUnder0P6M ||
                 deg > maxSetaDeg || deg < lowSetaDeg)
            {
                continue;
            }


            //Start degree는 우측 x축부터 0도 1사분면->2사분면으로
            if     (deg > _sv2D->p5_rightDeg && deg < _sv2D->p5_leftDeg)
            {
                page = 5;
                page_seta = page_from0_seta5;
                seta = seta - 0.001744 * (_sv2D->trunc_degree*3)*10; //0.001744 는 0.1의 라디안값
            }
            else if (deg > _sv2D->p3_rightDeg && deg < _sv2D->p3_leftDeg)
            {
                //continue;  //2 or 3 skip
                page = 3;
                page_seta = page_from0_seta3;
                seta = seta - 0.001744 * (_sv2D->trunc_degree*2)*10;
            }
            else if (deg > _sv2D->p1_rightDeg && deg < _sv2D->p1_leftDeg)
            {
                page = 1;
                page_seta = page_from0_seta1;
                seta = seta - 0.001744 * _sv2D->trunc_degree*10;
            }
            else if (deg > _sv2D->p0_rightDeg && deg < _sv2D->p0_leftDeg)
            {
                page = 0;
                page_seta = page_from0_seta0;
                seta = seta + 0.001744 * _sv2D->trunc_degree*10;
            }
            else if (deg > _sv2D->p2_rightDeg && deg < _sv2D->p2_leftDeg)
            {
                //continue;  //2 or 3 skip
                page = 2;
                page_seta = page_from0_seta2;
                seta = seta + 0.001744 * (_sv2D->trunc_degree*2)*10;
            }
            else if (deg > _sv2D->p4_rightDeg && deg < _sv2D->p4_leftDeg)
            {
                page = 4;
                page_seta = page_from0_seta4;
                seta = seta + 0.001744 * (_sv2D->trunc_degree*3)*10;
            }
            else
            {
                //printf("%f %f %f\n",deg, _sv2D->p0_rightDeg, _sv2D->p0_leftDeg);
                continue;
            }

            x_pos_offset = arrangeOrder[page] * ONEPAGE_SAMPLES;



            l_arc = line_r * onePageSeta;   //l_arc = line_r * maxSeta;


            // 1.부채꼴에서 좌표의 호길이를 맥스에서 쓸지 로우에서 쓸지 결정
            if (page == 1 || page == 3 || page == 5 )
                ratio_XinArc = ((seta_26 - (page_seta - seta)) * line_r) / l_arc;  //Hight->Low(Left->Right) //이게 꺼꿀로 L2
            else
                ratio_XinArc = ((page_seta - seta) * line_r) / l_arc;              //Low->High(Left->Right)  //이게 정방향 L1

            // 2.128 데이터의 앞에서 l을 잴지 뒤에서 잴지 결정
            if (_sv2D->drawSampleCount == 256)
            {
                if (page == 4 || page == 3 || page == 5 )
                {
                    index_ratioX = floor((ratio_XinArc)*1000+0.5); //160616 이상한거 같아서 1-ratio수정
                    if (index_ratioX > 999) index_ratioX =999;
                    else if(index_ratioX < 0) index_ratioX = 0;

                    newX = 127-freq_consts[index_ratioX];
                    if(freq_correction==2)
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0))*_sv2D->inputheight*1.0)
                            + freq_offset_under10m[127-newX];
                    }
                    else
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0))*_sv2D->inputheight*1.0)
                            + freq_offset[127-newX];
                    }
                }
                else
                {
                    index_ratioX = floor(ratio_XinArc * 1000 + 0.5);
                    if (index_ratioX > 999) index_ratioX = 999;
                    else if(index_ratioX <0) index_ratioX = 0;

                    newX = freq_consts[index_ratioX];// + x_pos_offset;
                    if(freq_correction==2)
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0)) * _sv2D->inputheight*1.0)
                           + freq_offset_under10m[newX];
                    }
                    else
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0)) * _sv2D->inputheight*1.0)
                           + freq_offset[newX];
                    }
                }
            }
            else if (_sv2D->drawSampleCount == 768)
            {
                if (page == 2 || page == 4 || page == 5)
                {
                    index_ratioX = floor((ratio_XinArc)*1000+0.5); //160616 이상한거 같아서 1-ratio수정
                    if (index_ratioX > 999) index_ratioX =999;
                    else if(index_ratioX < 0) index_ratioX = 0;

                    newX = 127-freq_consts[index_ratioX];
                    if(freq_correction==2)
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0))*_sv2D->inputheight*1.0)
                            + freq_offset_under10m[127-newX];
                    }
                    else
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0))*_sv2D->inputheight*1.0)
                            + freq_offset[127-newX];
                    }
                }
                else
                {
                    index_ratioX = floor(ratio_XinArc * 1000 + 0.5);
                    if (index_ratioX > 999) index_ratioX = 999;
                    else if(index_ratioX <0) index_ratioX = 0;

                    newX = freq_consts[index_ratioX];// + x_pos_offset;
                    if(freq_correction==2)
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0)) * _sv2D->inputheight*1.0)
                           + freq_offset_under10m[newX];
                    }
                    else
                    {
                    newY = floor((line_r/(_sv2D->lader_radius*1.0)) * _sv2D->inputheight*1.0)
                           + freq_offset[newX];
                    }
                }
            }
            newX = newX + x_pos_offset;


            //_sv2D->addrMap[y*_sv2D->drawwidth + x] = (newX << 11) + newY;
            _sv2D->addrMapX[y*_sv2D->drawwidth + x] = newX;
            _sv2D->addrMapY[y*_sv2D->drawwidth + x] = newY;
            //printf("X Y\n");
        }

    printf("addr Make End\n");
    return 0;
}

/**
@fn      reInit(sv2D_t *_sv2D, float _newRange, uint8_t _newGainMode)
@author  Bongsung Kim
@date    2017.04.24
@brief   데이터가 바뀌었을 때, 유동 변수와 어드레스 맵을 재갱신하는 함수
@param  _sv2D        : sonar view object
@param  _newRange    : 갱신할 range.
@param  _newGainMode : 갱신할 gain mode.
@return  성공시 0. 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t reInit(sv2D_t *_sv2D, float _newRange, uint8_t _newGainMode)
{
    //유동 정보//
    VariablePar_init(_sv2D, _newRange, _newGainMode);

    //Line Vector 정보//
    if (lineVector_init(_sv2D) < 0)
    {
        printf("[BXsonarview2D.c] ERR : drawLineVector Init Failed!!\n");
        return -1;
    }

    //Addr Map 생성
    if ( addrMap_init(_sv2D) < 0 )
    {
        printf("[BXsonarview2D.c] ERR : addrMap Initialize Failed!!\n");
        return -1;
    }

    return 0;
}

/**
@fn      setGain(sv2D_t *_sv2D, uint8_t _newGainMode)
@author  Bongsung Kim
@date    2017.04.24
@brief   Gain Mode 변경 하는 함수.
@param  _sv2D        : sonar view object
@param  _newGainMode : 갱신할 gain mode.
@return  성공시 0. 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t setGain(sv2D_t *_sv2D, uint8_t _newGainMode)
{
    switch(_newGainMode)
    {
        case GAIN_AUTO    :
        _sv2D->gain_value = (float)GAIN_MIDDLE;
        sprintf(_sv2D->gainStr, "    AUTO");
        break;

        case GAIN_LOW     :
        _sv2D->gain_value = (float)GAIN_LOW;
        sprintf(_sv2D->gainStr, "    LOW");
        break;

        case GAIN_LM     :
        _sv2D->gain_value = (float)GAIN_LM;
        sprintf(_sv2D->gainStr, "     L.M.");
        break;

        case GAIN_MIDDLE  :
        _sv2D->gain_value = (float)GAIN_MIDDLE;
        sprintf(_sv2D->gainStr, "MIDDLE");
        break;

        case GAIN_HM     :
        _sv2D->gain_value = (float)GAIN_HM;
        sprintf(_sv2D->gainStr, "     H.M.");
        break;

        case GAIN_HIGH    :
        _sv2D->gain_value = (float)GAIN_HIGH;
        sprintf(_sv2D->gainStr, "    HIGH");
        break;
    }
    _sv2D->gainMode   = _newGainMode;
    glob_gain = _sv2D->gain_value;

    return 0;
}

/**
@fn      initHistogram(sv2D_t* _sv2D)
@author  Bongsung Kim
@date    2017.04.24
@brief   sv2D_t의 histogram을 0으로 초기화한다.
@param  _sv2D        : sonar view object
@return  성공시 0. 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t initHistogram(sv2D_t* _sv2D)
{
    memset(_sv2D->dataHist, 0, HIST_TOTAL_NUM * sizeof(uint32_t));
    return 0;
}

/**
@fn      makeAccHistogram(sv2D_t* _sv2D)
@author  Bongsung Kim
@date    2017.04.24
@brief   현재 프레임의 히스토그램에 대한 누적 분포도를 구한다.
@param  _sv2D        : sonar view object
@return  성공시 0. 실패시 -1.
@warning none
@bug     none
@todo    none
*/
static int32_t makeAccHistogram(sv2D_t* _sv2D)
{

    int i;
    _sv2D->accHist[0] = _sv2D->dataHist[0];
    for (i=1; i < HIST_TOTAL_NUM ; i++)
    {

        _sv2D->accHist[i] = _sv2D->accHist[i-1] + _sv2D->dataHist[i];

    }
    return 0;
}

/**
@fn      setAutoGain(sv2D_t* _sv2D, float _coeffLowPer, float _coeffHighPer)
@author  Bongsung Kim
@date    2017.04.25
@brief   현재 프레임의 히스토그램에 대한 누적 분포도를 구한다.
@param  _sv2D         : sonar view object
@param  _coeffLowPer  : ???
@param  _coeffHighPer : ???
@return  성공시 0. 실패시 -1.
@warning _coeffLowPer, _coeffHighPer 현재 사용하지 않음.
@bug     none
@todo    \n
         불필요한 매개변수 제거.\n
         cnt_autogain_th_overflow가 굳이 sv2D_t 구조체에 없어도 됨. 구조 변경 필요.
*/
static float setAutoGain(sv2D_t* _sv2D, float _coeffLowPer, float _coeffHighPer)
{
    float gain = 0;
    float th_overflow ;
    float th_underflow;

    th_overflow = AUTO_GAIN_CNT_LIMIT*1.0 + _sv2D->range_meter*30.0 + AUTO_GAIN_CNT_HYS;
    th_underflow = AUTO_GAIN_CNT_LIMIT*1.0 + _sv2D->range_meter*30.0 - AUTO_GAIN_CNT_HYS;

    if ((float)_sv2D->cnt_autogain_th_overflow > th_overflow)
    {
        if (_sv2D->gain_value > GAIN_LOW)
            _sv2D->gain_value -= 0.1;
    }
    else if((float)_sv2D->cnt_autogain_th_overflow < th_underflow)
    {
        if (_sv2D->gain_value < GAIN_HIGH)
            _sv2D->gain_value += 0.1;

    }
    else
    {

    }
    glob_gain = _sv2D->gain_value;
    // printf("th_overflow :%f th_overflow :%f\n",th_overflow,th_underflow);
    // printf("cnt_autogain_th_overflow :%d\n",_sv2D->cnt_autogain_th_overflow);
    // printf("%f\n",_sv2D->gain_value);

    // makeAccHistogram(_sv2D);

    // uint32_t totalCnt = 0;
    // totalCnt = _sv2D->accHist[HIST_TOTAL_NUM-1];

    // //uint32_t valueLow  = floor((float)totalCnt * _coeffLowPer + 0.5);
    // uint32_t ind_accStdHighVal = floor((float)totalCnt * _coeffHighPer + 0.5);
    // uint32_t pixVal = 0;        //누적분포에서 coeffHighPer 퍼센트에 해당하는 Value
    // uint32_t histModeVal = 0;   //hist Mode 값
    // uint32_t ind_histMode = 0;  // hist Mode Index;
    // int32_t  i;
    // for (i=0; i < HIST_TOTAL_NUM ; i++)
    // {
    //     if (_sv2D->dataHist[i] > histModeVal)
    //     {
    //         histModeVal = _sv2D->dataHist[i];
    //         ind_histMode = i;
    //     }

    //     if (_sv2D->accHist[i] >= ind_accStdHighVal)
    //     {
    //         pixVal = i;
    //         break;
    //     }
    // }

    // float gain = (511.0 - (float)ind_histMode);
    // if (gain < 0.f )
    //     gain = 0;
    // gain = gain/767.0 * 2 + 1;
    // if (gain > 3.0) gain = 3.0;

    // _sv2D->gain_value = gain;
    // //printf("ind_histMode : %d\n" , ind_histMode);
    return 0;
}


/**
@fn      sv2D_t* sonarview2D_init(int32_t _drawwidth, int32_t _drawheight,
                         int32_t _lader_radius, int32_t _xz, int32_t _yz,
                         float _trunc_degree, float _onePageAngle,
                         int32_t _drawSampleCount,
                         int32_t _aver_x_ratio, int32_t _aver_y_ratio,
                         float _range_meter, uint8_t _gainMode)
@author  Bongsung Kim
@date    2017.04.25
@brief   Lader에 대한 Object를 초기화 한다. 가장 처음 한번 실행해야 한다.
@param   _drawwidth       : display 버퍼의 가로 해상도
@param   _drawheight      : display 버퍼의 세로 해상도
@param   _lader_radius    : lader 반지름 픽셀 개수.
@param   _xz              : lader의 중심(원점) x좌표
@param   _yz              : lader의 중심(원점) y좌표
@param   _trunc_degree    : 각 페이지당 truncate할 각도. 각 레이더는 y축에 가까운 부분이 잘림.
@param   _onePageAngle    : 하나의 레이더 페이지의 각도(degree)
@param   _drawSampleCount : Raw Data의 한 라인 width. 샘플 데이터 개수.
@param   _aver_x_ratio    : 평균 마스크의 x축 사이즈/2. 3*3이면 1.
@param   _aver_y_ratio    : 평균 마스크의 y축 사이즈/2. 3*3이면 1.
@param   _range_meter     : 현재 프레임의 range(Meter)
@param   _gainMode        : Gain Mode
@return  성공시, Lader Object(sv2D_t*). 실패시 NULL.
@warning none
@bug     none
@todo    none
*/
sv2D_t* sonarview2D_init(int32_t _drawwidth, int32_t _drawheight,
                         int32_t _lader_radius, int32_t _xz, int32_t _yz,
                         float _trunc_degree, float _onePageAngle,
                         int32_t _drawSampleCount,
                         int32_t _aver_x_ratio, int32_t _aver_y_ratio,
                         float _range_meter, uint8_t _gainMode)
{
    sv2D_t* _sv2D;

    _sv2D = (sv2D_t *)malloc(sizeof(sv2D_t));
    if (_sv2D == NULL)
    {
        printf("[BXsonarview2D.c] Err : sonarview2D malloc Err.\n");
        return NULL;
    }
    _sv2D->drawwidth      = _drawwidth;
    _sv2D->drawheight     = _drawheight;
    _sv2D->lader_radius   = _lader_radius;
    _sv2D->lader_Xz       = _xz;
    _sv2D->lader_Yz       = _yz;
    _sv2D->trunc_degree   = _trunc_degree;
    _sv2D->onePageAngle   = _onePageAngle;
    _sv2D->drawSampleCount= _drawSampleCount;
    //_sv2D->RAWdata        = RAWdata;
    _sv2D->dataHist       = (uint32_t *)malloc(HIST_TOTAL_NUM * sizeof(uint32_t));
    memset(_sv2D->dataHist, 0, HIST_TOTAL_NUM * sizeof(uint32_t));
    _sv2D->accHist        = (uint32_t *)malloc(HIST_TOTAL_NUM * sizeof(uint32_t));
    memset(_sv2D->accHist, 0, HIST_TOTAL_NUM * sizeof(uint32_t));
    _sv2D->cnt_autogain_th_overflow = 0;

    _sv2D->p0_rightDeg  = 90 + 0 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p0_leftDeg   = 90 + 1 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p1_rightDeg  = 90 - 1 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p1_leftDeg   = 90 + 0 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p2_rightDeg  = 90 + 1 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p2_leftDeg   = 90 + 2 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p3_rightDeg  = 90 - 2 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p3_leftDeg   = 90 - 1 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p4_rightDeg  = 90 + 2 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p4_leftDeg   = 90 + 3 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p5_rightDeg  = 90 - 3 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );
    _sv2D->p5_leftDeg   = 90 - 2 * ( _sv2D->onePageAngle - _sv2D->trunc_degree );

    _sv2D->maxDeg         = ( _sv2D->onePageAngle - _sv2D->trunc_degree ) * ( _sv2D->drawSampleCount/ONEPAGE_SAMPLES );
    _sv2D->realDrawWidth  = ceil(cos((90 - _sv2D->maxDeg*1.0/2.0)/180.0 * PI) * _sv2D->lader_radius ) * 2.0;

    _sv2D->x_draw_Sta     = _sv2D->lader_Xz - _sv2D->realDrawWidth / 2;
    _sv2D->x_draw_End     = _sv2D->lader_Xz + _sv2D->realDrawWidth / 2;
    _sv2D->y_draw_Sta     = _sv2D->lader_Yz - _sv2D->lader_radius;

    _sv2D->x_ratio        = _aver_x_ratio;
    _sv2D->y_ratio        = _aver_y_ratio;

    //유동 정보//
    VariablePar_init(_sv2D, _range_meter, _gainMode);

    //Line Vector 정보//
    if (lineVector_init(_sv2D) < 0)
    {
        printf("[BXsonarview2D.c] ERR : drawLineVector Init Failed!!\n");
        return NULL;
    }

    //Addr Map 생성
    _sv2D->addrMap = (uint32_t *)malloc(_sv2D->drawwidth*_sv2D->drawheight*sizeof(uint32_t));
    _sv2D->addrMapX = (uint32_t *)malloc(_sv2D->drawwidth*_sv2D->drawheight*sizeof(uint32_t));
    _sv2D->addrMapY = (uint32_t *)malloc(_sv2D->drawwidth*_sv2D->drawheight*sizeof(uint32_t));

    if (_sv2D->addrMap == NULL)
    {
        printf("[BXsonarview2D.c] ERR : addrMap Memory alloc Err!!\n");
        return NULL;
    }
    if ( addrMap_init(_sv2D) < 0 )
    {
        printf("[BXsonarview2D.c] ERR : addrMap Initialize Failed!!\n");
        return NULL;
    }

    _sv2D->getDrawLineNum         = getDrawLineNum;
    _sv2D->reInit                 = reInit;
    _sv2D->setGain                = setGain;
    _sv2D->initHistogram          = initHistogram;
    _sv2D->setAutoGain            = setAutoGain;

    return _sv2D;
}

/**
@fn      sonarview2D_close(sv2D_t* _sv2D)
@author  Bongsung Kim
@date    2017.04.25
@brief   sonar view object를 해제한다.
@param   _sv2D : close 할 sonar view object
@return  성공시, Lader Object(sv2D_t*). 실패시 NULL.
@warning none
@bug     none
@todo    none
*/
void sonarview2D_close(sv2D_t* _sv2D)
{
    free(_sv2D);
}
