#include <stdio.h>
#include <stdlib.h>    // atof 함수가 선언된 헤더 파일
#include <string.h>

#include <time.h>
#include <sys/time.h>

#include "NMEA_Parse.h"

typedef enum NP_STATE {
	NP_STATE_SOM =				0,		// Search for start of message
	NP_STATE_CMD,						// Get command
	NP_STATE_DATA,						// Get data
	NP_STATE_CHECKSUM_1,				// Get first checksum character
	NP_STATE_CHECKSUM_2,				// get second checksum character
}NP_STATE_t;

#define MAXFIELD	25		            // maximum field length

#define PI 3.14159265358979323846

TimeStamp_t gpsset_time;

static struct timespec t_check;

void NMEA_Parse_Create(void);
void NMEA_Parse_Destroy(void);

bool NMEA_Parse_MSGin(char* msg, int32_t len);

static void NMEA_Parse_Init(void);

static bool ParseBuffer(char *pBuff, int32_t len);
static bool ProcessNMEA(char cData);
static bool ProcessCommand(char *pCommand, char *pData);

static bool GetField(char *pData, char *pField, int nFieldNum, int nMaxFieldLen);
static bool IsSatUsedInSolution(int32_t wSatID);

static void ProcessGPGGA(char *pData);
static void ProcessGPGSA(char *pData);
static void ProcessGPRMB(char *pData);
static void ProcessGPGSV(char *pData);
static void ProcessGPRMC(char *pData);
static void ProcessGPZDA(char *pData);
static void ProcessGPVTG(char *pData);
static void ProcessGPGLL(char *pData);

void NMEA_Parse_Create(void)
{
    NMEA_Parse_Init();
}

void NMEA_Parse_Destroy(void)
{
}

bool NMEA_Parse_MSGin(char* msg, int32_t len)
{
    bool ret;

    ret = ParseBuffer(msg, len);
    if(ret == false)
    {
        //printf("[NMEA_Parse_MSGin] WRONG MSG FORMAT!!!! \n%s\n", msg);
    }
    return ret;
}
static bool ParseBuffer(char *pBuff, int32_t len)
{
    int32_t i;
    bool ret;

	for( i = 0; i < len; i++ )
	{
		if(pBuff[i] == (char)0x0d || pBuff[i] == (char)0x0A)
		{
			continue;
		}
		ret = ProcessNMEA(pBuff[i]);
	}
	return ret;
}
///////////////////////////////////////////////////////////////////////////////
// ProcessNMEA: This method is the main state machine which processes individual
//				bytes from the buffer and parses a NMEA sentence. A typical
//				sentence is constructed as:
//
//					$CMD,DDDD,DDDD,....DD*CS<CR><LF>
//
//				Where:
//						'$'			HEX 24 Start of sentence
//						'CMD'		Address/NMEA command
//						',DDDD'		Zero or more data fields
//						'*CS'		Checksum field
//						<CR><LF>	Hex 0d 0A End of sentence
//
//				When a valid sentence is received, this function sends the
//				NMEA command and data to the ProcessCommand method for
//				individual field parsing.
//
//				NOTE:
//
///////////////////////////////////////////////////////////////////////////////
typedef struct Parse_tool
{
	NP_STATE_t m_nState;            // Current state protocol parser is in
	int8_t     m_8Checksum;         // Calculated NMEA sentence checksum
	int8_t     m_8ReceivedChecksum; // Received NMEA sentence checksum (if exists)
	int32_t    m_32Index;           // Index used for command and data
} Parse_tool_t;

static bool ProcessNMEA(char cData)
{
    static Parse_tool_t p_tool;

    bool ret;

	switch(p_tool.m_nState)
	{
		///////////////////////////////////////////////////////////////////////
		// Search for start of message '$'
		case NP_STATE_SOM :
			if(cData == '$')
			{
				p_tool.m_8Checksum = 0;			// reset checksum
				p_tool.m_32Index = 0;				// reset index
				p_tool.m_nState = NP_STATE_CMD;
			}
		break;

		///////////////////////////////////////////////////////////////////////
		// Retrieve command (NMEA Address)
		case NP_STATE_CMD :
			if(cData != ',' && cData != '*')
			{
				nmea_pars.m_pCommand[p_tool.m_32Index++] = cData;
				p_tool.m_8Checksum ^= cData;

				// Check for command overflow
				if(p_tool.m_32Index >= NP_MAX_CMD_LEN)
				{
					p_tool.m_nState = NP_STATE_SOM;
					break;
				}
			}
			else
			{
				nmea_pars.m_pCommand[p_tool.m_32Index] = '\0';	// terminate command
				p_tool.m_8Checksum ^= cData;
				p_tool.m_32Index = 0;
				p_tool.m_nState = NP_STATE_DATA;		// goto get data state
			}
		break;

		///////////////////////////////////////////////////////////////////////
		// Store data and check for end of sentence or checksum flag
		case NP_STATE_DATA :
			if(cData == '*') // checksum flag?
			{
				nmea_pars.m_pData[p_tool.m_32Index] = '\0';
				p_tool.m_nState = NP_STATE_CHECKSUM_1;
			}
			else // no checksum flag, store data
			{
				//
				// Check for end of sentence with no checksum
				//
				if(cData == '\r')
				{
					nmea_pars.m_pData[p_tool.m_32Index] = '\0';
					ret = ProcessCommand(nmea_pars.m_pCommand, nmea_pars.m_pData);
					p_tool.m_nState = NP_STATE_SOM;
					return ret;
				}

				//
				// Store data and calculate checksum
				//
				p_tool.m_8Checksum ^= cData;
				nmea_pars.m_pData[p_tool.m_32Index] = cData;
				if(++p_tool.m_32Index >= NP_MAX_DATA_LEN) // Check for buffer overflow
				{
					p_tool.m_nState = NP_STATE_SOM;
					break;
				}
			}
		break;

		///////////////////////////////////////////////////////////////////////
		case NP_STATE_CHECKSUM_1 :
			if( (cData - '0') <= 9)
			{
				p_tool.m_8ReceivedChecksum = (cData - '0') << 4;
			}
			else
			{
				p_tool.m_8ReceivedChecksum = (cData - 'A' + 10) << 4;
			}

			p_tool.m_nState = NP_STATE_CHECKSUM_2;
		break;

		///////////////////////////////////////////////////////////////////////
		case NP_STATE_CHECKSUM_2 :
			if( (cData - '0') <= 9)
			{
				p_tool.m_8ReceivedChecksum |= (cData - '0');
			}
			else
			{
				p_tool.m_8ReceivedChecksum |= (cData - 'A' + 10);
			}
			p_tool.m_nState = NP_STATE_SOM;

			if(p_tool.m_8Checksum == p_tool.m_8ReceivedChecksum)
			{
				ret = ProcessCommand(nmea_pars.m_pCommand, nmea_pars.m_pData);
                return ret;
			}
		break;

		///////////////////////////////////////////////////////////////////////
		default : p_tool.m_nState = NP_STATE_SOM;
        break;
	}
    return false;
}
///////////////////////////////////////////////////////////////////////////////
// Process NMEA sentence - Use the NMEA address (*pCommand) and call the
// appropriate sentense data prossor.
///////////////////////////////////////////////////////////////////////////////
static bool ProcessCommand(char *pCommand, char *pData)
{
	bool retu = false;
	int ret = 0;
	static unsigned int gps_time_counts = 0;
	static int time_change = 0, time_change_HOURS = 0, time_change_DAYS = 0;

	// GPGGA
	if( strcmp((char *)(pCommand + 2), "GGA") == 0 )
	{
		ProcessGPGGA(pData);
	    nmea_pars.m_32CommandCount++;
        // printf("GGA!\n");
	    retu = true;
		time_change_HOURS = 1;
	}

	// GPGSA
	else if( strcmp((char *)(pCommand + 2), "GSA") == 0 )
	{
		ProcessGPGSA(pData);
	    nmea_pars.m_32CommandCount++;
        // printf("GSA!\n");
	    retu = true;
	}

	// GPGSV
	else if( strcmp((char *)(pCommand + 2), "GSV") == 0 )
	{
		ProcessGPGSV(pData);
	    nmea_pars.m_32CommandCount++;
        // printf("GSV!\n");
	    retu = true;
	}

	// GPRMB
	else if( strcmp((char *)(pCommand + 2), "RMB") == 0 )
	{
		ProcessGPRMB(pData);
	    nmea_pars.m_32CommandCount++;
        // printf("RMB!\n");
	    retu = true;
	}

	// GPRMC
	else if( strcmp((char *)(pCommand + 2), "RMC") == 0 )
	{
        // printf("RMC!\n");
		ProcessGPRMC(pData);
	    nmea_pars.m_32CommandCount++;
	    retu = true;
		time_change = 1;
	}

	// GPZDA
	else if( strcmp((char *)(pCommand + 2), "ZDA") == 0 )
	{
		ProcessGPZDA(pData);
	    nmea_pars.m_32CommandCount++;
        // printf("ZDA!\n");
	    retu = true;
		time_change_DAYS = 1;
	}

	// GPVTG
	else if( strcmp((char *)(pCommand + 2), "VTG") == 0 )
	{
		ProcessGPVTG(pData);
	    nmea_pars.m_32CommandCount++;
        // printf("VTG!\n");
	    retu = true;
	}

	// GPGLL
	else if( strcmp((char *)(pCommand + 2), "GLL") == 0 )
	{
		ProcessGPGLL(pData);
	    nmea_pars.m_32CommandCount++;
        // printf("GLL!\n");
	    retu = true;
		time_change_HOURS = 1;
	}

	if(time_change == 1 || (time_change_DAYS == 1 && time_change_HOURS == 1))
	{
		if (!gps_time_counts && retu == true)
		{
			//ret = TimeStamp_SetRTCtime(gpsset_time);
			//if (ret < 0)
			//	gps_time_counts = 5 * GPS_HZ;      /* retry after 5sec */
			//else
			//	gps_time_counts = 3600 * GPS_HZ;   /* set rtc-time after 1 hour */
		}
		else
			gps_time_counts--;
		time_change = 0;
		time_change_DAYS = 0;
		time_change_HOURS = 0;
	}

	return retu;
}

///////////////////////////////////////////////////////////////////////////////
// NMEA_Parse_Init: Initializing all NMEA data to start-up default values.
///////////////////////////////////////////////////////////////////////////////
void NMEA_Parse_Init(void)
{
	int i;

	// GPGGA Data
	nmea_pars.m_8GGAHour                = 0;    // GPS Hour value
	nmea_pars.m_8GGAMinute              = 0;    // GPS Minute value
	nmea_pars.m_8GGASecond              = 0;    // GPS Second value
	nmea_pars.m_16GGAMilliSecond        = 0;    // GPS MilliSecond value

	nmea_pars.m_dGGALatitude            = 0.0;  // South( Latitude < 0 ), North( Latitude > 0 )
	nmea_pars.m_dGGALongitude 		    = 0.0;  // West( Longitude < 0 ), East( Longitude > 0 )
	nmea_pars.m_8GGAGPSQuality          = 0;    // 0 = fix not available, 1 = GPS sps mode, 2 = Differential GPS, SPS mode, fix valid, 3 = GPS PPS mode, fix valid
	nmea_pars.m_8GGANumOfSatsInUse      = 0;    // Satellites being used ( 0 ~ 12 )
	nmea_pars.m_dGGAHDOP                = 0.0;  // Horizontal dilution of precision
	nmea_pars.m_dGGAAltitude            = 0.0;  // Altitude: mean-sea-level (geoid) meters, according to WGS-84 ellipsoid

	nmea_pars.m_32GGACount              = 0;    // Count of GGA get from the start of the system code
	nmea_pars.m_32GGAOldVSpeedSeconds   = 0;    // For calculation of "Vertical Speed", One time before present GGA time in second
	nmea_pars.m_dGGAOldVSpeedAlt        = 0.0;  // For calculation of "Vertical Speed", One time before present GGA Alititude
	nmea_pars.m_dGGAVertSpeed           = 0.0;  // "Vertical Speed" from calculation

	// GPGSA
	nmea_pars.m_cGSAMode                   = 'M';  // M = manual, A = automatic 2D/3D
	nmea_pars.m_8GSAFixMode                = 1;    // 1 = fix not available, 2 = 2D, 3 = 3D
	for(i = 0; i < NP_MAX_CHAN; i++)
	{
		nmea_pars.m_32GSASatsInSolution[i] = 0;    // ID of sats in solution
	}
	nmea_pars.m_dGSAPDOP                   = 0.0;  // Position dilution of precision
	nmea_pars.m_dGSAHDOP                   = 0.0;  // Horizontal dilution of precision
	nmea_pars.m_dGSAVDOP                   = 0.0;  // Vertical dilution of precision
	nmea_pars.m_32GSACount                 = 0;    // Count of GSA get from the start of the system code

	// GPGSV
	nmea_pars.m_8GSVTotalNumOfMsg                   = 0;     // Not use
	nmea_pars.m_32GSVTotalNumSatsInView             = 0;     // Number of satelite in view
	for(i = 0; i < NP_MAX_CHAN; i++)
	{
		nmea_pars.m_GSVSatInfo[i].m_32PRN           = 0;     // Informations of satelites
		nmea_pars.m_GSVSatInfo[i].m_32SignalQuality = 0;
		nmea_pars.m_GSVSatInfo[i].m_8UsedInSolution = false;
		nmea_pars.m_GSVSatInfo[i].m_32Azimuth       = 0;
		nmea_pars.m_GSVSatInfo[i].m_32Elevation     = 0;
	}
	nmea_pars.m_32GSVCount                          = 0;     // Count of GSV get from the start of the system code

	// GPRMB
	nmea_pars.m_cRMBDataStatus          = 'V';  // A = data valid, V = navigation receiver warning
	nmea_pars.m_dRMBCrosstrackError     = 0.0;  // Crosstrack error in nautical miles
	nmea_pars.m_cRMBDirectionToSteer    = '?';  // Direction to steer (L or R) to correct error
	nmea_pars.m_cRMBOriginWaypoint[0]   = '\0'; // Origin Waypoint ID
	nmea_pars.m_cRMBDestWaypoint[0]     = '\0'; // Destination waypoint ID
	nmea_pars.m_dRMBDestLatitude        = 0.0;  // destination waypoint latitude
	nmea_pars.m_dRMBDestLongitude       = 0.0;  // destination waypoint longitude
	nmea_pars.m_dRMBRangeToDest         = 0.0;  // Range to destination nautical mi
	nmea_pars.m_dRMBBearingToDest       = 0.0;  // Bearing to destination, degrees true
	nmea_pars.m_dRMBDestClosingVelocity = 0.0;  // Destination closing velocity, knots
	nmea_pars.m_cRMBArrivalStatus       = 'V';  // A = arrival circle entered, V = not entered
	nmea_pars.m_32RMBCount              = 0;    // Count of RMB get from the start of the system code

	// GPRMC
	nmea_pars.m_8RMCHour        = 0;    // GPS Hour value
	nmea_pars.m_8RMCMinute      = 0;    // GPS Minute value
	nmea_pars.m_8RMCSecond      = 0;    // GPS Second value
    nmea_pars.m_16RMCMillisecond = 0;   // GPS MilliSecond value

	nmea_pars.m_cRMCDataValid   = 'V';  // A = Data valid, V = navigation rx warning
	nmea_pars.m_dRMCLatitude    = 0.0;  // current latitude
	nmea_pars.m_dRMCLongitude   = 0.0;  // current longitude
	nmea_pars.m_dRMCGroundSpeed = 0.0;  // speed over ground, knots
	nmea_pars.m_dRMCCourse      = 0.0;  // course over ground, degrees true

	nmea_pars.m_8RMCDay         = 1;    // GPS Day value
	nmea_pars.m_8RMCMonth       = 1;    // GPS Month value
	nmea_pars.m_32RMCYear       = 2000; // GPS Year value

	nmea_pars.m_dRMCMagVar      = 0.0;  // magnetic variation, degrees East(+)/West(-)
	nmea_pars.m_32RMCCount      = 0;    // Count of RMC get from the start of the system code

	// GPZDA
	nmea_pars.m_8ZDAHour            = 0;    // GPS Hour value
	nmea_pars.m_8ZDAMinute          = 0;    // GPS Minute value
	nmea_pars.m_8ZDASecond          = 0;    // GPS Second value
	nmea_pars.m_8ZDADay             = 1;    // GPS Day value
	nmea_pars.m_8ZDAMonth           = 1;    // GPS Month value
	nmea_pars.m_8ZDAYear            = 2000; // GPS Year value

	nmea_pars.m_8ZDALocalZoneHour   = 0;    // Local Time(hour)
	nmea_pars.m_8ZDALocalZoneMinute = 0;    // Local Time(minute)
	nmea_pars.m_32ZDACount          = 0 ;   // Count of ZDA get from the start of the system code

	// GPVTG
	nmea_pars.m_dVTGTrueTrack       = 0.0;  // True Heading
	nmea_pars.m_dVTGMagneticTrack   = 0.0;  // Magnetic heading
	nmea_pars.m_dVTGSpeedKnots      = 0.0;  // GPS Horizontal Speed (knots)
	nmea_pars.m_dVTGSpeedKm         = 0.0;  // GPS Horizontal Speed (km/h)
    nmea_pars.m_32VTGCount          = 0;    // Count of VTG get from the start of the system code

	// GPGLL
	nmea_pars.m_dGLLLatitude    = 0.0;  // current latitude
	nmea_pars.m_dGLLLongitude   = 0.0;  // current longitude

	nmea_pars.m_8GLLHour        = 0;    // GPS Hour value
	nmea_pars.m_8GLLMinute      = 0;    // GPS Minute value
	nmea_pars.m_8GLLSecond      = 0;    // GPS Second value
    nmea_pars.m_16GLLMillisecond = 0;   // GPS MilliSecond value

	nmea_pars.m_cGLLDataValid   = 'V';  // A = Data valid, V = navigation rx warning
}

///////////////////////////////////////////////////////////////////////////////
// Name:		GetField
//
// Description:	This function will get the specified field in a NMEA string.
//
// Entry:		char *pData -		Pointer to NMEA string
//				char *pField -		pointer to returned field
//				int nfieldNum -		Field offset to get
//				int nMaxFieldLen -	Maximum of bytes pFiled can handle
///////////////////////////////////////////////////////////////////////////////
static bool GetField(char *pData, char *pField, int nFieldNum, int nMaxFieldLen)
{
	// Validate params
	if(pData == NULL || pField == NULL || nMaxFieldLen <= 0)
	{
		return false;
	}

	// printf("pData1 : %s\n", pData);

	// Go to the beginning of the selected field
	int i = 0;
	int nField = 0;
	while(nField != nFieldNum && pData[i])
	{
		if(pData[i] == ',')
		{
			nField++;
		}

		i++;

		if(pData[i] == 0)
		{
			pField[0] = '\0';
			return false;
		}
	}

	if(pData[i] == ',' || pData[i] == '*')
	{
		pField[0] = '\0';
		return false;
	}

	// printf("i %d(%c)\n", i, pData[i]);
	// copy field from pData to Field
	int i2 = 0;
	while(pData[i] != ',' && pData[i] != '*' && pData[i])
	{
		// memcpy(&pField[i2], &pData[i], 1);
		pField[i2] = pData[i];
		// printf("%d:%c, %d,%c\n", i2, pField[i2], i, pData[i]);
		i2++; i++;

		// check if field is too big to fit on passed parameter. If it is,
		// crop returned field to its max length.
		if(i2 >= nMaxFieldLen)
		{
			i2 = nMaxFieldLen-1;
			break;
		}
	}
	pField[i2] = '\0';

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Check to see if supplied satellite ID is used in the GPS solution.
// Retruned:	BOOL -	TRUE if satellate ID is used in solution
//						FALSE if not used in solution.
///////////////////////////////////////////////////////////////////////////////
static bool IsSatUsedInSolution(int32_t wSatID)
{
	int i;
	if(wSatID == 0) return false;
	for(i = 0; i < 12; i++)
	{
		if(wSatID == nmea_pars.m_32GSASatsInSolution[i])
		{
			return true;
		}
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
static void ProcessGPGGA(char *pData)
{
	char pField[MAXFIELD];
	char pBuff[10];

    // printf("%s\n", pData);
    // printf("%s\n", pField);
	// Time
	if(GetField(pData, pField, 0, MAXFIELD))
	{
    // printf("%s\n", pField);
		// Hour
		pBuff[0] = pField[0];
		pBuff[1] = pField[1];
		pBuff[2] = '\0';
		nmea_pars.m_8GGAHour = atoi(pBuff);
		gpsset_time.hour = (int32_t)nmea_pars.m_8GGAHour;

		// minute
		pBuff[0] = pField[2];
		pBuff[1] = pField[3];
		pBuff[2] = '\0';
		nmea_pars.m_8GGAMinute = atoi(pBuff);
		gpsset_time.min = (int32_t)nmea_pars.m_8GGAMinute;

		// Second
		pBuff[0] = pField[4];
		pBuff[1] = pField[5];
		pBuff[2] = '\0';
		nmea_pars.m_8GGASecond = atoi(pBuff);
		gpsset_time.sec = (int32_t)nmea_pars.m_8GGASecond;

		/// MilliSecond
		pBuff[0] = pField[7];
		pBuff[1] = pField[8];
		pBuff[2] = pField[9];
		pBuff[3] = '\0';
		nmea_pars.m_16GGAMilliSecond = atoi(pBuff);
		gpsset_time.usec = (int32_t)(nmea_pars.m_16GGAMilliSecond * MILI_SEC);
	}

	// Latitude
	if(GetField(pData, pField, 1, MAXFIELD))
	{
		nmea_pars.m_dGGALatitude = atof((char *)pField+2) / 60.0;
		pField[2] = '\0';
		nmea_pars.m_dGGALatitude += atof((char *)pField);
	}
	if(GetField(pData, pField, 2, MAXFIELD))
	{
		if(pField[0] == 'S')
		{
			nmea_pars.m_dGGALatitude = -nmea_pars.m_dGGALatitude;
		}
	}

	// Longitude
	if(GetField(pData, pField, 3, MAXFIELD))
	{
		nmea_pars.m_dGGALongitude = atof((char *)pField+3) / 60.0;
		pField[3] = '\0';
		nmea_pars.m_dGGALongitude += atof((char *)pField);
	}
	if(GetField(pData, pField, 4, MAXFIELD))
	{
		if(pField[0] == 'W')
		{
			nmea_pars.m_dGGALongitude = -nmea_pars.m_dGGALongitude;
		}
	}

    // GPS quality
	if(GetField(pData, pField, 5, MAXFIELD))
	{
		nmea_pars.m_8GGAGPSQuality = pField[0] - '0';
	}

	// Satellites in use
	if(GetField(pData, pField, 6, MAXFIELD))
	{
		pBuff[0] = pField[0];
		pBuff[1] = pField[1];
		pBuff[2] = '\0';
		nmea_pars.m_8GGANumOfSatsInUse = atoi(pBuff);
	}

	// HDOP
	if(GetField(pData, pField, 7, MAXFIELD))
	{
		nmea_pars.m_dGGAHDOP = atof((char *)pField);
	}

	// Altitude
	if(GetField(pData, pField, 8, MAXFIELD))
	{
		nmea_pars.m_dGGAAltitude = atof((char *)pField);
	}

	// Durive vertical speed (bonus)
	int nSeconds = (int)nmea_pars.m_8GGAMinute * 60 + (int)nmea_pars.m_8GGASecond;
	if(nSeconds > nmea_pars.m_32GGAOldVSpeedSeconds)
	{
		double dDiff = (double)(nmea_pars.m_32GGAOldVSpeedSeconds-nSeconds);
		double dVal = dDiff/60.0;
		if(dVal != 0.0)
		{
			nmea_pars.m_dGGAVertSpeed = (nmea_pars.m_dGGAOldVSpeedAlt - nmea_pars.m_dGGAAltitude) / dVal;
		}
	}
	nmea_pars.m_dGGAOldVSpeedAlt = nmea_pars.m_dGGAAltitude;
	nmea_pars.m_32GGAOldVSpeedSeconds = nSeconds;

	nmea_pars.m_32GGACount++;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
static void ProcessGPGSA(char *pData)
{
	char pField[MAXFIELD];
	char pBuff[10];

    // printf("%s\n", pData);
	// Mode
	if(GetField(pData, pField, 0, MAXFIELD))
	{
    // printf("%s\n", pField);
		nmea_pars.m_cGSAMode = pField[0];
	}

	// Fix Mode
	if(GetField(pData, pField, 1, MAXFIELD))
	{
		nmea_pars.m_8GSAFixMode = pField[0] - '0';
	}
	int i;
	// Active satellites
	for(i = 0; i < 12; i++)
	{
		if(GetField(pData, pField, 2 + i, MAXFIELD))
		{
			pBuff[0] = pField[0];
			pBuff[1] = pField[1];
			pBuff[2] = '\0';
			nmea_pars.m_32GSASatsInSolution[i] = atoi(pBuff);
		}
		else
		{
			nmea_pars.m_32GSASatsInSolution[i] = 0;
		}
	}

	// PDOP
	if(GetField(pData, pField, 14, MAXFIELD))
	{
		nmea_pars.m_dGSAPDOP = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dGSAPDOP = 0.0;
	}

	// HDOP
	if(GetField(pData, pField, 15, MAXFIELD))
	{
		nmea_pars.m_dGSAHDOP = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dGSAHDOP = 0.0;
	}

	// VDOP
	if(GetField(pData, pField, 16, MAXFIELD))
	{
		nmea_pars.m_dGSAVDOP = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dGSAVDOP = 0.0;
	}

	nmea_pars.m_32GSACount++;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
static void ProcessGPGSV(char *pData)
{
	int32_t nTotalNumOfMsg, nMsgNum;
	char pField[MAXFIELD];

	//
	// Total number of messages
	//
	if(GetField(pData, pField, 0, MAXFIELD))
	{
		nTotalNumOfMsg = atoi((char *)pField);

		//
		// Make sure that the nTotalNumOfMsg is valid. This is used to
		// calculate indexes into an array. I've seen corrept NMEA strings
		// with no checksum set this to large values.
		//
		if(nTotalNumOfMsg > 9 || nTotalNumOfMsg < 0) return;
	}
	if(nTotalNumOfMsg < 1 || nTotalNumOfMsg*4 >= NP_MAX_CHAN)
	{
		return;
	}

	//
	// message number
	//
	if(GetField(pData, pField, 1, MAXFIELD))
	{
		nMsgNum = atoi((char *)pField);

		//
		// Make sure that the message number is valid. This is used to
		// calculate indexes into an array
		//
		if(nMsgNum > 9 || nMsgNum < 0) return;
	}

	//
	// Total satellites in view
	//
	if(GetField(pData, pField, 2, MAXFIELD))
	{
		nmea_pars.m_32GSVTotalNumSatsInView = atoi((char *)pField);
	}

	//
	// Satelite data
	//
	int i;
	for(i = 0; i < 4; i++)
	{
		// Satellite ID
		if(GetField(pData, pField, 3 + 4*i, MAXFIELD))
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32PRN = atoi((char *)pField);
		}
		else
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32PRN = 0;
		}

		// Elevarion
		if(GetField(pData, pField, 4 + 4*i, MAXFIELD))
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32Elevation = atoi((char *)pField);
		}
		else
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32Elevation = 0;
		}

		// Azimuth
		if(GetField(pData, pField, 5 + 4*i, MAXFIELD))
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32Azimuth = atoi((char *)pField);
		}
		else
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32Azimuth = 0;
		}

		// SNR
		if(GetField(pData, pField, 6 + 4*i, MAXFIELD))
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32SignalQuality = atoi((char *)pField);
		}
		else
		{
			nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32SignalQuality = 0;
		}

		//
		// Update "used in solution" (m_bUsedInSolution) flag. This is base
		// on the GSA message and is an added convenience for post processing
		//
		nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_8UsedInSolution =
                                IsSatUsedInSolution(nmea_pars.m_GSVSatInfo[i+(nMsgNum-1)*4].m_32PRN);
	}

	nmea_pars.m_32GSVCount++;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
static void ProcessGPRMB(char *pData)
{
	char pField[MAXFIELD];

    // printf("%s\n", pData);
	// Data status
	if(GetField(pData, pField, 0, MAXFIELD))
	{
    // printf("%s\n", pField);
		nmea_pars.m_cRMBDataStatus = pField[0];
	}
	else
	{
		nmea_pars.m_cRMBDataStatus = 'V';
	}

	//
	// Cross track error
	//
	if(GetField(pData, pField, 1, MAXFIELD))
	{
		nmea_pars.m_dRMBCrosstrackError = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dRMBCrosstrackError = 0.0;
	}

	//
	// Direction to steer
	//
	if(GetField(pData, pField, 2, MAXFIELD))
	{
		nmea_pars.m_cRMBDirectionToSteer = pField[0];
	}
	else
	{
		nmea_pars.m_cRMBDirectionToSteer = '?';
	}

	//
	// Orgin waypoint ID
	//
	if(GetField(pData, pField, 3, MAXFIELD))
	{
		strcpy(nmea_pars.m_cRMBOriginWaypoint, (char *)pField);
	}
	else
	{
		nmea_pars.m_cRMBOriginWaypoint[0] = '\0';
	}

	//
	// Destination waypoint ID
	//
	if(GetField(pData, pField, 4, MAXFIELD))
	{
		strcpy(nmea_pars.m_cRMBDestWaypoint, (char *)pField);
	}
	else
	{
		nmea_pars.m_cRMBDestWaypoint[0] = '\0';
	}

	//
	// Destination latitude
	//
	if(GetField(pData, pField, 5, MAXFIELD))
	{
		nmea_pars.m_dRMBDestLatitude = atof((char *)pField+2) / 60.0;
		pField[2] = '\0';
		nmea_pars.m_dRMBDestLatitude += atof((char *)pField);

	}
	if(GetField(pData, pField, 6, MAXFIELD))
	{
		if(pField[0] == 'S')
		{
			nmea_pars.m_dRMBDestLatitude = -nmea_pars.m_dRMBDestLatitude;
		}
	}

	//
	// Destination Longitude
	//
	if(GetField(pData, pField, 7, MAXFIELD))
	{
		nmea_pars.m_dRMBDestLongitude = atof((char *)pField+3) / 60.0;
		pField[3] = '\0';
		nmea_pars.m_dRMBDestLongitude += atof((char *)pField);
	}
	if(GetField(pData, pField, 8, MAXFIELD))
	{
		if(pField[0] == 'W')
		{
			nmea_pars.m_dRMBDestLongitude = -nmea_pars.m_dRMBDestLongitude;
		}
	}

	//
	// Range to destination nautical mi
	//
	if(GetField(pData, pField, 9, MAXFIELD))
	{
		nmea_pars.m_dRMBRangeToDest = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dRMBCrosstrackError = 0.0;
	}

	//
	// Bearing to destination degrees true
	//
	if(GetField(pData, pField, 10, MAXFIELD))
	{
		nmea_pars.m_dRMBBearingToDest = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dRMBBearingToDest = 0.0;
	}

	//
	// Closing velocity
	//
	if(GetField(pData, pField, 11, MAXFIELD))
	{
		nmea_pars.m_dRMBDestClosingVelocity = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dRMBDestClosingVelocity = 0.0;
	}

	//
	// Arrival status
	//
	if(GetField(pData, pField, 12, MAXFIELD))
	{
		nmea_pars.m_cRMBArrivalStatus = pField[0];
	}
	else
	{
		nmea_pars.m_dRMBDestClosingVelocity = 'V';
	}

	nmea_pars.m_32RMBCount++;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
static void ProcessGPRMC(char *pData)
{
	char pBuff[10];
	char pField[MAXFIELD];

    // printf("1 %s\n", pData);
	// Time
	if(GetField(pData, pField, 0, MAXFIELD))
	{
		// Hour
		pBuff[0] = pField[0];
		pBuff[1] = pField[1];
		pBuff[2] = '\0';
		nmea_pars.m_8RMCHour = atoi(pBuff);
		gpsset_time.hour = (int32_t)nmea_pars.m_8RMCHour;

		// minute
		pBuff[0] = pField[2];
		pBuff[1] = pField[3];
		pBuff[2] = '\0';
		nmea_pars.m_8RMCMinute = atoi(pBuff);
		gpsset_time.min = (int32_t)nmea_pars.m_8RMCMinute;

		// Second
		pBuff[0] = pField[4];
		pBuff[1] = pField[5];
		pBuff[2] = '\0';
		nmea_pars.m_8RMCSecond = atoi(pBuff);
		gpsset_time.sec = (int32_t)nmea_pars.m_8RMCSecond;

        pBuff[0] = pField[7];
        pBuff[1] = pField[8];
        pBuff[2] = pField[9];
        pBuff[3] = '\0';
        nmea_pars.m_16RMCMillisecond = atoi(pBuff);
		gpsset_time.usec = (int32_t)(nmea_pars.m_16RMCMillisecond * MILI_SEC);
	}
    // printf("2 %s\n", pField);

	//
	// Data valid
	//
	if(GetField(pData, pField, 1, MAXFIELD))
	{
		nmea_pars.m_cRMCDataValid = pField[0];
	}
	else
	{
		nmea_pars.m_cRMCDataValid = 'V';
	}

	//
	// latitude
	//
	if(GetField(pData, pField, 2, MAXFIELD))
	{
		nmea_pars.m_dRMCLatitude = atof((char *)pField+2) / 60.0;
		pField[2] = '\0';
		nmea_pars.m_dRMCLatitude += atof((char *)pField);
	}

	if(GetField(pData, pField, 3, MAXFIELD))
	{
		if(pField[0] == 'S')
		{
			nmea_pars.m_dRMCLatitude = -nmea_pars.m_dRMCLatitude;
		}
	}

	//
	// Longitude
	//
	if(GetField(pData, pField, 4, MAXFIELD))
	{
		nmea_pars.m_dRMCLongitude = atof((char *)pField+3) / 60.0;
		pField[3] = '\0';
		nmea_pars.m_dRMCLongitude += atof((char *)pField);
	}

	if(GetField(pData, pField, 5, MAXFIELD))
	{
		if(pField[0] == 'W')
		{
			nmea_pars.m_dRMCLongitude = -nmea_pars.m_dRMCLongitude;
		}
	}

	//
	// Ground speed
	//
	if(GetField(pData, pField, 6, MAXFIELD))
	{
		nmea_pars.m_dRMCGroundSpeed = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dRMCGroundSpeed = 0.0;
	}

	//
	// course over ground, degrees true
	//
	if(GetField(pData, pField, 7, MAXFIELD))
	{
		nmea_pars.m_dRMCCourse = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dRMCCourse = 0.0;
	}

	//
	// Date
	//
	if(GetField(pData, pField, 8, MAXFIELD))
	{
		// Day
		pBuff[0] = pField[0];
		pBuff[1] = pField[1];
		pBuff[2] = '\0';
		nmea_pars.m_8RMCDay = atoi(pBuff);
		gpsset_time.day = (int32_t)nmea_pars.m_8RMCDay;

		// Month
		pBuff[0] = pField[2];
		pBuff[1] = pField[3];
		pBuff[2] = '\0';
		nmea_pars.m_8RMCMonth = atoi(pBuff);
		gpsset_time.month = (int32_t)nmea_pars.m_8RMCMonth;

		// Year (Only two digits. I wonder why?)
		pBuff[0] = pField[4];
		pBuff[1] = pField[5];
		pBuff[2] = '\0';
		nmea_pars.m_32RMCYear = atoi(pBuff);
		nmea_pars.m_32RMCYear += 2000;				// make 4 digit date -- What assumptions should be made here?
		gpsset_time.year = (int32_t)nmea_pars.m_32RMCYear;
	}

	//
	// course over ground, degrees true
	//
	if(GetField(pData, pField, 9, MAXFIELD))
	{
		nmea_pars.m_dRMCMagVar = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dRMCMagVar = 0.0;
	}

	if(GetField(pData, pField, 10, MAXFIELD))
	{
		if(pField[0] == 'W')
		{
			nmea_pars.m_dRMCMagVar = -nmea_pars.m_dRMCMagVar;
		}
	}

	nmea_pars.m_32RMCCount++;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
static void ProcessGPZDA(char *pData)
{
	char pBuff[10];
	char pField[MAXFIELD];

	// Time
	if(GetField(pData, pField, 0, MAXFIELD))
	{
		// Hour
		pBuff[0] = pField[0];
		pBuff[1] = pField[1];
		pBuff[2] = '\0';
		nmea_pars.m_8ZDAHour = atoi(pBuff);
		gpsset_time.hour = (int32_t)nmea_pars.m_8ZDAHour;

		// minute
		pBuff[0] = pField[2];
		pBuff[1] = pField[3];
		pBuff[2] = '\0';
		nmea_pars.m_8ZDAMinute = atoi(pBuff);
		gpsset_time.min = (int32_t)nmea_pars.m_8ZDAMinute;

		// Second
		pBuff[0] = pField[4];
		pBuff[1] = pField[5];
		pBuff[2] = '\0';
		nmea_pars.m_8ZDASecond = atoi(pBuff);
		gpsset_time.sec = (int32_t)nmea_pars.m_8ZDASecond;
	}

	//
	// Day
	//
	if(GetField(pData, pField, 1, MAXFIELD))
	{
		nmea_pars.m_8ZDADay = atoi((char *)pField);
		gpsset_time.day = (int32_t)nmea_pars.m_8ZDADay;
	}
	else
	{
		nmea_pars.m_8ZDADay = 1;
	}

	//
	// Month
	//
	if(GetField(pData, pField, 2, MAXFIELD))
	{
		nmea_pars.m_8ZDAMonth = atoi((char *)pField);
		gpsset_time.month = (int32_t)nmea_pars.m_8ZDAMonth;
	}
	else
	{
		nmea_pars.m_8ZDAMonth = 1;
	}

	//
	// Year
	//
	if(GetField(pData, pField, 3, MAXFIELD))
	{
		nmea_pars.m_8ZDAYear = atoi((char *)pField);
		gpsset_time.year = (int32_t)nmea_pars.m_8ZDAYear;
	}
	else
	{
		nmea_pars.m_8ZDAYear = 2000;
	}

	//
	// Local zone hour
	//
	if(GetField(pData, pField, 4, MAXFIELD))
	{
		nmea_pars.m_8ZDALocalZoneHour = atoi((char *)pField);
	}
	else
	{
		nmea_pars.m_8ZDALocalZoneHour = 0;
	}

	//
	// Local zone hour
	//
	if(GetField(pData, pField, 5, MAXFIELD))
	{
		nmea_pars.m_8ZDALocalZoneMinute = atoi((char *)pField);
	}
	else
	{
		nmea_pars.m_8ZDALocalZoneMinute = 0;
	}

	nmea_pars.m_32ZDACount++;
}

static void ProcessGPVTG(char *pData)
{
	char pField[MAXFIELD];

	if(GetField(pData, pField, 0, MAXFIELD))
	{
		nmea_pars.m_dVTGTrueTrack = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dVTGTrueTrack = 0.0;
	}

	if(GetField(pData, pField, 2, MAXFIELD))
	{
		nmea_pars.m_dVTGMagneticTrack = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dVTGMagneticTrack = 0.0;
	}

	if(GetField(pData, pField, 4, MAXFIELD))
	{
		nmea_pars.m_dVTGSpeedKnots = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dVTGSpeedKnots = 0.0;
	}

	if(GetField(pData, pField, 6, MAXFIELD))
	{
		nmea_pars.m_dVTGSpeedKm = atof((char *)pField);
	}
	else
	{
		nmea_pars.m_dVTGSpeedKm = 0.0;
	}

	nmea_pars.m_32VTGCount++;
}

static void ProcessGPGLL(char *pData)
{
	char pBuff[10];
	char pField[MAXFIELD];

    // printf("1 %s\n", pData);
	//
	// latitude
	//
	if(GetField(pData, pField, 0, MAXFIELD))
	{
		nmea_pars.m_dGLLLatitude = atof((char *)pField+2) / 60.0;
		pField[2] = '\0';
		nmea_pars.m_dGLLLatitude += atof((char *)pField);
	}

	if(GetField(pData, pField, 1, MAXFIELD))
	{
		if(pField[0] == 'S')
		{
			nmea_pars.m_dGLLLatitude = -nmea_pars.m_dGLLLatitude;
		}
	}

	//
	// Longitude
	//
	if(GetField(pData, pField, 2, MAXFIELD))
	{
		nmea_pars.m_dGLLLongitude = atof((char *)pField+3) / 60.0;
		pField[3] = '\0';
		nmea_pars.m_dGLLLongitude += atof((char *)pField);
	}

	if(GetField(pData, pField, 3, MAXFIELD))
	{
		if(pField[0] == 'W')
		{
			nmea_pars.m_dGLLLongitude = -nmea_pars.m_dGLLLongitude;
		}
	}

	// Time
	if(GetField(pData, pField, 4, MAXFIELD))
	{
		// Hour
		pBuff[0] = pField[0];
		pBuff[1] = pField[1];
		pBuff[2] = '\0';
		nmea_pars.m_8GLLHour = atoi(pBuff);
		gpsset_time.hour = (int32_t)nmea_pars.m_8GLLHour;

		// minute
		pBuff[0] = pField[2];
		pBuff[1] = pField[3];
		pBuff[2] = '\0';
		nmea_pars.m_8GLLMinute = atoi(pBuff);
		gpsset_time.min = (int32_t)nmea_pars.m_8GLLMinute;

		// Second
		pBuff[0] = pField[4];
		pBuff[1] = pField[5];
		pBuff[2] = '\0';
		nmea_pars.m_8GLLSecond = atoi(pBuff);
		gpsset_time.sec = (int32_t)nmea_pars.m_8GLLSecond;

        pBuff[0] = pField[7];
        pBuff[1] = pField[8];
        pBuff[2] = pField[9];
        pBuff[3] = '\0';
        nmea_pars.m_16GLLMillisecond = atoi(pBuff);
		gpsset_time.usec = (int32_t)(nmea_pars.m_16GLLMillisecond * MILI_SEC);
	}
    // printf("2 %s\n", pField);

	//
	// Data valid
	//
	if(GetField(pData, pField, 5, MAXFIELD))
	{
		nmea_pars.m_cGLLDataValid = pField[0];
	}
	else
	{
		nmea_pars.m_cGLLDataValid = 'V';
	}

	nmea_pars.m_32GLLCount++;
}


/*void TimeStamp_Create(TimeStamp_t *time_stamp)
{
    struct tm *time_data;
    time_t  time_get;

    time(&time_get);
    time_data = localtime(&time_get);
    //TimeStamp_Clock_Set();
    mktime(time_data);

    time_stamp->year  = time_data->tm_year + 1900;
    time_stamp->month = time_data->tm_mon + 1;
    time_stamp->day   = time_data->tm_mday;
    time_stamp->hour  = time_data->tm_hour;
    time_stamp->min   = time_data->tm_min;
    time_stamp->sec   = time_data->tm_sec;
    time_stamp->usec  = (int32_t)(t_check.tv_nsec/1000);
    // printf("%04d-%02d-%02d %02d:%02d:%02d(%02ld).%d\n", time_stamp.year, time_stamp.month, time_stamp.day
    //                                             , time_stamp.hour, time_stamp.min, time_stamp.sec, t_check.tv_sec, time_stamp.nsec);
}*/

/**
@fn      TimeStamp_SetRTCtime(TimeStamp_t set_time)
@author  JunMyeung Kim
@date    2018.01.22
@brief   rtc의 시간을 setting 한다.
@return  0 : 성공.
*/
int32_t TimeStamp_SetRTCtime(TimeStamp_t set_time)
{
    char temp[64];

    //date --utc -s '2016-5-17 11:09:21'
    //date --utc -s '2016-5-17 11:09:21'
    //hwclock --utc --systohc
    //hwclock -w

    sprintf(temp,"date --utc -s \'%d-%d-%d %02d:%02d:%02d\'", set_time.year, set_time.month, set_time.day,
                                                                set_time.hour, set_time.min, set_time.sec);
    system(temp);
    system("hwclock --utc --systohc");
    system("hwclock -r");
    system("date");

    printf("[time] RTC %4d/%02d/%02d-%02d:%02d:%02d UTC\n\n", set_time.year, set_time.month, set_time.day,
                                                                set_time.hour, set_time.min, set_time.sec);

    return 0;
}
