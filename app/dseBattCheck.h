/**
    @file       dseBattCheck.h
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 배터리 관련 헤더 파일
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#ifndef _DSEBATTCHECK_H_
#define _DSEBATTCHECK_H_

#include <BXi2c.h>

#define BATT_ST0 0      /**< Battery state 0 */
#define BATT_ST1 1      /**< Battery state 1 */
#define BATT_ST2 2      /**< Battery state 2 */
#define BATT_ST3 3      /**< Battery state 3 */
#define BATT_ST4 4      /**< Battery state 4 */
#define BATT_ST5 5      /**< Battery state 5 */
#define BATT_ST6 6      /**< Battery state 6 */

#define BATT_VOL_MAX     12.5        /**< 배터리량 최대일 때의 전압값 */     //MAX 12.5 MIN 10.5
#define BATT_VOL_MIN     10.5        /**< 배터리량이 최소일 때의 전압값 */   //Li-ion MAX 16.5 MIN 10.7   //etc MAX 12.6 MIN 8.3    //MAX 16.8 MIN 10.8
/** 배터리 최대 최소 간의 전압 차 */
#define BATT_MAX_MIN_GAP (BATT_VOL_MAX-BATT_VOL_MIN)

/**
    @struct  _BATTINFO
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   배터리 관련 정보 구조체
    @warning none
*/
typedef struct _BATTINFO
{
    float  *arrVin;        ///< 전압값의 이동평균을 구하기 위한 전압값 배열
    float   nowVin;        ///< 측정 전압값
    float   averVin;       ///< 이동 평균으로 계산한 측정값. 이동 평균 개수는 MAX_BATT_AVER_NUM

    int32_t ind_arrVin;    ///< arrVin에 nowVin을 저장하기 위한 현재 인덱스
    int32_t maxIndex;      ///< arrVin의 최대 인덱스

    char    i2cDevDir[20]; ///< 전압 측정 IC가 연결된 i2c의 디바이스 파일 경로
    int32_t i2cAddr;       ///< 전압 측정 IC의 i2c ID
    BXi2c_t *i2c_t;        ///< i2c 통신 인터페이스 구조체 포인터

    int32_t st;            ///< Battery 상태 값
    int32_t battBorder[6]; ///< Battery 상태의 경계값(Percentage)
    float   battPer;       ///< Battery 잔량의 percentage 값
} _battInfo;

extern int32_t batt_initBattState(_battInfo *_info);        ///< Battery의 초기 상태 측정 함수
extern int32_t batt_getBattState(_battInfo *_info);         ///< Battery의 상태를 측정하는 함수

/** 배터리 전압 측정기능을 초기화한다. */
extern int32_t batt_battCheckOpen (_battInfo *info, int32_t maxAverNum, char* i2cDevName, int32_t i2cAddr);
/** 배터리 전압 측정을 기능을 정리하는 함수 */
extern int32_t batt_battCheckClose(_battInfo *info);

#endif
