/**
    @file       dsePressCheck.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 수압, 수심, 수온 모니터링 인터페이스
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/
#include <unistd.h>         // open() close() write() read() lseek()
#include <stdint.h>         // int32_t 등 library
#include <fcntl.h>          // O_RDWR
#include <stdlib.h>         //malloc atoi
#include <stdio.h>          //FILE
#include <string.h>
#include <time.h>

#include <BXi2c.h>
#include <BXlog.h>

#include "dsePressCheck.h"
#include "dseMain.h"

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
/** 수압,수심,수온 측정 기능을 활성화한다. */
int32_t press_pressCheckOpen(_pressInfo *info,
                             char* i2cDevName,
                             int32_t i2cAddr);

int32_t press_pressCheckClose(_pressInfo *info); ///< 수압, 수심, 수온 측정 기능을 종료한다.

int32_t press_logFileOpen(_pressInfo *_info);    ///< Log File을 초기화한다.
int32_t press_logFileClose(_pressInfo *_info);   ///< Log File을 Close한다.
int32_t press_putLog(_pressInfo *_info);         ///< Log File에 현재 상태를 출력한다.

int32_t press_updatePressTemp(_pressInfo *info); ///< 수압, 수심, 수온을 업데이트한다.
int32_t press_divingCheck    (_pressInfo *info); ///< Diving 상태를 확인한다.

static float press_calPressure(char _msbs, char  _lsbs);   ///< i2c로부터 측정한 디지털 수압 값을 아날로그 값으로 계산한다.
static float press_calTemperature(char _msbs, char  _lsbs);///< i2c로부터 측정한 디지털 온도 값을 아날로그 값으로 계산한다.
static float press_calWaterDepth(float _press);            ///< 수압 값으로부터 수심을 계산한다.
static void  press_makeLogFileName(char* _str);            ///< Log File을 생성한다.
///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////

int32_t numLogingLine = 0;  ///< Log File의 라인 넘버


/**
@fn      press_updatePressTemp(_pressInfo *info)
@author  Bongsung Kim
@date    2017.04.24
@brief   수압, 수심, 수온을 업데이트한다.
@param   *info 센서 정보 구조체 포인터
@return  성공시 0, read 실패시 -1, 센서 이상 시, -2. 값이 갑자기 튀었을 겨우 -4.
@warning press_pressCheckOpen() 수행 후 수행하여야 한다.
@bug     none
@todo    none
*/
int32_t press_updatePressTemp(_pressInfo *info)
{
    int32_t ret = -1;
    char temp[4] = {0,};
    char status = 0;

    float t_press = 0.0;
    float t_temp  = 0.0;
    float t_depth = 0.0;

label_PressRead:
    ret = info->i2c_t->BXi2c_read(info->i2c_t, temp, 4);
    if (ret <0)
        return -1;

    status = (temp[0] & 0xC0) >> 6;

    switch (status)
    {
    case 0 :
        t_press = press_calPressure(temp[0], temp[1]);
        t_temp  = press_calTemperature(temp[2], temp[3]);
        t_depth = press_calWaterDepth(t_press);
        break;
    case 1 :
        return -2;
        break;
    case 2 :
        goto label_PressRead;
        break;
    case 3 :
        return -3;
        break;
    }

    if (info->press == PRESS_TEMP_INIT_VALUE)
    {
        info->press = t_press;
        info->temp  = t_temp;
        info->depth = t_depth;
    }
    //gap of obtained pressure and previous pressure
    else if ((abs(info->press - t_press) < PRESS_THRESHOLD_BAR) &&
    //gap of obtained temperature and previous temperature
             (abs(info->temp  - t_temp ) < TEMP_THRESHOLD))
    {
        info->press = t_press;
        info->temp  = t_temp;
        info->depth = t_depth;
    }
    else
    {
        return -4;
    }


    return 0;
}

/**
@fn      press_divingCheck(_pressInfo *info)
@author  Bongsung Kim
@date    2017.04.24
@brief   Diving 상태를 확인한다..
@param   *info 센서 정보 구조체 포인터
@return  성공시 0.
@warning press_pressCheckOpen() 수행 후 수행하여야 한다.\n
         info->depth 값이 유효하여야 한다.
@bug     none
@todo    none
*/
int32_t press_divingCheck(_pressInfo *info)
{
    time_t t_time = 0;

    if (info->isDiving == DIVING_OFF) //다이빙 모드 off 일때
    {
        if (info->depth > DIVING_ON_DEPTH)
            info->divingCnt++;
        else
            info->divingCnt = 0;

        if (info->divingCnt > DIVING_ON_CNT)
        {
            info->divingMin = 0;
            info->divingSec = 0;

            info->isDiving = DIVING_ON;
            info->divingCnt = 0;
                                    //다이빙 시작 시간
            time(&info->divingStartTime);
            BXlogPutLogMsg("[dsePressCheck]Diving Mode Start!");
        }
    }
    else                                //다이빙 모드 on 일때
    {
        if (info->depth <= DIVING_OFF_DEPTH)
        {
            info->isDiving = DIVING_OFF;
            BXlogPutLogMsg("[dsePressCheck]Diving Mode Stop!");
            BXlogPrintf("[dsePressCheck]Diving Time - %02d:%02d",
                            info->divingMin, info->divingSec );
            return 0;
        }

        time(&t_time);
        t_time = t_time - info->divingStartTime;
        info->divingMin = t_time / 60;
        info->divingSec = t_time % 60;
    }

    return 0;
}

/**
@fn      press_pressCheckOpen(_pressInfo *info, char* i2cDevName, int32_t i2cAddr)
@author  Bongsung Kim
@date    2017.04.24
@brief    수압,수심,수온 측정 기능을 활성화한다.
@param   *info 센서 정보 구조체 포인터
@param   i2cDevName i2c 버스의 디바이스 경로
@param   i2cAddr 센서의 i2c ID
@return  성공시 0. 실패시 -1.
@warning none.
@bug     none
@todo    none
*/
int32_t press_pressCheckOpen(_pressInfo *info, char* i2cDevName, int32_t i2cAddr)
{
    info->i2cAddr = i2cAddr;
    strcpy(info->i2cDevDir, i2cDevName);

    info->devSt = ST_RESERV;
    info->depth = PRESS_TEMP_INIT_VALUE;
    info->press = PRESS_TEMP_INIT_VALUE;
    info->temp  = PRESS_TEMP_INIT_VALUE;


    info->i2c_t = BXi2c_device_open(info->i2cDevDir, info->i2cAddr);
    if (info->i2c_t == NULL)
        return -1;

    return 0;
}

/**
@fn      press_pressCheckClose(_pressInfo *info)
@author  Bongsung Kim
@date    2017.04.24
@brief   수압, 수심, 수온 측정 기능을 종료한다.
@param   *info 센서 정보 구조체 포인터
@return  성공시 0.
@warning none.
@bug     none
@todo    none
*/
int32_t press_pressCheckClose(_pressInfo *info)
{
    BXi2c_device_close(info->i2c_t);
    return 0;
}

/**
@fn      press_logFileOpen(_pressInfo *_info)
@author  Bongsung Kim
@date    2017.04.24
@brief   Log File을 초기화한다.
@param   *_info 센서 정보 구조체 포인터
@return  성공시 0. 실패 시, -1.
@warning none.
@bug     none
@todo    none
*/
int32_t press_logFileOpen(_pressInfo *_info)
{
    char t_logName[256];
    _info->enLog = 0;

    press_makeLogFileName(t_logName);
    _info->logFd = open(t_logName, O_WRONLY | O_CREAT, 0777);
    if ( 0 > _info->logFd )
    {
        BXlogPutLogMsg("[dsePressCheck] ERR: Log File Create Failed...!");
        return -1;
    }
    BXlogPutLogMsg("[dsePressCheck] Log File Created. Logging Start!");

    _info->enLog = 1;

    return 0;
}

/**
@fn      press_logFileClose(_pressInfo *_info)
@author  Bongsung Kim
@date    2017.04.24
@brief   Log File을 Close한다.
@param   *_info 센서 정보 구조체 포인터
@return  성공시 0
@warning none.
@bug     none
@todo    none
*/
int32_t press_logFileClose(_pressInfo *_info)
{
    close(_info->logFd);
    _info->enLog = 0;
    return 0;
}

/**
@fn      press_putLog(_pressInfo *_info)
@author  Jinmo Je
@date    2021.06.14
@brief   Log File에 현재 상태를 출력한다.
@param   *_info 센서 정보 구조체 포인터
@return  성공시 0
@warning none.
@bug     none
@todo    none
*/
int32_t press_putLog(_pressInfo *_info)
{
    char t_log[200];

    if (numLogingLine==1799)
    {
        numLogingLine = 0;
        press_logFileClose(_info);
        if (press_logFileOpen(_info) < 0)
        {
            BXlogPutLogMsg("[dsePressCheck] ERR: LogFile Create Failed....");
            BXlogPutLogMsg("[dsePressCheck] LogFile Not Sync...");
            return -2;
        }
    }

    if (_info->isDiving)
        sprintf(t_log, "[%d]<%04d/%02d/%02d-%02d:%02d:%02d> D.:%0.2fm, T.:%0.2f℃, DIVING\n",
                    numLogingLine, t_rtc.tm_year, t_rtc.tm_mon,
                    t_rtc.tm_mday, t_rtc.tm_hour, t_rtc.tm_min, t_rtc.tm_sec,
                    _info->depth, _info->temp);
    else
        sprintf(t_log, "[%d]<%04d/%02d/%02d-%02d:%02d:%02d> D.:%0.2fm, T.:%0.2f℃\n",
                    numLogingLine, t_rtc.tm_year, t_rtc.tm_mon,
                    t_rtc.tm_mday, t_rtc.tm_hour, t_rtc.tm_min, t_rtc.tm_sec,
                    _info->depth, _info->temp);

    write(_info->logFd, t_log, strlen(t_log));

    numLogingLine++;

    return 0;
}

/**
@fn      press_makeLogFileName(char* _str)
@author  Jinmo Je
@date    2021.06.14
@brief   Log File을 생성한다.
@param   *_str Log File 저장 경로가 저장될 빈 string 버퍼.
@return  none
@warning none.
@bug     none
@todo    none
*/
static void press_makeLogFileName(char* _str)
{
    char t_str[256];

    sprintf(_str, "%s/diveLog", diskName);
    if (access(_str, F_OK))
    {
        BXlogPrintf("[dsePressCheck] %s Dir. is invalid.. data Folder Create!", _str);
        sprintf(t_str, "mkdir %s", _str);
        system(t_str);
    }
    sprintf(_str, "%s/%02d%02d%02d_%02d%02d.txt", _str,
                                t_rtc.tm_year-2000,
                                t_rtc.tm_mon,
                                t_rtc.tm_mday,
                                t_rtc.tm_hour,
                                t_rtc.tm_min);
}

/**
@fn      press_calPressure(char _msbs, char  _lsbs)
@author  JunMyeung Kim
@date    2017.10.11
@brief   i2c로부터 측정한 디지털 수압 값을 아날로그 값으로 계산한다.
@param   _msbs 취득한 디지털 상위 6bit 값.
@param   _lsbs 취득한 디지털 하위 8bit 값.
@return  계산된 아날로그 수압 값.
@warning PSI나 Bar 변경 시에 Pmax, Pmin을 변경하여야 한다..
@bug     none
@todo    none
*/
static float press_calPressure(char _msbs, char  _lsbs)
{
    uint32_t Dpressure;
    float P1 = 16383 * 0.1; /*P1 = 10% * 16383 - A type */
    float P2 = 16383 * 0.8; /*P2 = 80% * 16383 - A type */

#ifdef BARGAGE_SENSOR20
    float Pmax = BARGAGE_SENSOR20;
#endif

#ifdef PSIABSOL_SENSOR150
    float Pmax = PSIABSOL_SENSOR150;
#endif

    float Pmin = 0.0;

    Dpressure = ((uint32_t)(_msbs & 0x3f) << 8) + _lsbs;
    //printf("pressure digital : %d\n",Dpressure);

#ifdef BARGAGE_SENSOR20
    return (((float)Dpressure)- P1) * (Pmax-Pmin) / P2 + Pmin;
#endif    

#ifdef PSIABSOL_SENSOR150
    return ((((float)Dpressure)- P1) * (Pmax-Pmin) / P2 + Pmin) / PRESS_UNIT - BAR;
#endif
}

/**
@fn      press_calWaterDepth(float _press)
@author  Bongsung Kim
@date    2017.04.24
@brief   수압 값으로부터 수심을 계산한다.
@param   _press 수압 값.
@return  수심 값 (Meter)
@warning none
@bug     none
@todo    none
*/
static float press_calWaterDepth(float _press)
{
  //float d = (_press / (float)PRESS_UNIT * 10.0);
    float d;
    if (_press < 0.05)
        d = 0;
    else
        d = (_press)*10;
    //printf("Press: %f, Depth: %3.3f\n", _press, d);

    if (d<0)
        return 0.0;
    else
        return d;
}

/**
@fn      press_calTemperature(char _msbs, char  _lsbs)
@author  Bongsung Kim
@date    2017.04.24
@brief   i2c로부터 측정한 디지털 온도 값을 아날로그 값으로 계산한다.
@param   _msbs 취득한 디지털 상위 8bit 값.
@param   _lsbs 취득한 디지털 하위 3bit 값.
@return  계산된 아날로그 온도 값(화씨).
@warning none
@bug     none
@todo    none
*/
static float press_calTemperature(char _msbs, char  _lsbs)
{
    uint32_t Dtemp;

    Dtemp = (((uint32_t)_msbs) << 3) + (((uint32_t)_lsbs>>5) & 0x03);

    //printf("temp: %f\n", ((float)Dtemp) * 200 / 2047 - 50 -3);     //'-3' is correction value for Digitalvalue
    return ((float)Dtemp) * 200 / 2047 - 50 -3;
}
