/*******************************************************************
Name        : BXctrlLED.c
Author      : Bongsung Kim
Version     : 1.0.0
Description : Class의 LED 컨트롤

Version     :
         1.0.0  - LED Controller
********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <asm/ioctl.h>
#include <stdint.h>         // int32_t 등 library

#include <getopt.h>

#include "BXctrlLED.h"

//#define LED1_DIR "/sys/class/leds/LED1"
//#define LED2_DIR "/sys/class/leds/LED2"

//--------------------------------------------------------------------
// 설명: LED를 none으로 만들고 최고 밝게 만든다. 한마디로 점등
// 인수: char* ledPath  : led class의 폴더 트리
//--------------------------------------------------------------------
int32_t BXledBrightness_255(char* ledPath)
{
    int32_t fd, len, fd2;
    char buf[50];
    char t_buf[100];

    sprintf(t_buf, "%s/brightness", ledPath);
    fd = open(t_buf, O_WRONLY);
    if (fd < 0) {
        perror("[laderView_led.c] led_brightness_select");
        return fd;
    }
    sprintf(t_buf, "%s/trigger", ledPath);
    fd2 = open(t_buf, O_WRONLY);
    if (fd2 < 0) {
        perror("[laderView_led.c] led_brightness_select");
        return fd2;
    }

    len = snprintf(buf, sizeof(buf), "none");
    write(fd2, buf, len);

    len = snprintf(buf, sizeof(buf), "255");
    write(fd, buf, len);


    close(fd);
    close(fd2);
    return 0;
}

//--------------------------------------------------------------------
// 설명: LED를 끈다.
// 인수: char* ledPath  : led class의 폴더 트리
//--------------------------------------------------------------------
int32_t BXledBrightness_0(char* ledPath)
{
    int32_t fd, len, fd2;
    char buf[50];
    char t_buf[100];

    sprintf(t_buf, "%s/brightness", ledPath);
    fd = open(t_buf, O_WRONLY);
    if (fd < 0) {
        perror("[laderView_led.c] led_brightness_select");
        return fd;
    }
    sprintf(t_buf, "%s/trigger", ledPath);
    fd2 = open(t_buf, O_WRONLY);
    if (fd2 < 0) {
        perror("[laderView_led.c] led_brightness_select");
        return fd2;
    }


    len = snprintf(buf, sizeof(buf), "none");
    write(fd2, buf, len);

    len = snprintf(buf, sizeof(buf), "0");
    write(fd, buf, len);


    close(fd);
    close(fd2);
    return 0;
}

//--------------------------------------------------------------------
// 설명: LED Trigger 고르기
// 인수: char* ledPath  : led class의 폴더 트리
//      char* trigger  : trigger 종류. heartbeat, timer, none ...
//--------------------------------------------------------------------
int32_t BXledTriggerSelect(char* ledPath, char* trigger)
{
    int32_t fd, len;
    char buf[50];
    char t_buf[100];

    sprintf(t_buf, "%s/trigger", ledPath);
    fd = open(t_buf, O_WRONLY);
    if (fd < 0)
    {
        perror("[BXctrlLED.c] led_trgger_select");
        return fd;
    }

    len = snprintf(buf, sizeof(buf), trigger);
    write(fd, buf, len);


    close(fd);
    return 0;
}
