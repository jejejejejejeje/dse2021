#ifndef _BXOPENCLCTRL_H_
#define _BXOPENCLCTRL_H_


typedef struct opencl_t_ BXopencl_t;
struct opencl_t_
{
    char                kernel_dir[50];
    cl_context          GPUContext;
    cl_kernel           OpenCLKernel;
    cl_program          OpenCLProgram;
    cl_command_queue    cqCommandQueue;

    cl_mem              GPUinputVector[3];
    cl_mem              GPUoutputVector;
};

extern BXopencl_t* BXopenCL_init(char *, char *);
extern void      BXopenCL_close(BXopencl_t *);

#endif
