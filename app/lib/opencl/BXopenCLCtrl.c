/*******************************************************************
Name        : BXopenClCtrl.c
Author      : Bongsung Kim
Version     : 0.0.1
Description : OpenCL Control Library

Version     :
         0.0.1  - openCLInit 함수 및 close 함수 추가.
********************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <CL/cl.h>          //openCL

#include "BXopenCLCtrl.h"

///////////////////////////////////////////////////////////
//           Function Definition                         //
///////////////////////////////////////////////////////////
BXopencl_t* BXopenCL_init(char* kernel_dir, char* kernel_name);
void      BXopenCL_close(BXopencl_t* _cl_t);
///////////////////////////////////////////////////////////
//           Function Definition                         //
///////////////////////////////////////////////////////////

//--------------------------------------------------------------------
// 설명: OpenCL을 사용하기 위한 Initialze.
// 인수: kernel_dir : openCL 커널 소스의 디렉토리
//--------------------------------------------------------------------
BXopencl_t* BXopenCL_init(char* kernel_dir, char* kernel_name)
{
    int ret;

    BXopencl_t* _opencl_t;

    _opencl_t = (BXopencl_t *)malloc(sizeof(BXopencl_t));
    strcpy( _opencl_t->kernel_dir, kernel_dir);

    //Get an OpenCL platform
    cl_platform_id cpPlatform;
    clGetPlatformIDs(1, &cpPlatform, NULL);

    // Get a GPU device
    cl_device_id cdDevice;
    clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &cdDevice, NULL);

    // Create a context to run OpenCL enabled GPU
    _opencl_t->GPUContext = clCreateContextFromType(0, CL_DEVICE_TYPE_GPU, NULL, NULL, NULL);

    // Create a command-queue on the GPU device
    _opencl_t->cqCommandQueue = clCreateCommandQueue(_opencl_t->GPUContext, cdDevice, 0, NULL);

    FILE *fp;
    char *ProgramSource;
    long fsize;
    fp = fopen(kernel_dir, "rb");
    if (fp==NULL)
    {
        printf("[BXopenClCtrl.c] ERR : Kernel file Open Failed!!\n");
        return NULL;
    }

    //파일의 끝으로 가서...
    fseek(fp, 1, SEEK_END);

    // 파일의 총 크기를 구하고,
    fsize = ftell(fp);

    // 처음으로 되 돌립니다.
    fseek(fp, 0, SEEK_SET);

    // 버퍼를 생성하고....
    ProgramSource = (char*)malloc(sizeof(char) * fsize);

    // 커널 코드를 읽습니다.
    fread(ProgramSource, sizeof(char), fsize, fp);

    /// 그리고 파일을 닫습니다.
    fclose(fp);
    size_t SourceSize = fsize;

    //printf("%s\n",ProgramSource);

    // Create OpenCL program with source code
    _opencl_t->OpenCLProgram = clCreateProgramWithSource(_opencl_t->GPUContext, 1, (const char **)&ProgramSource, &SourceSize, NULL);

    // Build the program (OpenCL JIT compilation)
    ret = clBuildProgram(_opencl_t->OpenCLProgram, 0, NULL, NULL, NULL, NULL);
    if(ret != CL_SUCCESS)
    {
        // Shows the log
        char* build_log;
        size_t log_size;
        // 로그의 적절한 길이를 알기 위해 처음 함수를 실행해 봅니다
        clGetProgramBuildInfo(_opencl_t->OpenCLProgram, cdDevice, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        build_log = (char *)malloc((log_size+1)*sizeof(char));
        // 로그를 따오기 위해 두번재로 호출합니다
        clGetProgramBuildInfo(_opencl_t->OpenCLProgram, cdDevice, CL_PROGRAM_BUILD_LOG, log_size+1, build_log, NULL);
        build_log[log_size] = '\0';
        printf("%d\n",log_size);
        printf("%s\n",build_log);
        //fprintf(stderr, "%s", build_log);
        //return EXIT_FAILURE;

        free(build_log);

        printf("[BXopenClCtrl.c]ERR: Kernel Compile Err.\n");
        return NULL;
    }


    // Create a handle to the compiled OpenCL function (Kernel)
    _opencl_t->OpenCLKernel = clCreateKernel(_opencl_t->OpenCLProgram, kernel_name, NULL);

    return _opencl_t;
}

//--------------------------------------------------------------------
// 설명: opencl_t를 close한다.
// 반환: 없음
// 인수: _cl_t   : 소멸할 구조체
//--------------------------------------------------------------------
void BXopenCL_close(BXopencl_t* _cl_t)
{
    free(_cl_t);
}

