#ifndef _BXELOG_H_
#define _BXELOG_H_

#include <string.h>

char msg[200];
#define BXlogPrintf(fmt,args...)  {     \
                   memset(msg, 0x00, sizeof(msg));\
                   sprintf(msg, fmt, ## args);\
                   BXlogPutLogMsg(msg);    \
            }

extern void    BXlogErrHandling(char *msg);
extern void    BXlogPrintLogo();
extern int32_t BXlogWriteVersionLog(char* _path, char* _ver);
extern int32_t BXlogCreateLogFile(char* _path, _Bool _rtcEN);
extern int32_t BXlogPutLogMsg(char* _msg);


#endif
