/*******************************************************************
Name        : BXepoll.c
Author      : Bongsung Kim
Version     : 0.0.1
Description : epoll Manager

Version     :
         0.0.1  -
********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include <time.h>
#include <linux/rtc.h>
#include <sys/types.h>
#include <assert.h>

#include "BXlog.h"

static char logFileName[100];

///////////////////////////////////////////////////////////
//                Function Definition                    //
///////////////////////////////////////////////////////////
static void logFileNameCheck(char* _fileName);


//--------------------------------------------------------------------
// 설명: 에러 핸들
// 인수: char* msg      : 출력할 메세지
//--------------------------------------------------------------------
void BXlogErrHandling (char *msg)
{
  BXlogPutLogMsg(msg);

  fputs (msg, stderr);
  fputc ('\n', stderr);
  exit (1);
}

//--------------------------------------------------------------------
// 설명: 로고 출력
// 인수:
//--------------------------------------------------------------------
void BXlogPrintLogo()
{
    printf(" ____   ___  _   _    _    ____ _____ _____ ____ _   _ \n");
    printf("/ ___| / _ \\| \\ | |  / \\  |  _ \\_   _| ____/ ___| | | |\n");
    printf("\\___ \\| | | |  \\| | / _ \\ | |_) || | |  _|| |   | |_| |\n");
    printf(" ___) | |_| | |\\  |/ ___ \\|  _ < | | | |__| |___|  _  |\n");
    printf("|____/ \\___/|_| \\_/_/   \\_\\_| \\_\\|_| |_____\\____|_| |_|\n\n\n");
    //printf("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n");
    //printf("QQQQQQQQQWQQWQQPQHWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQQWQ\n");
    //printf("#############Q~ - 3####Q####Q###Q##Q##Q##Q##Q##Q##Q##Q##Q##Q##Q##Q##Q##Q##Q##Q##\n");
    //printf("########@TIY#F  - ]QQ###YY9Q##Q########################HMHHHM############@HMQ###\n");
    //printf("#####Q@1l1qqQ_ - -<Q##^ ,v,Q#####Q#Q#Q##Q#Q#Q##Q#Q#Q##Q,, -,d##Q#Q#Q#Q###r ]Q###\n");
    //printf("####@TlIUQ#WM#V,  ]Q#Q- '?9###?~'?Y##Y'''Y##Y''?YQ##?'9#[ =##P?''YYQ#@Y??Qr ''9Q\n");
    //printf("###@1lId#0U#t:3q- J##Q6av_ ?P( aa_-]@ ,&z 9P vav ?#^ vd#[ =#P ,,, ]M' ,aWr ,a_'#\n");
    //printf("##M1lII4#Q#####P ,####????-<$r YY^ ]E ]#E 4L ?Y'-=Q- U##[ =#k -,=-]Q- YY#r ]D=`#\n");
    //printf("##ClIIII9####@? j####QavvvaWQ#&avaQQ$vd#$vd#$vvaaaQvvW##hvaQ#$vvva#Q#6vvQ&vJ#&aQ\n");
    //printf("#QIIIIInQ#QQ6  ,#Q##Q#QWWQQQ###QWWWQ####Q###Q#QWWW#QQ#Q##QQQ#QQQQQ##Q#QQ##Q###Q#\n");
    //printf("##II1dW#######Q#######Q###Q##########Q###Q#Q##Q###Q#####Q################Q######\n");
    //printf("????????????????????????????????????????????????????????????????????????????????\n");

    printf("\n\n\n\n");
}

//--------------------------------------------------------------------
// 설명: 프로그램의 Version 정보를 이름으로하는 파일을 만든다.
// 인수:
//--------------------------------------------------------------------
int32_t BXlogWriteVersionLog(char* _path, char* _ver)
{
    FILE *fp;
    char t_string[128];
    char ver_path[128];

    if (access(_path, F_OK))
    {
        printf("[BXlog] Disk Recognition Failed...\n\n");
        exit(1);
    }

    sprintf(ver_path, "%s/0_FIRMWARE/", _path);

    if (access(ver_path, F_OK))
    {
        printf("[BXlog] CreateFirmWare: 0_FIRMWARE Folder Not Exist!!\n");
        printf("[BXlog] CreateFirmWare: Make 0_FIRMWARE Folder\n");
        sprintf(t_string, "mkdir %s", ver_path);
        system(t_string);
    }

    strcat(ver_path, "Ver-");
    strcat(ver_path,_ver);

    printf("[BXlog] CreateFirmWare: Ver. File Created...\n");
    printf("[BXlog] CreateFirmWare: Ver. File is '%s'\n", ver_path);

    fp = fopen(ver_path, "wb");
    if ( fp == NULL)
    {
        printf("\n [BXlog] CreateFirmWare: Ver. File Open Failed...!\n");
        return -1;
    }

    fclose(fp);
    return 0;
}

//--------------------------------------------------------------------
// 설명: 에러 및 상태 확인을 위한 Log 파일 생성.
// 인수:
//--------------------------------------------------------------------
int32_t BXlogCreateLogFile(char* _path, _Bool _rtcEN)
{
    FILE *fp;
    int _pathlen = strlen(_path);
    char t_string[_pathlen+20];
    char _logPath[_pathlen+5];
    struct tm *time_data;
    time_t  curtime;

    if (access(_path, F_OK))
    {
        printf("[BXlog] Disk Recognition Failed...\n\n");
        exit(1);
    }

    sprintf(logFileName, "%s/LOG", _path);
    sprintf(_logPath, "%s/LOG", _path);

    if (access(logFileName, F_OK))
    {
        printf("[BXlog] CreateLogFile: LOG Folder Not Exist!!\n");
        printf("[BXlog] CreateLogFile: Make LOG Folder\n");
        sprintf(t_string, "mkdir %s", logFileName);
        system(t_string);
    }

    if (_rtcEN)
    {
        time(&curtime);
        time_data = localtime(&curtime);
        mktime( time_data);

        snprintf(logFileName, strlen(_logPath)+19 , "%s/%04d%02d%02d_%02d%02d.log", _logPath,
                time_data->tm_year + 1900,
                time_data->tm_mon +1,
                time_data->tm_mday,
                time_data->tm_hour,
                time_data->tm_min);
    }
    else
    {
        logFileNameCheck(logFileName);
    }

    printf("[BXlog] CreateLogFile: LOG File Created...\n");
    printf("[BXlog] CreateLogFile: LOG File is '%s'\n", logFileName);

    fp = fopen(logFileName, "wb");
    if ( fp == NULL)
    {
        printf("\n [BXlog] CreateLogFile: LOG File Open Failed...!\n");
        return -1;
    }

    fclose(fp);
    return 0;
}

//--------------------------------------------------------------------
// 설명: 에러 및 상태 확인을 위한 Log 파일 생성.
// 인수:
//--------------------------------------------------------------------
int32_t BXlogPutLogMsg(char* _msg)
{
    FILE *err_fp;
    time_t  curtime;
    struct tm *time_data;

    char    msgbuf[200];

    err_fp = fopen(logFileName, "a+");
    if ( err_fp == NULL )
    {
        printf("logfile open fail\n");
        return -1;
    }

    time(&curtime);
    time_data = localtime(&curtime);
    mktime( time_data);

    sprintf(msgbuf, "[%04d-%02d-%02d %02d:%02d:%02d]: %s\n",
        time_data->tm_year + 1900,
        time_data->tm_mon +1,
        time_data->tm_mday,
        time_data->tm_hour,
        time_data->tm_min,
        time_data->tm_sec,
        _msg);
    printf("%s", msgbuf);
    fputs(msgbuf, err_fp);
    fclose(err_fp);

    return 0;
}



/*--------------------------------------------------------------------
 설명: 파일 이름을 생성하기 위해 00000부터 확인하는 함수
 인수: char* _fileName : 파일 이름이 저장될 스트링
--------------------------------------------------------------------*/
static void logFileNameCheck(char* _fileName)
{
    int i = 0;
    char _temp[100];
    strcpy(_temp, _fileName);

    do{
        sprintf(_fileName, "%s/%05d.log", _temp, i);
        if (access(_fileName, F_OK))
        {
            break;
        }
        i++;
    }while(1);
}
