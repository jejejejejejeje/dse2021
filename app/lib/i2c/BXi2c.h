#ifndef _BXI2C_H_
#define _BXI2C_H_

                                            //컴파일러 linux/i2c.h에 정의 나머지도 정의해야함!!
#define BX_I2C_SMBUS_QUICK       0
#define BX_I2C_SMBUS_BYTE        1
#define BX_I2C_SMBUS_BYTE_DATA   2
#define BX_I2C_SMBUS_WORD_DATA   3
#define BX_I2C_SMBUS_BLOCK_DATA  5
#define I2C_SMBUS_I2C_BLOCK_DATA 8
//ERROR code

typedef struct i2c_t_ BXi2c_t;

                                            // i2c를 위한 구조체
struct i2c_t_
{
    int32_t  i2cID;
    char     i2c_devname[20];
    int32_t  i2c_device_number;
    int32_t  (*i2c_smbus_readWrite)(BXi2c_t *, int32_t, uint8_t *, int32_t, int32_t);
    int32_t  (*BXi2c_read)(BXi2c_t* _i2c_t, char* _t, int32_t byteSize);
    int32_t  (*BXi2c_write)(BXi2c_t* _i2c_t, char* _t, int32_t byteSize);

};


                                            //ioctl을 통해 SMBUS를 사용하기 위한 가상 함수 접근 구조체


extern BXi2c_t* BXi2c_device_open(char* _devname, int32_t _devnumber);
extern void     BXi2c_device_close(BXi2c_t* _i2c_t);

#endif
