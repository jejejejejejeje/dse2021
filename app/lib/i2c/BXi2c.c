/*******************************************************************
Name        : BXi2c.c
Author      : Bongsung Kim
Version     : 0.0.1
Description : User Level I2C Controller

Version     :
         0.0.1  -
********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdint.h>
#include <fcntl.h>          // O_RDWR , O_NOCTTY
#include <sys/ioctl.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>


#include "BXi2c.h"

static void BXi2c_error_handling (char *message)
{
    fputs (message, stderr);
    fputc ('\n', stderr);
    exit (1);
}

//--------------------------------------------------------------------
// 설명: IOCTL을 이용해 device에 연결한다.
// 반환: 0 <  - No Error
//       0 >  - Error
// 인수: _i2c_t    : i2c 구조체 정보
//--------------------------------------------------------------------
static int32_t BXi2c_connect_device(BXi2c_t* _i2c_t)
{

    return ioctl(_i2c_t->i2cID, I2C_SLAVE, _i2c_t->i2c_device_number);
}

//--------------------------------------------------------------------
// 설명: i2c Read
// 반환: 0 <  - No Error
//       0 >  - Error
// 인수: _i2c_t    : i2c 구조체 정보
//--------------------------------------------------------------------
static int32_t BXi2c_read(BXi2c_t* _i2c_t, char* _t, int32_t byteSize)
{
    return read(_i2c_t->i2cID, _t, byteSize);
}

//--------------------------------------------------------------------
// 설명: i2c Write
// 반환: 0 <  - No Error
//       0 >  - Error
// 인수: _i2c_t    : i2c 구조체 정보
//--------------------------------------------------------------------
static int32_t BXi2c_write(BXi2c_t* _i2c_t, char* _t, int32_t byteSize)
{
    return write(_i2c_t->i2cID, _t, byteSize);
}


//--------------------------------------------------------------------
// 설명: IOCTL을 이용한 SMBUS 컨트롤
// 반환: 0 <  - No Error
//       0 >  - Error
// 인수: _i2c_t    : i2c 구조체 정보
//       _writeEN  : Write Enable    1 - Write  0 - Read
//--------------------------------------------------------------------
static int32_t BXi2c_smbus_readWrite(BXi2c_t* _i2c_t, int32_t _writeEn, uint8_t* _inout, int32_t _addr, int32_t _transactionTypes)
{

    struct i2c_smbus_ioctl_data smbus_data =
    {
        .read_write = I2C_SMBUS_READ,
        .command    = _addr,
        .size       = _transactionTypes,
        .data       = (void *)_inout,
    };

    if (_writeEn)
        smbus_data.read_write = I2C_SMBUS_WRITE;
    else
        smbus_data.read_write = I2C_SMBUS_READ;


    if ( ioctl(_i2c_t->i2cID, I2C_SMBUS, (void *)&smbus_data) < 0 )
    {
        printf("[BXi2c.c]i2c_smbus_readWrite I2C_SMBUS ioctl Error..!!\n");
        return -1;
    }

    return 1;
}

//--------------------------------------------------------------------
// 설명: I2C 드라이버를 오픈하고 해당 디바이스를 등록한다.
// 반환: I2C_T의 포인터
// 인수: _devname   : I2C 드라이버의 위치와 이름
//       _devnumber : 해당 I2C 버스에서 연결할 디바이스의 ID
//--------------------------------------------------------------------
BXi2c_t* BXi2c_device_open(char* _devname, int32_t _devnumber)
{
    BXi2c_t* i2c_t;

    //Device Driver 있는지 확인
    //(수정사항!!) i2c MAX부터 찾아서 제일 마지막의 i2c 버스 자동디텍트하게 고치기?
    if (access(_devname, F_OK))
        BXi2c_error_handling("[BXi2c.c]_devname Not Exist!!");

    i2c_t = (BXi2c_t *)malloc(sizeof(BXi2c_t));
    if (i2c_t==NULL)  //할당 해제시
        BXi2c_error_handling("[BXi2c.c]i2c_t memory allocation Failed !!");

    //소켓 정의한다.
    i2c_t->i2cID               = -1;
    strcpy( i2c_t->i2c_devname, _devname);
    i2c_t->i2c_device_number   = _devnumber;
    i2c_t->i2c_smbus_readWrite = BXi2c_smbus_readWrite;
    i2c_t->BXi2c_read          = BXi2c_read;
    i2c_t->BXi2c_write         = BXi2c_write;

    if ( ( i2c_t->i2cID = open(i2c_t->i2c_devname, O_RDWR) ) < 0)
        BXi2c_error_handling("[BXi2c.c]i2c_t I2C Drver Open Failed !!");

    if (BXi2c_connect_device(i2c_t) < 0)
        BXi2c_error_handling("[BXi2c.c]i2c_t I2C Device Connect ioctl I2C_SLAVE Failed !!");



    return i2c_t;
}

//--------------------------------------------------------------------
// 설명: RS232_T 를 close하고 소멸한다.
// 반환: 없음
// 인수: _sock   : 소멸할 소켓 구조체
//--------------------------------------------------------------------
void BXi2c_device_close(BXi2c_t* _i2c_t)
{
    close(_i2c_t->i2cID);

    free(_i2c_t);
}

