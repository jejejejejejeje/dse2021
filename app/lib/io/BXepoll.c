/*******************************************************************
Name        : BXepoll.c
Author      : Bongsung Kim
Version     : 0.0.1
Description : epoll Manager

Version     :
         0.0.1  -
********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include <sys/epoll.h>


#include "BXepoll.h"

//--------------------------------------------------------------------
// 설명: epoll 파일을 만든다.
// 인수: size      : epoll 최대 싸이즈
//--------------------------------------------------------------------
BXepoll_t* BXepollOpen(int32_t _epollSize, int32_t _maxEventNum)
{
    BXepoll_t *obj;
    obj = (BXepoll_t *)malloc(sizeof(BXepoll_t));
    if (obj == NULL)
    {
        printf("BXepoll_t Malloc Failed!!\n");
        return NULL;
    }

    obj->epollfd = epoll_create(_epollSize);
    obj->events = (struct epoll_event *)malloc(sizeof(struct epoll_event)*_maxEventNum);
    if (obj->epollfd < 0)
    {
        return NULL;
    }


    return obj;
}

//--------------------------------------------------------------------
// 설명: epoll 파일 클로즈
// 인수: BXepoll_t* _fd      : 소켓 정보
//--------------------------------------------------------------------
void BXepollClose(BXepoll_t* _fd)
{
    free(_fd);

}

//--------------------------------------------------------------------
// 설명: epoll add 동작
// 인수: BXepoll_t* _fd      : epoll 구조체
//      int32_t _fd       : 등록할 fd
//--------------------------------------------------------------------
int32_t BXepollAdd(BXepoll_t* _obj, int32_t _fd, int32_t _oper_enum)
{
    struct epoll_event ev;

    ev.events = _oper_enum;
    ev.data.fd = _fd;

    return epoll_ctl(_obj->epollfd, EPOLL_CTL_ADD, _fd, &ev);
    /*int epoll_ctl(int epoll_fd,             //epoll_fd
              int operate_enum,         //어떤 변경을 할지 결정하는 enum값
              int enroll_fd,            //등록할 fd
              struct epoll_event* event //관찰 대상의 관찰 이벤트 유형
              );
    operate_enum : EPOLL_CTL_ADD, EPOLL_CTL_DEL, EPOLL_CTL_MOD(이벤트 상황 변경)
                                                                        */

    /*struct epoll_event
    {
      __uint32_t events;
      epoll_data_t data;
    }

    typedef epoll_data
    {
       void* ptr;
       int fd;
       __uint32_t u32;
       __uint64_t u64;
    }epoll_data_t;

    enum Events
    {
       EPOLLIN,   //수신할 데이터가 있다.
       EPOLLOUT,  //송신 가능하다.
       EPOLLPRI,  //중요한 데이터(OOB)가 발생.
       EPOLLRDHUP,//연결 종료 or Half-close 발생
       EPOLLERR,  //에러 발생
       EPOLLET,   //엣지 트리거 방식으로 설정
       EPOLLONESHOT, //한번만 이벤트 받음  }*/
}

//--------------------------------------------------------------------
// 설명: epoll delete 동작
// 인수: BXepoll_t* _fd      : epoll 구조체
//      int32_t _fd       : 삭제할 fd
//--------------------------------------------------------------------
int32_t BXepollDel(BXepoll_t* _obj, int32_t _fd)
{
    return epoll_ctl(_obj->epollfd, EPOLL_CTL_DEL, _fd, NULL);
}

//--------------------------------------------------------------------
// 설명: epoll wait 동작
// 인수: BXepoll_t* _fd       - epoll 구조체
//      int32_t _eventSize - 버퍼에 들어갈수있는 구조체 최대 개수
//      int32_t _timeout   - timeout. 1ms단위. 0 - 바로 -1 - block
// 반환 : 성공시 발생한 events의 개수를 반환
//--------------------------------------------------------------------
int32_t BXepollWait(BXepoll_t* _obj, int32_t _eventSize, int32_t _timeout)
{
    return epoll_wait(_obj->epollfd, _obj->events, _eventSize, _timeout);
}
