#ifndef _BXEPOLL_H_
#define _BXEPOLL_H_

typedef struct EPOLL_T_ BXepoll_t;
struct EPOLL_T_
{
    int epollfd;
    struct epoll_event* events;
};

extern BXepoll_t* BXepollOpen(int32_t size, int32_t _maxEventNum);
extern void BXepollClose(BXepoll_t* _fd);
extern int32_t BXepollAdd(BXepoll_t* _obj, int32_t _fd, int32_t _oper_enum);
extern int32_t BXepollDel(BXepoll_t* _obj, int32_t _fd);
extern int32_t BXepollWait(BXepoll_t* _obj, int32_t _eventSize, int32_t _timeout);
#endif
