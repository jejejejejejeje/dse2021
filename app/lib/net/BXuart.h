#ifndef _BXUART_H_
#define _BXUART_H_

#define BX_RS_NONE_PARITY      0                   // 패리티 없음
#define BX_RS_ODD_PARITY       (PARODD|PARENB)     // 홀수 패리티
#define BX_RS_EVEN_PARITY      PARENB              // 짝수 패리티
#define BX_RS_1_STOP_BIT       0                   // 1 stop bit
#define BX_RS_2_STOP_BIT       CSTOPB              // 2 stop bit


//ERROR code
#define BX_ERR_NONE       0
#define BX_ERR_UART_OPEN -2

typedef struct rs232_t_ BXrs232_t;

struct rs232_t_
{
    int32_t  poll_ndx;
    int32_t  fd;
    char     devname[20];
    int32_t  baud;
    int      databit;
    int      stopbit;
    int      parity;

    int      (*on_write)(BXrs232_t *_sock, char *_buf, int _buf_size);
    int      (*on_read)(BXrs232_t *_sock, char *_buf, int _buf_size);

};

extern BXrs232_t* BXrs232_open(char* _devname, int32_t _baud, int32_t _databit, int32_t _parity, int _stopbit);

extern void       BXrs232_close(BXrs232_t* _sock);

#endif
