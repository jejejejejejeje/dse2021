/*******************************************************************
Name        : BXtcpudp.c
Author      : Bongsung Kim
Version     : 0.0.1
Description : TCP UDP Communication Library

Version     :
         0.0.1  - socket_t_ : 소켓 정보 및 반대 측 주소 정보를 가짐.
                  newSocket()으로 TCP, UDP 설정 및 bind, listen 까지 수행
********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>


#include "BXtcpudp.h"

static void BXtcpudp_error_handling (char *message)
{
    fputs (message, stderr);
    fputc ('\n', stderr);
    exit (1);
}

//--------------------------------------------------------------------
// 설명: 소켓을 close 한다.
// 인수: _sock      : 소켓 정보
//--------------------------------------------------------------------
static void  BXsocket_close( BXsocket_t* _sock)
{
  close(_sock->socketID);
}

//--------------------------------------------------------------------
// 설명: 소켓을 bind 한다.
// 반환: bind함수 반환값. 0이하면 Fail
// 인수: _sock      : 소켓 정보
//--------------------------------------------------------------------
static int BXsocketBind(BXsocket_t* _sock)
{
    if (_sock->serverType == BX_NET_CLIENT)
    {
        printf("[BXtcpudp.c] You Tried Bind at Client!!\n");
        return BX_ERR_CLIENTBIND;
    }

    return bind(_sock->socketID, (struct sockaddr*)&_sock->addr, sizeof(_sock->addr));
}

//--------------------------------------------------------------------
// 설명: 소켓을 Listen 상태로 만든다.
// 반환: listen 함수 반환값. 0이하면 Fail
// 인수: _sock      : 소켓 정보
//--------------------------------------------------------------------
static int BXsocketListen(BXsocket_t* _sock)
{
    if (_sock->serverType == BX_NET_CLIENT)
    {
        printf("[BXtcpudp.c] You Tried Listen at Client!!\n");
        return BX_ERR_CLIENTLISTEN;
    }
    else if (_sock->protoType == BX_UDP_SOCKET)
    {
        printf("[BXtcpudp.c] You Tried Listen on UDP!!\n");
        return BX_ERR_UDPLISTEN;
    }

    return listen(_sock->socketID, 5);
}

//--------------------------------------------------------------------
// 설명: 소켓을 Accept 한다. 블로킹으로 할지 논블로킹으로 할지 결정해야한다,(추가해야함)
// 반환: 접속한 클라이언트의 ID.   0< - Fail
// 인수: _sock      : 소켓 정보
//--------------------------------------------------------------------
static int BXsocketAccept(BXsocket_t* _sock, BXsocket_t* _clntSock)
{
     if (_sock->serverType == BX_NET_CLIENT)
    {
        printf("[BXtcpudp.c] You Tried Accept at Client!!\n");
        return BX_ERR_CLIENTACCEPT;
    }
    else if (_sock->protoType == BX_UDP_SOCKET)
    {
        printf("[BXtcpudp.c] You Tried Accept on UDP!!\n");
        return BX_ERR_UDPACCEPT;
    }


    return accept(_sock->socketID, (struct sockaddr*)&_clntSock->addr,
         (socklen_t *)&_sock->len_addr);
}

//--------------------------------------------------------------------
// 설명: 소켓 통신을 이용한 데이터 전송 함수
// 반환: 전송한 데이터의 Byte 수
//       0>   ERROR
// 인수: _recvSock      :  받는 소켓 정보
//       _sendData      : 전송할 Data
//       _sendByteSize  : 전송할 Data의 Byte Size
//       _sendFlag      : send 옵션 플래그 (작성할 것!)
//--------------------------------------------------------------------
static ssize_t BXsocketSend(BXsocket_t* _recvSock, char* _sendData, int _sendByteSize, int _sendFlag)
{
    return send (_recvSock->socketID, (char *)_sendData, _sendByteSize, _sendFlag);
}

//--------------------------------------------------------------------
// 설명: 소켓 통신을 이용한 데이터 수신 함수
// 반환: 수신한 데이터의 Byte 수
//       0>   ERROR
// 인수:  _sendSock      : 소켓 정보
//       _recvData      : 수신할 Data Buffer의 포인터
//       _recvByteSize  : 수신할 Data의 Byte Size
//       _recvFlag      : recv 옵션 플래그 (작성할 것!)
//--------------------------------------------------------------------
static ssize_t BXsocketRecv(BXsocket_t* _sendSock, char* _recvData, int _recvByteSize, int _recvFlag)
{
    return recv (_sendSock->socketID, (char *)_recvData, _recvByteSize, _recvFlag);
}

//--------------------------------------------------------------------
// 설명: 소켓 통신을 이용한 데이터 송신 함수. UDP 용
// 반환: 수신한 데이터의 Byte 수
//       0>   ERROR
// 인수: _sendSock      : 송신 측 소켓 정보
//       _sendData      : 수신할 Data Buffer의 포인터
//       _sendByteSize  : 수신할 Data의 Byte Size
//       _sendFlag      : recv 옵션 플래그 (작성할 것!)
//       _recvSock      : 데이터 수신 측 정보
//--------------------------------------------------------------------
static ssize_t BXsocketSendTo(BXsocket_t* _sendSock, char* _sendData, int _sendByteSize, int _sendFlag, BXsocket_t* _recvSock)
{
    return sendto (_sendSock->socketID, (char *)_sendData, _sendByteSize, _sendFlag, (struct sockaddr*)&_recvSock->addr, sizeof(_recvSock->addr));
}

//--------------------------------------------------------------------
// 설명: 소켓 통신을 이용한 데이터 수신 함수. UDP 용
// 반환: 수신한 데이터의 Byte 수
//       0>   ERROR
// 인수: _sendSock      : 수신 측 소켓 정보
//       _recvData      : 수신할 Data Buffer의 포인터
//       _recvByteSize  : 수신할 Data의 Byte Size
//       _recvFlag      : recv 옵션 플래그 (작성할 것!)
//       _sendSock      : 데이터 송신측 정보
//--------------------------------------------------------------------
static ssize_t BXsocketRecvFrom(BXsocket_t* _recvSock, char* _recvData, int _recvByteSize, int _recvFlag, BXsocket_t* _sendSock)
{
    int addr_size = sizeof(_sendSock->addr);

    return recvfrom (_recvSock->socketID, (char *)_recvData, _recvByteSize, _recvFlag, (struct sockaddr*)&_sendSock->addr, (socklen_t *)&addr_size);
}

//--------------------------------------------------------------------
// 설명: 새로운 소켓을 만들고 경우에 따라 bind에서 listen 까지 수행한다.
// 반환: SOCKET_T의 포인터
// 인수: _protoType   : UDP 인지 TCP인지 정보
//       _servrerType : Server인지 Client인지
//       _IP          : 자신 혹은 접속 IP 주소  inet_addr("_IP") or htonl(INADDR_ANY)
//       _port        : 자신 혹은 접속 대상의 포트
//--------------------------------------------------------------------
BXsocket_t* BXnewSocket(int _protoType, int _servrerType, /*char* _IP*/unsigned long _IP, int _port)
{
    //int32_t ret;
    BXsocket_t* sock;

    sock = (BXsocket_t *)malloc(sizeof(BXsocket_t));
    sock->len_addr = sizeof(sock->addr);
    sock->protoType = _protoType;
    switch(_protoType)
    {
        case BX_TCP_SOCKET :
            sock->socketID = socket(AF_INET, SOCK_STREAM, 0);
            break;

        case BX_UDP_SOCKET :
            sock->socketID = socket(AF_INET, SOCK_DGRAM, 0);
            break;

        default         :
            BXtcpudp_error_handling("[BXtcpudp.c]ERR_TCPUDPTYPE");
            break;
    }

    if (sock->socketID < 0)
        BXtcpudp_error_handling("[BXtcpudp.c]ERR_SOCKETOPEN");



    sock->serverType           = _servrerType;
    bzero((char*)&sock->addr, sizeof(sock->addr));
    sock->addr.sin_family      = AF_INET;
    sock->addr.sin_addr.s_addr = _IP;//inet_addr(_IP);
    sock->addr.sin_port        = htons(_port);
    sock->socketBind           = BXsocketBind;
    sock->socketListen         = BXsocketListen;
    sock->socketAccept         = BXsocketAccept;
    sock->socketSend           = BXsocketSend;
    sock->socketRecv           = BXsocketRecv;
    sock->socketRecvFrom       = BXsocketRecvFrom;
    sock->socketSendTo         = BXsocketSendTo;

    return sock;
}

//--------------------------------------------------------------------
// 설명: BXsocket_t 를 close하고 소멸한다.
// 반환: 없음
// 인수: _sock   : 소멸할 소켓 구조체
//--------------------------------------------------------------------
void BXtcpudp_close(BXsocket_t* _sock)
{
    BXsocket_close(_sock);
    free(_sock);
}
