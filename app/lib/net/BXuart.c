/*******************************************************************
Name        : BXuart.c
Author      : Bongsung Kim
Version     : 0.0.1
Description : UART Communication Library

Version     :
         0.0.1  -
********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <termios.h>        // B115200, CS8 등 RS232 통신 관련 상수 정의
#include <fcntl.h>          // O_RDWR , O_NOCTTY



#include "BXuart.h"

static void BXuart_error_handling (char *message)
{
    fputs (message, stderr);
    fputc ('\n', stderr);
    exit (1);
}

//--------------------------------------------------------------------
// 설명: 소켓을 open 한다.
// 반환: 0 <  - No Error
//       0 >  - Error
// 인수: _rs232_t    : 소켓 정보
//--------------------------------------------------------------------
static int BXsocket_open( BXrs232_t* _rs232_t)
{
    struct termios   newtio;

    //디바이스 오픈
    _rs232_t->fd = open(_rs232_t->devname, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (_rs232_t->fd < 0)
        return BX_ERR_UART_OPEN;

    memset( &newtio, 0, sizeof(newtio) );

    //baud Init.
    switch(_rs232_t->baud )
    {
        case 2400   : newtio.c_cflag = B2400  ; break;
        case 4800   : newtio.c_cflag = B4800  ; break;
        case 9600   : newtio.c_cflag = B9600  ; break;
        case 19200  : newtio.c_cflag = B19200 ; break;
        case 38400  : newtio.c_cflag = B38400 ; break;
        case 57600  : newtio.c_cflag = B57600 ; break;
        default     : newtio.c_cflag = B115200; break;// default: 115200
    }

    //Data Bits Init.
    switch(_rs232_t->databit)
    {
        case 5  : newtio.c_cflag |= CS5; break;
        case 6  : newtio.c_cflag |= CS6; break;
        case 7  : newtio.c_cflag |= CS7; break;
        default : newtio.c_cflag |= CS8; break;       // default : 8 bit
    }

    // stop bit Init.
    if (_rs232_t->stopbit == 2)
        newtio.c_cflag |= BX_RS_2_STOP_BIT;
    else
      newtio.c_cflag |= CLOCAL | CREAD;   //CRTSCTS 제외

    switch(_rs232_t->parity | 0x20 ) // | 0x20 대문자 값이라도 소문자로
    {
    case 'o'  : newtio.c_cflag |= BX_RS_ODD_PARITY;   break;
    case 'e'  : newtio.c_cflag |= BX_RS_EVEN_PARITY;  break;
    }

    newtio.c_oflag      = 0;
    newtio.c_lflag      = 0;
    newtio.c_cc[VTIME]  = 0;  // time-out 값으로 사용. time-out 값은 TIME*0.1초.
    newtio.c_cc[VMIN]   = 1;  // MIN은 read가 리턴되기 위한 최소한의 문자 개수

    tcflush  (_rs232_t->fd, TCIFLUSH );
    tcsetattr(_rs232_t->fd, TCSANOW, &newtio );

    return BX_ERR_NONE;
}
//--------------------------------------------------------------------
// 설명: 소켓을 close 한다.
// 인수: _rs232_t    : 소켓 정보
//--------------------------------------------------------------------
static void  BXsocket_close( BXrs232_t* _rs232_t)
{
  close(_rs232_t->fd);
}

//--------------------------------------------------------------------
// 설명: 소켓에 데이터를 전송한다.
// 반환: write()의 결과를 반환.
//       0 <    - Error
// 인수: _sock      : 소멸 대상 소켓 정보
//       _buf       : 전송할 데이터의 버퍼 포인터
//       _buf_size  : 데이터의 길이
//--------------------------------------------------------------------
static int BXrs232_write( BXrs232_t *_sock, char *_buf, int _buf_size)
{
  return  write(_sock->fd,_buf,_buf_size);
}

//--------------------------------------------------------------------
// 설명: 소켓에 데이터를 읽는다.
// 반환: read()의 결과를 반환.
//       0 <    - Error
// 인수: _sock      : 소멸 대상 소켓 정보
//       _buf       : 전송할 데이터의 버퍼 포인터
//       _buf_size  : 데이터의 길이
//--------------------------------------------------------------------
static int BXrs232_read( BXrs232_t *_sock, char *_buf, int _buf_size)
{
  return  read(_sock->fd,_buf,_buf_size);
}


//--------------------------------------------------------------------
// 설명: 새로운 RS232 소켓을 오픈하고 RS232_T를 반환한다.
// 반환: RS232_T의 포인터
// 인수: _protoType   : UDP 인지 TCP인지 정보
//       _servrerType : Server인지 Client인지
//       _IP          : 자신 혹은 접속 IP 주소  inet_addr("_IP") or htonl(INADDR_ANY)
//       _port        : 자신 혹은 접속 대상의 포트
//--------------------------------------------------------------------
BXrs232_t* BXrs232_open(char* _devname, int32_t _baud, int32_t _databit, int32_t _parity, int _stopbit)
{
    BXrs232_t* rs232_t;

    //Device Driver 있는지 확인
    if (access(_devname, F_OK))
        BXuart_error_handling("[BXuart.c]_devname Not Exist!!");

    rs232_t = (BXrs232_t *)malloc(sizeof(BXrs232_t));
    if (rs232_t==NULL)  //할당 해제시
        BXuart_error_handling("[BXuart.c]rs232_t memory allocation Failed !!");

    //소켓 정의한다.
    rs232_t->fd              = -1;
    strcpy( rs232_t->devname, _devname);
    rs232_t->baud            = _baud;
    rs232_t->databit         = _databit;
    rs232_t->stopbit         = _stopbit;
    rs232_t->parity          = _parity;
    rs232_t->on_write        = BXrs232_write;
    rs232_t->on_read         = BXrs232_read;

    if (BXsocket_open(rs232_t))
        BXuart_error_handling("[BXuart.c]rs232_t memory allocation Failed !!");


    return rs232_t;
}

//--------------------------------------------------------------------
// 설명: BXrs232_t 를 close하고 소멸한다.
// 반환: 없음
// 인수: _sock   : 소멸할 소켓 구조체
//--------------------------------------------------------------------
void BXrs232_close(BXrs232_t* _sock)
{
    BXsocket_close(_sock);
    free(_sock);
}

