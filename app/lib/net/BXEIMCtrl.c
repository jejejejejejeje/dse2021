/*******************************************************************
Name        : BXEIMCtrl.c
Author      : Bongsung Kim
Version     : 1.0.0
Description : KIRO EIM Controller

Version     :
         1.0.0  - KIRO EIM Controller
********************************************************************/

#include <stdio.h>
#include <stdint.h>         // int32_t 등 library
#include <signal.h>         // Signal Process
#include <unistd.h>         // open() close() write() read() lseek()
#include <fcntl.h>          // O_RDWR
#include <stdlib.h>         //malloc
#include <sys/ioctl.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <math.h>
#include <string.h>

//#include <pthread.h>

#include "BXEIMCtrl.h"


///////////////////////////////////////////////////////////
//           Function Definition                         //
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//           Function Definition                         //
///////////////////////////////////////////////////////////


//--------------------------------------------------------------------
// 설명: FPGA 의 데이터를 메로리로 카피 한다.
// 반환:  성공 시 0
//      실패 시 -1
//--------------------------------------------------------------------
int32_t BXeimFPGA2MemCpy(int32_t dataSize, void* dst, void* src)
{
    memcpy( dst, src , dataSize );

    return 0;
}

//--------------------------------------------------------------------
// 설명: FPGA EIM memory mapping
// 반환: 성공 시 해당 FPGA 메모리의 시작 주소 포인터
//      실패 시 NULL
//--------------------------------------------------------------------
void *BXeimFPGAmmap(int32_t byteSize)
{
    int memfd = open("/dev/mem",O_RDWR | O_SYNC);
    if (memfd < 0)
    {
        printf("[EIMCtrl.c] ERR : memfd Open Failed...\n");
        return NULL;
    }

    void *ret = (void *)mmap(0, byteSize, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, 0x08000000);

    close(memfd);

    return ret;
}

//--------------------------------------------------------------------
// 설명: FPGA EIM memory mapping
// 인수: mm - 반환할 eim 메모리 포인터
//      byteSize - 반환할 크기
// 반환: 성공 시 해당 FPGA 메모리의 시작 주소 포인터
//      실패 시 NULL
//--------------------------------------------------------------------
int32_t BXeimFPGAmunmap(void *mm, int32_t byteSize)
{
    return munmap(mm, byteSize);
}
