#ifndef _BXEIMCTRL_H
#define _BXEIMCTRL_H


extern int32_t BXeimFPGA2MemCpy(int32_t dataSize, void* dst, void* src);
extern void   *BXeimFPGAmmap(int32_t byteSize);
extern int32_t BXeimFPGAmunmap(void *mm, int32_t byteSize);

#endif
