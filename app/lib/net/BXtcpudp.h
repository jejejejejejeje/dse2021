#ifndef _BXTCPUDP_H_
#define _BXTCPUDP_H_

//_protoType
#define BX_TCP_SOCKET 0
#define BX_UDP_SOCKET 1
//_servrerType
#define BX_NET_SERVER 0
#define BX_NET_CLIENT 1

//ERROR code
#define BX_ERR_CLIENTBIND    0
#define BX_ERR_CLIENTLISTEN  0
#define BX_ERR_UDPLISTEN     0
#define BX_ERR_CLIENTACCEPT  0
#define BX_ERR_UDPACCEPT     0
#define BX_ERR_TCPUDPTYPE   -2
#define BX_ERR_SOCKETOPEN   -3
#define BX_ERR_BIND         -4
#define BX_ERR_LISTEN       -5

typedef struct socket_t_ BXsocket_t;
struct socket_t_
{
    int socketID;                   //soeck ID
    int protoType;                  // TCP or UDP
    int serverType;                 //Server인지 Client인지
    struct sockaddr_in addr;       //addr 정보
    int len_addr;                  //clnt_addr의 바이트 수

    int (*socketBind)(BXsocket_t *);
    int (*socketListen)(BXsocket_t *);
    int (*socketAccept)(BXsocket_t *, BXsocket_t *);
    ssize_t (*socketSend)(BXsocket_t *, char *, int, int);
    ssize_t (*socketRecv)(BXsocket_t *, char *, int, int);
    ssize_t (*socketRecvFrom)(BXsocket_t *, char *, int, int, BXsocket_t *);
    ssize_t (*socketSendTo)(BXsocket_t *, char *, int, int, BXsocket_t *);
};

int32_t errcode;

extern BXsocket_t* BXnewSocket(int _protoType, int _servrerType, /*char* _IP*/unsigned long _IP, int _port);
extern void        BXtcpudp_close(BXsocket_t* _sock);

#endif
