#ifndef D_SerialOperator_H
#define D_SerialOperator_H

#include "SerialBase.h"
#include "NMEA_Parse.h"
/**********************************************************
 *
 * SerialOperator is responsible for ...
 *
 **********************************************************/

#define DEV_GPS2LINUX "/dev/ttymxc1"   ///< uart 통신 디바이스 드라이버 경로
//#define DEV_SERI2LINUX "/dev/ttymxc2"


#pragma pack(push, 1)
struct Serial_Ports
{
    int32_t gps_fd;
    //int32_t seri2_fd;
} Serial_Ports_t;
#pragma pack(pop)

extern Serial_Setting_t    gps_sets; //, seri2_sets;

extern void SerialOperator_Create(void);
extern void SerialOperator_Destroy(void);

extern int32_t SerialOperator_readGPS(void);
//extern int32_t SerialOperator_readSERI2(void);

extern TimeStamp_t gps_time;

#endif  /* D_SerialOperator_H */
