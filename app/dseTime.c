/**
    @file       dseTime.c
    @date       2017.04.25
    @author     Bongsung Kim
    @brief      다이버 소나 Time 관련 인터페이스
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
    BongsungKim/2017.04.25 : rtcGetTime Definition
    BongsungKim/2017.04.25 : get Linux Time Function
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <linux/rtc.h>
#include <sys/ioctl.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>         // open() close() write() read() lseek()

#include <BXlog.h>

#include "dseMain.h"

///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////
int32_t        time_getLinuxSecond();
int32_t        time_setTime(int32_t *_timeInfo);
int32_t        time_rtcGetTime(struct rtc_time *rtc_time_info);

static int32_t time_setRTCTime(int32_t _year, int32_t _mon, int32_t _day,
                               int32_t _hour, int32_t _min, int32_t _sec);
static int32_t time_setTimeZone(int32_t _timeDiff);



int32_t time_getProcessClock();
///////////////////////////////////////////////////////////
//           USER Function Definition                    //
///////////////////////////////////////////////////////////


////////////////global Variable////////////////////////////
pthread_mutex_t rtc0_lock = PTHREAD_MUTEX_INITIALIZER;
////////////////global Variable////////////////////////////

/**
@fn      time_getLinuxSecond()
@author  Bongsung Kim
@date    2017.04.25
@brief   시스템 시간을 로딩해서 초만 반환한다.
@param   none
@return  현재 시스템 시간의 초.
@warning none
@bug     none
@todo    none
*/
int32_t time_getLinuxSecond()
{
    time_t _time;

    _time = time((time_t *)0);

    struct tm *ptm = gmtime(&_time);

    return ptm->tm_sec;
}

/**
@fn      time_setRTCTime(int32_t _year, int32_t _mon, int32_t _day,
                         int32_t _hour, int32_t _min, int32_t _sec)
@author  Bongsung Kim
@date    2017.04.25
@brief   RTC에 시간을 기록한다.
@param   _year : 년  YYYY
@param   _mon  : 월  1~12
@param   _day  : 일
@param   _hour : 시
@param   _min  : 분
@param   _sec  : 초
@return  성공 시 0, 실패 시 -1
@warning none
@bug     none
@todo    none
*/
static int32_t time_setRTCTime(int32_t _year, int32_t _mon, int32_t _day,
                               int32_t _hour, int32_t _min, int32_t _sec)
{
    int fd;
    struct tm rtc_tm;

    rtc_tm.tm_year = _year - 1900;  //2xxx
    rtc_tm.tm_mon  = _mon  - 1;     //1~12
    rtc_tm.tm_mday = _day;
    rtc_tm.tm_hour = _hour;
    rtc_tm.tm_min  = _min;
    rtc_tm.tm_sec  = _sec;

    pthread_mutex_lock(&rtc0_lock);
    fd = open(RTC0_DEV, O_RDONLY);
    if (fd < 0)
    {
        BXlogPutLogMsg("[dseTime] ERR: RTC Set. RTC dev Open Failed..");
        return -1;
    }

    if (ioctl(fd, RTC_SET_TIME, &rtc_tm) < 0)
    {
        BXlogPutLogMsg("[dseTime] ERR: RTC set. RTC_SET_TIME ioctl Failed..");
        return -1;
    }
    close(fd);

    pthread_mutex_unlock(&rtc0_lock);

    BXlogPutLogMsg("[dseTime] RTC Time Set!!");

    return 0;
}

/**
@fn      time_setTime(int32_t *_timeInfo)
@author  Bongsung Kim
@date    2017.04.25
@brief   전체 시간 세팅 및 표준 시간대 변경한다.
@param   *_timeInfo : 7개 int 배열. \n
         [0] : year\n
         [1] : mon\n
         [2] : day\n
         [3] : hour\n
         [4] : min\n
         [5] : sec\n
         [6] : GMT 와의 시간 차 \n
@return  성공 시 0, 실패 시 -1
@warning none
@bug     none
@todo    none
*/
int32_t time_setTime(int32_t *_timeInfo)
{
    int32_t ret = 0;
    ret = time_setRTCTime(_timeInfo[0], _timeInfo[1], _timeInfo[2],
                          _timeInfo[3], _timeInfo[4], _timeInfo[5]);
    if (ret < 0)
        return -1;

    ret = time_setTimeZone(_timeInfo[6]);
    if (ret < 0)
        return -1;

    system("hwclock -s");

    return 0;
}

/**
@fn      time_setTimeZone(int32_t _timeDiff)
@author  Bongsung Kim
@date    2017.04.25
@brief   리눅스의 표준시간대 변경하는 함수.
@param   _timeDiff : 변경하고자 하는 시간대의 GMT와의 시간 차
@return  성공 시 0, 실패 시 -1
@warning tzdata 패키지가 설치되어 있어야 한다.
@bug     none
@todo    none
*/
static int32_t time_setTimeZone(int32_t _timeDiff)
{
    char t_string[256];

    if ((_timeDiff < -14) && (_timeDiff > 12))
    {
        BXlogPrintf("[dseTime] ERR: time_setTimeZone. Time Diff Value UTC+%d is wrong...", _timeDiff);
        return -1;
    }

    if (_timeDiff < 0)
        snprintf(t_string, 256, "cp /usr/share/zoneinfo/Etc/GMT-%d /etc/localtime",_timeDiff*(-1));
    else
        snprintf(t_string, 256, "cp /usr/share/zoneinfo/Etc/GMT+%d /etc/localtime",_timeDiff);

    system(t_string);

    return 0;
}

/**
@fn      time_rtcGetTime(struct rtc_time *rtc_time_info)
@author  Jinmo Je
@date    2021.06.14
@brief   현재 rtc 시간을 불러옴.
@param   *rtc_time_info : rtc_time 구조체. 이 구조체에 시간 정보를 저장함.
@return  성공 시 0, 실패 시 -1
@warning none
@bug     none
@todo    none
*/
int32_t time_rtcGetTime(struct rtc_time *rtc_time_info)
{
    int fd;

    pthread_mutex_lock(&rtc0_lock);
    fd = open(RTC0_DEV, O_RDONLY);
    if (fd < 0)
    {
        BXlogPutLogMsg("[dseTime] ERR: RTC Get. RTC dev Open Failed..");
        return -1;
    }

    if (ioctl(fd, RTC_RD_TIME, rtc_time_info) < 0)
    {
        BXlogPutLogMsg("[dseTime] ERR: RTC_RD_TIME");
        return -1;
    }

    rtc_time_info->tm_year += 1900;
    rtc_time_info->tm_mon  += 1;

    close(fd);
    pthread_mutex_unlock(&rtc0_lock);

    return 0;
}


/**
@fn      time_getProcessClock()
@author  Bongsung Kim
@date    2017.04.25
@brief   프로세스 시간을 구해서 ms 단위로 반환한다.
@param   none
@return  프로세스 시간 ms
@warning none
@bug     none
@todo    none
*/
int32_t time_getProcessClock()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}