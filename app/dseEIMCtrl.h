/**
    @file       dseEIMCtrl.h
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 Fpga 통신(EIM) 인터페이스 헤더
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#ifndef _DSEEIMCTRL_H_
#define _DSEEIMCTRL_H_

#define TIMEUNIT             1.33333 /**< 단위 ms. 초음파 1m 왕복 시간 */
#define FPGA_RDDATA_SIZE     128     /**< 한번 FPGA 데이터 읽을 때, 읽는 데이터 개수 */

#define METER_PER_PIXEL      39.37   /**< 데이터 개수 per Meter  */

/**
    @struct  EIM_T
    @date    2017.04.24
    @author  Bongsung Kim
    @brief   FPGA 통신 관련 구조체.
    @warning none
*/
typedef struct EIM_T
{
    int32_t waitTimeforSona;  ///< 한 프레임 데이터 취득 시간(미사용)
    int32_t h;                ///< 한 프레임의 Raw data height(data count)
    int32_t w;                ///< 한 프레임의 Raw data width(channel count). 3ch일 경우, 교차빔 동작 모드에서는 768/2가 된다.
    float   rangeMeter;       ///< range 정보

} fpga_t;


#endif
