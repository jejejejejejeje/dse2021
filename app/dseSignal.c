/**
    @file       dseSignal.c
    @date       2017.04.24
    @author     Bongsung Kim
    @brief      다이버 소나 메인 헤더파일
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#include <stdio.h>          //FILE
#include <sys/ioctl.h>
#include <signal.h>         // Signal Process
#include <stdint.h>
#include <unistd.h>         // open() close() write() read() lseek()
#include <pthread.h>

#include <BXlog.h>
#include <BXctrlLED.h>

#include "dseMain.h"
#include "dseSave.h"

//-------------------------------------------------------------------
int32_t     sig_sigFuncInit(pid_t userPid);

static void sig_sigCmdProc(int signo);
static void sig_clickRangeSW();
static void sig_clickGainSW();
static int32_t sig_clickSaveSW();
static void sig_clickChangeSW();
//-------------------------------------------------------------------

_Bool firstStartSig = 0;  ///< 첫화면에서 넘어가는 flag

/**
@fn      sig_sigFuncInit(pid_t userPid)
@author  Bongsung Kim
@date    2017.04.24
@brief   Signal Control Function Initialize
@param   userPid : 해당 프로세스의 pid
@return  성공 시, 0. 실패 시, 프로그램 종료.
@warning none
@bug     none
@todo    none
*/
int32_t sig_sigFuncInit(pid_t userPid)
{
                        //keys 드라이버로 pid 전송
    if( write(keysfd, &userPid, sizeof(pid_t))<0 )
        BXlogErrHandling("[dseMain] ERR : keys Write - IOCTL_SEND_PID");

                        //keys 드라이버로부터 signal 수신을 위한 sigaction 구조체 Init.
    struct sigaction sig_btn_action;
    sigset_t block_mask;

    //sigfillset (&block_mask);
    sigemptyset(&block_mask);
    sigaddset(&block_mask, SIGUSR1);
    sigaddset(&block_mask, SIGALRM);
    sig_btn_action.sa_handler = sig_sigCmdProc;
    sig_btn_action.sa_mask    = block_mask;
    sig_btn_action.sa_flags   = SA_NODEFER;
    sigaction(SIGUSR1, &sig_btn_action, NULL);
    sigaction(SIGALRM, &sig_btn_action, NULL);

    return 0;
}

/**
@fn      sig_sigCmdProc(int signo)
@author  Jinmo Je
@date    2021.06.14
@brief   Signal Handler Function
@param   signo : signal Number
@return  none
@warning none
@bug     none
@todo    none
*/
static void sig_sigCmdProc(int signo)
{
    int sw_cmd = 0;

    switch (signo)
    {
    case SIGALRM :
        f_gainOrRange = 0;
        //main_sendSignalADCConditionVar();
        return;
        break;
    case SIGUSR1 :
        //main_sendSignalADCConditionVar();
        goto sigusr_proc;
        break;
    default      :

        break;
    }

sigusr_proc:
    if( ioctl(keysfd, IOCTL_SW_CMD_GET, &sw_cmd)<0 )
        BXlogErrHandling("[dseMain.c]IOCTL Error : IOCTL_SW_CMD_GET\n");

    printf("sig_command : %d\n", sw_cmd);


    //cmd에 따라서 처리하기
    switch(sw_cmd)
    {
    case SW_CMD_RANGE :

        sig_clickRangeSW();
        BXlogPrintf("[dseSignal] RangeUp %d", t_range);
        f_gainOrRange = 2;
        alarm(2);
        break;

    case SW_CMD_GAIN  :

        sig_clickGainSW();
        BXlogPrintf("[dseSignal] Gain Change %d", gainMode);
        f_gainOrRange = 1;
        alarm(2);
        break;

    case SW_CMD_SAVE  :
        BXlogPrintf("[dseSignal] SAVE COMMAND %d", f_save);
        //printf("SAVE COMMAND : %d \n\n", f_save);
        sig_clickSaveSW();
        break;

    case SW_CMD_FIRST :
        BXlogPutLogMsg("[dseSignal] DSE Start Signal...");
        BXlogPutLogMsg("[dseSignal] First Page Shutdown.");
        firstStartSig = 1;
        break;
    case SW_CMD_CHANGE :
        BXlogPrintf("[dseSignal] Change COMMAND %d", f_change);
        sig_clickChangeSW();
        break;

    default           :
        break;
    }
}

/**
@fn      sig_clickRangeSW()
@author  Bongsung Kim
@date    2017.04.24
@brief   Range Switch Push 할 시에 temp Range를 변경하는 함수
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void sig_clickRangeSW()
{
    uint8_t gainsel = 0;
    uint8_t pwdsel  = 0;

    if(maxRange <= 50)
    {
        if (t_range == maxRange)
            t_range = MIN_RANGE;
        else if (t_range >= 30)
            t_range += RANGE_UNIT_OVER30M;
        else
            t_range += RANGE_UNIT_UNDER30M;
    }
    else if(maxRange > 50)
    {
        if (t_range == maxRange)
            t_range = MIN_RANGE;
        else if (t_range >= 30 && t_range < 50)
            t_range += RANGE_UNIT_OVER30M;
        else if (t_range >= 50)
            t_range += RANGE_UNIT_OVER50M;
        else
            t_range += RANGE_UNIT_UNDER30M;
    }

    if (t_range == 5)
        gainsel = 0x03;
    else if (t_range > 5 && t_range <= 10)
        gainsel = 0x02;
    else if (t_range >= 15 && t_range <= 20)
        gainsel = 0x01;
    else
        gainsel = 0x00;

    if (ioctl(gpiofd, IOCTL_GAIN_SEL, &gainsel) < 0)
    {
        BXlogPutLogMsg("[dseSignal] ERR : set IOCTL_GAIN_SEL");
    }

    if (t_range < 15 && t_range >= 5)
    {
        pwdsel = 0x00;
        freq_correction = 2;
    }
    else
    {
        pwdsel = 0x01;
        freq_correction = 1;
    }

    if (ioctl(gpiofd, IOCTL_PWD_SEL, &pwdsel) < 0)
    {
        BXlogPutLogMsg("[dseSignal] ERR : set IOCTL_GAIN_SEL");
    }

    f_callAvailableSaveTime = 1;
}

/**
@fn      sig_clickGainSW()
@author  Bongsung Kim
@date    2017.04.24
@brief   Gain Switch Push 할 시에 temp Gain를 변경하는 함수
@param   none
@return  none
@warning none
@bug     none
@todo    none
*/
static void sig_clickGainSW()
{
    switch (gainMode)
    {
    case GAIN_LOW    :
        gainMode = GAIN_LM;
        break;
    case GAIN_LM     :
        gainMode = GAIN_MIDDLE;
        break;
    case GAIN_MIDDLE :
        gainMode = GAIN_HM;
        break;
    case GAIN_HM    :
        gainMode = GAIN_HIGH;
        break;
    case GAIN_HIGH   :
        gainMode = GAIN_LOW;
        break;
    }
}

/**
@fn      sig_clickSaveSW()
@author  Bongsung Kim
@date    2017.04.24
@brief   Save Switch Push 할 시에 Save 함수
@param   none
@return  성공 시, 0. 실패 시, -1.
@warning none
@bug     none
@todo    none
*/
static int32_t sig_clickSaveSW()
{
    if (f_save == SAVE_WAIT)
    {
        f_save = save_saveFileInit();
        if ( ERR_SAVEERR == f_save )
            return -1;
        else if ( ERR_DISKSIZE_FULL == f_save )
            return 0;

        if ( BXledTriggerSelect(LED2_DIR, "timer")<0 )
            BXlogPutLogMsg("[dseSignal] ERR: BXledTriggerSelect LED is missing!");

        //save Thread Start!!
        if ( pthread_create(&thr_save, NULL, save_saveThread, NULL) < 0 )
            BXlogErrHandling("[dseSignal] ERR : save_saveThread Create Failed!!");

        pthread_detach(thr_save);

        f_drawSaveFileName = 1;
    }
    else if (f_save == SAVE_START)
    {

        f_save = SAVE_STOP;

        if ( BXledBrightness_0(LED2_DIR)<0 )
            BXlogPutLogMsg("[dseSignal] ERR: BXledTriggerSelect LED is missing!");

        BXlogPutLogMsg("[dseSignal] Save End!");

        f_drawSaveFileName = 0;
    }

    return 0;
}

static void sig_clickChangeSW()
{
    if(f_change == 0)
    {
        f_change = 1;
    }
    else
        f_change = 0;
}