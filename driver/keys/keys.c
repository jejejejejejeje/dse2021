/**
    @file       keys.c
    @date       2017.04.25
    @author     Bongsung Kim
    @brief      GPIO Driver
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역    
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/ioport.h>
//#include <unistd.h>
#include <linux/slab.h>
#include <linux/mm.h>
//#include <asm/hardware.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <linux/gpio.h>

#include <scsi/scsi.h> // public constants and structures
#include <linux/ioctl.h> //ioctl

#include <linux/interrupt.h>
#include <linux/irq.h>
#include <asm/irq.h>

#include <linux/timer.h>
#include <linux/delay.h>

#include <linux/sched.h>
#include <asm/siginfo.h>
#include <linux/pid_namespace.h>
#include <linux/pid.h>

#include <linux/workqueue.h>  //work quese

#define KEYS_MAJOR    240
#define KEYS_NAME    "keys"

#define IMX_GPIO_NR(bank, nr)   (((bank)-1)*32 + (nr))
#define SW_SAVE_RANGE     IMX_GPIO_NR(4,20)  ///< Range SW       USER_BUTTON(110)
#define SW_POWER_GAIN     IMX_GPIO_NR(4,8)   ///< Power & Gain SW UART5_TXD(162)
#define SW_ETC            IMX_GPIO_NR(1,0)   ///< AUD4_CLK(114)


//#define SW_SAVE_RANGE     IMX_GPIO_NR(4,8)  ///< Range SW       USER_BUTTON(110)
//#define SW_POWER_GAIN     IMX_GPIO_NR(4,20)   ///< Power & Gain SW UART5_TXD(162)

#define SW_CMD_RANGE    1
#define SW_CMD_GAIN     2
#define SW_CMD_SAVE     3
#define SW_CMD_FIRST    4
#define SW_CMD_CHANGE   5

#define IOCTL_MAGIC			'h'
#define IOCTL_SW_CMD_GET	_IOR(IOCTL_MAGIC, 0, int)
#define IOCTL_WAIT_FIRST    _IO(IOCTL_MAGIC, 1)

#define SCREEN_SHOT		0

unsigned int irq_swSaveRange;
unsigned int irq_swPowerGain;
unsigned int irq_swEtc;

struct task_struct *task;
struct siginfo info;

int cnt_swSaveRange = 0;
int tick_swSaveRange = 0;
int cnt_swSR_afterFirstClick = 0;
int cnt_swPowerGain = 0;
int sw_cmd          = 0;
int f_firstInput    = 1;
int cnt_swEtc = 0;

///////////////////////////////////////////////////////////
//               Function Definition                     //
///////////////////////////////////////////////////////////
int            keys_open(struct inode *inode, struct file *filp);
static ssize_t keys_write(struct file *file, const char *buffer, size_t length, loff_t *offset);
static ssize_t keys_read(struct file *file, char *buffer, size_t length, loff_t *offset);
long           keys_ioctl(struct file* filp, unsigned int cmd, unsigned long arg);
int            keys_release(struct inode *inode, struct file *filp);

static void workQ_funcSaveRange(struct work_struct *unused);
static void workQ_funcPowerGain(struct work_struct *unused);
static void workQ_funcEtc(struct work_struct *unused);
irq_handler_t handler_saveRange(int irq, void *dev_id, struct pt_regs *regs);
irq_handler_t handler_powerGain(int irq, void *dev_id, struct pt_regs *regs);
irq_handler_t handler_Etc(int irq, void *dev_id, struct pt_regs *regs);
///////////////////////////////////////////////////////////
//               Function Definition                     //
///////////////////////////////////////////////////////////
//declare work delay queue
static DECLARE_DELAYED_WORK(workQ_saveRange, workQ_funcSaveRange);
static DECLARE_DELAYED_WORK(workQ_powerGain, workQ_funcPowerGain);
static DECLARE_DELAYED_WORK(workQ_Etc, workQ_funcEtc);
DECLARE_WAIT_QUEUE_HEAD( waitQ_firstStart );

//workqueue Func.
static void workQ_funcSaveRange(struct work_struct *unused)
{
	int value = gpio_get_value(SW_SAVE_RANGE);

	if (!value) //Key Input
	{
		/* Save Operation */
		if (cnt_swSaveRange>70) //over 700ms
		{
			if (f_firstInput)
			{
				cnt_swSaveRange = 0;
				return;
			}
			else
			{
				sw_cmd = SW_CMD_SAVE;
				send_sig_info(SIGUSR1, &info, task);
				cnt_swSaveRange = 0;
			}
		}
		/* Switch On Continue */
		else					// + wait 10ms
		{
			cnt_swSaveRange++;
			schedule_delayed_work(&workQ_saveRange, 10);
		}
	}
	else
	{
		if (cnt_swSaveRange >= 4) // if Key input is over 50ms
        {
            if (f_firstInput)
            {
                sw_cmd = SW_CMD_FIRST;
                f_firstInput = 0;
                wake_up_interruptible(&waitQ_firstStart);
            }
            else
                sw_cmd = SW_CMD_RANGE;

            send_sig_info(SIGUSR1, &info, task);
            cnt_swSaveRange = 0;
        }
        else
        {
            cnt_swSaveRange = 0;
        }
		/*--------------------------------*/
		/* Range And ScreenShot Operation */
		/*--------------------------------*/
	}
}

static void workQ_funcPowerGain(struct work_struct *unused)
{
	int value;

	value = gpio_get_value(SW_POWER_GAIN);

	if (!value) //Key Input
	{
		cnt_swPowerGain++;
		schedule_delayed_work(&workQ_powerGain, 10);
	}
	else
	{
		if (cnt_swPowerGain >= 4) // if Key input is over 50ms
		{
			if (f_firstInput)
			{
				sw_cmd = SW_CMD_FIRST;
				f_firstInput = 0;
				wake_up_interruptible(&waitQ_firstStart);
			}
			else
				sw_cmd = SW_CMD_GAIN;

			send_sig_info(SIGUSR1, &info, task);
			cnt_swPowerGain = 0;
		}
		else
		{
			cnt_swPowerGain = 0;
		}
	}
}

static void workQ_funcEtc(struct work_struct *unused)
{
	int value;

	value = gpio_get_value(SW_ETC);

	if (!value) //Key Input
	{
		cnt_swEtc++;
		schedule_delayed_work(&workQ_Etc, 10);
	}
	else
	{
		if (cnt_swEtc >= 4)
		{
			if (f_firstInput)
			{
				sw_cmd = SW_CMD_FIRST;
				f_firstInput = 0;
				wake_up_interruptible(&waitQ_firstStart);
				//send_sig_info(SIGUSR1, &info, task);
			}
			else
				sw_cmd = SW_CMD_CHANGE;

			send_sig_info(SIGUSR1, &info, task);
			cnt_swEtc = 0;
		}
		else
		{
			cnt_swEtc = 0;
		}
	}
}

//Key Interrupt
irq_handler_t handler_saveRange(int irq, void *dev_id, struct pt_regs *regs)
{
	//printk("key1 Push!!\n");
	schedule_delayed_work(&workQ_saveRange, 10);
	return (irq_handler_t)IRQ_HANDLED;
}
//Key Interrupt
irq_handler_t handler_powerGain(int irq, void *dev_id, struct pt_regs *regs)
{
	//printk("key1 Push!!\n");
	schedule_delayed_work(&workQ_powerGain, 10);
	return (irq_handler_t)IRQ_HANDLED;
}
//Key Interrupt
irq_handler_t handler_Etc(int irq, void *dev_id, struct pt_regs *regs)
{
	//printk("key3 Push!!\n");
	schedule_delayed_work(&workQ_Etc, 10);
	return (irq_handler_t)IRQ_HANDLED;
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

struct file_operations keys_fops = {
open            : keys_open,
write           : keys_write,
unlocked_ioctl	: keys_ioctl,
read            : keys_read,
release         : keys_release,
};

int keys_open(struct inode *inode, struct file *filp)
{
	//MOD_INC_USE_COUNT;
	int result;

	irq_swSaveRange = gpio_to_irq(SW_SAVE_RANGE);
	irq_swPowerGain = gpio_to_irq(SW_POWER_GAIN);
	irq_swEtc = gpio_to_irq(SW_ETC);

	//Service Routine Register  IRQF_DISABLED
	result = request_irq(irq_swSaveRange,
		                (irq_handler_t)handler_saveRange,
		                IRQ_TYPE_EDGE_FALLING | IRQF_DISABLED,
		                "SW_SAVE_RANGE",
		                NULL);
	if (result < 0)
	{
		printk(KERN_ERR "[keys] ERR: SW_SAVE_RANGE Interrupt Error\n");
		return -1;
	}
	result = request_irq(irq_swPowerGain,
		                (irq_handler_t)handler_powerGain,
		                IRQ_TYPE_EDGE_FALLING | IRQF_DISABLED,
		                "SW_POWER_GAIN",
		                NULL);
	if (result < 0)
	{
		printk(KERN_ERR "[keys] ERR: SW_POWER_GAIN Interrupt Error\n");
		return -1;
	}
	result = request_irq(irq_swEtc,
		                (irq_handler_t)handler_Etc,
		                IRQ_TYPE_EDGE_FALLING | IRQF_DISABLED,
		                "SW_Etc",
		                NULL);
	if (result < 0)
	{
		printk(KERN_ERR "[keys] ERR: SW_Etc Interrupt Error\n");
		return -1;
	}

	//init. siginfo For Sigaction
	memset(&info, 0, sizeof(struct siginfo));
	info.si_signo = SIGUSR1;
	info.si_code = 0;
	info.si_int = 1234;

	f_firstInput = 1;

	return 0;
}

//PID Get
static ssize_t keys_write(struct file *file, const char *buffer, size_t length, loff_t *offset) {

	int ret;
	pid_t pid;

	//printk("PID GET\n");

	ret = copy_from_user(&pid, (void*)buffer, sizeof(pid_t));
	if (ret < 0)
		printk("[Driver]Error: copy_from_user - IOCTL_SEND_PID\n");

	task = pid_task(find_vpid(pid), PIDTYPE_PID);
	if (task==NULL)
	{
		printk("[Driver]Error: No Such PID - IOCTL_SEND_PID\n");
	}

	return 1;
}

static ssize_t keys_read(struct file *file, char *buffer, size_t length, loff_t *offset)
{

	return 1;
}

long keys_ioctl(struct file* file, unsigned int cmd, unsigned long arg)
{
	int ret;

	switch(cmd)
	{
	case IOCTL_SW_CMD_GET :
		ret = copy_to_user((void*)arg, &sw_cmd, 4);
		if(ret<0)
			printk("[keys Driver]Error: copy_to_user - IOCTL_SW_CMD_GET\n");
		break;

    case IOCTL_WAIT_FIRST :
    	printk("%d", file->f_flags);
    	if ( !(file->f_flags & O_NONBLOCK) )
    	{
    		//printk("abc\n");
    	    interruptible_sleep_on(&waitQ_firstStart);
    	}
    	break;

	default               :
		break;
	}
	return SUCCESS;
}

int keys_release(struct inode *inode, struct file *filp)
{
	//MOD_DEC_USE_COUNT;
	free_irq(irq_swSaveRange, NULL);
	free_irq(irq_swPowerGain, NULL);
	free_irq(irq_swEtc, NULL);
	return 0;
}

static int __init init_keys(void)
{
	int result;

	result = gpio_request(SW_SAVE_RANGE,"SW_SAVE_RANGE");
	result = gpio_request(SW_POWER_GAIN,"SW_POWER_GAIN");
	result = gpio_request(SW_ETC,"SW_ETC");

	//SW init - input
	gpio_direction_input(SW_SAVE_RANGE);
	gpio_direction_input(SW_POWER_GAIN);
	gpio_direction_input(SW_ETC);

	//register Driver
	result = register_chrdev(KEYS_MAJOR, KEYS_NAME, &keys_fops);
	if (result < 0)
	{
		printk(KERN_WARNING "register_chrdev() FAIL!\n");
		return result;
	}

	printk("[keys] KEYS initialized!!");

	return result;
}

static void __exit exit_keys(void)
{
	gpio_free(SW_SAVE_RANGE);
	gpio_free(SW_POWER_GAIN);
	gpio_free(SW_ETC);
	printk("KEYS Exit!!");
	unregister_chrdev(KEYS_MAJOR, KEYS_NAME);
}

module_init(init_keys);
module_exit(exit_keys);

MODULE_AUTHOR("kbs1439@gmail.com");
MODULE_LICENSE("Dual BSD/GPL");
