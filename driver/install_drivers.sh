#!/bin/bash

echo "------------------------------------------------"
echo "-> poky Toolchain Init."
echo "-> source /opt/poky/1.7/environment-setup-cortexa9hf-vfp-neon-poky-linux-gnueabi"
source /opt/poky/1.7/environment-setup-cortexa9hf-vfp-neon-poky-linux-gnueabi

echo "-------------------------------------------------"
echo "-> keys driver make.............................."
make clean -C "${0%/*}"/keys/
if [ $? -ne 0 ]; then
    exit 1
fi

make -C "${0%/*}"/keys/
if [ $? -ne 0 ]; then
    exit 1
fi

echo "-------------------------------------------------"
echo "-> GPIO_to_FPGA driver make......................"
make clean -C "${0%/*}"/toFPGA_GPIO/
if [ $? -ne 0 ]; then
    exit 1
fi

make -C "${0%/*}"/toFPGA_GPIO/
if [ $? -ne 0 ]; then
    exit 1
fi

echo "--------------------------------------------"
echo "-> copy drivers............................."
echo "-> to ../../0_BSP/1_image/rootfs/lib/modules"

cp "${0%/*}"/keys/keys.ko ../../0_BSP/1_image/rootfs/lib/modules/
if [ $? -ne 0 ]; then
    exit 1
fi

cp "${0%/*}"/toFPGA_GPIO/GPIO_to_FPGA.ko ../../0_BSP/1_image/rootfs/lib/modules/
if [ $? -ne 0 ]; then
    exit 1
fi

echo "copy End!!"
