/**
    @file       GPIO_to_FPGA.c
    @date       2017.04.25
    @author     Bongsung Kim
    @brief      GPIO Driver
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

/**
    @mainpage   Diver Sonar Eye Drivier 메인 페이지
    @section intro 소개
    - 소개    : Diver Sonar Eye 리눅스 드라이버
    @section Program 프로그램 명
    - 프로그램명 : dse 드라이버
    - 프로그램 내용 : GPIO Control & Keys control
    @section CREATEINFO 작성정보
    - 작성자 : Bongsung Kim
    - 작성일 : 2017.04.25
    @section MODIFYINFO 수정정보
    - 수정자/수정일 : 수정내역
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/ioport.h>

#include <linux/slab.h>
#include <linux/mm.h>

#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <linux/gpio.h>

#include <scsi/scsi.h> // public constants and structures
#include <linux/ioctl.h> //ioctl

#include <linux/timer.h>
#include <linux/delay.h>

#include <linux/sched.h>

#include <linux/interrupt.h>
#include <linux/irq.h>
#include <asm/irq.h>

#include <linux/workqueue.h>

#define DRV_MAJOR    241				///< Drvier Major Number
#define DRV_NAME    "GPIO_to_FPGA"      ///< Drvier NAME

#define IMX_GPIO_NR(bank, nr)   (((bank)-1)*32 + (nr)) ///< GPIO Number Calc.
#define FPGA_TRIG               IMX_GPIO_NR(4,29)   ///< DISP0_DAT8  GPIO_OR_PWM_1(117)
#define FPGA_DONE               IMX_GPIO_NR(4,30)   ///< DISP0_DAT9  GPIO_OR_PWM_2(119)
#define KILL                    IMX_GPIO_NR(2,31)   ///< EIM_EB3     UART3_RTS(275)
#define ETC_ONOFF               IMX_GPIO_NR(3,25)   ///< EIM_D25     UART3_RXD(273)
#define TXPWR_ONOFF             IMX_GPIO_NR(3,24)   ///< EIM_D24     UART3_TXD(271)
#define RTC_WKUP_N              IMX_GPIO_NR(3,23)   ///< EIM_D23     UART3_CTS(277)
#define PROCESS_MODE1           IMX_GPIO_NR(4,06)   ///< KEY_COL0    UART4_TXD(158)
#define PROCESS_MODE0           IMX_GPIO_NR(4,07)   ///< KEY_ROW0    UART4_RXD(160)
#define FIFO_EMPTY_CHECK        IMX_GPIO_NR(6,15)   ///< NANDF_CS2   CPU_GPIO0(113)
#define FPGA_RESET              IMX_GPIO_NR(3,22)   ///< EIM_D22     CPU_GPIO1(115)

#define CHANNEL_SWITCH          IMX_GPIO_NR(5,18)   ///< CSI0_PIXCLK  VID_IN_CSI0_PIXCLK(43)
#define CHANNEL_MODE            IMX_GPIO_NR(5,19)   ///< CSI0_MCLK    VID_IN_CSI0_HS(47)
#define FPGA_RESET2             IMX_GPIO_NR(7,13)   ///< GPIO_18      VID_IN_CSI0_INT(73)

#define GAIN_SEL1               IMX_GPIO_NR(1, 7)   ///< GPIO_7       VID_IN_CSI0_RSTn(71)
#define GAIN_SEL2				IMX_GPIO_NR(2, 15)  ///< FPGA_STATUS3
#define PWD_SEL					IMX_GPIO_NR(2, 14)  ///< SD4_DATA6

#define ONOFF                   IMX_GPIO_NR(1,12)	///< AUD4_TXC(116)

#define IOCTL_MAGIC			'g' ///< IOCTL Magic Number
#define IOCTL_TRIGER_TOGGLE		_IOW(IOCTL_MAGIC, 0, int)			///< IOCTL 데이터 취득 Start Trigger
#define IOCTL_DONE_SINAL_ON		_IO (IOCTL_MAGIC, 1)				///< IOCTL 데이터 취득 end
#define IOCTL_TIMER_INTERRUPT	_IOR(IOCTL_MAGIC, 2, unsigned char) ///< 안씀
#define IOCTL_GET_FIFOCHECK 	_IOR(IOCTL_MAGIC, 3, unsigned int)  ///< Fifo rdy 신호 확인
#define IOCTL_DONE_SINAL_OFF    _IO (IOCTL_MAGIC, 4)				///< 데이터 취득 start
#define IOCTL_MODE_SELECT   	_IOW(IOCTL_MAGIC, 5, unsigned char) ///< 2ch,3ch mode select
#define IOCTL_CHANNELSWITCH     _IOW(IOCTL_MAGIC, 6, unsigned char) ///< 교차빔 채널 스위칭
#define IOCTL_TXPWRON           _IO (IOCTL_MAGIC, 7)				///< 아날로그 전원 on
#define IOCTL_TXPWROFF          _IO (IOCTL_MAGIC, 8)				///< 아날로그 전원 off
#define IOCTL_KILL              _IO (IOCTL_MAGIC, 9)				///< 시스템 강제 종료
#define IOCTL_GAIN_SEL          _IO (IOCTL_MAGIC, 10)				///< HW GAIN SELECT
#define IOCTL_PWD_SEL           _IO (IOCTL_MAGIC, 11)				///< HW PWD SELECT

#define IOCTL_ONOFF_ON          _IO (IOCTL_MAGIC, 12)
#define IOCTL_ONOFF_OFF         _IO (IOCTL_MAGIC, 13)


//#define SIG_SETUP 63
#define T_VER_OLD               0	///< real time 이전 버전.

struct timer_list *gpio_timer;		///< 타이머 인터럽트 timer_list(Not use)

unsigned char flag_timer = 0;		///< 타이머 인터럽트 알림 flag (Not use)

static void __iomem* base_addr;		///< GPIO1 Data Register 포인터 (Not use)

int drvGPIO_open(struct inode *inode, struct file *filp);
static ssize_t drvGPIO_write(struct file *file, const char *buffer, size_t length, loff_t *offset);
static ssize_t drvGPIO_read(struct file *file, char *buffer, size_t length, loff_t *offset);
long drvGPIO_ioctl(struct file* filp, unsigned int cmd, unsigned long arg);
int drvGPIO_release(struct inode *inode, struct file *filp);
void kerneltimer_registertimer(struct timer_list* ptimer, unsigned long timeover, unsigned long what);
void kerneltimer_handler(unsigned long arg );

struct file_operations drvGPIO_fops = {
read    		: drvGPIO_read,
write   		: drvGPIO_write,
unlocked_ioctl	: drvGPIO_ioctl,
open    		: drvGPIO_open,
release 		: drvGPIO_release,
};

//wait Queue for Timer
DECLARE_WAIT_QUEUE_HEAD(waitQtimer);

static void fifoqueue_func(struct work_struct *unused);

static DECLARE_DELAYED_WORK(fifo_queue, fifoqueue_func);

static void fifoqueue_func(struct work_struct *unused)
{
	int value;
	value = gpio_get_value(FIFO_EMPTY_CHECK);

	//printk("WorkQueu123e\n");
	if (value == 0 )
	{
		//printk("WorkQueue\n");
	}
}

void kerneltimer_registertimer(struct timer_list* ptimer, unsigned long timeover, unsigned long what)
{
     init_timer( ptimer );
     ptimer->expires  = get_jiffies_64() + timeover;
     ptimer->data     = what;
     ptimer->function = kerneltimer_handler       ;
     add_timer( ptimer);
}

void kerneltimer_handler(unsigned long arg )
{
		//printk("[Driver]KernelTimerHandler Interrupt   %ld\n", arg);
		//if (arg==1)
		//{
		//	flag_timer = 1;
		//}
	wake_up_interruptible(&waitQtimer);
        //kerneltimer_registertimer( timer, TIME_STEP ); <--timerover 하면 재 등록
}

int drvGPIO_open(struct inode *inode, struct file *filp)
{
	//MOD_INC_USE_COUNT;
	flag_timer = 0;

	gpio_timer = kmalloc(sizeof(struct timer_list), GFP_KERNEL);
	if (gpio_timer == NULL) return -ENOMEM;
	memset (gpio_timer,0,sizeof(struct timer_list));

	//GPIO1 Registers
	base_addr = ioremap(0x0209C000, 4*sizeof(unsigned int));
	//gpio1_reg = (unsigned int *)base_addr;

	//printk("[Driver]drvGPIO device open \n");
	//printk("HZ = %d \n", HZ);

	return 0;
}

static ssize_t drvGPIO_write(struct file *file, const char *buffer, size_t length, loff_t *offset)
{

	return 1;
}

static ssize_t drvGPIO_read(struct file *file, char *buffer, size_t length, loff_t *offset)
{
	return 1;
}


long drvGPIO_ioctl(struct file* filp, unsigned int cmd, unsigned long arg)
{
	int time_step = 0;
	unsigned char uCvalue = 0;
	int ret; int value;

    switch(cmd)
    {
    case IOCTL_TRIGER_TOGGLE:
    	gpio_set_value(FPGA_TRIG, 1);	//TRIGER OUT PIN Toggle
		udelay(35);

		gpio_set_value(FPGA_TRIG, 0);	//TRIGER OUT PIN Enabled

#if T_VER_OLD==1
		ret = copy_from_user(&time_step, (void*)arg, sizeof(time_step));
		if (ret < 0)
			printk("[Driver]Error: copy_from_user - IOCTL_TRIGER_TOGGLE\n");
		//printk("%d\n", time_step);
		kerneltimer_registertimer( gpio_timer,time_step, 1);
		interruptible_sleep_on(&waitQtimer);
#endif

        break;

    case IOCTL_DONE_SINAL_ON:

		flag_timer = 0;
		gpio_set_value(FPGA_DONE, 1);	//DONE IN PIN Enabled

        break;

	case IOCTL_DONE_SINAL_OFF:

		//flag_timer = 0;
		gpio_set_value(FPGA_DONE, 0);	//DONE IN PIN Enabled

        break;

	case IOCTL_TIMER_INTERRUPT:

		ret = copy_to_user((void*)arg,&flag_timer, 1);
		if (ret < 0)
			printk("[Driver]Error: copy_to_user - IOCTL_TIMER_INTERRUPT\n");

		break;

	case IOCTL_GET_FIFOCHECK:

		value = gpio_get_value(FIFO_EMPTY_CHECK);
		ret = copy_to_user((void*)arg, &value, 4);

		break;

	case IOCTL_MODE_SELECT   :

		ret = copy_from_user(&uCvalue, (void*)arg, sizeof(uCvalue));
		if (ret < 0)
			printk("[Driver]Error: copy_from_user-IOCTL_MODE_SELECT\n");
		gpio_set_value(CHANNEL_MODE, uCvalue);

		break;

	case IOCTL_CHANNELSWITCH  :

		ret = copy_from_user(&uCvalue, (void*)arg, sizeof(uCvalue));
		if (ret < 0)
			printk("[Driver]Error: copy_from_user-IOCTL_CHANNELSWITCH\n");
		gpio_set_value(CHANNEL_SWITCH, uCvalue);

		break;

	case IOCTL_TXPWRON :
		gpio_set_value(TXPWR_ONOFF, 0);
		break;

	case IOCTL_TXPWROFF :
		gpio_set_value(TXPWR_ONOFF, 1);
		break;

	case IOCTL_KILL :
		gpio_set_value(KILL, 0);
		break;

	case IOCTL_GAIN_SEL :
		ret = copy_from_user(&uCvalue, (void*)arg, sizeof(uCvalue));
		if (ret < 0)
			printk("[Driver]Error: copy_from_user-IOCTL_GAIN_SEL\n");
		gpio_set_value(GAIN_SEL2, uCvalue & 0x01);
		gpio_set_value(GAIN_SEL1, (uCvalue & 0x02) >> 1);
			printk("[Driver]copy_from_user-IOCTL_GAIN_SEL : %d\n", uCvalue);
		break;

	case IOCTL_PWD_SEL :
		ret = copy_from_user(&uCvalue, (void*)arg, sizeof(uCvalue));
		if (ret < 0)
			printk("[Driver]Error: copy_from_user-IOCTL_GAIN_SEL\n");
		gpio_set_value(PWD_SEL, uCvalue);
			printk("[Driver]copy_from_user-IOCTL_GAIN_SEL : %d\n", uCvalue);
		break;

	case IOCTL_ONOFF_ON :
		gpio_set_value(ONOFF, 1);
		//printk("ONOFF ON\n");
		break;

	case IOCTL_ONOFF_OFF :
		gpio_set_value(ONOFF, 0);
		//printk("ONOFF OFF\n");
		break;


    default:
        break;
    }

    return SUCCESS;
}

int drvGPIO_release(struct inode *inode, struct file *filp)
{
	//MOD_DEC_USE_COUNT;

	  if( gpio_timer!= NULL )
    {
        del_timer( gpio_timer) ;
        kfree( gpio_timer);
    }

	iounmap((void *)base_addr);

	return 0;
}

static int __init init_drvGPIO(void)
{
	int result;

	result = gpio_request(FPGA_TRIG,"TRIG_OUT");
	//if (result<0)
	//	return -EINVAL;
	result = gpio_request(FPGA_DONE,"DONE_OUT");
	//if (result<0)
	//	return -EINVAL;

	result = gpio_request(KILL,"KILL");
	//if (result<0)
	//	return -EINVAL;
	result = gpio_request(ETC_ONOFF,"ETC_ONOFF");
	//if (result<0)
	//	return -EINVAL;
	result = gpio_request(TXPWR_ONOFF,"TXPWR_ONOFF");
	//if (result<0)
	//	return -EINVAL;
	result = gpio_request(RTC_WKUP_N,"T2VDC_OFOFF");
	//if (result<0)
	//	return -EINVAL;
	result = gpio_request(PROCESS_MODE1,"PROCESS_MODE1");
	//if (result<0)
	//	return -EINVAL;
	result = gpio_request(PROCESS_MODE0,"PROCESS_MODE0");
	//if (result<0)
	//	return -EINVAL;
	result = gpio_request(FIFO_EMPTY_CHECK,"FPGA_FIFO_EMPTY");

	result = gpio_request(FPGA_RESET,"FPGA_RESET");

	result = gpio_request(CHANNEL_SWITCH,"CHANNEL_SWITCH");
	result = gpio_request(CHANNEL_MODE,"CHANNEL_MODE");

	result = gpio_request(GAIN_SEL1,"GAIN_SEL1");
	result = gpio_request(GAIN_SEL2,"GAIN_SEL2");

	result = gpio_request(PWD_SEL,"PWD_SEL");

	result = gpio_request(ONOFF, "ONOFF");

	//drvGPIO init - output
	gpio_direction_output(KILL, 1); 		  //??
 	gpio_direction_output(ETC_ONOFF, 1);	  //??
	gpio_direction_output(TXPWR_ONOFF, 1);	  // TX Power Off: 1 On: 0
	//gpio_direction_output(RTC_WKUP_N, 1);
	gpio_direction_output(FPGA_RESET,1);
	gpio_direction_output(PROCESS_MODE1, 0);
	gpio_direction_output(PROCESS_MODE0, 1);

	gpio_direction_output(CHANNEL_SWITCH, 0); // 0 or 1
	gpio_direction_output(CHANNEL_MODE, 0);	  // 0: 2ch 1: 3ch

	gpio_direction_output(FPGA_TRIG, 0);
	gpio_direction_output(FPGA_DONE, 0);

	gpio_direction_output(GAIN_SEL1, 0);
	gpio_direction_output(GAIN_SEL2, 0);

	gpio_direction_output(PWD_SEL, 1);

	gpio_direction_output(ONOFF, 1);
	//led
	//gpio_set_value(ETC_ONOFF, 1);
	//gpio_set_value(TXPWR_ONOFF, 1);
	//gpio_set_value(RTC_WKUP_N, 1);
	//gpio_set_value(FPGA_RESET,1);

	//drvGPIO init - input
	gpio_direction_input(FIFO_EMPTY_CHECK);
	gpio_direction_input(RTC_WKUP_N);             //오류인듯

	//register Driver
	result = register_chrdev(DRV_MAJOR, DRV_NAME, &drvGPIO_fops);
	if (result < 0)
	{
		printk(KERN_WARNING "[Driver]register_chrdev() FAIL!\n");
		return result;
	}

	printk("[Driver]drvGPIO initialized!!\n");


	return result;
}

static void __exit exit_drvGPIO(void)
{
	gpio_free(FPGA_TRIG);
	gpio_free(FPGA_DONE);

	gpio_free(KILL);
	gpio_free(ETC_ONOFF);
	gpio_free(TXPWR_ONOFF);
	gpio_free(RTC_WKUP_N);
	gpio_free(PROCESS_MODE1);
	gpio_free(PROCESS_MODE0);
	gpio_free(FIFO_EMPTY_CHECK);
	gpio_free(FPGA_RESET);

	gpio_free(CHANNEL_SWITCH);
	gpio_free(CHANNEL_MODE);

	gpio_free(GAIN_SEL1);
	gpio_free(GAIN_SEL2);

	gpio_free(PWD_SEL);

	printk("[Driver]drvGPIO Exit!!\n");
	unregister_chrdev(DRV_MAJOR, DRV_NAME);
}

module_init(init_drvGPIO);
module_exit(exit_drvGPIO);

MODULE_AUTHOR("chrismail42@gmail.com");
MODULE_LICENSE("Dual BSD/GPL");
//MODULE_LICENSE("Proprietary");
